var Import =
{
	// Initialized by calling script.
	React: null,
	ReactDOM: null,
	BusyButton: null,
	
	callbacks: {},
	timer: null,
	destination: null,
	confirm: null,
	
	init: function(settings, reactSettings) {
		this.callbacks = settings;
		
		this.React = reactSettings.react;
		this.ReactDOM = reactSettings.reactDOM;
		this.BusyButton = reactSettings.busyButton;

		$("body").on("mousedown", "a#importmodalbtn", this.startImport.bind(this));
		$("body").on("click", "button.btn-importingredients", this.startImport.bind(this));
		$("body").on("click", "button.btn-importdirections", this.startImport.bind(this));
		
		this.confirm = this.ReactDOM.render(
			this.React.createElement(this.BusyButton,
				{
					id: "importRecipeSubmit",
					type: "submit",
					style: "primary",
					label: "Import",
					classes: {
						button: "extra-padding btn-confirm"
					},
					onClick: this.confirmImport.bind(Import)
				}
			),
			document.getElementById("importPlaceholder")
		);

		$("div.bs-import-modal")
			.on("show.bs.modal", function(event)
			{
				this.timer = setInterval(this.timerGrabSelection.bind(this), 150);
			}.bind(this))
			.on("hide.bs.modal", function(event)
			{
				if (this.timer)
				{
					clearInterval(this.timer);
				}
			}.bind(this));
	},
	
	timerGrabSelection: function() {
		var selectedText = this.getIframeSelectionText(document.getElementById("recipe"));
		if ($.trim(selectedText) !== "")
		{
			$('.captured-text').val(selectedText);
		}
	},
	
	showPopover: function(baseInput, componentName, label) {
		$(baseInput).popover({
			trigger: 'manual',
			placement: "bottom",
			html: 'true',
			container: 'body',
			content : "<a href='#importModal' tabindex='-1' role='button' class='btn btn-default' id='importmodalbtn' data-parent='" + componentName + "' " 
				+ "data-label='" + label + "'>Import...</a>"
		});
		
		$(baseInput).blur(function() { $(this).popover('destroy'); });
		$(baseInput).popover('show');
	},
	
	reshowPopover: function(event) {
		$(event.target).popover('show');
	},
	
	addPopover: function(component, placement)
	{
		var $component = $("." + component);
	
		$component.popover({
			trigger: 'manual',
			placement: placement,
			html: 'true',
			container: 'body',
			content : "<a href='#importModal' tabindex='-1' role='button' class='btn btn-default' id='importmodalbtn' data-parent='" + component + "' " 
				+ "data-label='" + $component.data('label') + "'>Import...</a>"
		});
	},

	startImport: function(event)
	{
		//event.preventDefault();
		$(".bs-import-modal span.import-type").text($(event.target).data('label'));
		this.destination = $(event.target).data('parent'); 
		$(".bs-import-modal").modal('show');
	},

	confirmImport: function(event)
	{
		var targetButton = this.confirm;
		//buttonWait($('.bs-import-modal .btn-confirm'), true);
		targetButton.busy();
	
		var text_selection = $.trim($('.captured-text').val());
		var type = this.destination;
		var destComponent = this.destination;
	
		if (type != "ingredients" && type != "directions")
		{
			type = "other";
		}
	
		var params = 
		{
			content: text_selection
		};
	
	    ajaxPost("/recipe/parse/" + type, params)
			.done(function(data) // success callback
			{
				if (data.success == 1 && data.result) {
					/* Disabling jshint error; variable definitions are in separate if blocks. */
					/* jshint shadow: true */
					if (destComponent == "ingredients")
					{
						for (var i = 0; i < data.result.length; i++) 
						{ 
							var ingredient = data.result[i];
							this.callbacks.importIngredient(ingredient);
						}
					}
					else if (destComponent == "directions")
					{
						for (var i = 0; i < data.result.length; i++) 
						{ 
							var direction = data.result[i];
							this.callbacks.importDirection(direction);
						}
					}
					else
					{
						this.callbacks.importMetadata(destComponent, data.result);
					}
				}
			}.bind(this))
			.always(function(data)
			{
				$(".bs-import-modal").modal('hide');
				targetButton.activate();
			});
	},

	getIframeSelectionText: function(iframe) 
	{
		var win = iframe.contentWindow;
		var doc = win.document;
	
		try
		{
			var sel = rangy.getIframeSelection(iframe);
			return sel.toHtml();
		}
		catch (err)
		{}
	}
};