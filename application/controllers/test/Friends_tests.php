<?php
require_once(APPPATH . '/controllers/test/ProjectNom_Toast.php');

class Friends_tests extends ProjectNom_Toast
{
	function __construct()
	{
		parent::__construct(__FILE__); // Remember this
	}

	function test_session_failure()
	{
 		$curlResult = $this->doCurlGet("/api/1/friends");
		$this->message = $curlResult['body'] . "<BR>" . $curlResult['headers'][0]['http_code'];
		$api_result = json_decode($curlResult['body']);

		$this->_assert_equals($api_result->success, 0);
		$this->_assert_true(stripos($curlResult['headers'][0]['http_code'], "401 Unauthorized"));
	}

	function test_friends_get()
	{
		$this->establishSession();

 		$curlResult = $this->doCurlGet("/api/1/friends");
		$this->message = $curlResult['body'];
		$api_result = json_decode($curlResult['body']);
		
		$this->_assert_equals($api_result->success, 1);
		$this->_assert_true(count($api_result->result) > 0);
		
		$match = false;
		foreach($api_result->result as $friend)
		{
			if ($friend->username == "jadieladie")
			{
				$match = true;
			}
		}
		$this->_assert_true($match);
	}

	function test_friends_add_success()
	{
		$this->establishSession();

 		$fields = array (
 			'email' => 'perria@alum.rpi.edu'
 		);

 		$curlResult = $this->doCurlPost("/api/1/friends", json_encode($fields, JSON_FORCE_OBJECT));
		$this->message = $curlResult['body'];
		$api_result = json_decode($curlResult['body']);
		
		$this->_assert_equals($api_result->success, 1);
		$this->_assert_equals($api_result->result->username, "Wolf");
	}

	function test_friends_add_success_exists()
	{
		$this->establishSession();

 		$fields = array (
 			'email' => 'perria@alum.rpi.edu'
 		);

 		$curlResult = $this->doCurlPost("/api/1/friends", json_encode($fields, JSON_FORCE_OBJECT));
		$this->message = $curlResult['body'];
		$api_result = json_decode($curlResult['body']);
		
		$this->_assert_equals($api_result->success, 1);
		$this->_assert_equals($api_result->result, "Friend already added.");
	}
	
	function test_friends_add_failure_nomatch()
	{
		$this->establishSession();

 		$fields = array (
 			'email' => 'dude@nonexistant.com'
 		);

 		$curlResult = $this->doCurlPost("/api/1/friends", json_encode($fields, JSON_FORCE_OBJECT));
		$this->message = $curlResult['body'];
		$api_result = json_decode($curlResult['body']);
		
		$this->_assert_equals($api_result->success, 0);
		$this->_assert_equals($api_result->error, "Email does not exist.");
 	}

	function test_friends_add_failure_bademail()
	{
		$this->establishSession();

 		$fields = array (
 			'email' => "'; SELECT * FROM USERS --"
 		);

 		$curlResult = $this->doCurlPost("/api/1/friends", json_encode($fields, JSON_FORCE_OBJECT));
		$this->message = $curlResult['body'];
		$api_result = json_decode($curlResult['body']);
		
		$this->_assert_equals($api_result->success, 0);
		$this->_assert_equals($api_result->error, "Email does not exist.");
 	}
 	
	function test_friends_add_failure_badrequest()
	{
		$this->establishSession();

 		$fields = array (
 			'something' => 'random'
 		);

 		$curlResult = $this->doCurlPost("/api/1/friends", json_encode($fields, JSON_FORCE_OBJECT));
		$this->message = $curlResult['body'];
		$api_result = json_decode($curlResult['body']);
		
		$this->_assert_equals($api_result->success, 0);
		$this->_assert_equals($api_result->error, "Email does not exist.");
 	}

	function test_friends_remove_success()
	{
		$this->establishSession();

 		$curlResult = $this->doCurlDelete("/api/1/friends/3");
		$this->message = $curlResult['body'];
		$api_result = json_decode($curlResult['body']);
		
		$this->_assert_equals($api_result->success, 1);
	}

	function test_friends_remove_failure_noid()
	{
		$this->establishSession();

 		$curlResult = $this->doCurlDelete("/api/1/friends/");
		$this->message = $curlResult['body'];
		$api_result = json_decode($curlResult['body']);
		
		$this->_assert_equals($api_result->success, 0);
		$this->_assert_equals($api_result->error, "Unable to remove friend");
	}

	function test_friends_remove_failure_invalid()
	{
		$this->establishSession();

 		$curlResult = $this->doCurlDelete("/api/1/friends/2");
		$this->message = $curlResult['body'];
		$api_result = json_decode($curlResult['body']);
		
		$this->_assert_equals($api_result->success, 0);
		$this->_assert_equals($api_result->error, "Unable to remove friend");
	}

	function test_friends_remove_failure_badrequest()
	{
		$this->establishSession();

 		$curlResult = $this->doCurlDelete("/api/1/friends/blah");
		$this->message = $curlResult['headers'][0]['http_code'];
		
		$this->_assert_equals($curlResult['headers'][0]['http_code'], "HTTP/1.1 404 Not Found");
	}
	
	// Messages
	function test_messages_get()
	{
		$this->establishSession();

 		$curlResult = $this->doCurlGet("/api/1/messages");
		$this->message = $curlResult['body'];
		$api_result = json_decode($curlResult['body']);
		
		$this->_assert_equals($api_result->success, 1);
		$this->_assert_equals(count($api_result->result), 10);

		$author_match = $direct_match = $stranger_match = $image_match = $directauthor_match = $directbad_match = false;
		foreach($api_result->result as $message)
		{
			if ($message->author == "mageuzi" && $message->type == "post" && !empty($message->recipe_title) && $message->recipe_title == "Perfect Pie Crust")
			{
				$author_match = true;
			}
			if ($message->author == "Fox" && $message->type == "direct" && !empty($message->message) && $message->message == "Hello!")
			{
				$direct_match = true;
			}
			if ($message->author == "Wolf" && $message->type == "post")
			{
				$stranger_match = true;
			}
			if ($message->author == "jadieladie" && $message->type == "post" && !empty($message->message) && $message->message == "Post from a completely different user lorem ipsum" && $message->recipe_title = "The Right Bank" && !empty($message->image_url))
			{
				$image_match = true;
			}
			if ($message->author == "mageuzi" && $message->type == "direct" && !empty($message->to_user) && $message->to_user == "jadieladie" && !empty($message->message) && $message->message == "New messages should appear at the top of the list\nThis is a newline")
			{
				$directauthor_match = true;
			}
			if ($message->author != "mageuzi" && !empty($message->to_user) && $message->to_user != "mageuzi")
			{
				$directbad_match = true;
			}
		}
		$this->_assert_true($author_match);
		$this->_assert_true($direct_match);
		$this->_assert_false($stranger_match);
		$this->_assert_true($image_match);
		$this->_assert_true($directauthor_match);
		$this->_assert_false($directbad_match);
	}
	
	function test_messages_get_nofriends()
	{
		$this->establishSession(array('username'=>'wolf', 'password'=>'jessiekat'), true);

		// make sure this user doesn't have any friends
 		$curlResult = $this->doCurlGet("/api/1/friends");
		$this->message = $curlResult['body'];
		$api_result = json_decode($curlResult['body']);
		
		$this->_assert_equals($api_result->success, 1);
		$this->_assert_equals(count($api_result->result), 0);
		
		// check own posts & direct posts show up
 		$curlResult = $this->doCurlGet("/api/1/messages");
		$this->message .= "<BR>" . $curlResult['body'];
		$api_result = json_decode($curlResult['body']);
		
		$this->_assert_equals($api_result->success, 1);
		$this->_assert_equals(count($api_result->result), 2);

		$message = $api_result->result[0];
		$this->_assert_equals($message->author, "jadieladie");
		$this->_assert_equals($message->type, "direct");
		$this->_assert_equals($message->message, "Testing directs");
		
		$message = $api_result->result[1];
		$this->_assert_equals($message->author, "Wolf");
		$this->_assert_equals($message->type, "post");
		$this->_assert_equals($message->message, "This message should not be visible to mageuzi");
	}

	function test_messages_addpost_success()
	{
		$this->establishSession(array('username'=>'Mageuzi', 'password'=>'jessiekat'), true);

		$fields = array (
			"type" => "post",
			"message" => "Unit test message"
		);

 		$curlResult = $this->doCurlPost("/api/1/messages", json_encode($fields, JSON_FORCE_OBJECT));
		$this->message = $curlResult['body'];
		$api_result = json_decode($curlResult['body']);
		
		$this->_assert_equals($api_result->success, 1);
		
		$this->_assert_true($api_result->result->id > 0);
		$this->_assert_equals($api_result->result->author, "mageuzi");
		$this->_assert_equals($api_result->result->type, "post");
		$this->_assert_equals($api_result->result->message, "Unit test message");
		$this->_assert_equals($api_result->result->is_current_user, 1);
	}

	function test_messages_addpost_success_badtype()
	{
		$this->establishSession();

		$fields = array (
			"type" => "invalid",
			"message" => "Unit test message"
		);

 		$curlResult = $this->doCurlPost("/api/1/messages", json_encode($fields, JSON_FORCE_OBJECT));
		$this->message = $curlResult['body'];
		$api_result = json_decode($curlResult['body']);
		
		$this->_assert_true($api_result->result->id > 0);
		$this->_assert_equals($api_result->result->author, "mageuzi");
		$this->_assert_equals($api_result->result->type, "post");
		$this->_assert_equals($api_result->result->message, "Unit test message");
		$this->_assert_equals($api_result->result->is_current_user, 1);
	}

	function test_messages_addpost_success_recipe()
	{
		$this->establishSession();

		$fields = array (
			"type" => "invalid",
			"message" => "Unit test message",
			"recipeid" => 1
		);

 		$curlResult = $this->doCurlPost("/api/1/messages", json_encode($fields, JSON_FORCE_OBJECT));
		$this->message = $curlResult['body'];
		$api_result = json_decode($curlResult['body']);
		
		$this->_assert_true($api_result->result->id > 0);
		$this->_assert_equals($api_result->result->author, "mageuzi");
		$this->_assert_equals($api_result->result->type, "post");
		$this->_assert_equals($api_result->result->message, "Unit test message");
		$this->_assert_equals($api_result->result->is_current_user, 1);
		$this->_assert_equals($api_result->result->recipeid, 1);
	}

	function test_messages_addpost_success_direct()
	{
		$this->establishSession();

		$fields = array (
			"type" => "direct",
			"message" => "Unit test message",
			"to_user" => "jadieladie"
		);

 		$curlResult = $this->doCurlPost("/api/1/messages", json_encode($fields, JSON_FORCE_OBJECT));
		$this->message = $curlResult['body'];
		$api_result = json_decode($curlResult['body']);
		
		$this->_assert_true($api_result->result->id > 0);
		$this->_assert_equals($api_result->result->author, "mageuzi");
		$this->_assert_equals($api_result->result->type, "direct");
		$this->_assert_equals($api_result->result->message, "Unit test message");
		$this->_assert_equals($api_result->result->is_current_user, 1);
		$this->_assert_equals($api_result->result->to_user, "jadieladie");
	}
	
	function test_messages_addpost_failure_invalid()
	{
		$this->establishSession();

		$fields = array (
			"invalid" => "junk"
		);

 		$curlResult = $this->doCurlPost("/api/1/messages", json_encode($fields, JSON_FORCE_OBJECT));
		$this->message = $curlResult['body'];
		$api_result = json_decode($curlResult['body']);
		
		$this->_assert_equals($api_result->success, 0);
		$this->_assert_equals($api_result->error, "Required information missing.");
	}

	function test_messages_addpost_success_badrequest()
	{
		$this->establishSession();

		$fields = array (
			"type" => "post",
			"message" => "'; SELECT * FROM USERS --"
		);

 		$curlResult = $this->doCurlPost("/api/1/messages", json_encode($fields, JSON_FORCE_OBJECT));
		$this->message = $curlResult['body'];
		$api_result = json_decode($curlResult['body']);
		
		$this->_assert_equals($api_result->success, 1);
		$this->deleteMessage($api_result->result->id);
	}

	function test_messages_addpost_success_badrecipe()
	{
		$this->establishSession();

		$fields = array (
			"type" => "post",
			"message" => "Unit test message",
			"recipeid" => 204
		);

 		$curlResult = $this->doCurlPost("/api/1/messages", json_encode($fields, JSON_FORCE_OBJECT));
		$this->message = $curlResult['body'];
		$api_result = json_decode($curlResult['body']);
		
		$this->_assert_equals($api_result->success, 1);
		$this->_assert_true($api_result->result->id > 0);
		$this->_assert_equals($api_result->result->author, "mageuzi");
		$this->_assert_equals($api_result->result->type, "post");
		$this->_assert_equals($api_result->result->message, "Unit test message");
		$this->_assert_equals($api_result->result->is_current_user, 1);
		$this->_assert_true(empty($api_result->result->recipeid));
	}

	function test_messages_remove_success()
	{
		$this->establishSession();

		$this->load->database();
		$this->db->select("id");
		$this->db->where("message", "Unit test message");
		$query = $this->db->get("users_messages");
		
		foreach($query->result() as $row)
		{
	 		$curlResult = $this->deleteMessage($row->id);
			$this->message = $curlResult['body'];
			$api_result = json_decode($curlResult['body']);
			
			$this->_assert_equals($api_result->success, 1);
		}

		$this->db->close();
	}

	function test_messages_remove_failure_noid()
	{
		$this->establishSession();
		
 		$curlResult = $this->doCurlDelete("/api/1/messages");
		$this->message = $curlResult['body'];
		$api_result = json_decode($curlResult['body']);
		
		$this->_assert_equals($api_result->success, 0);
		$this->_assert_equals($api_result->error, "Unable to remove message");
	}

	function test_messages_remove_failure_invalid()
	{
		$this->establishSession();
		
 		$curlResult = $this->doCurlDelete("/api/1/messages/12");
		$this->message = $curlResult['body'];
		$api_result = json_decode($curlResult['body']);
		
		$this->_assert_equals($api_result->success, 0);
		$this->_assert_equals($api_result->error, "Unable to remove message");
	}

	function test_messages_remove_failure_badrequest()
	{
		$this->establishSession();
		
 		$curlResult = $this->doCurlDelete("/api/1/messages/blah");
		$this->message = $curlResult['headers'][0]['http_code'];
		
		$this->_assert_equals($curlResult['headers'][0]['http_code'], "HTTP/1.1 404 Not Found");
	}

	function deleteMessage($id)
	{
		return $this->doCurlDelete("/api/1/messages/$id");
	}
}
?>