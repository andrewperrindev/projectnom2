var React = require('react');
var BusyButton = require('./BusyButton.js');

module.exports = React.createClass({
	getInitialState: function() {
		return {
			screenName: this.props.screenName
		};
	},

	busy: function() {
		if (this.refs.redirect) {
			this.refs.redirect.busy();
		}
	},
	
	activate: function() {
		if (this.refs.redirect) {
			this.refs.redirect.activate();
		}
	},
	
	disable: function() {
		if (this.refs.redirect) {
			this.refs.redirect.disable();
		}
	},
	
	enable: function() {
		if (this.refs.redirect) {
			this.refs.redirect.enable();
		}
	},
	
	setScreenName: function(newScreenName) {
		this.setState({screenName: newScreenName});
	},
	
	render: function() {		
		if (this.state.screenName)
		{
			return React.DOM.div({},
				React.DOM.button({className: "btn btn-default", type: "button", id: "twitterAccount", disabled: "disabled"},
					React.DOM.i({className: "fa fa-twitter", style: {paddingRight: 5}}),
					this.state.screenName),
				React.DOM.div({className: "checkbox"},
					React.DOM.label({},
						React.DOM.input({type: "checkbox", id: "removeTwitter"}),
						" Remove Account"))
			);
		}
		else
		{
			var isDisabled = this.props.disabled ? true : false;

			return React.createElement(BusyButton, {type: "button", label: "Connect Your Twitter Account", icon: "twitter",
				style: "default", initialDisabled: this.props.initialDisabled, id: "addTwitter", onClick: this.props.onConnect, ref: "redirect",
				initialBusy: this.props.initialBusy, busyLabel: "Redirecting to Twitter..."}
			);
		}
	}
});
