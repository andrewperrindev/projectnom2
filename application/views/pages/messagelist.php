<!-- Intro Header -->
<header class="intro">
    <div class="intro-body">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2 greeting">
                    <h1 class='brand-heading'>Messages</h1>
                </div>
            </div>
        </div>
    </div>
</header>

<!-- New Message Section -->
<section id="message" class="container content-section text-dark">
    <div class="container wait-spinner">
        <div class="row">
            <div class="col-sm-12 text-dark text-center wait">
                <i class="fa fa-circle-o-notch fa-spin"></i>
            </div>
        </div>
    </div>
    <div class="container error message-error hidden">
        <div class="row">
            <div class="col-sm-10 col-sm-offset-1 text-dark">
                <div class="alert alert-danger" role="alert"></div>
            </div>
        </div>
	</div>
    <div class="container add-message hidden">
        <form id="add-message">
			<div class="row">
				<div class="col-sm-10 col-sm-offset-1">
					<div class="file-upload dropzone" id="fileUpload">
						<div class='dz-message'>
							<p class="custom-label text-center">
								<i class='fa fa-upload'></i>&nbsp;&nbsp;Attach Image <small>(drag file here or click to choose)</small>
							</p>
						</div>
					</div>
					<div class="fallback hidden" id="fallback"></div>
				</div>
			</div>
			<div class="upload-preview dropzone-previews">
			</div>
			<div class="row">
				<div class="col-sm-8 col-sm-offset-2">
						<div id="uploadResult"></div>
				</div>
			</div>
	       <div class="row">
		        <div class="col-sm-10 col-sm-offset-1">
		            <div class='form-group message-container'>
		            	<label class='sr-only' for='new-message'>New Message</label>
						<textarea class="form-control new-message" id='new-message' placeholder="Message" rows='3' aria-describedby="helpBlock" data-type="post"></textarea>
						<span id="helpBlock" class="help-block">
							Enter the message you'd like to post. Anyone who has added you as a friend will see it. &nbsp;<br>
							<span class="label label-default char-count">255</span> characters left.
						</span>
		            </div>
		            <?php
						if (!empty($twitter))
						{
							?>
							<div class="row tweet-option">
								<div class="col-sm-12">
									<div class="checkbox">
										<label>
											<input type="checkbox" id="twitter" name="twitter" checked="checked">&nbsp;<i class="fa fa-twitter"></i> Tweet to @<?php print $twitter; ?>
										</label>
									</div>
								</div>
							</div>
							<?php
						}
					?>
		        </div>
            </div>
            <div class="row">
		        <div class="col-sm-10 col-sm-offset-1 text-right">
		            <div class='add-container'>
						<input type="submit" class="btn btn-lg btn-default add-message" value="Post Message">
		            </div>
		        </div>
	       </div>
        </form>
    </div>
</section>
<!-- Message List Section -->
<section id="messageList" class="container content-section text-dark">
    <div class="container messages hidden">
	    <div class="row">
	        <div class="col-sm-10 col-sm-offset-1">
	            <div class='message-list'>
	            </div>
	        </div>
	    </div>
	    <div class="row show-more hidden">
		    <div class="col-sm-10 col-sm-offset-1">
			    <div class="thumbnail">
				    <div class='text-center'>
					    <input type="hidden" id="current-page" value="0">
					    <p>Show More</p>
				    </div>
			    </div>
		    </div>
	    </div>
    </div>
</section>

<div class="row message-list-item hidden">
	<div class="col-xs-12">
		<div class="thumbnail">
			<div class="row header">
				<div class="col-xs-12 message-type">
					<p class="comment-icon"><i class='fa fa-comment'></i></p>
					<p class="direct-icon hidden"><i class='fa fa-envelope'></i></p>
					<p class="username"></p>
					<p class="to_username text-muted"></p>
	    		</div>
			</div>
			<div class="row body">
				<div class="col-xs-12 message-section">
					<p class="message"></p>
				</div>
			</div>
			<div class="row attachment hidden">
				<div class="col-xs-10 col-xs-offset-1 attachment-section">
					<div class="alert alert-info">
						<div class="row">
							<div class="col-xs-8 attachment-content-section">
								<div class="col-xs-3 col-sm-2">
									<i class="fa fa-cutlery"></i>
								</div>
								<div class="col-xs-9 col-sm-10">
									<span class="attachment-content"></span>
								</div>
							</div>
							<div class="col-xs-4">
								<div class="col-xs-12 col-sm-5 col-sm-offset-1 col-md-4 col-md-offset-2 col-lg-3 col-lg-offset-3 text-right">
									<button class="btn btn-default preview" data-toggle="modal" data-target="div.bs-recipe-preview-modal" title="Preview"><i class="fa fa-search-plus"></i></button>
								</div>
								<div class="col-xs-12 col-sm-6 text-right">
									<button class="btn btn-default import" data-toggle="modal" data-target="div.bs-recipeimport-modal" title="Import"><i class="fa fa-plus"></i></button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row attachment-img hidden">
				<div class="col-xs-10 col-xs-offset-1 attachment-section">
					<div class="alert alert-info">
						<div class="row">
							<div class="col-xs-1">
								<i class="fa fa-paperclip"></i>
							</div>
							<div class="col-xs-10 attachment-content-section">
								<span class="attachment-content">
									<img src="" class="img-thumbnail">
								</span>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row footer">
				<div class="col-xs-6 message-timestamp">
					<p class="timestamp text-muted"></p>
				</div>
	    		<div class="col-xs-6 text-right message-options">
		    		<button class="btn btn-sm btn-default view-tweet pull-right hidden" data-tweetid=""><i class="fa fa-twitter"></i></button>
		    		<button class="btn btn-sm btn-danger remove-message pull-right hidden" data-messageid=""><i class="fa fa-trash-o"></i></button>
	    		</div>
			</div>
		</div>
    </div>
</div>

<div class='upload-preview-template hidden'>
	<div id="preview-template" class="file-row">
		<div class="row">
			<div class='col-sm-10 col-sm-offset-1'>
				<div class="alert alert-info">
					<div class="row">
						<div class="col-xs-1">
							<i class="fa fa-paperclip"></i>&nbsp;
						</div>
						<div class="col-xs-8 col-sm-3">
							<img data-dz-thumbnail />
						</div>
						<div class="hidden-xs col-sm-6">
							<h4 class="text-overflow" style="text-overflow: ellipsis"><span class="name" data-dz-name></span> <p class="size small" data-dz-size></p></h4>
						</div>
						<div class="col-xs-2 col-sm-1">
							<button data-dz-remove class="btn btn-danger delete">
								<i class="fa fa-trash-o"></i>
							</button>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class='col-sm-10 col-sm-offset-1'>
				<div id="total-progress" class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
					<div class="progress-bar progress-bar-success" style="width:0%;" data-dz-uploadprogress>
						<i class="fa fa-check hidden"></i>
						<i class="fa fa-times hidden"></i>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>