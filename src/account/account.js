var pnAccount = (function($) {
	var React = require('react');
	var ReactDOM = require('react-dom');
	
	// Custom React Component
	var TwitterConnect = require("../components/TwitterConnect.js");
	var BusyButton = require("../components/BusyButton.js");
	
	var connectButton = null, saveButton = null;

	var publicApi = {
		init: initPage
	};

	return publicApi;

	// ---------------------
	
	function updateProfile(event)
	{
		event.preventDefault();
		saveButton.busy();
	
		var submitUrl = $(this).attr("action");
		
		var removeTwitter = $("input#removeTwitter").length
			? $("input#removeTwitter").prop("checked")
			: false;
	
		var parsedResult = {
			username: $("input#inputUsername", this).val(),
			email: $("input#inputEmail", this).val(),
			password: $("input#newPassword", this).val(),
			password_confirm: $("input#confirmPassword", this).val(),
			remove_twitter: removeTwitter
		};
	
		var parsedResultJson = JSON.stringify(parsedResult);
	
	    ajaxPostJson(submitUrl, parsedResultJson)
			.done(function(data) {
				if (data.success == 1)
				{
					$("section#profile > div.success div.alert")
						.text("Your account has been updated successfully.");
					$("section#profile > div.success")
						.removeClass("hidden");
						
					if (removeTwitter)
					{
						connectButton.setScreenName("");
					}
				}
				else
				{
					if (data.error == "You must login before you can edit your account.")
					{
						$(".bs-login-modal").modal('show');
					}
					else
					{
						$("section#profile > div.error div.alert")
							.html(data.error);
						$("section#profile > div.error")
							.removeClass("hidden");
					}				
				}
			})
			.always(function(data)
			{
				saveButton.activate();
			});
	}
	
	function accountLoginHandler(event)
	{
		modalLoginHandler(event)
			.done(function(data) { // success callback
				if (data.success == 1)
				{
					$(".bs-login-modal").modal('hide');
					connectButton.enable();
				}
			});
	}

	function twitterConnect(event)
	{
		bootbox.confirm({
			message: "If you continue, you will be forwarded to Twitter's website so that you can confirm the account you'd like to connect to ProjectNom. <strong>Please save any other changes or they will be lost.</strong>",
			title: "Connect Twitter Account",
			className: "text-dark",
			buttons: {
				cancel: {
				  label: "Later",
				  className: "btn-default"
				},
				confirm: {
				  label: "Continue",
				  className: "btn-primary"
				}
			},
			callback: function(result) { 
				if (result)
				{
					connectButton.busy();
					window.location.href = "/twitter";
				}
			}
		});
	}

	function initPage()
	{
		$("form#formUpdateProfile").on("submit", updateProfile);
	
		$(".btn-login").on("click", accountLoginHandler);
		$(".bs-login-modal").modal('show');

		saveButton = ReactDOM.render(
			React.createElement(BusyButton,
				{type: "submit", style: "primary", id: "editProfileSubmit", label: "Save"}
			),
			document.getElementById('saveButton')
		);

		connectButton = ReactDOM.render(
			React.createElement(TwitterConnect, 
				{screenName: $("div.twitter").text(), onConnect: twitterConnect, initialDisabled: $(".bs-login-modal").length === 1}
			),
			document.getElementById('twitterConnect')
		);		

	}
})(jQuery);

$(function() {
	initPage_base();
	pnAccount.init();
});