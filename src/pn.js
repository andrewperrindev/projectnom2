/*
 * Portions of this code --
 * Start Bootstrap - Grayscale Bootstrap Theme (http://startbootstrap.com)
 * Code licensed under the Apache License v2.0.
 * For details, see http://www.apache.org/licenses/LICENSE-2.0.
 */

module.exports = (function($) {
	var dayParts = [
		{
			id: "morning",
			name: "Morning",
			hourStart: 4,
			hourEnd: 12,
			isCurrent: isCurrentDayPart
		},
		{
			id: "afternoon",
			name: "Afternoon",
			hourStart: 12,
			hourEnd: 16,
			isCurrent: isCurrentDayPart
		},
		{
			id: "evening",
			name: "Evening",
			hourStart: 16,
			hourEnd: 21,
			isCurrent: isCurrentDayPart
		},
		{
			id: "latenight",
			name: "Late Night",
			hourStart: 21,
			hourEnd: 4,
			isCurrent: isCurrentDayPart
		}
	];
	
	var publicApi = {
		// ref data
		dayParts: dayParts,
		
		// functions
		init: init,
		getDayPart: getDayPart,
		
		restGet: ajaxGet,
		restPost: ajaxPost,
		restDelete: ajaxDelete,
		restPostJson: ajaxPostJson,
		
		logout: logout
	};

	return publicApi;

	// ---------------------

	function init() {
		// jQuery to collapse the navbar on scroll
		$(window).scroll(function() {
		    if ($(".navbar").offset().top > 50) {
		        $(".navbar-fixed-top").addClass("top-nav-collapse");
		    } else {
		        $(".navbar-fixed-top").removeClass("top-nav-collapse");
		    }
		});

		// Closes the Responsive Menu on Menu Item Click
		$('.navbar-collapse ul li a').click(function() 
		{
		    if (!$(this).hasClass("dropdown-toggle"))
		    {
		        $('.navbar-toggle:visible').click();
		    }
		});
		
		// jQuery for page scrolling feature - requires jQuery Easing plugin
	    $('a.page-scroll').bind('click', function(event) {
	        var $anchor = $(this);
	        $('html, body').stop().animate({
	            scrollTop: $($anchor.attr('href')).offset().top
	        }, 1500, 'easeInOutExpo');
	        event.preventDefault();
	    });
	
	    if ($('.nav-search').length)
	    {
	        $('.nav-search').on('shown.bs.dropdown', searchInputFocus);
	        $('div.bs-recipeimport-modal .btn-confirm').on('click', doRecipeImport);
	        $('div.bs-recipeimport-modal form').on('submit', startRecipeImport);
	        $('a.logout').on('click', logout);
	    }
	    
	    $("div.modal").on('shown.bs.modal', function (event) 
	    {
		    $("input[data-default='focus']", this).focus();
		});
	}
	
	function searchInputFocus()
	{
	    $(".nav-search #search-query").focus();
	}

	function getDayPart()
	{
	    var current = {};
	    
	    $.each(dayParts, function compare(index, dayPart) {
		    if (dayPart.isCurrent()) {
			    current = dayPart;
			    return false;
		    }
	    });
	    
	    return current;
	}
	
	function isCurrentDayPart() {
		var d = new Date();
		var hour = d.getHours();
		
		if (hour >= this.hourStart && hour < this.hourEnd) {
			return true;
		}
		// some day parts span am/pm
		else if (this.hourStart > this.hourEnd && (hour >= this.hourStart || hour < this.hourEnd)) {
			return true;
		}
		
		return false;
	}

	function ajaxGet(url, data)
	{
	    return $.get(url, data, null, 'json')
			.fail(function()
			{
				$("div.error .alert")
					.html("<strong>Something went wrong.</strong> Please try again. If you keep seeing this, let us know.");
				$("div.error")
					.removeClass("hidden");
				
			});
	}
	
	function ajaxPost(url, data)
	{
		return $.post(url, data, null, 'json')
			.fail(function()
			{
				$("div.error .alert")
					.html("<strong>Something went wrong.</strong> Please try again. If you keep seeing this, let us know.");
				$("div.error")
					.removeClass("hidden");
				
			});
	}
	
	function ajaxPostJson(url, json)
	{
	    return $.ajax({
	        type: "POST",
	        url: url,
	        data: json,
	        dataType: 'json',
	        contentType: 'application/json'
	    });
	}
	
	function ajaxDelete(url, data)
	{
	    return $.ajax(
	        {
	            url: url,
	            type: 'DELETE',
	            dataType: 'text json'
	        }
	    );
	}

	function logout(event)
	{
	    event.preventDefault();
	
	    ajaxGet("/api/1/user/logout")
	        .done(function(data)
	            {
	                window.location.href = "/";
	            });
	}

})(jQuery);