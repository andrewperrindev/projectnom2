<?php
class Ingredient
{
	public $id;
	public $name;
	public $quantity;
	public $unit;
	public $preparation;
	
	public function isEmpty()
	{
		return trim($this->name) == FALSE;
	}
}
?>