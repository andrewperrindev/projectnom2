<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Epicurious
{
	private $CI;
	private $content;

	public function __construct($params)
    {
    	$this->CI =& get_instance();

    	if (!empty($params['content']))
    	{
    		$this->content = $params['content'];
    	}
    }

    public function getTitle()
    {
		return $this->returnMatch("/<div class=\"?title-source\"?>.+<h1 itemprop=\"?name\"?>(.+)<\/h1>/imsU", $this->content);
    }

    public function getAuthor()
    {
	    $author = "";
	    preg_match_all("/<a class=\"contributor\" [^>]+>([^<]+)<\/a>/", $this->content, $matches);

	    foreach($matches[1] as $match) {
		    $author .= ($author != "" ? ", " : "")
		    	. $match;
	    }
	    
	    return $author;
    }

    public function getTotalTime()
    {
		return $this->returnMatch("/<dt class=\"?total-time\"?>Total Time<\/dt><dd class=\"?total-time\"?>(.+)<\/dd>/imsU", $this->content);
    }

    public function getPrepTime()
    {
		return $this->returnMatch("/<dt class=\"?active-time\"?>Active Time<\/dt><dd class=\"?active-time\"?>(.+)<\/dd>/imsU", $this->content);
    }

    public function getServings()
    {
		return $this->returnMatch("/<dd class=\"?yield\"? itemprop=\"?recipeYield\"?>(.+)<\/dd>/imsU", $this->content);
    }

    public function getImage()
    {
    	$imgUrl = $this->returnMatch("/<meta itemprop=\"?image\"? content=\"?([^\"]+?)\"?\s?\/?>/imsU", $this->content);

    	if (!empty($imgUrl))
    	{
	    	if (substr($imgUrl, 0, 2) == "//")
	    	{
		    	$imgUrl = "http:$imgUrl";
	    	}
	    	if ($imgUrl[0] == "/")
	    	{
		    	$imgUrl = "http://www.epicurious.com$imgUrl";
		    }

			return $imgUrl;
    	}
    	
    	return "";
    }

/*
    public function getIngredients()
    {
    	$this->CI->load->helper("html2text");

    	if (!empty($this->content))
    	{
    		preg_match("/<div id=\"ingredients\">\s+<h2>Ingredients<\/h2>\s+<ul class=\"ingredientsList first no-header\">(.+)<\/ul>/imsU",
    			$this->content, $matches);
    		//return print_r($matches, true);

    		if (!empty($matches[1]))
    		{
    			$result = array();
    			preg_match_all("/<li class=\"ingredient\" itemprop=\"ingredients\">(.+)<\/li>/imsU", trim($matches[1]), $ingredients);

    			foreach ($ingredients[1] as $ingredient)
    			{
    				$result[] = convert_html_to_text($ingredient);
    			}
    			return $result;
    		}
    	}
    }
*/
    public function getIngredients()
    {
    	$this->CI->load->helper("html2text");

    	if (!empty($this->content))
    	{
    		preg_match("/<div class=\"?ingredients-info\"?><h2 class=\"?section-title\"?>Ingredients<\/h2>(.+)<\/div>/imsU",
    			$this->content, $matches);
    		//return print_r($matches, true);

    		if (!empty($matches[1]))
    		{
    			$result = array();
    			preg_match_all("/(<strong>(.+)<\/strong>)|(<li class=\"?ingredient\"? itemprop=\"?ingredients\"?>(.+)<\/li>)/imsU", trim($matches[1]), $ingredients);
				//return print_r($ingredients, true);
				
    			for ($i = 0; $i < count($ingredients[4]); $i++)
    			{
	    			$ingredient = $ingredients[4][$i];
	    			
	    			if (empty($ingredient))
	    			{
		    			$ingredient = $ingredients[2][$i];
	    			}
    				$result[] = convert_html_to_text($ingredient);
    			}
    			return $result;
    		}
    	}
    }

    public function getDirections()
    {
        $this->CI->load->helper("html2text");
        $result = array();

        if (!empty($this->content))
        {
            preg_match("/<div class=\"?instructions\"? itemprop=\"?recipeInstructions\"?><h2 class=\"?section-title\"?>Preparation<\/h2>(.+)<\/div>/imsU", $this->content, $matches);
            //return print_r($matches, true);

            if (!empty($matches[1]))
            {
                preg_match_all("/(<strong>(.+)<\/strong>)|(<li class=\"?preparation-step\"?>(.+)<\/li>)/imsU", $matches[1], $directions);

				for ($i = 0; $i < count($directions[4]); $i++)
				{
					$direction = $directions[4][$i];
					
					if (empty($direction))
					{
						$direction = $directions[2][$i];
					}
					$result[] = convert_html_to_text($direction);
				}
            }
        }

        return $result;
    }

    private function returnMatch($pattern, $content)
    {
    	if (!empty($content))
    	{
			preg_match($pattern, $content, $matches);

			if (!empty($matches[1]))
			{
				return trim($matches[1]);
			}
		}

		return "";
    }
}
?>