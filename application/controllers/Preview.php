<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once(APPPATH . '/controllers/WebAppControllerBase.php');

class Preview extends WebAppControllerBase 
{
   public function __construct()
   {
        parent::__construct();

		if ($this->session->userdata('username') !== NULL)
		{
		}
   }

	public function index($id = 0)
	{
		$this->load->model('Recipe_model', 'recipeService', TRUE);

		$data['username'] = "";
		$data['recipeid'] = $id;
		$data['cuisines'] = $this->recipeService->getCuisines();
		$data['types'] = $this->recipeService->getTypes();

		try
		{
			$recipe = $this->recipeService->getRecipe($data);
		}
		catch (RecipeException $re)
		{
			$recipe = $re->baseRecipe;
		}

		if (empty($recipe))
		{
			$this->failureRedirect();
		}
		else if ($recipe->origin_url != "")
		{
			header("Location: " . $recipe->origin_url);
		}
		else
		{
			$data['recipe'] = $recipe;
			
			//add_js(array('recipe/recipe.js','recipe/recipe-common.js'));
			add_css('recipe/preview.css');
	
			$this->load->view('templates/header', $data);
			$this->load->view('templates/nav_empty', $data);
			$this->load->view('pages/preview', $data);
			$this->load->view('templates/footer', $data);
		}
	}
}
?>