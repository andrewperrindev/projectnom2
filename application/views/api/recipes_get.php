<?php	
	if (isset($error))
	{
		$result = array
		(
			'success' => 0,
			'error' => $error
		);
	}
	else
	{
		$row_count = $recipes['rows'];
		unset($recipes['rows']);

		if ($row_count == 1 && empty($recipes[0]))
		{
			$result = array('success' => 0);
		}
		else if ($row_count == 1 && !$recipes[0]->editable)
		{
			$result = array('readonly' => 1);
			
			if (!empty($recipes[0]->origin_url) && $preview != 1)
			{
				$result['success'] = 0;
				$result['origin_url'] = $recipes[0]->origin_url;
			}
			else
			{
				if ($preview == 1 && !empty($recipes[0]->origin_url))
				{
					$url_parsed = parse_url($recipes[0]->origin_url);
					
					$recipes[0]->title = $recipes[0]->title . " <p class='text-muted'><small><a href='" . $recipes[0]->origin_url . "'>"
						. "Courtesy of " . $url_parsed["host"] . "</a></small></p>";
				}
				
				$result['success'] = 1;
				$result['recipes'] = $recipes;
			}
		}
		else
		{
			$result = array
			(
				'success' => 1,
				'recipes' => $recipes,
				'total' => $row_count
			);
		}
	}
	
	print json_encode($result, JSON_UNESCAPED_UNICODE);
?>