casper.test.begin('Login with "Remember Me"', 1, 
function suite(test) 
{
	casper.start(getServerName(), function() {
		login_wolf_credentials(test, this);
		this.waitForUrl(/home/);
	});
	
	casper.then(function() {
		var cookie = "";
		
		for (var i = 0; i < this.page.cookies.length; i++) {
			if (this.page.cookies[i].name == "remember_accesskey") {
				cookie = this.page.cookies[i];
			}
		}
		this.echo("Remeber Me cookie detail: " + JSON.stringify(cookie));
		this.page.clearCookies();
		this.page.addCookie(cookie);		
	});
	
	casper.run(function() {
        test.done();
    });

});

casper.test.begin('Verify "Remember Me" Works', 2, 
function suite(test) 
{
	casper.start(getServerName(), function() {
		this.waitForUrl(/home/);
	});
	
	casper.then(function() {
		test.assertTitle("Home :: ProjectNom", "Account homepage title is the one expected");
	});
	
	casper.thenOpen(getServerName() + "friends/messages", function() {
		this.waitForUrl(/friends\/messages/);
	});
	
	casper.then(function() {
		test.assertExists("input#twitter", "Twitter option exists for Remember Me");
	});
	
	casper.then(function() {
		this.page.clearCookies();
	});
	
    casper.run(function() {
        test.done();
    });	
});

casper.test.begin('ProjectNom shows correct welcome page & home page', 11, 
function suite(test) 
{
	// Load public welcome page, check for limited toolbar, check for sign up form, check failed login handling
    casper.start(getServerName(), function() {
        test.assertTitle("Welcome :: ProjectNom", "Public homepage title is the one expected");
        test.assertExists('form#signupForm', "Sign up form is found");
        test.assertEval(function() {
            return __utils__.findAll("ul.navbar-nav li:not(.hidden)").length == 3;
        }, "Nav Bar Shows 3 Options");


        test.assertExists('form#loginForm', "Login form is found");
        this.fill('form#loginForm', {
            username: "mageuzi",
            password: "meerkat"
        }, false);
        this.click("button.btn-login");
        casper.waitForSelector('div#loginResult div.alert', function() {
			test.assertEquals(this.fetchText('div#loginResult div.alert'), 
				"×Incorrect user name or password specified. Please try again.",
				"Login error received");
		});
    });
    
    // Check successful login loads /home
    casper.then(function() {
        this.fill('form#loginForm', {
            username: "mageuzi",
            password: "jessiekat"
        }, false);
        this.click("button.btn-login");
	    this.echo("Waiting for login to process...");
	    this.waitForUrl(/home/);
    });

	// Check /home allows search and has full toolbar
    casper.then(function() {
        test.assertTitle("Home :: ProjectNom", "Account homepage title is the one expected");
        test.assertExists("form.recipe-search", "Main search form is found");
        test.assertEval(function() {
            return __utils__.findAll("div.navbar-right li.dropdown").length == 5;
        }, "Nav Bar Shows 5 Options");
        /*
        test.assertUrlMatch(/q=casperjs/, "search term has been submitted");
        */
        this.waitForSelector("div.part-select select");
    });
    
    casper.then(function() {
	    this.evaluate(function() {
		    $("div.part-select select").val("evening");

		    //var event = new Event('change', { bubbles: true });
		    var event = document.createEvent('HTMLEvents');
			event.initEvent('change', true, false);
		    return document.querySelector("div.part-select select").dispatchEvent(event);
	    });

        this.waitForSelector("div#new div.loader");
    });
    
    // Check recipe surfacing tabs load properly
    casper.then(function() {
	    this.echo("Waiting for New recipes to load...");
	    this.waitWhileSelector('div#new div.loader');
    });
    
    casper.then(function() {
        test.assertEval(function() {
            return __utils__.findAll("div#new div.thumbnail").length == 4;
        }, "New tab shows 4 recipes");
	    
    });

    casper.then(function() {
	    this.echo("Waiting for Favorite recipes to load...");
	    this.click("#tabFavorites");
	    this.waitWhileSelector('div#favorites div.loader');
    });
    
    casper.then(function() {
        test.assertEval(function() {
            return __utils__.findAll("div#favorites div.thumbnail").length == 3;
        }, "Favorite tab shows 3 recipes");
	    
    });

    casper.then(function() {
	    this.echo("Waiting for Different recipes to load...");
	    this.waitWhileSelector('div#different div.loader');
    });
    
    casper.then(function() {
        test.assertEval(function() {
            return __utils__.findAll("div#different div.thumbnail").length == 4;
        }, "Different tab shows 4 recipes");
	    
    });

    casper.run(function() {
        test.done();
    });
});