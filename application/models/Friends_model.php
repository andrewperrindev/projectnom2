<?php
class Friends_model extends CI_Model 
{
    function __construct()
    {
        parent::__construct();
    }

    function getList($username)
    {
    	$result = array();

    	$this->db->from('users_friends');
        $this->db->join('users u1', 'u1.id = users_friends.user_id');
    	$this->db->join('users u2', 'u2.id = users_friends.friend_user_id');
        $this->db->select("u2.*");
    	$this->db->where('u1.username', $username);
    	$query = $this->db->get();

    	foreach ($query->result() as $row)
		{
			$result[] = array(
				"id" => $row->id,
                "username" => $row->username,
                "email" => $row->email
            );
		}

		return $result;
    }

    function addFriend($username, $friend_email)
    {
        if (!empty($friend_email) && !empty($username))
        {
            $userid = $friend_userid = 0;
            $friend_username = "";

            $this->db->from("users");
            $this->db->select("id");
            $this->db->where('username', $username);
            $query = $this->db->get();

            $userid = $query->row()->id;

            $this->db->from("users");
            $this->db->select("id, username");
            $this->db->where("email", $friend_email);
            $query = $this->db->get();

            if ($query->num_rows > 0)
            {
                $friend_userid = $query->row()->id;
                $friend_username = $query->row()->username;

                $this->db->from("users_friends");
                $this->db->where("user_id", $userid);
                $this->db->where("friend_user_id", $friend_userid);
                $query = $this->db->get();
            }

            if ($userid > 0 && $friend_userid > 0 && $userid != $friend_userid && $query->num_rows() == 0)
            {
                $this->db->insert('users_friends', array("user_id" => $userid, "friend_user_id" => $friend_userid));
                return array("id" => $friend_userid, "username" => $friend_username);
            }
            else if ($query->num_rows() > 0)
            {
                return TRUE;
            }
            else
            {
                return FALSE;
            }
        }
        
        return FALSE;
    }

    function removeFriend($username, $friend_userid)
    {
        if (intval($friend_userid) > 0 && !empty($username))
        {
            $userid = 0;

            $this->db->from("users");
            $this->db->select("username,id");
            $this->db->where('username', $username);
            $query = $this->db->get();

            if ($query->num_rows() > 0)
            {
                $userid = $query->row()->id;
            }

            if ($userid > 0 && $friend_userid > 0 && $userid != $friend_userid)
            {
                $this->db->delete('users_friends', array("user_id" => $userid, "friend_user_id" => $friend_userid));
                
                if ($this->db->affected_rows() > 0)
                {
	                return TRUE;
	            }
            }
            else
            {
                return FALSE;
            }
        }
        
        return FALSE;
    }
    
    function getMessage($id)
    {
	    if ($id > 0)
	    {
		    $this->db->from("users_messages");
			$this->db->join("users a", "users_messages.author_userid = a.id");
		    $this->db->join("recipes r", "users_messages.recipeid = r.id", "left");
		    $this->db->where("users_messages.id", $id);
		    $this->db->where("`users_messages`.`to_userid` IS NULL AND `users_messages`.`tweet_id` IS NOT NULL", NULL, FALSE);
			$this->db->select("a.`username`, users_messages.`id`, `message_type`, `message`, users_messages.`image_url`, `timestamp`, IFNULL(r.`id`, 0) AS recipeid, r.`title` AS recipetitle, r.`origin_url`",
				FALSE);
		    $query = $this->db->get();
		    
		    if ($query->num_rows() == 1)
		    {
			    return $this->composeMessage($query->row());
		    }
		}
		
		return null;
    }
    
    function getMessagesForRecipe($username, $recipe_id, $only_images = false)
    {
	    return $this->getMessages($username, 0, $recipe_id, $only_images);
    }
    
    function getMessages($username, $page = 0, $recipe_id = null, $only_images = false)
    {
	    $result = array();
	    $message_data = array();
	    
	    if (!empty($username))
	    {
		    if ($page < 0)
		    {
			    $page = 0;
		    }

			/************************* WARNING *************************
				Original library DB_active_rec.php modified to add
				get_compiled_select method. MAY BREAK ON UPGRADES.
			************************************************************/

			// Get all messages posted by a user or sent directly to a user
		    $this->db->from("users_messages");
			$this->db->join("users a", "users_messages.author_userid = a.id");
			$this->db->join("users u", "users_messages.to_userid = u.id", "left");
			$this->db->join("recipes r", "users_messages.recipeid = r.id", "left");
		    $this->db->join("users curr", "a.id = curr.id OR (users_messages.to_userid = curr.id AND users_messages.message_type = 'direct')");
			$this->db->where("curr.username", $username);
			
			if (!empty($recipe_id))
			{
				$this->db->where("users_messages.recipeid", $recipe_id);
			}
			if ($only_images)
			{
				$this->db->where("users_messages.image_url IS NOT NULL");
			}
			
			$this->db->select("a.`username`, users_messages.`id`, `message_type`, `message`, users_messages.`image_url`, `timestamp`, IFNULL(r.`id`, 0) AS recipeid, r.`title` AS recipetitle, r.`origin_url`, u.`username` AS to_user, UNIX_TIMESTAMP(`timestamp`) AS timestamp_unix, `tweet_id`",
	    		FALSE);
			$user_query = $this->db->get_compiled_select();
		    
		    // Get all messages from the user's friends
		    $this->db->from("users_messages");
		    $this->db->join("users_friends", "users_messages.author_userid = users_friends.friend_user_id");
		    $this->db->join("users a", "users_messages.author_userid = a.id");
		    $this->db->join("users u", "users_messages.to_userid = u.id", "left");
		    $this->db->join("recipes r", "users_messages.recipeid = r.id", "left");
		    $this->db->join("users curr", "users_friends.user_id = curr.id");
		    $this->db->where("curr.username", $username);
		    $this->db->where("users_messages.to_userid", null);
		    
		    if (!empty($recipe_id))
			{
				$this->db->where("users_messages.recipeid", $recipe_id);
			}
			if ($only_images)
			{
				$this->db->where("users_messages.image_url IS NOT NULL");
			}

		    $this->db->order_by("timestamp", "desc");
		    $this->db->limit(10, (10 * $page));
		    $this->db->select("a.`username`, users_messages.`id`, `message_type`, `message`, users_messages.`image_url`, `timestamp`, IFNULL(r.`id`, 0) AS recipeid, r.`title` AS recipetitle, r.`origin_url`, u.`username` AS to_user, UNIX_TIMESTAMP(`timestamp`) AS timestamp_unix, `tweet_id`",
		    	FALSE);
		    $friend_query = $this->db->get_compiled_select();
		    
		    $union_query = "$user_query UNION $friend_query";
		    $query = $this->db->query($union_query);
		    
			// Add these results
		    foreach($query->result() as $row)
		    {
				$message = $this->composeMessage($row, $username);
			    $result[] = $message;
		    }
	    }
	    	    
	    return $result;
    }
    
    function addMessage($username, $type, $messageText, $recipeId, $toUsername, $image_url)
    {
	    if (!empty($username))
	    {
		    try
		    {
			    $userid = $toUserId = 0;
	            $this->db->from("users");
	            $this->db->select("username,id");
	            $this->db->where('username', $username);
	            
	            if (!empty($toUsername))
	            {
		            $this->db->or_where('username', $toUsername);
	            }
	            
	            $query = $this->db->get();
			    
			    foreach ($query->result() as $row)
			    {
				    if ($row->username == $username)
				    {
						$userid = $row->id;
					}
					else if ($row->username == $toUsername)
					{
						$toUserId = $row->id;
					}
				}
			    
			    $messageData = array(
				    "author_userid" => $userid,
				    "message_type" => $type,
				    "message" => $messageText,
				    "timestamp" => date("Y-m-d H:i:s")
			    );
			    
			    if ($toUserId > 0)
			    {
				    $messageData['to_userid'] = $toUserId;
			    }
			    if (!empty($image_url))
			    {
				    $messageData['image_url'] = $image_url;
			    }
			    
			    if ($recipeId > 0)
			    {
			    	$this->db->select('recipes.*', FALSE);
			    	$this->db->from("recipes");
				    $this->db->join('user_recipebox', 'recipes.id = user_recipebox.recipeid');
				    $this->db->join('users', 'user_recipebox.userid = users.id');
			
				    $query = $this->db->where('username', $username);
				    $query = $this->db->where('recipes.id', $recipeId);
				    $query = $this->db->get();
				    
				    if ($query->num_rows() > 0)
				    {
					    $messageData["recipeid"] = $recipeId;
				    }
			    }
			    
			    $this->db->insert("users_messages", $messageData);
			    $insertResult = array (
				    "id" => $this->db->insert_id(),
			    	"author" => $username,
			    	"type" => $messageData['message_type'],
			    	"message" => $messageData['message'],
			    	"timestamp" => $messageData['timestamp'],
			    	"is_current_user" => 1
			    );
			    
			    if (!empty($messageData["recipeid"])) { $insertResult["recipeid"] = $messageData["recipeid"]; }
			    if (!empty($toUserId)) { $insertResult["to_user"] = $toUsername; }
			    if (!empty($image_url)) { $insertResult["image_url"] = $image_url; }
			    
			    return $insertResult;
		    }
		    catch (Exception $e)
		    {
			    log_message('error', "Exception occurred saving message: " . print_r($e, TRUE));
		    }
	    }
	    
	    return false;
    }

    function updateMessage($username, $messageId, $tweetId)
    {
	    if (!empty($username) && $messageId > 0 && $tweetId > 0)
	    {
		    try
		    {
			    /*
			    $this->db->where("m.id", $messageId);
			    $this->db->where("u.username", $username);
			    $this->db->where("m.author_userid = u.id", NULL, FALSE);
			    $this->db->update("users_messages as m, users as u", array("m.tweet_id" => $tweetId));
			    */
			    // Since CI3 doesn't support joined tables in UPDATEs anymore, we have to write our own query.
			    $this->db->query("UPDATE `users_messages` as m, `users` as u"
			    	. " SET m.tweet_id = " . $this->db->escape($tweetId)
			    	. " WHERE m.id = " . $this->db->escape($messageId)
			    	. " AND u.username = " . $this->db->escape($username)
			    	. " AND m.author_userid = u.id");
			}
		    catch (Exception $e)
		    {
			    log_message('error', "Exception occurred updating message: " . print_r($e, TRUE));
		    }
	    }
    }
    
    function removeMessage($username, $id)
    {
	    if (!empty($username) && $id > 0)
	    {
		    try
		    {
	            $this->db->from("users");
	            $this->db->select("username,id");
	            $this->db->where('username', $username);
	            $query = $this->db->get();
			    
			    $userid = $query->row()->id;

				$this->db->delete("users_messages", array("author_userid" => $userid, "id" => $id));
				
				if ($this->db->affected_rows() > 0)
				{
					return true;
				}
		    }
		    catch (Exception $e)
		    {
			    // log
		    }
	    }
	    
		return false;
    }
    
    private function composeMessage($messageRow, $username = null)
    {
		$message = array(
		    "id" => $messageRow->id,
		    "author" => $messageRow->username,
		    "type" => $messageRow->message_type,
		    "message" => $messageRow->message,
		    "timestamp" => $messageRow->timestamp
	    );
	    
	    if ($username != null)
	    {
			$message["is_current_user"] = ($messageRow->username == $username) ? 1 : 0;
	    }
	    
	    if (!empty($messageRow->image_url))
	    {
		    $message["image_url"] = $messageRow->image_url;
	    }
	    
	    if (!empty($messageRow->to_user))
	    {
		    $message["to_user"] = $messageRow->to_user;
	    }
	    
	    if (!empty($messageRow->tweet_id))
	    {
		    $message["tweet_id"] = $messageRow->tweet_id;
	    }
	    
	    if ($messageRow->recipeid > 0)
	    {
		    $message["recipe_id"] = $messageRow->recipeid;
		    $message["recipe_url"] = (!empty($messageRow->origin_url) 
		    	? $messageRow->origin_url 
		    	: (!empty($_SERVER['HTTPS']) ? "https" : "http") . "://" . $_SERVER['SERVER_NAME'] . "/recipe/" . $messageRow->recipeid);
		    $message["recipe_title"] = $messageRow->recipetitle;
	    }
	    
	    return $message;
    }
}
?>