    <!-- Intro Header -->
    <header class="intro">
        <div class="intro-body">
            <div class="container">
                <div class="row">
                    <div class="col-md-10 col-md-offset-1 greeting">
                        <h1 class='brand-heading'>
	                        <span id="recipe-title"></span>
	                        <p class="text-center recipe-author hidden"><small></small></p>
                        </h1>
                    </div>
                </div>
            </div>
        </div>
    </header>

	<!-- Recipe Header Section -->
    <section id="header" class="container content-section text-dark">
    	<input type='hidden' id='recipe-id' value='<?php print $recipeid; ?>'>
        <div class="wait-spinner">
            <div class="row">
                <div class="col-sm-12 text-dark text-center wait">
                    <i class="fa fa-circle-o-notch fa-spin"></i>
                </div>
            </div>
        </div>
    	<div class="error hidden">
			<div class="row">
            	<div class="col-sm-12 text-dark">
                	<div class="alert alert-danger" role="alert"></div>
            	</div>
        	</div>
		</div>
        <div class="recipe-header invisible">
	        <div class="row">
		        <div class="col-xs-12 text-center">
		    		<div class="row recipe-intro">
		    			<div class="col-xs-12 recipe-img"><img src='' class='img-thumbnail'></div>
					</div>
		        </div>
	        </div>
			<div class="row">
    			<div class="col-xs-12 col-sm-6 col-sm-offset-1 recipe-stats">
	    			<div class="panel panel-default">
		    			<div class="panel-heading">
							<h6>At a Glance</h6>
      					</div>
		  				<div class="panel-body">
			  				<div class="row">
				  				<div class="col-xs-12 col-lg-12">
				  					<div class="recipe-comment hidden"></div>
				  				</div>
			  				</div>
			  				<div class="row">
			  					<div class="col-xs-12 servings-metadata hidden">
				  					<div class="row">
					  					<div class="col-xs-1">
						  					<i class="fa fa-pie-chart"></i>
					  					</div>
					  					<div class="col-xs-10">
					  						Yield: <span class="servings"></span>
					  					</div>
				  					</div>
				  				</div>
			  					<div class="col-xs-12 time-metadata hidden">
				  					<div class="row">
					  					<div class="col-xs-1">
						  					<i class="fa fa-clock-o"></i>
					  					</div>
					  					<div class="col-xs-10 time-text">
							  			</div>
							  		</div>
							  	</div>
	      					</div>
	  					</div>
	  				</div>
    			</div>
				<div class="col-xs-12 col-sm-4 recipe-tags">
					<div class='panel panel-default'>
		    			<div class="panel-heading">
							<h6>Tags</h6>
      					</div>
						<div class='panel-body'>
							<div class="row">
								<div class="col-xs-12 recipe-user-metadata">
									<div class="col-xs-12 text-center">
										<div class='dropdown recipe-rating'>
											<button class='btn btn-default dropdown-toggle' type='button' id='dropdownRating' data-toggle='dropdown' aria-expanded='false'>
												<span class='button-text'></span>
												<span class='caret'></span>
											</button>
											<ul class='dropdown-menu' role='menu' aria-labelledby='dropdownRating'>
												<li role='presentation' class='rating-item'>
													<a role='menuitem' class='star-0' data-value='0' tabindex='-1' href='#'><i class='fa fa-star-o'></i>&nbsp;</a>
												</li>
											<?php
												for($i = 1; $i <= 5; $i++)
												{
											?>
													<li role='presentation' class='rating-item'><a role='menuitem' class='star-<?php print $i; ?>' data-value='<?php print $i; ?>' tabindex='-1' href='#'>
													<?php print str_repeat("<i class='fa fa-star'></i>&nbsp;", $i); ?>
													</a></li>
											<?php
												}
											?>
											</ul>
										</div>
									</div>
									<div class="col-xs-12 text-center">
										<div class='dropdown recipe-cuisine'>
											<button class='btn btn-default dropdown-toggle' type='button' id='dropdownCuisine' data-toggle='dropdown' aria-expanded='false'>
												<span class='button-icon'><i class='fa fa-globe'></i>&nbsp;</span>
												<span class='button-text'></span>
												<span class='caret'></span>
											</button>
											<ul class='dropdown-menu' role='menu' aria-labelledby='dropdownCuisine'>
												<li role='presentation' class='cuisine-item'>
													<a role='menuitem' class='empty' data-value='' tabindex='-1' href='#'>&nbsp;</a>
												</li>
											<?php
												for($i = 0; $i < count($cuisines); $i++)
												{
											?>
													<li role='presentation' class='cuisine-item'><a role='menuitem' class='<?php print $cuisines[$i]; ?>' data-value='<?php print $cuisines[$i]; ?>' tabindex='-1' href='#'>
													<?php print $cuisines[$i]; ?>
													</a></li>
											<?php
												}
											?>
											</ul>
										</div>
									</div>
									<div class="col-xs-12 text-center">
										<div class='dropdown recipe-types'>
											<button class='btn btn-default dropdown-toggle' type='button' id='dropdownTypes' data-toggle='dropdown' aria-expanded='false'>
												<span class='button-icon'><i class='fa fa-tag'></i>&nbsp;</span>
												<span class='button-text'></span>
												<span class='caret'></span>
											</button>
											<ul class='dropdown-menu' role='menu' aria-labelledby='dropdownTypes'>
												<li role='presentation' class='type-item'>
													<a role='menuitem' class='empty' data-value='' tabindex='-1' href='#'>&nbsp;</a>
												</li>
											<?php
												for($i = 0; $i < count($types); $i++)
												{
											?>
													<li role='presentation' class='type-item'><a role='menuitem' class='<?php print $types[$i]; ?>' data-value='<?php print $types[$i]; ?>' tabindex='-1' href='#'>
													<?php print $types[$i]; ?>
													</a></li>
											<?php
												}
											?>
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<!-- Recipe Body Section -->
	<section id="recipe-body" class="container content-section text-dark">
		<div class="row">
			<div class="ingredient-frame col-xs-offset-1 col-xs-10 col-lg-offset-0 col-lg-5">
				<div class="ingredient-section">
					<div class='row ingredient-row hidden'>
						<div class='col-xs-12 text'></div>
					</div>
					<div class="ingredient-view"></div>
				</div>
			</div>
			<div class="direction-frame col-xs-offset-1 col-xs-10 col-lg-offset-0 col-lg-7">
				<div class="direction-section">
					<div class='row direction-row hidden'>
						<div class='col-xs-1 direction-label direction-number' id='direction-num'>
							<h1></h1>
						</div>
						<div class='col-xs-11 recipe-direction'></div>
					</div>
					<div class="direction-view"></div>
				</div>
			</div>
		</div>
	</section>

	<!-- Recipe Photo Section -->
	<section id="recipe-photos" class="container content-section text-dark hidden">
		<div class="row">
			<div class="col-xs-12">
				<div class='panel panel-default'>
	    			<div class="panel-heading">
						<h6>Recent Photos</h6>
  					</div>
					<div class='panel-body'>
						<div class="row photos">
							<div class="col-xs-10 col-xs-offset-1 col-sm-6 col-sm-offset-0 col-md-4 hidden photo-template">
								<img src="" class="thumbnail img-responsive">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<!-- Recipe Footer Section -->
	<section id="recipe-footer" class="container content-section text-dark">
		<div class="row hidden">
			<div class="col-xs-12">
				<div class='panel panel-default'>
					<div class='panel-body'>
						<div class="btn-group dropup pull-right" id="recipe-settings">
							<button type="button" class="btn btn-default btn-lg dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
								<i class="fa fa-cog"></i> <span class="caret"></span>
							</button>
							<ul class="dropdown-menu" role="menu">
								<li><a href="/recipe/edit/<?php print $recipeid; ?>" class="edit">Edit</a></li>
								<li><a href="#" class="list-add" data-toggle="modal" data-target="div.bs-shopping-modal">Add to Shopping List...</a></li>
								<li class="original-link hidden"><a href="">View Original Recipe</a></li>
							</ul>
						</div>
						<div class="btn-group dropup pull-right" id="recipe-messages">
							<button type="button" class="btn btn-default btn-lg dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
								<i class="fa fa-comments"></i> <span class="caret"></span>
							</button>
							<ul class="dropdown-menu social" role="menu">
								<li><a href="#" class="send-message">Post Message About This Recipe...</a></li>
								<li class="message-friend-menu-item hidden"><a href="#" class="send-message-friend">Send Recipe to Friend...</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>