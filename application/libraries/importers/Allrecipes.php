<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Allrecipes
{
	private $CI;
	private $content;

	public function __construct($params)
    {
    	$this->CI =& get_instance();

    	if (!empty($params['content']))
    	{
    		$this->content = $params['content'];
    	}
    }

    public function getTitle()
    {
		return $this->returnMatch("/<h1 class=\"recipe-summary__h1\" itemprop=\"name\">(.+)<\/h1>/", $this->content);
    }

    public function getAuthor()
    {
		return $this->returnMatch("/<span class=\"submitter__byline\">Recipe by: <\/span><span class=\"submitter__name\" itemprop=\"author\">(.+)<\/span>/", $this->content);
    }

    public function getTotalTime()
    {
		return $this->returnMatch("/<span class=\"ready-in-time\">(.+)<\/span>/", $this->content);
    }

    public function getPrepTime()
    {
	    // Not using the helper method here because we need to access multiple matches.
	    if (!empty($this->content))
	    {
			preg_match("/<p class=\"prepTime__item--type\">Prep<\/p><time itemprop=\"prepTime\" datetime=\".+\"><span class=\"prepTime__item--time\">(\d+)<\/span>(.+)<\/time>/", $this->content, $matches);

			if (!empty($matches[1]) && !empty($matches[2]))
			{
				return $matches[1] . $matches[2];
			}
		}
    }

    public function getServings()
    {
		return $this->returnMatch("/<meta id=\"metaRecipeServings\" itemprop=\"recipeYield\" content=\"(.+)\">/imsU", $this->content);
    }

    public function getImage()
    {
    	$pattern = "/<img class=\"rec-photo\" (id=\"?.+\"?)? src=\"(.+)\" alt=\"/";
    	
    	if (!empty($this->content))
    	{
			preg_match($pattern, $this->content, $matches);

			if (!empty($matches[2]))
			{
				return $matches[2];
			}
		}

		return "";
    }

    public function getIngredients()
    {
    	$this->CI->load->helper("html2text");

		// AllRecipes has multiple ingredients lists to parse.
    	if (!empty($this->content))
    	{
    		preg_match_all("/<ul class=\"checklist dropdownwrapper list-ingredients-\d+\" ng-hide=\"reloaded\" id=\"lst_ingredients_\d+\">(.+)<\/ul>/imsU",
    			$this->content, $matches);

    		if (!empty($matches[1]))
    		{
    			$result = array();
    			foreach($matches[1] as $ingredients_section)
    			{
	    			preg_match_all("/<span class=\"recipe-ingred_txt added\" data-id=\"\d+\" data-nameid=\"\d+\" itemprop=\"ingredients\">(.+)<\/span>/imsU", trim($ingredients_section), $ingredients);
	
	    			foreach ($ingredients[1] as $ingredient)
	    			{
	    				$result[] = convert_html_to_text($ingredient);
	    			}
    			}
    			return $result;
    		}
    	}
    }

    public function getDirections()
    {
        $this->CI->load->helper("html2text");
        $result = array();

        if (!empty($this->content))
        {
            //preg_match("/<h6>Directions<\/h6>\s+(.+)?\s+(<p>(.+)<\/p>)+?/imsU", $this->content, $matches);
            preg_match("/<ol class=\"list-numbers recipe-directions__list\" itemprop=\"recipeInstructions\">(.+)<\/ol>/imsU", $this->content, $matches);
            //return print_r($matches, true);

            if (!empty($matches[1]))
            {
                preg_match_all("/<span class=\"recipe-directions__list--item\">(.+)<\/span>/imsU", $matches[1], $directions);

                foreach ($directions[1] as $direction)
                {
                    $result[] = convert_html_to_text($direction);
                }
            }
        }

        return $result;
    }

    private function returnMatch($pattern, $content)
    {
    	if (!empty($content))
    	{
			preg_match($pattern, $content, $matches);

			if (!empty($matches[1]))
			{
				return $matches[1];
			}
		}

		return "";
    }
}
?>