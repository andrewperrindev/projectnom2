    <!-- Footer -->
    <footer>
        <div class="container text-center text-muted">
            <hr>
            <p><small>Copyright &copy; ProjectNom 2015</small></p>
            <span class="footer-links"><a href="/">Home</a> | <a href="http://twitter.com/projectnom">Twitter</a> | <a href="mailto:support@projectnom.com">Support</a></span>
        </div>
    </footer>

    <?php
        if (!empty($username))
        {
        	$this->view('templates/modal-recipeimport');
        	add_js("recipe/import.js");
        }
	?>

	<!-- Core JS -->
	<?php print put_js(); ?>
	
</body>

</html>
