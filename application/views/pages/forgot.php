    <!-- Intro Header -->
    <header class="intro">
        <div class="intro-body">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2 greeting">
	                    <h1 class='brand-heading'>Forgot Password</h1>
	                </div>
                </div>
            </div>
        </div>
    </header>

    <section id="result" class="container content-section text-dark">
		<div class="row">
			<div class="col-sm-8 col-sm-offset-2">
				<?php
					if ($step == 1)
					{
						if (!empty($badkey) && $badkey)
						{
					?>
					<div class="alert alert-danger text-center" role="alert"><strong>Invalid key</strong>. This link was already used or has expired. Please try enterting your email again for a new link, or <a href="mailto:support@projectnom.com">email support if this error persists</a>.</div>
					<?php							
						}

				?>
				<div class="alert alert-info text-center" role="alert"><strong>Forget your password?</strong> No problem. Enter your email address here, and we'll send you instructions for how to reset it.</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-4 col-xs-6 col-sm-offset-4 col-xs-offset-3">
				<form action="/welcome/forgot" method="post" id="email">
					<div class="form-group">
						<label for="userEmail">Email address</label>
						<input type="email" class="form-control email-field" id="userEmail" name="email" placeholder="Email">
					</div>
					<button type="submit" class="btn btn-default">Submit</button>
				</form>
				<?php
					}
					else if ($step == 2)
					{
				?>
				<div class="alert alert-success text-center" role="alert"><?php if ($found) { ?><i class="fa fa-check"></i> <?php }?><strong>Thanks!</strong> You should receive an email shortly. If not, <a href="mailto:support@projectnom.com">let us know</a> and we'll set things right.</div>
				<?php						
					}
					else if ($step == 3 || $badpassword || (!empty($changeSuccess) && !$changeSuccess))
					{
						if ($badpassword)
						{
					?>
					<div class="alert alert-danger text-center" role="alert"><strong>Invalid password</strong>. Please make sure your password is at least six characters, and that you typed your password correctly in both fields.</div>
					<?php							
						}
						else if (!empty($changeSuccess) && !$changeSuccess)
						{
					?>
					<div class="alert alert-danger text-center" role="alert"><strong>There was a problem changing your password</strong>. If this error persists, <a href="mailto:support@projectnom.com">please let us know</a>.</div>
					<?php							
						}
				?>
				<div class="alert alert-info text-center" role="alert">Enter a new password here. Remember, it should be at least six characters long.</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-4 col-xs-6 col-sm-offset-4 col-xs-offset-3">
				<form action="/welcome/forgot/<?php print $key; ?>" method="post" id="password">
					<div class="form-group">
						<label for="password">Password</label>
						<input type="password" minlength='6' class="form-control password-field" id="password" name="password" placeholder="Password">
					</div>
					<div class="form-group">
						<label for="confirmPassword">Confirm Password</label>
						<input type="password" minlength='6' class="form-control confirm-password-field" id="confirmPassword" name="confirmPassword" placeholder="Confirm Password">
					</div>
					<button type="submit" class="btn btn-default">Submit</button>
				</form>
				<?php
					}
					else if ($step == 4)
					{
				?>
				<div class="alert alert-success text-center" role="alert"><?php if ($changeSuccess) { ?><i class="fa fa-check"></i> <?php }?><strong>You're done!</strong> Your password has been reset. <a href="/">Click here to return to the home page and log in with your new password</a>.</div>
				<?php
					}
					else
					{
				?>
				<div class="alert alert-danger" role="alert">There was a problem changing your password. Please try again, or <a href="support@projectnom.com">email ProjectNom support</a> if you continue to receive this error.</div>
				<?php
					}
				?>
			</div>
		</div>
    </section>