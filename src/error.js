$(function() {
	var pnError = window.pnError || {};

	pnError.React = require('react');
	pnError.ReactDOM = require('react-dom');
	
	// Custom React Component
	pnError.BootstrapError = require("./components/BootstrapError.js");
	
	if (pnError.renderError) {
		pnError.renderError();
	}
});