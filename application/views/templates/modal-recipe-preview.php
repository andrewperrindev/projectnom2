<!-- Recipe Preview Modal -->
<div class="modal fade bs-recipe-preview-modal" role="dialog">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header text-dark">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Preview</h4>
			</div>
	    	<div class="modal-body text-dark">
		    	<div class="container-fluid">
					<div class="row wait-spinner <?php if (!empty($recipe)) {?>hidden<?php }?>">
						<div class="col-sm-12 text-dark text-center wait">
							<i class="fa fa-circle-o-notch fa-spin"></i>
						</div>
					</div>
			    	<div class="error hidden">
						<div class="row">
			            	<div class="col-sm-12 text-dark">
			                	<div class="alert alert-danger" role="alert"></div>
			            	</div>
			        	</div>
					</div>
					<div class="recipe-preview-header">
				        <div class="recipe-header <?php if (empty($recipe)) {?>hidden<?php }?>">
				    		<div class="row recipe-intro">
					    		<div class="col-xs-12 recipe-title text-center"><h2 class="recipe-title"><?php if (!empty($recipe->title)) { print $recipe->title; }?></h2></div>
				    			<div class="col-xs-12 text-center recipe-author <?php if (empty($recipe->author)) { ?>hidden<?php } ?>"><?php if (!empty($recipe->author)) { print "<p><small><em>by " . $recipe->author . "</em></small></p>"; }?></div>
				    			<div class="col-xs-8 col-xs-offset-2 col-sm-8 col-sm-offset-2 text-center recipe-img <?php if (empty($recipe->image_url)) { ?>hidden<?php } ?>"><img src='<?php if (!empty($recipe->image_url)) { print "/" . $recipe->image_url; } ?>' class='img-thumbnail'></div>
				    			<div class="col-xs-12 text-center recipe-comment <?php if (empty($recipe->comment)) { ?>hidden<?php }?>"><?php if (!empty($recipe->comment)) { print $recipe->comment; }?></div>
				    			<div class="col-xs-12 text-center recipe-meta-details">
					    			<div class="recipe-meta-detail time <?php if (empty($recipe->displayTimes)) {?>hidden<?php } ?>"><i class='fa fa-clock-o'></i> <span class="recipe-time"><?php if (!empty($recipe->displayTimes)) { print $recipe->displayTimes; }?></span></div>
					    			<div class="recipe-meta-detail servings <?php if (empty($recipe->servings)) { ?>hidden<?php } ?>"><i class='fa fa-users'></i> <span class="recipe-servings"><?php if (!empty($recipe->servings)) { print $recipe->servings; }?></span></div>
				    			</div>
							</div>
				        </div>
					</div>
					<div class="recipe-preview-ingredients">
				        <div class="recipe-ingredients <?php if (empty($recipe->displayIngredients)) {?>hidden<?php }?>">
							<div class='row ingredient-row hidden'>
								<div class='col-xs-10 col-xs-offset-1 text'></div>
							</div>
							<?php
								if (!empty($recipe->displayIngredients))
								{
									foreach($recipe->displayIngredients as $ingredient)
									{
										?>
										<div class="row ingredient-row">
											<div class="col-xs-10 col-xs-offset-1 <?php print $ingredient["type"]; ?>">
												<?php print $ingredient["text"]; ?>
											</div>
										</div>
										<?php
									}
								}
							?>
				        </div>
					</div>
					<div class="recipe-preview-directions">
				        <div class="recipe-directions <?php if (empty($recipe->directions)) {?>hidden<?php }?>">
							<div class='row direction-row hidden'>
								<div class='col-xs-1 col-xs-offset-1 direction-label direction-number' id='direction-num'>
									<h1></h1>
								</div>
								<div class='col-xs-9 col-md-8 recipe-direction'></div>
							</div>
					        <?php
						        if (!empty($recipe->directions))
						        {
								    $direction_number = 0;
								    
								    foreach($recipe->directions as $direction)
								    {
									    $direction_number++;
									    
									    ?>
										<div class='row direction-row'>
											<div class='col-xs-1 col-xs-offset-1 direction-label direction-number' id='direction-num'>
												<h1><?php print $direction_number; ?></h1>
											</div>
											<div class='col-xs-9 col-md-8 recipe-direction'><?php print $direction; ?></div>
										</div>
										<?php
									}
								}
							?>

				        </div>
					</div>
		    	</div>
	    	</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary btn-cancel" data-dismiss="modal">Close</button>
	    	</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
