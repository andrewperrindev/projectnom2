<?php
include_once("Base_model.php");
require 'lib/password.php';
	
class User_model extends Base_model
{
	var $SALT = ".a,scxife";

    function __construct()
    {
        parent::__construct();
    }
    
    public function createAccount($userData)
    {
	    $this->db->from('users');
	    $query = $this->db->where('username', $userData['username']);
	    $query = $this->db->or_where('email', $userData['email']);
	    
	    $query = $this->db->get();
	    
	    if ($query->num_rows() > 0)
	    {
		    throw new Exception("Username or email exists.");
	    }
	    else
	    {
		    $crypt_password = password_hash($userData['password'], PASSWORD_DEFAULT);
		    $activation_key = $this->getGUID();
		    $this->db->insert('users',
		    	array
		    	(
		    		'username' => $userData['username'],
		    		'email' => $userData['email'],
		    		'password' => $crypt_password,
		    		'is_locked' => 1
		    	)
		    );
		    $this->db->insert('users_accesskeys',
		    	array
		    	(
			    	'user_id' => $this->db->insert_id(),
			    	'key' => $activation_key,
			    	'task' => 'ActivationKey'
		    	)
		    );
		    
		    return $activation_key;
	    }
    }
    
    public function updateAccount($userData)
    {
	    $this->db->from('users');
	    $this->db->where('email', $userData['email']);
	    $this->db->where('username !=', $userData['username']);
	    
	    $query = $this->db->get();
	    
	    if ($query->num_rows() > 0)
	    {
		    throw new Exception("Email exists.");
	    }
		else
		{
		    $newUserData = array (
			    'email' => $userData['email']
		    );
		    
		    if (!empty($userData['password']))
		    {
			    $newUserData['password'] = password_hash($userData['password'], PASSWORD_DEFAULT);
		    }
		    if ($userData['removeTwitter'])
		    {
			    $newUserData['twitter_id'] = null;
			    
			    $this->db->from('users');
			    $this->db->where('username', $userData['username']);
			    $query = $this->db->get();
			    $row = $query->row();
			    
			    if (!empty($row->twitter_id))
			    {
				    $this->db->delete('users_twitter', array('id' => $row->twitter_id));
			    }
		    }
		    
		    $this->db->where('username', $userData['username']);
		    $this->db->update('users', $newUserData);
		}
    }
    
    public function validateAccount(&$credentials)
    {
	    $this->db->select('users.*, users_twitter.username AS twitter');
	    $this->db->from('users');
	    $this->db->join('users_twitter', 'users_twitter.id = users.twitter_id', 'left');
	    $query = $this->db->where('users.username', $credentials['username']);
	    $query = $this->db->get();
	    
	    if ($query->num_rows() == 1)
	    {
		    $row = $query->row();
		    $credentials['username'] = $row->username;
		    $credentials['twitter'] = $row->twitter;
		    
		    if ($row->is_locked == 1)
		    {
			    throw new Exception("Account is locked.");
		    }
		    if ($row->password == crypt($credentials['password'],$this->SALT))
		    {
			    $updated_hash = 
			    	array('password' => password_hash($credentials['password'], PASSWORD_DEFAULT));
			    $this->db->where('id', $row->id);
			    $this->db->update('users', $updated_hash);
			    
			    return TRUE;
		    }
		    else if (password_verify($credentials['password'], $row->password))
		    {
				$this->db->delete("users_accesskeys", array("user_id" => $row->id, "task" => "ResetPassword"));
				return TRUE;
		    }
		    
		    throw new Exception("Incorrect password.");
	    }
	    else
	    {
		    throw new Exception("Account does not exist in database.");
	    }
    }

    public function getAccountDetails($username, $access_key = '')
    {
    	$user_result = array();
    	$this->db->select('users.username, email, users_twitter.username AS twitter, oauth_token, oauth_token_secret, profile, is_locked, is_admin');
    	$this->db->from('users');
		$this->db->join('users_twitter', 'users_twitter.id = users.twitter_id', 'left');
    	$this->db->where('users.username', $username);

    	if (!empty($access_key))
    	{
    		$this->db->join("users_accesskeys", "users.id=users_accesskeys.user_id");
    		$this->db->where('key', $access_key);
    		$this->db->where('task', "RememberMe");
    	}

    	$query = $this->db->get();

	    foreach ($query->result() as $row)
	    {
	    	$user_result['username'] = $row->username;
	    	$user_result['email'] = $row->email;
	    	$user_result['twitter'] = $row->twitter;
	    	$user_result['profile'] = $row->profile;
	    	$user_result['is_locked'] = $row->is_locked;
	    	$user_result['is_admin'] = $row->is_admin;
	    	$user_result['twitter_oauth_token'] = $row->oauth_token;
	    	$user_result['twitter_oauth_token_secret'] = $row->oauth_token_secret;
	    }

	    return $user_result;
    }
    
    public function setTwitterAccount($username, $oauth_token, $oauth_token_secret, $twitter_username)
    {
	    $this->db->from('users');
	    $this->db->join('users_twitter', 'users_twitter.id = users.twitter_id');
	    $this->db->where('users.username', $username);
	    $this->db->select('twitter_id, oauth_token, oauth_token_secret');
	    
	    $query = $this->db->get();
	    
	    if ($query->num_rows() > 0)
		{
			$prev = $query->row();
			
			if ($prev->twitter_id > 0 && ($prev->oauth_token != $oauth_token || $prev->oauth_token_secret != $oauth_token_secret))
			{
				$this->db->where('username', $username);
				$this->db->update('users', array('twitter_id' => null));
				
				$this->db->where('id', $twitterId);
				$this->db->delete('users_twitter');
			}
			else
			{
				return;
			}
		}
		
		$this->db->trans_start();
		
		$this->db->insert('users_twitter', array(
			'oauth_token' => $oauth_token,
			'oauth_token_secret' => $oauth_token_secret,
			'username' => $twitter_username
		));
		$twitterId = $this->db->insert_id();
		
		$this->db->where('username', $username);
		$this->db->update('users', array('twitter_id' => $twitterId));
		
		$this->db->trans_complete();
    }

    public function setAccessKey($username, $old_key = '')
    {
    	if (!empty($old_key))
    	{
    		$this->db->where('key', $old_key);
			$this->db->delete('users_accesskeys'); 
    	}

    	$new_access_key = $this->getGUID();

    	if ($new_access_key !== FALSE)
    	{
    		$userid = $this->getIdForUsername($username);

    		if ($userid > 0)
    		{
	    		$access_key_update = array (
	    			'user_id' => $userid,
	    			'key' => $new_access_key,
	    			'task' => 'RememberMe'
				);
	    		$this->db->insert('users_accesskeys', $access_key_update);
    		}
    	}

    	return "$username&$new_access_key";
    }

	public function verifyActivationKey($activation_key)
	{
		$this->db->from("users_accesskeys");
		$this->db->join("users", "users.id = users_accesskeys.user_id");
		$this->db->where('key', $activation_key);
		$this->db->where("task", "ActivationKey");
		
		$query = $this->db->get();
		
		if ($query->num_rows() > 0)
		{
			$userid = $query->row()->id;
			
			$this->db->where("id", $userid);
			$this->db->update("users", array("is_locked" => 0));
			
			$this->db->where('key', $activation_key);
			$this->db->delete("users_accesskeys");
			
			return true;
		}
		
		return false;
	}
	
	public function addResetPasswordKey($email)
	{
		$key = FALSE;
		
		$this->db->select("id");
		$this->db->from("users");
		$this->db->where("email", $email);
		$query = $this->db->get();
		
		if ($query->num_rows() == 1)
		{
			$key = $this->getGUID();
			$this->db->insert("users_accesskeys", 
				array(
					"user_id" => $query->row()->id,
					"task" => "ResetPassword",
					"key" => $key
				)
			);
		}
		
		return $key;
	}
	
	public function verifyResetPasswordKey($key)
	{
		$query = $this->getResetPasswordKeyDetails($key);
		
		if ($query->num_rows() == 1)
		{
			return TRUE;
		}		
		
		return FALSE;
	}
	
	public function changePasswordWithKey($key, $newPassword)
	{
		$query = $this->getResetPasswordKeyDetails($key);
		
		if ($query->num_rows() == 1)
		{
			//$2y$10$hdgdHiKt6pl3jfy/PvlE.eKRjY5jHx7YafBD1W4wCLR8LpFo4hSMu
		    $crypt_password = password_hash($newPassword, PASSWORD_DEFAULT);
		    $this->db->where("id", $query->row()->id);
		    $this->db->update("users", array("password" => $crypt_password));
		    
		    $this->db->where("key", $key);
		    $this->db->delete("users_accesskeys");
		    
		    return TRUE;
		}
		
		return FALSE;
	}
	
	private function getResetPasswordKeyDetails($key)
	{
		$this->db->select("id");
		$this->db->from("users");
		$this->db->join("users_accesskeys", "users.id = users_accesskeys.user_id");
		$this->db->where("key", $key);
		$this->db->where("task", "ResetPassword");
		
		return $this->db->get();		
	}

    private function getGUID()
    {
	    if (function_exists('com_create_guid'))
	    {
	        return com_create_guid();
	    }
	    else
	    {
	        mt_srand((double)microtime()*10000);//optional for php 4.2.0 and up.
	        $charid = strtoupper(md5(uniqid(rand(), true)));
	        $hyphen = chr(45);// "-"
	        $uuid = //chr(123)// "{"
	            substr($charid, 0, 8).$hyphen
	            .substr($charid, 8, 4).$hyphen
	            .substr($charid,12, 4).$hyphen
	            .substr($charid,16, 4).$hyphen
	            .substr($charid,20,12);
	            //.chr(125);// "}"

	        return $uuid;
	    }

	    return FALSE;
	}
}
?>