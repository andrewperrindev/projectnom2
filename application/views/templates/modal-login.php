<div class="modal fade bs-login-modal">
	<div class="modal-dialog">
		<div class="modal-content">
			<form name='loginForm' id='loginForm'>
				<div class="modal-header text-dark">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">Log In</h4>
    			</div>
		    	<div class="modal-body">
		    		<?php
		    			if (!empty($readonly) && $readonly)
		    			{
	    				?>
				    		<div class="row text-dark">
				    			<div class="col-sm-12">
									<div class="alert alert-warning" role="alert"><strong>Please login if you wish to save your changes.</strong></div>
				    			</div>
			    			</div>
		    			<?php
	    				}
    				?>
					<div class="row text-dark">
						<div class="col-sm-12">
							<div class="row">
								<div class="col-sm-6 col-sm-offset-3">
									<div class="form-group">
										<label for="username">User name</label>
										<input type="text" class="form-control" id="username" name="username" tabindex='10' required />
									</div>
									<div class="form-group">
										<label for="password">Password</label>
										<input type="password" class="form-control" id="password" name="password" tabindex='11' aria-describedby="helpBlock" required />
										<span id="helpBlock" class="help-block">
											<small><a href="/welcome/forgot">Forgot password?</a></small>
										</span>
									</div>
									<div class="checkbox <?php if (!empty($readonly) && $readonly) { ?>hidden<?php } ?>">
										<label>
											<input type="checkbox" id="rememberme" name="rememberme" tabindex='12'> Remember Me
											<input type="hidden" id="returnUrl" name="returnUrl" value="<?php if (!empty($returnUrl)) { print $returnUrl; } ?>">
										</label>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-8 col-sm-offset-2">
										<div id="loginResult"></div>
								</div>
							</div>
						</div>
					</div>
		    	</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary btn-login extra-padding" data-label='Log In'>Log In</button>
		    	</div>
			</form>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
