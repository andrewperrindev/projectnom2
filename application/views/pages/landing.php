	<!-- This isn't really necessary yet, but will be when React is implemented in full force -->
	<script>
		window.PN = window.PN || {};
		window.PN.messageJson = <?php print json_encode($message); ?>;
	</script>
	
    <!-- Intro Header -->
    <header class="intro">
        <div class="intro-body">
            <div class="container">
            </div>
        </div>
    </header>

	<!-- Message Section -->
    <section id="header" class="container content-section text-dark">
	    <?php 
		    if (empty($username))
		    {
			    ?>
			    <div class="row">
				    <div class="col-xs-12 col-md-offset-1 col-md-10 col-lg-offset-2 col-lg-8">
					    <div class="alert alert-success" role="alert">
						    <strong>Welcome to ProjectNom!</strong> <a href="/">We help cooks organize and share recipes.</a>
					    </div>
				    </div>
			    </div>
			    <?php
	    	}
	    ?>
	    <?php $this->load->view("templates/message-card"); ?>
    </section>