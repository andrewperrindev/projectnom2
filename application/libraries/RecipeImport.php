<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
//require_once(APPPATH . "/models/recipe_class.php");

class RecipeImport 
{
	private $CI;
	private $units;
	private $commaExceptions;

	public function __construct($params)
    {
    	$this->CI =& get_instance();
		$this->CI->load->library("ImageUtils");

    	if (empty($params['units']))
    	{
    		$params['units'] = array('cup','teaspoon','tablespoon','pound','ounce');
    	}

    	$this->units = $params['units'];
    	
    	$this->commaExceptions = array("boneless", "bone-in");
    }

	function parseIngredient($rawText)
	{
		if (!empty($rawText))
		{
			$unitslist = join("|", $this->units);
			$unitslist = str_replace(array(".","(",")","/"), array("\.","\(","\)","\/"), $unitslist);

			$fractions = html_entity_decode("&frac34;&frac12;&frac14;",  ENT_QUOTES, "UTF-8");
			$regexp = "/(([0-9\/.$fractions\-\–\s⅓⅛⅔]|to)+)?(\s+|^)((($unitslist)\s+)?)\s*([^,]*)(, (.*))*/";
			$quantityUnitRegex = "/^(([0-9\/.$fractions\-\–⅓⅛⅔]|\sto\s)+)([a-zA-Z]+)\s+/";

			$ingredient = array();
			$ingredient['name'] = $rawText;
			$ingredient['quantity'] = $ingredient['unit'] = $ingredient['preparation'] = "";

			if(rtrim(strrchr($rawText, ":")) !== ":")
			{
				$rawText = preg_replace($quantityUnitRegex, "$1 $3 ", $rawText);
				
				preg_match($regexp, $rawText, $matches);
				if (!empty($matches[1])) $ingredient['quantity'] = trim($matches[1]);
				if (!empty($matches[4])) $ingredient['unit'] = trim($matches[4]);
				
				$prep_suffix = "";
				
				// Use RegEx results UNLESS
				// [name] in special cases OR
				// [name] contains '('
				if (!empty($matches[7]) && !empty($ingredient['unit']) && strpos($matches[7], "(") === 0)
				{
					$paren = strpos($matches[7], ")");
					$prep_suffix = trim(substr($matches[7], 0, $paren+1));
					$matches[7] = trim(substr($matches[7], $paren+1));
				}
				
				if (!empty($matches[7]) && $this->stringStartsWithException($matches[7]))
				{
					if (!empty($matches[8]))
					{
						$fullName = $matches[7] . $matches[8];
						$secondComma = stripos($fullName, ",", strlen($matches[7]) + 1);
						
						if ($secondComma !== FALSE)
						{
							$ingredient["name"] = trim(substr($fullName, 0, $secondComma));
							$ingredient["preparation"] = trim(substr($fullName, $secondComma + 1));
						}
						else
						{
							$ingredient["name"] = $fullName;
						}
					}
				}
				// We actually do mean 0 here because we don't want this condition to
				// fire if the parenthesis is the first character.
				else if (!empty($matches[7]) && strpos($matches[7], "(or") === FALSE && strpos($matches[7], "(") > 0)
				{
					$paren = strpos($matches[7], "(");
					$ingredient["name"] = trim(substr($matches[7], 0, $paren));
					$ingredient["preparation"] = trim(substr($matches[7], $paren));
					
					if (!empty($matches[8]))
					{
						$ingredient["preparation"] .= $matches[8];
					}
				}
				else
				{
					if (!empty($matches[7])) $ingredient['name'] = trim($matches[7]);
					if (!empty($matches[9])) $ingredient['preparation'] = trim($matches[9]);
				}
				
				if (!empty($prep_suffix)) 
				{
					if (!empty($ingredient['preparation'])) $ingredient['preparation'] .= " ";
					$ingredient['preparation'] .= $prep_suffix;
				}
			}

			return $ingredient;
		}
	}

	function parseImage($imageUrl, $fileNameHash)
	{
		$imageUrlResult = "";

		if ($imageUrl != "")
		{
			$parsed_url = parse_url($imageUrl);

			if ($parsed_url === FALSE || empty($parsed_url['scheme']) || ($parsed_url['scheme'] != 'http' && $parsed_url['scheme'] != 'https'))
			{
				$temp_img = realpath(dirname(__FILE__)) . '/../../temp/' . $imageUrl;
				$actual_img = realpath(dirname(__FILE__)) . '/../../recipecache/' . $imageUrl;
			}
			else
			{
				$temp_img = $imageUrl;
				
				$qpos = strpos($temp_img, "?"); 
				if ($qpos !== FALSE)
				{
					$temp_img = substr($temp_img, 0, $qpos);
				}
  
				$imgpathinfo = pathinfo($temp_img);
				$extension = !empty($imgpathinfo['extension']) ? $imgpathinfo['extension'] : "jpg";
				$actual_img = realpath(dirname(__FILE__)) . "/../../recipecache/$fileNameHash" . time()
					. ".$extension";
			}

			if ( copy($temp_img, $actual_img) )
			{
				$imageUrlResult = "recipecache/" . basename($actual_img);
			}

			if ($imageUrlResult != "")
			{
				$image_thumbnail = $actual_img . ".thumb";
				if (!file_exists($image_thumbnail))
				{
					$image_thumbnail = $this->CI->imageutils->resizeImage($actual_img, 300, 200, $image_thumbnail);
				}
			}
		}

		return $imageUrlResult;
	}
	
	private function stringStartsWithException($haystack)
	{
		foreach ($this->commaExceptions as $word)
		{
			if (stripos($haystack, $word) === 0)
			{
				return true;
			}
		}
		
		return false;
	}
}
?>