<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class PNUpload
{
	private $CI;
	private $username;
	
	public $config;

	public function __construct($params)
    {
    	$this->CI =& get_instance();
		$this->CI->load->library("ImageUtils");
    	$this->username = $params['username'];
    	
    	$this->config['upload_path'] = realpath(dirname(__FILE__)) . '/../../temp/';
		$this->config['allowed_types'] = 'gif|jpg|jpeg|png';
		$this->config['max_size']	= '4096';
		$this->config['file_name'] = md5($this->username) . time();
	}
	
	public function do_upload()
	{
		// Cleanup old temp files
		$path_pattern = $this->config['upload_path'] . md5($this->username) . '*';
		array_map('unlink', glob($path_pattern));

    	$this->CI->load->library('upload', $this->config);

		if ($this->CI->upload->do_upload())
		{
			$upload_info = $this->CI->upload->data();
			
			if ($upload_info['file_size'] > 1 || $upload_info['image_height'] > 600)
			{
				$resized = $this->CI->imageutils->resizeImage($upload_info['full_path'], $upload_info['image_width'], 600, $upload_info['full_path']);
			}
			
			return TRUE;
		}
		
		return FALSE;
	}
	
	public function display_errors()
	{
		return $this->CI->upload->display_errors();
	}
	
	public function data()
	{
		return $this->CI->upload->data();
	}
}
?>