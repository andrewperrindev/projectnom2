var ShoppingList =
{
	init: (function() {
		return function()
		{
			ShoppingList.timer = setInterval(ShoppingList.save, 5000);
		};
	})(),
	save: (function(){
		return function() {
			if (ShoppingList.hasChange)
			{
				ShoppingList.hasChange = false;
				saveShoppingList();
			}
		};
	})(),
	timer: null,
	hasChange: false
};

$(function() {
	initPage_base();
	initPage();
});

function initPage()
{
    $( "div.shopping-list" ).sortable({
		items: "> div.ingredient-list-item",
		handle: ".handle",
		containment: $("section#shoppingList"),
		axis: "y",
		//placeholder: "sortable-placeholder",
		start: function(e, ui){
			ui.placeholder.height(ui.item.height());
		},
		stop: function(e, ui) { setHasChange(); }
	});

	$("div.shopping-list").on("change", "input", setHasChange);

	$("button.reset").on("click", function(event)
	{
		buttonWait(this, true);

		clearShoppingList()
			.done(function(data)
			{
				if (data.success == 1)
				{
					window.location.href = "/shoppinglist";
				}
				else
				{
					buttonWait(this, false);
				}
			});
	});

    ajaxGet("/api/1/shoppinglist", null)
		.done(loadShoppingList);

	ShoppingList.init();
}

function setHasChange()
{
	ShoppingList.hasChange = true;

	if ($(this).attr("type") == "checkbox")
	{
		var seq = $(this).val();
		if ($(this).prop("checked"))
		{
			$("input[name=ingredient-name" + seq + "]").addClass("done");
		}
		else
		{
			$("input[name=ingredient-name" + seq + "]").removeClass("done");
		}
	}
}

function loadShoppingList(data)
{
	if (data.success == 1)
	{
		for (var i = 0; i < data.result.length; i++)
		{
			// shopping list
			$list_item = $("div.row.ingredient-list-item.hidden").clone().removeClass("hidden");
			$("input.add-ingredient", $list_item).val(i);
			$("input.ingredient-name", $list_item).attr("name", "ingredient-name" + i);
			$("input.ingredient-name", $list_item).val(data.result[i]);
			$("div.shopping-list").append($list_item);
		}

		$("div.wait-spinner div.wait").remove();

		if (data.result.length === 0)
		{
			$("div.wait-spinner .alert")
				.html("<strong>Your shopping list is currently empty.</strong> Visit a recipe to add its ingredients.")
				.removeClass("invisible");
		}
		else
		{
			$("div.wait-spinner").remove();
			$("section#shoppingList .footer").removeClass("invisible");
			$("div.shopping-list").height($("div.shopping-list").height());
		}
	}
}

function saveShoppingList(data)
{
	var response = clearShoppingList();

	response.done(function(data)
	{
		if (data.success == 1)
		{
			var submitUrl = "/api/1/shoppinglist";
			var result = [];
			$("div.shopping-list .ingredient-list-item").each(function(index)
			{
				if (!$(".add-ingredient", this).prop("checked") && $(".ingredient-name", this) !== null)
				{
					result.push($(".ingredient-name", this).val());
				}
			});

		    ajaxPost(submitUrl, { shoppinglist: JSON.stringify(result) })
				.done(function(data) {


				});
	    }
	});
}

function clearShoppingList()
{
	return ajaxDelete('/api/1/shoppinglist', null);
}