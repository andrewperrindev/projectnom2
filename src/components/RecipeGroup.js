var React = require('react');
var Loader = require('react-loader');

var RecipeSummary = require('./RecipeSummary.js');

module.exports = React.createClass({
	componentWillUpdate: function(nextProps, nextState) {
		$(this.refs.groupRow).height($(this.refs.groupRow).height());
	},

	componentDidUpdate: function(prevProps, prevState) {
		if (this.props.recipes.length > 0) {
			$(this.refs.groupRow).css("height", "auto");
		}
	},
	
	render: function() {
		var recipes = this.props.recipes.map(function addSummaries(r) {
			return React.DOM.div({className: "col-sm-6 col-md-3", key: r.id}, React.createElement(RecipeSummary, {recipe: r}));
		});
		// TODO: Ideally this should be inserted at every multiple of two in order to support an arbitary number of items.
		recipes.splice(2, 0, React.DOM.div({className: "clearfix visible-sm-block", key: "clearfix-sm"}));
		var loadWrapper = React.createElement(Loader, {loaded: this.props.loaded}, recipes);
		
		return React.DOM.div({className: "row", ref: "groupRow"}, loadWrapper);
	}
});