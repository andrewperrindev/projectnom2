<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Bonappetit
{
	private $CI;
	private $content;

	public function __construct($params)
    {
    	$this->CI =& get_instance();

    	if (!empty($params['content']))
    	{
    		$this->content = $params['content'];
    	}
    }

    public function getTitle()
    {
		return $this->returnMatch("/<div class=\"recipe__title__hed\" data-reactid=\"[^\"]+\">(.+)<\/div>/imsU", $this->content);
    }

    public function getAuthor()
    {
		return $this->returnMatch("/<span class=\"recipe__credits\" data-reactid=\"[^\"]+\">Recipe by\s+<\/span><span data-reactid=\"[^\"]+\">(.+)<\/span>/imsU", $this->content);
    }

    public function getTotalTime()
    {
		return $this->returnMatch("/<span class=\"recipe__header__times\" itemprop=\"totalTime\" data-reactid=\"[^\"]+\">total:\s+(.+)<\/span>/imsU", $this->content);
    }

    public function getPrepTime()
    {
		return $this->returnMatch("/<span class=\"recipe__header__times\" itemprop=\"activeTime\" data-reactid=\"[^\"]+\">active:\s+(.+)<\/span>/imsU", $this->content);
    }

    public function getServings()
    {
		return $this->returnMatch("/<span itemprop=\"recipeYield\" class=\"recipe__header__servings\" data-reactid=\"[^\"]+\">(.+)<\/span>/imsU", $this->content);
    }

    public function getImage()
    {
    	return $this->returnMatch("/,\"image\"\s*:\s*\"([^\"]+)\"/", $this->content);
    }

    public function getIngredients()
    {
    	$this->CI->load->helper("html2text");

    	if (!empty($this->content))
    	{
    		preg_match("/<ul class=\"ingredients__group\" data-reactid=\"[^\"]+\">(.+)<\/ul>/imsU",
    			$this->content, $matches);
    		//return print_r($matches, true);

    		if (!empty($matches[1]))
    		{
    			$result = array();
    			preg_match_all("/<li class=\"ingredient\" itemtype=\"[^\"]+\" itemprop=\"ingredients\" data-reactid=\"[^\"]+\">(.+)<\/li>/imsU", trim($matches[1]), $ingredients);
				//return print_r($ingredients, true);
				
    			for ($i = 0; $i < count($ingredients[1]); $i++)
    			{
	    			//$ingredient = $ingredients[1][$i] . " " . $ingredients[2][$i] . " " . $ingredients[3][$i];
	    			$ingredient = $ingredients[1][$i];
    				$result[] = convert_html_to_text($ingredient);
    			}
    			return $result;
    		}
    	}
    }

    public function getDirections()
    {
        $this->CI->load->helper("html2text");
        $result = array();

        if (!empty($this->content))
        {
            preg_match("/<div class=\"preparation__group\" data-reactid=\"[^\"]+\">(.+)<\/div>/imsU", $this->content, $matches);
            //return print_r($matches, true);

            if (!empty($matches[1]))
            {
                preg_match_all("/<p class=\"preparation__step\" data-reactid=\"[^\"]+\">(.+)<\/p>/imsU", $matches[1], $directions);

                foreach ($directions[1] as $direction)
                {
                    $result[] = convert_html_to_text($direction);
                }
            }
        }

        return $result;
    }

    private function returnMatch($pattern, $content)
    {
    	if (!empty($content))
    	{
			preg_match($pattern, $content, $matches);

			if (!empty($matches[1]))
			{
				return trim($matches[1]);
			}
		}

		return "";
    }
}
?>