<!-- New Message Modal -->
<div class="modal fade bs-message-modal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header text-dark">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Share Recipe</h4>
			</div>
	    	<div class="modal-body text-dark">
		    	<div class="message-error hidden">
					<div class="row">
						<div class="col-sm-12 text-dark">
							<div class="alert alert-danger" role="alert"></div>
            			</div>
        			</div>
				</div>
		    	<div class="message-success hidden">
					<div class="row">
						<div class="col-sm-12 text-dark">
							<div class="alert alert-success" role="alert"></div>
            			</div>
        			</div>
				</div>
		    	<div class="message-recipe">
					<div class="row">
						<div class="col-sm-12 text-dark">
							<div class="alert alert-info" role="alert">
								<i class="fa fa-paperclip"></i>&nbsp;&nbsp; <span class='message-recipe-title'></span>
							</div>
            			</div>
        			</div>
				</div>
				<div class="row row-error hidden">
					<div class='col-sm-12'>
						<div class="alert alert-danger" role="alert">
							<span style="padding-right: 5px;"><i class='fa fa-exclamation-triangle'></i></span>
							<span class='error-text'></span>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<div class="file-upload dropzone" id="fileUpload">
							<div class='dz-message'>
								<p class="custom-label text-center">
									<i class='fa fa-upload'></i>&nbsp;&nbsp;Attach Image <small>(drag file here or click to choose)</small>
								</p>
							</div>
						</div>
						<div class="fallback hidden" id="fallback"></div>
					</div>
				</div>
				<div class="upload-preview dropzone-previews">
				</div>
				<div class="row">
					<div class="col-sm-8 col-sm-offset-2">
							<div id="uploadResult"></div>
					</div>
				</div>
				<div class="row friend-list hidden">
					<div class="col-sm-12">
						<div class='form-group'>
							<label class='sr-only' for='friends'>Friend List</label>
							<select class='form-control friends' id='friends'></select>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12">
			            <div class='form-group message-container'>
			            	<label class='sr-only' for='new-message'>New Message</label>
							<textarea class="form-control new-message" id='new-message' placeholder="Message" rows='3' aria-describedby="helpBlock"></textarea>
							<span id="helpBlock" class="help-block">
								Enter a message you'd like to post along with this recipe. &nbsp;
								<span class="label label-default char-count">255</span> characters left.
							</span>
			            </div>
					</div>
				</div>
				<?php
					if (!empty($twitter))
					{
						?>
						<div class="row tweet-option hidden">
							<div class="col-sm-12">
								<div class="checkbox">
									<label>
										<input type="checkbox" id="twitter" name="twitter" checked="checked">&nbsp;<i class="fa fa-twitter"></i> Tweet this to @<?php print $twitter; ?>
									</label>
								</div>
							</div>
						</div>
						<?php
					}
				?>
	    	</div>
			<div class="modal-footer">
				<input type='hidden' id='upload-file-name'>
				<button type="button" class="btn btn-default btn-cancel" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-primary btn-confirm add-message" data-label='View My Messages'>Post Message</button>
	    	</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class='upload-preview-template hidden'>
	<div id="preview-template" class="file-row">
		<div class="row">
			<div class='col-sm-12'>
				<div class="alert alert-info">
					<div class="row">
						<div class="col-xs-9 col-sm-4">
							<i class="fa fa-paperclip"></i>&nbsp;
							<img data-dz-thumbnail />
						</div>
						<div class="hidden-xs col-sm-5">
							<h4 class="text-overflow" style="text-overflow: ellipsis"><span class="name" data-dz-name></span> <p class="size small" data-dz-size></p></h4>
						</div>
						<div class="col-xs-3">
							<button data-dz-remove class="btn btn-danger delete">
								<i class="fa fa-trash-o"></i>
							</button>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class='col-sm-12'>
				<div id="total-progress" class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
					<div class="progress-bar progress-bar-success" style="width:0%;" data-dz-uploadprogress>
						<i class="fa fa-check hidden"></i>
						<i class="fa fa-times hidden"></i>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>