<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Blueapron
{
	private $CI;
	private $content;

	public function __construct($params)
    {
    	$this->CI =& get_instance();

    	if (!empty($params['content']))
    	{
    		$this->content = $params['content'];
    	}
    }

    public function getTitle()
    {
	    $main_title = $this->returnMatch("/<h1 class=\'main-title\'>\s+(.+)\s+<\/h1>/imsU", $this->content);
	    $sub_title = $this->returnMatch("/<h2 class=\'sub-title\'>\s+(.+)\s+<\/h2>/imsU", $this->content);
		return "$main_title $sub_title";
    }

    public function getAuthor()
    {
		return "Blue Apron";
    }

    public function getTotalTime()
    {
		$cookTime = $this->returnMatch("/<div class=\'.+\'>\s+<div class=\'field-name\'>Cook Time<\/div>\s+(.+)\s+<\/div>/imsU", $this->content);
		return str_replace(array("\n","<span>","</span>"), "", $cookTime);
    }

    public function getPrepTime()
    {
		return;
    }

    public function getServings()
    {
		return $this->returnMatch("/<span itemprop=\'recipeYield\'>(.+)<\/span>/", $this->content);
    }

    public function getImage()
    {
    	$imgUrl = $this->returnMatch("/<img class=\"rec-splash-img\" alt=\".+\" src=\"(.+)\" \/>/", $this->content);
    	
    	if (!empty($imgUrl))
    	{
	    	if (substr($imgUrl, 0, 2) == "//")
	    	{
		    	$imgUrl = "https:$imgUrl";
	    	}
	    	if ($imgUrl[0] == "/")
	    	{
		    	$imgUrl = "https://media.blueapron.com$imgUrl";
		    }

			return $imgUrl;
    	}
    	
    	return "";
    }

    public function getIngredients()
    {
    	$this->CI->load->helper("html2text");

    	if (!empty($this->content))
    	{
    		preg_match("/<ul class=\'ingredients-list\'>(.+)<\/ul>/imsU",
    			$this->content, $matches);
    		//return print_r($matches, true);

    		if (!empty($matches[1]))
    		{
    			$result = array();
    			preg_match_all("/<li itemprop=\'ingredients\'>(.+)<\/li>/imsU", trim($matches[1]), $ingredients);
				//return print_r($ingredients, true);
				
    			for ($i = 0; $i < count($ingredients[1]); $i++)
    			{
	    			$ingredient = $ingredients[1][$i];
    				$result[] = strtolower(convert_html_to_text($ingredient));
    			}
    			return $result;
    		}
    	}
    }

    public function getDirections()
    {
        $this->CI->load->helper("html2text");
        $result = array();

        if (!empty($this->content))
        {
            preg_match("/<section class=\'section-rec-instructions container\' id=\'instructions\'>(.+)<\/section>/imsU", $this->content, $matches);
            //return print_r($matches, true);

            if (!empty($matches[1]))
            {
                preg_match_all("/<div class=\'instr-txt\'>(.+)<\/div>/imsU", $matches[1], $directions);

                foreach ($directions[1] as $direction)
                {
                    $result[] = convert_html_to_text($direction);
                }
            }
        }

        return $result;
    }

    private function returnMatch($pattern, $content)
    {
    	if (!empty($content))
    	{
			preg_match($pattern, $content, $matches);

			if (!empty($matches[1]))
			{
				return trim($matches[1]);
			}
		}

		return "";
    }
}
?>