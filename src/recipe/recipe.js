var pnRecipe = (function($) {
	var React = require('react');
	var ReactDOM = require('react-dom');

	// Custom React Component
	var BootstrapError = require("../components/BootstrapError.js");
	
	var messageUtils = require("../friends/messageutils.js")({initDropzone: initDropzone});

	var publicApi = {
		init: initPage
	};

	return publicApi;

	// ---------------------

	function initDropzone()
	{
		this
		.on("addedfile", function(file) 
			{
				$('.btn-confirm').prop('disabled', true);
			}
		)
		.on("maxfilesreached", function(file) 
			{
				$(".dropzone").addClass("hidden");
			}
		)
		.on("removedfile", function(file) 
			{
				$(".dropzone").removeClass("hidden");
				$('input#upload-file-name').val("");
				$("#total-progress i.fa-check").addClass("hidden");
				resetUploadError();
			}
		)
		.on("success", function(file, response)
			{
				resetUploadError();
				var result = JSON.parse(response);
	
				if (result.success == 1)
				{
					$('input#upload-file-name').val(result.result);
					$("#total-progress").removeClass("progress-striped active");
					$("#total-progress i.fa-check").removeClass("hidden");
				}
				else
				{
					$('div.row-error span.error-text').html(result.error);
					showUploadError();
				}
			}
		)
		.on("error", function(file, errorMessage)
			{
				this.removeAllFiles();
				$('div.row-error span.error-text').html(errorMessage);
				showUploadError();
			}
		)
		.on("queuecomplete", function(progress) 
			{
				$('.btn-confirm').prop('disabled', false);
			}
		);
	}

	function showUploadError()
	{
		$('div.row-error').removeClass('hidden');
		$("#total-progress").removeClass("progress-striped active");
		$("#total-progress i.fa-times").removeClass("hidden");
		$('div.progress-bar')
			.removeClass('progress-bar-success')
			.addClass('progress-bar-danger');
	}
	
	function resetUploadError()
	{
		$('div.row-error').addClass('hidden');
		$("#total-progress").addClass("progress-striped active");
		$("#total-progress i.fa-times").addClass("hidden");
		$('div.progress-bar')
			.addClass('progress-bar-success')
			.removeClass('progress-bar-danger');
	}

	function loadRecipe(data)
	{
		if (data.success === 0)
		{
			var $recipeAlert = $("section#header > div.error");
			var alertText = "<strong>It looks like you don't have access to this recipe!</strong>";
			
			if (data.origin_url)
			{
				alertText += " But don't worry, <a href='" + data.origin_url + "'>you can probably view the original here</a>.";
			}
	
			$("div.alert", $recipeAlert).html(alertText);
			$recipeAlert.removeClass('hidden');
		}
		else
		{
			var recipeData = data.recipes[0];
			document.title = recipeData.title + " :: " + document.title;
			
			if (recipeData.title.split(' ').length >= 9) 
			{
				$("h1.brand-heading")
					.addClass("brand-heading-small");
					//.removeClass("brand-heading");
			}

			$("span#recipe-title").html(recipeData.title);

			if (recipeData.author)
			{
				$("h1.brand-heading p.recipe-author small")
					.text("by " + recipeData.author);

				$("h1.brand-heading p.recipe-author")
					.removeClass("hidden");
			}
	
			var $recipeHeader = $("section#header > div.recipe-header").detach();
			$recipeHeader.removeClass('invisible');
			//$recipeHeader.empty();
	
			if (recipeData.image_url !== null && recipeData.image_url !== "")
			{
				$(".recipe-img > img.img-thumbnail",$recipeHeader).attr("src", "/" + recipeData.image_url);
			}
			else
			{
				$(".recipe-img",$recipeHeader).remove();
				$(".recipe-intro > div", $recipeHeader)
					.removeClass("col-xs-12 col-md-5")
					.addClass("col-md-6 col-md-offset-3");
			}
	
			if (recipeData.comment && data.readonly != 1)
			{
				$(".recipe-comment",$recipeHeader)
					.html(recipeData.comment.replace(/\r\n|\n/g, "<br>"))
					.removeClass("hidden");
				recipeData.stats = true;
			}
			
			if (recipeData.servings)
			{
				$("div.servings-metadata", $recipeHeader).removeClass("hidden");
				$("div.servings-metadata span.servings", $recipeHeader).text(recipeData.servings);
				recipeData.stats = true;
			}
			if (recipeData.total_time || recipeData.prep_time)
			{
				var time_details = "";
				recipeData.stats = true;

				$("div.time-metadata", $recipeHeader).removeClass("hidden");
				
				if (recipeData.total_time)
				{
					time_details += " Total: " + recipeData.total_time;
				}
				if (recipeData.prep_time)
				{
					if (recipeData.total_time) time_details += "<br>";
					time_details += " Prep: " + recipeData.prep_time;
				}
	
				$("div.time-metadata div.time-text", $recipeHeader).html(time_details);
			}
			
			if (!recipeData.stats)
			{
				$("div.recipe-stats", $recipeHeader).remove();
				$("div.recipe-tags", $recipeHeader)
					.addClass("col-sm-offset-4");
			}

			if (data.readonly != 1)
			{
				setRatingDropdown(recipeData.rating, $('button#dropdownRating > span.button-text', $recipeHeader));
				setCuisineDropdown(recipeData.cuisine, $('button#dropdownCuisine > span.button-text', $recipeHeader));
				setTypeDropdown(recipeData.type, $('button#dropdownTypes > span.button-text', $recipeHeader));
			}
			else
			{
				if (details !== "")
				{
					$("div.recipe-user-metadata", $recipeHeader).remove();
				}
				else
				{
					$("div.panel", $recipeHeader).remove();
				}
			}
	
			$recipeHeader.appendTo("section#header");
	
			var $ingredientSection = $("div.ingredient-section .ingredient-view").detach();
			for (var i = 0; i < recipeData.ingredients.length; i++) 
			{ 
				var ingredient = recipeData.ingredients[i];
				var ingredientText = "";
				var $ingredientRow = $("div.ingredient-row.hidden").clone();
				$ingredientRow.removeClass("hidden");
	
				if (ingredient.id > 0)
				{
					if (ingredient.quantity !== "")
					{
						ingredientText += ingredient.quantity + " ";
					}
					if (ingredient.unit !== "")
					{
						ingredientText += ingredient.unit + " ";
					}
					if (ingredient.name !== "")
					{
						ingredientText += ingredient.name;
					}
					if (ingredient.preparation !== "")
					{
						if (ingredient.preparation[0] == "(")
						{
							spacer = " ";
						}
						else
						{
							spacer = ", ";
						}
						ingredientText += spacer + ingredient.preparation;
					}
				
					$(".text", $ingredientRow).html(ingredientText);
	
					// shopping list
					var shoppingItem = $.trim(ingredient.quantity + " " + ingredient.unit + " " + ingredient.name);
					$list_item = $("div.row.ingredient-list-item.hidden").clone().removeClass("hidden");
					$("input.add-ingredient", $list_item).val(ingredient.id);
					$("input.ingredient-name", $list_item).attr("name", "ingredient-name" + ingredient.id);
					$("input.ingredient-name", $list_item).val(shoppingItem);
					$("span.ingredient-name-label", $list_item).html(shoppingItem);
					$(".bs-shopping-modal form#shopping-list").append($list_item);
				}
				else
				{
					$(".text", $ingredientRow)
						.removeClass("text")
						.addClass("heading")
						.html(ingredient.name);
				}
	
				$ingredientSection.append($ingredientRow);
			}
	
			$("div.ingredient-section").append($ingredientSection);
	
			var $directionSection = $("div.direction-section .direction-view").detach();
			for (i = 0; i < recipeData.directions.length; i++)
			{
				var direction = recipeData.directions[i].replace(/^([^:\.!\?]+):/, "<strong>$&</strong>");
				var $directionRow = $("div.direction-row.hidden").clone();
				$directionRow.removeClass("hidden");
	
				$("h1", $directionRow).text(i+1);
				$(".recipe-direction", $directionRow).html(direction);
	
				$directionSection.append($directionRow);
			}
	
			$("div.direction-section").append($directionSection);
	
			if (recipeData.origin_url && recipeData.origin_url !== "")
			{
				$("li.original-link a").attr("href", recipeData.origin_url);
				$("li.original-link").removeClass("hidden");
			}
			if (data.readonly != 1)
			{
				// load friends
				ajaxGet('/api/1/friends', null).done(messageLoadFriends);
				$(".bs-message-modal span.message-recipe-title").html(recipeData.title);
				$("textarea.new-message").on("change keyup paste", messageUtils.charCount);
				$("ul.social a").on("click", showMessageModal);
				$(".bs-message-modal button.add-message").on("click", addMessage);
				$("section#recipe-footer div.hidden").removeClass("hidden");
			}
	
			$("div.recipe-user-metadata a").on("click", recipeMetadataSelect);
		}
	}

	function loadPhotos(data)
	{
		if (data.success == 1 && data.result.length > 0)
		{
			var photoResult = [];
			var $photoTemplate = $("section#recipe-photos div.photo-template")
				.detach()
				.removeClass("photo-template hidden")
				.addClass("photo-img");
			
			$.each(data.result, function addPhoto(index, msg) {
				var $photo = $photoTemplate.clone();
				$("img", $photo).attr("src", "/" + msg.image + "/small");
				photoResult.push($photo);
			});
			
			$("section#recipe-photos div.photos").append(photoResult);
			$("section#recipe-photos").removeClass("hidden");
		}
	}

	function recipeMetadataSelect(itemEvent)
	{
		/* Disabling jshint error; variable definitions are in separate if blocks. */
		/* jshint shadow: true */
		itemEvent.preventDefault();
		$selectedItem = $(this);
		$parentButton = $selectedItem.closest("div.dropdown");
		$parentButtonLabel = $("span.button-text", $parentButton);
		var selectedValue = $selectedItem.data("value");
	
		if ($("button", $parentButton).attr('id') == 'dropdownRating')
		{
			if (selectedValue != $parentButtonLabel.attr("id"))
			{
				var recipeId = $("#recipe-id").val();
	
				var post_data = 
					{
						rating: selectedValue
					};
	
				ajaxPost("/api/1/recipes/rating/" + recipeId, post_data);
				/*
					.done(function(data) // success callback
					{ 
					}
				*/
	
				setRatingDropdown(selectedValue, $parentButtonLabel);
			}
		}
		else if ($("button", $parentButton).attr('id') == 'dropdownCuisine')
		{
			if (selectedValue != $parentButtonLabel.attr("id"))
			{
				var recipeId = $("#recipe-id").val();
	
				var post_data = 
					{
						cuisine: selectedValue
					};
	
				ajaxPost("/api/1/recipes/cuisine/" + recipeId, post_data);
				/*
					.done(function(data) // success callback
					{ 
					}
				*/
	
				setCuisineDropdown(selectedValue, $parentButtonLabel);
			}
		}
		else if ($("button", $parentButton).attr('id') == 'dropdownTypes')
		{
			if (selectedValue != $parentButtonLabel.attr("id"))
			{
				var recipeId = $("#recipe-id").val();
	
				var post_data = 
					{
						type: selectedValue
					};
	
				ajaxPost("/api/1/recipes/type/" + recipeId, post_data);
				/*
					.done(function(data) // success callback
					{ 
					}
				*/
	
				setTypeDropdown(selectedValue, $parentButtonLabel);
			}
		}
	}

	function modalShoppingHandler()
	{
		buttonWait(this, true);
	
		if ($(this).hasClass("btn-success"))
		{
			window.location.href = "/shoppinglist";
			return;
		}
	
		var submitUrl = $("form#shopping-list").attr("action");
	
		var result = [];
		$("div.ingredient-list-item").each(function(index)
		{
			if ($(".add-ingredient", this).prop("checked"))
			{
				result.push($(".ingredient-name", this).val());
			}
		});
	
	    ajaxPost(submitUrl, { shoppinglist: JSON.stringify(result) })
			.done(function(data) {
				listButton = $(".bs-shopping-modal .btn-confirm");
				buttonWait(listButton, false);
				$(listButton)
					.removeClass('btn-primary')
					.addClass('btn-success');
			});	
	}

	function messageLoadFriends(data)
	{
		if (data.success == 1 && data.result.length > 0)
		{
			$.each(data.result, function(index, value)
			{
				$("select.friends").append("<option value='" + value.username + "'>" + value.username + "</option>");
			});
			
			$("li.message-friend-menu-item.hidden").removeClass("hidden");
		}
	}
	
	function clearMessageAlerts()
	{
		$("div.message-error").addClass("hidden");
		$("div.message-success").addClass("hidden");
	}

	function showMessageModal(event)
	{
		event.preventDefault();
		clearMessageAlerts();
		
		if($(this).hasClass("send-message-friend"))
		{
			$("div.friend-list").removeClass("hidden");
			$("div.tweet-option").addClass("hidden");
			$("textarea.new-message").data("type", "direct");
		}
		else
		{
			$("div.friend-list").addClass("hidden");
			$("div.tweet-option").removeClass("hidden");
			$("textarea.new-message").data("type", "post");
		}
		
		$(".bs-message-modal").modal('show');
	}

	function addMessage(event)
	{
		event.preventDefault();
		clearMessageAlerts();

		var $messageInput = $("textarea.new-message");
		$messageInput.prop("disabled", true);
		$(".btn.add-message").prop("disabled", true);
		
		var messageData = {
			message: $messageInput.val(),
			type: $messageInput.data("type"),
			recipeid: $("input#recipe-id").val(),
			image: $("input#upload-file-name").val()
		};

		if ($("div.friend-list").not(".hidden").length)
		{
			messageData.to_user = $("div.friend-list select.friends").val();
		}
		
		if ($("div.tweet-option").not(".hidden").length)
		{
			messageData.tweet = $("#twitter").prop("checked");
		}
	
		messageUtils.postMessage(messageData)
			.done(function(data)
			{
				if (data.success == 1)
				{
					$messageInput.val("");
					$messageInput.trigger("paste");
					Dropzone.forElement("div.file-upload").removeAllFiles();

					if (data.result && data.result.twitter_error) {
						$("div.message-error .alert")
							.html("<strong>There was a problem posting your message to Twitter.</strong> Your message was still posted to ProjectNom.");
						$("div.message-error")
							.removeClass("hidden");						
					}
					else {
						$("div.message-success .alert").html("<i class='fa fa-check'></i> Message posted successfully.");
						$("div.message-success").removeClass("hidden");
					}
				}
			})
			.fail(function(error)
			{
				$("div.message-error .alert")
					.html(error);
				$("div.message-error")
					.removeClass("hidden");
			})
			.always(function(data) 
			{
				$messageInput.prop("disabled", false);
				$(".btn.add-message").prop("disabled", false);
			});
	}

	function initPage()
	{
		var recipeId = $("#recipe-id").val();
	
	    ajaxGet("/api/1/recipes/" + recipeId, null)
			.done(loadRecipe)
			.always(function()
			{
				$("div.wait-spinner").remove();
			});
		
		if (window.innerWidth > 768)
		{
			ajaxGet("/api/1/recipes/" + recipeId + "/photos", null)
				.done(loadPhotos);
		}

		//dropzoneConfig.url += recipeId;
		Dropzone.options.fileUpload = messageUtils.dropzoneConfig;

		$(".bs-shopping-modal .btn-confirm").on("click", modalShoppingHandler);
	}
	
})(jQuery);

$(function() {
	initPage_base();
	pnRecipe.init();
});