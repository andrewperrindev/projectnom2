<?php
require_once(APPPATH . '/controllers/test/ProjectNom_Toast.php');

class Recipe_tests extends ProjectNom_Toast
{

	function __construct()
	{
		parent::__construct(__FILE__); // Remember this
	}

	function test_session_failure()
	{
 		$curlResult = $this->doCurlGet("/api/1/recipes/index");
		$this->message = $curlResult['body'] . "<BR>" . $curlResult['headers'][0]['http_code'];
		$api_result = json_decode($curlResult['body']);

		$this->_assert_equals($api_result->success, 0);
		$this->_assert_true(stripos($curlResult['headers'][0]['http_code'], "401 Unauthorized"));
	}

	function test_session_rememberme_success()
	{	
		$this->establishReadOnlySession();
		$this->message = $this->session_cookie_readonly;
		
		$curlResult = $this->doCurlGet("/recipe/edit/1", $this->session_cookie_readonly);
		//$this->message .= "<BR>" . print_r($curlResult, true);
		$this->_assert_true(stripos($curlResult['body'], "bs-login-modal") !== FALSE);
	}

	function test_recipe_success()
	{
		$this->establishSession();

 		$curlResult = $this->doCurlGet("/api/1/recipes/215");
		$this->message = $curlResult['body'];
		$api_result = json_decode($curlResult['body']);

		$this->_assert_equals($api_result->success, 1);
		$this->_assert_equals($api_result->total, 1);
		$this->_assert_equals($api_result->recipes[0]->id, 215);
	}

	function test_recipe_preview_success()
	{
		$this->establishSession();

 		$curlResult = $this->doCurlGet("/api/1/recipes/201/preview");
		$this->message = $curlResult['body'];
		$api_result = json_decode($curlResult['body']);

		$this->_assert_equals($api_result->success, 1);
		$this->_assert_equals($api_result->recipes[0]->id, 201);
		$this->_assert_equals($api_result->recipes[0]->title, "The Right Bank <p class='text-muted'><small><a href='http://www.bonappetit.com/recipes/2013/06/the-right-bank'>Courtesy of www.bonappetit.com</a></small></p>");
	}

	function test_recipe_photos_success()
	{
		$this->establishSession();

 		$curlResult = $this->doCurlGet("/api/1/recipes/152/photos");
		$this->message = $curlResult['body'];
		$api_result = json_decode($curlResult['body']);

		$this->_assert_equals($api_result->success, 1);
		$this->_assert_equals(count($api_result->result), 1);
		$this->_assert_equals($api_result->result[0]->id, 29);
		$this->_assert_equals($api_result->result[0]->image, "image/13b59e4f022fa2d9b00b0da9d4afbc0a1466907929.JPG");
	}

	function test_recipe_failure()
	{
		$this->establishSession();

		// Invalid permission
 		$curlResult = $this->doCurlGet("/api/1/recipes/201");
		$this->message = $curlResult['body'];
		$api_result = json_decode($curlResult['body']);

		$this->_assert_equals($api_result->success, 0);
	}

	function test_recipe_search()
	{
		$this->establishSession();

		$fields = array();

		$curlResult = $this->doCurlPost('/api/1/recipes/search', $fields);
		$this->message = $curlResult['body'];
		$api_result = json_decode($curlResult['body']);

		$this->_assert_equals($api_result->success, 1);
		$this->_assert_equals($api_result->total, 111);
		$this->_assert_equals(count($api_result->recipes), 15);
		
		$found = 0;
		foreach($api_result->recipes as $recipe)
		{
			if ($recipe->title == "Sweet Potato Gnocchi with Maple Cinnamon Sage Brown Butter")
			{
				$found++;
				$this->_assert_equals(count($recipe->ingredients), 14);
				$this->_assert_equals($recipe->ingredients[0]->name, "For the Gnocchi:");
			}
			else if ($recipe->title == "Lime Tortilla Chips")
			{
				$found++;
				$this->_assert_equals(count($recipe->ingredients), 4);
				$this->_assert_true($recipe->ingredients[0]->id > 0);
				$this->_assert_equals($recipe->ingredients[0]->name, "freshly squeezed lime juice");
				$this->_assert_equals($recipe->ingredients[0]->unit, "cup");
				$this->_assert_equals($recipe->ingredients[0]->quantity, "1/4");
			}
		}
		$this->_assert_equals($found, 2);
	}

	function test_recipe_search_invalid()
	{
		$this->establishSession();

		$curlResult = $this->doCurlPost('/api/1/recipes/search', "{\"query\": \"'; SELECT * FROM USERS --\"}");
		$this->message = $curlResult['body'];
		$api_result = json_decode($curlResult['body']);

		$this->_assert_equals($api_result->success, 1);
		$this->_assert_equals($api_result->total, 0);
		$this->_assert_equals(count($api_result->recipes), 0);
	}

	function test_recipe_search_keyword()
	{
		$this->establishSession();

 		$fields = array (
 			'query' => 'chicken'
		);

 		$curlResult = $this->doCurlPost("/api/1/recipes/search", $fields);
		$this->message = $curlResult['body'];
		$api_result = json_decode($curlResult['body']);

		$this->_assert_equals($api_result->success, 1);
		$this->_assert_equals($api_result->total, count($api_result->recipes));

		foreach($api_result->recipes as $recipe)
		{
			$this->_assert_true(stripos($recipe->title, "chicken") !== FALSE);
		}
	}

	function test_recipe_search_keyword_invalid()
	{
		$this->establishSession();

 		$fields = array (
 			'query' => array('chicken')
		);

 		$curlResult = $this->doCurlPost("/api/1/recipes/search", json_encode($fields));
		$this->message = $curlResult['body'];
		$api_result = json_decode($curlResult['body']);

		$this->_assert_equals($api_result->success, 1);
	}

	function test_recipe_search_keyword_sort_title()
	{
		$this->establishSession();

 		$fields = array (
 			'query' => 'chicken',
 			'sort' => 'title'
		);

 		$curlResult = $this->doCurlPost("/api/1/recipes/search", $fields);
		$this->message = $curlResult['body'];
		$api_result = json_decode($curlResult['body']);

		$this->_assert_equals($api_result->success, 1);
		$this->_assert_equals($api_result->total, count($api_result->recipes));

		$previous_title = '';
		foreach($api_result->recipes as $recipe)
		{
			$this->_assert_true(stripos($recipe->title, "chicken") !== FALSE);

			if (isset($previous_title))
			{
				$this->_assert_true(strcmp($previous_title, $recipe->title) <= 0);
			}

			$previous_title = $recipe->title;
		}
	}

	function test_recipe_search_keyword_sort_rating()
	{
		$this->establishSession();

 		$fields = array (
 			'query' => 'chicken',
 			'sort' => 'rating'
		);

 		$curlResult = $this->doCurlPost("/api/1/recipes/search", $fields);
		$this->message = $curlResult['body'];
		$api_result = json_decode($curlResult['body']);

		$this->_assert_equals($api_result->success, 1);
		$this->_assert_equals($api_result->total, count($api_result->recipes));

		$previous_rating = -1;
		foreach($api_result->recipes as $recipe)
		{
			$this->_assert_true(stripos($recipe->title, "chicken") !== FALSE);

			if ($previous_rating >= 0)
			{
				$this->_assert_true($previous_rating >= $recipe->rating);
			}

			$previous_rating = $recipe->rating;
		}
	}

	function test_recipe_search_keyword_sort_ingredients()
	{
		$this->establishSession();

 		$fields = array (
 			'query' => 'chicken',
 			'sort' => 'ingredients'
		);

 		$curlResult = $this->doCurlPost("/api/1/recipes/search", $fields);
		$this->message = $curlResult['body'];
		$api_result = json_decode($curlResult['body']);

		$this->_assert_equals($api_result->success, 1);
		$this->_assert_equals($api_result->total, count($api_result->recipes));

		$previous_ingredients = -1;
		foreach($api_result->recipes as $recipe)
		{
			$this->_assert_true(stripos($recipe->title, "chicken") !== FALSE);

			if ($previous_ingredients >= 0)
			{
				$this->_assert_true($previous_ingredients <= count($recipe->ingredients));
			}

			$previous_ingredients = count($recipe->ingredients);
		}
	}

	function test_recipe_search_keyword_sort_time()
	{
		$this->establishSession();

 		$fields = array (
 			'query' => 'chicken',
 			'sort' => 'time'
		);

 		$curlResult = $this->doCurlPost("/api/1/recipes/search", $fields);
		//$this->message = $curlResult['body'];
		$api_result = json_decode($curlResult['body']);

		$this->_assert_equals($api_result->success, 1);
		$this->_assert_equals($api_result->total, count($api_result->recipes));

		$previous_time = -1;
		foreach($api_result->recipes as $recipe)
		{
			$this->_assert_true(stripos($recipe->title, "chicken") !== FALSE);
			$current_time = 9999;
			
			if (!empty($recipe->total_time) && $this->parse_time_string($recipe->total_time) > 0)
			{
				$this->message .= $recipe->total_time;
				$current_time = $this->parse_time_string($recipe->total_time);
			}
			else if (!empty($recipe->prep_time) && $this->parse_time_string($recipe->prep_time) > 0)
			{
				$this->message .= $recipe->prep_time;
				$current_time = $this->parse_time_string($recipe->prep_time);
			}
			
			$this->message .= "=> $current_time (" . $recipe->title . ")<BR>";

			if ($previous_time >= 0)
			{
				$this->_assert_true($previous_time <= $current_time);
			}

			$previous_time = $current_time;
		}
	}

	function test_recipe_search_cuisine()
	{
		$this->establishSession();

 		$fields = array (
	 		'query' => 'chicken',
 			'cuisine' => 'European',
 			'sort' => 'title'
		);

 		$curlResult = $this->doCurlPost("/api/1/recipes/search", $fields);
		$this->message = $curlResult['body'];
		$api_result = json_decode($curlResult['body']);

		$this->_assert_equals($api_result->success, 1);
		$this->_assert_equals($api_result->total, count($api_result->recipes));

		$recipe = $api_result->recipes[0];
		$this->_assert_equals($recipe->title, "Easy Ratatouille with Chicken");
	}

	function test_recipe_search_cuisine_invalid()
	{
		$this->establishSession();

 		$fields = array (
	 		'query' => 'chicken',
 			'cuisine' => array('European'),
 			'sort' => 'title'
		);

 		$curlResult = $this->doCurlPost("/api/1/recipes/search", json_encode($fields));
		$this->message = $curlResult['body'];
		$api_result = json_decode($curlResult['body']);

		$this->_assert_equals($api_result->success, 1);
		$this->_assert_equals($api_result->total, count($api_result->recipes));

		$recipe = $api_result->recipes[0];
		$this->_assert_equals($recipe->title, "Chicken Satay Stir-Fry with Orange Scented Jasmine Rice");
	}

	function test_recipe_search_type()
	{
		$this->establishSession();

 		$fields = array (
	 		'query' => 'chicken',
 			'type' => 'Dinner',
 			'sort' => 'title'
		);

 		$curlResult = $this->doCurlPost("/api/1/recipes/search", $fields);
		$this->message = $curlResult['body'];
		$api_result = json_decode($curlResult['body']);

		$this->_assert_equals($api_result->success, 1);
		$this->_assert_equals($api_result->total, count($api_result->recipes));

		$recipe = $api_result->recipes[0];
		$this->_assert_equals($recipe->title, "Chicken Satay Stir-Fry with Orange Scented Jasmine Rice");
	}

	function test_recipe_search_type_invalid()
	{
		$this->establishSession();

 		$fields = array (
	 		'query' => 'chicken',
 			'type' => array('Dinner'),
 			'sort' => 'title'
		);

 		$curlResult = $this->doCurlPost("/api/1/recipes/search", json_encode($fields));
		$this->message = $curlResult['body'];
		$api_result = json_decode($curlResult['body']);

		$this->_assert_equals($api_result->success, 1);
		$this->_assert_equals($api_result->total, count($api_result->recipes));

		$recipe = $api_result->recipes[0];
		$this->_assert_equals($recipe->title, "Chicken Satay Stir-Fry with Orange Scented Jasmine Rice");
	}

	function test_recipe_search_rating()
	{
		$this->establishSession();

 		$fields = array (
	 		'query' => 'chicken',
 			'rating' => array(4),
 			'sort' => 'title'
		);

 		$curlResult = $this->doCurlPost("/api/1/recipes/search", json_encode($fields));
		$this->message = $curlResult['body'];
		$api_result = json_decode($curlResult['body']);

		$this->_assert_equals($api_result->success, 1);
		$this->_assert_equals($api_result->total, count($api_result->recipes));

		$recipe = $api_result->recipes[0];
		$this->_assert_equals($recipe->title, "Why-the-Chicken-Crossed-the-Road Santa Fe-Tastic Tortilla Soup");
	}

	function test_recipe_search_rating_invalid()
	{
		$this->establishSession();

		// Invalid array values
 		$fields = array (
	 		'query' => 'chicken',
 			'rating' => array('a','b',2.34),
 			'sort' => 'title'
		);

 		$curlResult = $this->doCurlPost("/api/1/recipes/search", json_encode($fields));
		$this->message = $curlResult['body'];
		$api_result = json_decode($curlResult['body']);

		$this->_assert_equals($api_result->success, 1);
		$this->_assert_equals($api_result->total, count($api_result->recipes));

		$recipe = $api_result->recipes[0];
		$this->_assert_equals($recipe->title, "Chicken Satay Stir-Fry with Orange Scented Jasmine Rice");
		
		// Non-array value passed
		$fields = array (
	 		'query' => 'chicken',
 			'rating' => "this is not a rating",
 			'sort' => 'title'
		);

 		$curlResult = $this->doCurlPost("/api/1/recipes/search", json_encode($fields));
		$this->message = $curlResult['body'];
		$api_result = json_decode($curlResult['body']);

		$this->_assert_equals($api_result->success, 1);
		$this->_assert_equals($api_result->total, count($api_result->recipes));

		$recipe = $api_result->recipes[0];
		$this->_assert_equals($recipe->title, "Chicken Satay Stir-Fry with Orange Scented Jasmine Rice");
	}

	function test_recipe_recent()
	{
		$this->establishSession();
		$this->load->database();

 		$curlResult = $this->doCurlGet("/api/1/recipes/recent");
		$this->message = $curlResult['body'];
		$api_result = json_decode($curlResult['body']);

		$this->_assert_equals($api_result->success, 1);
		$this->_assert_equals(count($api_result->recipes), 4);

		foreach($api_result->recipes as $recipe)
		{
			$this->db->from("user_recipebox");
			$this->db->where("userid", 1);
			$this->db->where("recipeid", $recipe->id);
			$this->db->select("date_added");
			
			$query = $this->db->get();
			$row = $query->row();
			$new_date = date_create($row->date_added);

			$this->message .= "<BR>" . $new_date->format('Y-m-d H:i:sP') . " >= 2015-06-01 00:00:00";

			$this->_assert_true($new_date >= date_create("2015-06-01 00:00:00"));
		}
	}

	function test_recipe_favorite()
	{
		$this->establishSession();

 		$curlResult = $this->doCurlGet("/api/1/recipes/favorite");
		$this->message = $curlResult['body'];
		$api_result = json_decode($curlResult['body']);

		$this->_assert_equals($api_result->success, 1);
		$this->_assert_equals(count($api_result->recipes), 4);

		foreach($api_result->recipes as $recipe)
		{
			$this->_assert_true($recipe->rating >= 4);
		}
	}

	function test_recipe_different()
	{
		$this->load->database();
		$this->establishSession();

 		$curlResult = $this->doCurlGet("/api/1/recipes/different");
		$this->message = $curlResult['body'];
		$api_result = json_decode($curlResult['body']);

		$this->_assert_equals($api_result->success, 1);
		$this->_assert_equals(count($api_result->recipes), 4);

		foreach($api_result->recipes as $recipe)
		{
			$this->db->from("user_recipebox");
			$this->db->where("userid", 1);
			$this->db->where("recipeid", $recipe->id);
			$this->db->select("IFNULL(`date_lastviewed`, '1900-01-01') AS `date_lastviewed`", FALSE);
			
			$query = $this->db->get();
			$row = $query->row();
			$new_date = date_create($row->date_lastviewed);

			$this->message .= "<BR>" . $new_date->format('Y-m-d H:i:sP') . " <= 2013-07-01 00:00:00";

			$this->_assert_true($new_date <= date_create("2013-07-01 00:00:00"));
		}
	}

	function test_recipe_rating_set()
	{
		$this->establishSession();

 		$fields = array (
 			'rating' => 3
		);

		$curlResult = $this->doCurlPost("/api/1/recipes/rating/210", $fields);

 		$fields = array (
 			'rating' => 4
		);

		$curlResult = $this->doCurlPost("/api/1/recipes/rating/210", $fields);
		$this->message = $curlResult['body'];
		$api_result = json_decode($curlResult['body']);

		$this->_assert_equals($api_result->success,1);
	}

	function test_recipe_rating_get()
	{
		$this->establishSession();

		$curlResult = $this->doCurlGet("/api/1/recipes/rating/210", null);
		$this->message = $curlResult['body'];
		$api_result = json_decode($curlResult['body']);

		$this->_assert_equals($api_result->success,1);
		$this->_assert_equals($api_result->result, 4);
	}

	function test_recipe_rating_set_failure()
	{
		$this->establishSession();

 		$fields = array (
 			'rating' => 4
		);

		$curlResult = $this->doCurlPost("/api/1/recipes/rating/204", $fields);
		$this->message = $curlResult['body'];
		$api_result = json_decode($curlResult['body']);

		$this->_assert_equals($api_result->success,0);
	}

	function test_recipe_rating_set_invalid()
	{
		$this->establishSession();

		$curlResult = $this->doCurlPost('/api/1/recipes/rating/210', "{\"rating\": \"'; SELECT * FROM USERS --\"}");
		$this->message = $curlResult['body'];
		$api_result = json_decode($curlResult['body']);

		$this->_assert_equals($api_result->success, 1);
	}

	function test_recipe_rating_get_failure()
	{
		$this->establishSession();

		$curlResult = $this->doCurlGet("/api/1/recipes/rating/4", null);
		$this->message = $curlResult['body'];
		$api_result = json_decode($curlResult['body']);

		$this->_assert_equals($api_result->success,0);
	}

	function test_recipe_rating_get_ref()
	{
		$this->establishSession();

		$curlResult = $this->doCurlGet("/api/1/recipes/rating", null);
		$this->message = $curlResult['body'];
		$api_result = json_decode($curlResult['body']);

		$this->_assert_equals($api_result->success,1);
		$this->_assert_equals($api_result->result[2], 3);
	}

	function test_recipe_cuisine_set()
	{
		$this->establishSession();

 		$fields = array (
 			'cuisine' => 'Asian'
		);

		$curlResult = $this->doCurlPost("/api/1/recipes/cuisine/210", $fields);

 		$fields = array (
 			'cuisine' => 'American'
		);

		$curlResult = $this->doCurlPost("/api/1/recipes/cuisine/210", $fields);
		$this->message = $curlResult['body'];
		$api_result = json_decode($curlResult['body']);

		$this->_assert_equals($api_result->success,1);
	}

	function test_recipe_cuisine_get()
	{
		$this->establishSession();

		$curlResult = $this->doCurlGet("/api/1/recipes/cuisine/210", null);
		$this->message = $curlResult['body'];
		$api_result = json_decode($curlResult['body']);

		$this->_assert_equals($api_result->success,1);
		$this->_assert_equals($api_result->result, 'American');
	}

	function test_recipe_cuisine_get_failure()
	{
		$this->establishSession();

		$curlResult = $this->doCurlGet("/api/1/recipes/cuisine/4", null);
		$this->message = $curlResult['body'];
		$api_result = json_decode($curlResult['body']);

		$this->_assert_equals($api_result->success,0);
	}

	function test_recipe_cuisine_set_failure()
	{
		$this->establishSession();

 		$fields = array (
 			'cuisine' => 'American'
		);

		$curlResult = $this->doCurlPost("/api/1/recipes/cuisine/204", $fields);
		$this->message = $curlResult['body'];
		$api_result = json_decode($curlResult['body']);

		$this->_assert_equals($api_result->success,0);
	}

	function test_recipe_cuisine_set_invalid()
	{
		$this->establishSession();

		$curlResult = $this->doCurlPost('/api/1/recipes/cuisine/210', "{\"cuisine\": \"'; SELECT * FROM USERS --\"}");
		$this->message = $curlResult['body'];
		$api_result = json_decode($curlResult['body']);

		$this->_assert_equals($api_result->success, 1);
	}

	function test_recipe_cuisine_get_ref()
	{
		$cuisines_master = array('European', 'French','Italian','Spanish','Asian','Chinese','Indian','Japanese','Korean','American','Mexican','Fusion','Nothing','Unique');
		$this->establishSession();

		$curlResult = $this->doCurlGet("/api/1/recipes/cuisine", null);
		$this->message = $curlResult['body'];
		$api_result = json_decode($curlResult['body']);

		$this->_assert_equals($api_result->success,1);
		$this->_assert_equals(count($api_result->result), count($cuisines_master));

		foreach ($api_result->result as $cuisine)
		{
			$this->_assert_true(in_array($cuisine, $cuisines_master));
		}
	}

	function test_recipe_type_set()
	{
		$this->establishSession();

 		$fields = array (
 			'type' => 'Dinner'
		);

		$curlResult = $this->doCurlPost("/api/1/recipes/type/210", $fields);

 		$fields = array (
 			'type' => 'Lunch'
		);

		$curlResult = $this->doCurlPost("/api/1/recipes/type/210", $fields);
		$this->message = $curlResult['body'];
		$api_result = json_decode($curlResult['body']);

		$this->_assert_equals($api_result->success,1);
	}

	function test_recipe_type_get()
	{
		$this->establishSession();

		$curlResult = $this->doCurlGet("/api/1/recipes/type/210", null);
		$this->message = $curlResult['body'];
		$api_result = json_decode($curlResult['body']);

		$this->_assert_equals($api_result->success,1);
		$this->_assert_equals($api_result->result, 'Lunch');
	}

	function test_recipe_type_set_failure()
	{
		$this->establishSession();

 		$fields = array (
 			'type' => 'Lunch'
		);

		$curlResult = $this->doCurlPost("/api/1/recipes/type/204", $fields);
		$this->message = $curlResult['body'];
		$api_result = json_decode($curlResult['body']);

		$this->_assert_equals($api_result->success,0);
	}

	function test_recipe_type_set_invalid()
	{
		$this->establishSession();

		$curlResult = $this->doCurlPost('/api/1/recipes/type/210', "{\"type\": \"'; SELECT * FROM USERS --\"}");
		$this->message = $curlResult['body'];
		$api_result = json_decode($curlResult['body']);

		$this->_assert_equals($api_result->success, 1);
	}

	function test_recipe_type_get_failure()
	{
		$this->establishSession();

		$curlResult = $this->doCurlGet("/api/1/recipes/type/4", null);
		$this->message = $curlResult['body'];
		$api_result = json_decode($curlResult['body']);

		$this->_assert_equals($api_result->success,0);
	}

	function test_recipe_type_get_ref()
	{
		$types_master = array("Breakfast", "Brunch", "Lunch", "Dinner", "Side Dish", "Salad","Snack", "Appetizer", "Fourthmeal","Dessert","Dressing/Sauce", "Drink");
		$this->establishSession();

		$curlResult = $this->doCurlGet("/api/1/recipes/type", null);
		$this->message = $curlResult['body'];
		$api_result = json_decode($curlResult['body']);

		$this->_assert_equals($api_result->success,1);
		$this->_assert_equals(count($api_result->result), count($types_master));

		foreach ($api_result->result as $type)
		{
			$this->_assert_true(in_array($type, $types_master));
		}
	}

	function test_recipe_addedit_success()
	{
		$this->establishSession();

 		$recipe_fields = array (
			"recipe-id" => 0,
			"recipe-img" => "http://mageuzi.com/scratch/sample_remote_recipe_image.png",
			"origin-url" => "http://recipe.com/yum",
			"cache-url" => "recipecache/abc",
			"title" => "Unit test recipe",
			"comment" => "These are additional notes for the \"unit test recipe\".",
			"author" => "Mageuzi",
			"servings" => "4",
			"prep-time" => "20 minutes",
			"total-time" => "1 hour",
			"rating" => "3",
			"cuisine" => "Italian",
			"type" => "Breakfast",
			"ingredient[type][1]" => "i",
			"ingredient[quantity][1]" => "1",
			"ingredient[unit][1]" => "head",
			"ingredient[name][1]" => "lettuce",
			"ingredient[preparation][1]" => "torn",
			"ingredient[type][2]" => "t",
			"ingredient[text][2]" => "Syrup",
			"ingredient[type][3]" => "i",
			"ingredient[quantity][3]" => "2",
			"ingredient[unit][3]" => "cups",
			"ingredient[name][3]" => "sugar",
			"ingredient[preparation][3]" => "",
			"ingredient[type][4]" => "p",
			"ingredient[rawtext][4]" => "Unparsed ingredient",
			"directions[seq][1]" => "",
			"directions[text][1]" => "Mix well and bake for perfect unit testing.",
			"directions[seq][2]" => "",
			"directions[text][2]" => "Serve immediately."
		);

		$fields_string = "{\"id\":\"0\",\"origin_url\":\"http://recipe.com/yum\",\"cache_url\":\"recipecache/abc\",\"recipe_img\":\"http://mageuzi.com/scratch/sample_remote_recipe_image.png\",\"title\":\"Unit test recipe\",\"comment\":\"These are additional notes for the \\\"unit test recipe\\\".\",\"author\":\"Mageuzi\",\"servings\":\"4\",\"prep\":\"20 minutes\",\"total\":\"1 hour\",\"rating\":\"3\",\"cuisine\":\"Italian\",\"type\":\"Breakfast\",\"ingredients\":[{\"type\":\"i\",\"name\":\"lettuce\",\"quantity\":\"1\",\"unit\":\"head\",\"prep\":\"torn\"},{\"type\":\"t\",\"text\":\"Syrup\"},{\"type\":\"i\",\"name\":\"sugar\",\"quantity\":\"2\",\"unit\":\"cups\",\"prep\":\"\"},{\"type\":\"p\",\"rawtext\":\"Unparsed ingredient\"}],\"directions\":[\"Mix well and bake for perfect unit testing.\",\"Serve immediately.\"]}";

 		$curlResult = $this->doCurlPost("/api/1/recipes/0", $fields_string);
 		$this->message = "Add: " . $curlResult['body'];
 		$api_result = json_decode($curlResult['body']);

 		// Make sure recipe was saved properly.
 		$this->_assert_equals($api_result->success, 1);
 		$this->_assert_not_empty($api_result->result);

 		// Retrieve recipe and ensure it was saved correctly.
 		$recipeid = $api_result->result;

 		$curlResult = $this->doCurlGet("/api/1/recipes/index/$recipeid");
		$this->message .= "<BR>Retrieve (Add Verify): " . $curlResult['body'];
		$api_result = json_decode($curlResult['body']);

		$this->_assert_equals($api_result->success, 1);
		$this->_assert_equals($api_result->total, 1);

		$recipe = $api_result->recipes[0];
		$this->_assert_equals($recipe->id, $recipeid);
		$this->_assert_equals($recipe->title, $recipe_fields["title"]);
		$this->_assert_equals($recipe->comment, $recipe_fields["comment"]);
		$this->_assert_equals($recipe->author, $recipe_fields["author"]);
		$this->_assert_equals($recipe->servings, $recipe_fields["servings"]);
		$this->_assert_equals($recipe->prep_time, $recipe_fields["prep-time"]);
		$this->_assert_equals($recipe->total_time, $recipe_fields["total-time"]);
		$this->_assert_equals($recipe->rating, $recipe_fields["rating"]);
		$this->_assert_equals($recipe->cuisine, $recipe_fields["cuisine"]);
		$this->_assert_equals($recipe->type, $recipe_fields["type"]);
		$this->_assert_true(stripos($recipe->image_url, ".png") !== FALSE);
		$this->_assert_true(stripos($recipe->image_url, "mageuzi.com") === FALSE);
		$this->_assert_equals($recipe->origin_url, $recipe_fields["origin-url"]);
		$this->_assert_equals($recipe->cache_url, $recipe_fields["cache-url"]);

		$this->_assert_true($recipe->ingredients[0]->id > 0);		
		$this->_assert_equals($recipe->ingredients[0]->quantity, $recipe_fields["ingredient[quantity][1]"]);
		$this->_assert_equals($recipe->ingredients[0]->unit, $recipe_fields["ingredient[unit][1]"]);
		$this->_assert_equals($recipe->ingredients[0]->name, $recipe_fields["ingredient[name][1]"]);
		$this->_assert_equals($recipe->ingredients[0]->preparation, $recipe_fields["ingredient[preparation][1]"]);

		$this->_assert_equals($recipe->ingredients[1]->id, 0);
		$this->_assert_equals($recipe->ingredients[1]->name, $recipe_fields["ingredient[text][2]"]);

		$this->_assert_true($recipe->ingredients[2]->id > 0);
		$this->_assert_equals($recipe->ingredients[2]->quantity, $recipe_fields["ingredient[quantity][3]"]);
		$this->_assert_equals($recipe->ingredients[2]->unit, $recipe_fields["ingredient[unit][3]"]);
		$this->_assert_equals($recipe->ingredients[2]->name, $recipe_fields["ingredient[name][3]"]);
		$this->_assert_equals($recipe->ingredients[2]->preparation, $recipe_fields["ingredient[preparation][3]"]);

		$this->_assert_equals($recipe->ingredients[3]->id, 0);
		$this->_assert_equals($recipe->ingredients[3]->name, $recipe_fields["ingredient[rawtext][4]"]);

		$this->_assert_equals($recipe->directions[0], $recipe_fields["directions[text][1]"]);
		$this->_assert_equals($recipe->directions[1], $recipe_fields["directions[text][2]"]);

		// Edit existing recipe
		$recipe_fields["title"] = "Unit test edit recipe";
		$recipe_fields["comment"] = "These are more additional notes for the \"unit test recipe\".";
		$fields_string = "{\"id\":\"0\",\"recipe_img\":\"http://mageuzi.com/scratch/sample_remote_recipe_image.png\",\"title\":\"Unit test edit recipe\",\"comment\":\"These are more additional notes for the \\\"unit test recipe\\\".\",\"author\":\"Mageuzi\",\"servings\":\"4\",\"prep\":\"20 minutes\",\"total\":\"1 hour\",\"rating\":\"3\",\"cuisine\":\"Italian\",\"type\":\"Breakfast\",\"ingredients\":[{\"type\":\"i\",\"name\":\"lettuce\",\"quantity\":\"1\",\"unit\":\"head\",\"prep\":\"torn\"},{\"type\":\"t\",\"text\":\"Syrup\"},{\"type\":\"i\",\"name\":\"sugar\",\"quantity\":\"2\",\"unit\":\"cups\",\"prep\":\"\"},{\"type\":\"i\",\"name\":\"\",\"quantity\":\"\",\"unit\":\"\",\"prep\":\"\"},{\"type\":\"p\",\"rawtext\":\"\"}],\"directions\":[\"Mix well and bake for perfect unit testing.\",\"Serve immediately.\"]}";
		//$fields_string = "recipe-id=0&recipe-img=&title=Unit+test+edit+recipe&comment=These+are+more+additional+notes+for+the+%22unit+test+recipe%22.&author=Mageuzi&servings=4&prep-time=20+minutes&total-time=1+hour&rating=3&cuisine=Italian&type=Breakfast&ingredient%5Btype%5D%5B%5D=i&ingredient%5Bquantity%5D%5B%5D=1&ingredient%5Bunit%5D%5B%5D=head&ingredient%5Bname%5D%5B%5D=++++lettuce&ingredient%5Bpreparation%5D%5B%5D=++++torn+++++&ingredient%5Btype%5D%5B%5D=t&ingredient%5Btext%5D%5B%5D=Syrup&ingredient%5Btype%5D%5B%5D=i&ingredient%5Bquantity%5D%5B%5D=2&ingredient%5Bunit%5D%5B%5D=cups&ingredient%5Bname%5D%5B%5D=sugar&ingredient%5Bpreparation%5D%5B%5D=&directions%5Bseq%5D%5B%5D=&directions%5Btext%5D%5B%5D=Mix+well+and+bake+for+perfect+unit+testing.";

 		$curlResult = $this->doCurlPost("/api/1/recipes/$recipeid", $fields_string);
 		$api_result = json_decode($curlResult['body']);
 		//$this->message = print_r($curlResult, false);

 		// Make sure recipe was saved properly.
 		$this->_assert_equals($api_result->success, 1);
 		$this->_assert_not_empty($api_result->result);
 		$this->_assert_equals($api_result->result, $recipeid);

		$curlResult = $this->doCurlGet("/api/1/recipes/$recipeid");
		$this->message .= "<BR>Retrieve (Edit Verify): " . $curlResult['body'];
		$api_result = json_decode($curlResult['body']);

		$this->_assert_equals($api_result->success, 1);
		$this->_assert_equals($api_result->total, 1);

		$recipe = $api_result->recipes[0];
		$this->_assert_equals($recipe->id, $recipeid);
		$this->_assert_equals($recipe->title, $recipe_fields["title"]);
		$this->_assert_equals($recipe->comment, $recipe_fields["comment"]);

		$this->_assert_equals(count($recipe->ingredients), 3);
		$this->_assert_equals(count($recipe->directions), 2);
/*
		$this->load->database();
		$this->db->delete('recipe_ingredients', array('recipe_id' => $recipeid));
		$this->db->delete('recipe_directions', array('recipe_id' => $recipeid));
		$this->db->delete('user_recipebox', array('recipeid' => $recipeid));
		$this->db->delete('recipes', array('id' => $recipeid));
		$this->db->close();
*/
		$curlResult = $this->doCurlDelete("/api/1/recipes/$recipeid", null);
		$this->message .= "<BR>Delete: " . $curlResult['body'];
		$api_result = json_decode($curlResult['body']);
		$this->_assert_equals($api_result->success, 1);
	}

	function test_recipe_delete_failure()
	{
		$this->establishSession();

		// Recipe not owned		
		$curlResult = $this->doCurlDelete("/api/1/recipes/201", null);
		$this->message = $curlResult['body'];
		$api_result = json_decode($curlResult['body']);
		$this->_assert_equals($api_result->success, 0);
		$this->_assert_equals($api_result->error, "Unable to delete");

		// Nonexistant recipe
		$curlResult = $this->doCurlDelete("/api/1/recipes/999", null);
		$this->message .= "<BR>Nonexistant: " . $curlResult['body'];
		$api_result = json_decode($curlResult['body']);
		$this->_assert_equals($api_result->success, 0);
		$this->_assert_equals($api_result->error, "Unable to delete");

		// Invalid
		$curlResult = $this->doCurlDelete("/api/1/recipes/index/foo", null);
		$this->message .= "<BR>Invalid: " . $curlResult['body'];
		$api_result = json_decode($curlResult['body']);
		$this->_assert_equals($api_result->success, 0);
		$this->_assert_equals($api_result->error, "Unable to delete");
		
		// Read-only session
		$this->establishReadOnlySession();

 		$curlResult = $this->doCurlDelete("/api/1/recipes/201", $this->session_cookie_readonly);
 		$this->message .= "<BR>Read only: " . $curlResult['body'];
 		$api_result = json_decode($curlResult['body']);

 		// Make sure recipe failed validation.
 		$this->_assert_equals($api_result->success, 0);
 		$this->_assert_not_empty($api_result->error);
 		$this->_assert_true(stripos($api_result->error, "You must login before you can edit a recipe") !== FALSE);
	}

	function test_recipe_addedit_failure()
	{
		$this->establishSession();

		// Missing title
		$fields_string = "{\"id\":\"0\",\"recipe_img\":\"http://mageuzi.com/scratch/sample_remote_recipe_image.png\",\"title\":\"\",\"comment\":\"These are more additional notes for the \\\"unit test recipe\\\".\",\"author\":\"Mageuzi\",\"servings\":\"4\",\"prep\":\"20 minutes\",\"total\":\"1 hour\",\"rating\":\"3\",\"cuisine\":\"Italian\",\"type\":\"Breakfast\",\"ingredients\":[{\"type\":\"i\",\"name\":\"lettuce\",\"quantity\":\"1\",\"unit\":\"head\",\"prep\":\"torn\"},{\"type\":\"t\",\"text\":\"Syrup\"},{\"type\":\"i\",\"name\":\"sugar\",\"quantity\":\"2\",\"unit\":\"cups\",\"prep\":\"\"}],\"directions\":[\"Mix well and bake for perfect unit testing.\",\"Serve immediately.\"]}";
		//$fields_string = "recipe-id=0&recipe-img=&title=&comment=These+are+additional+notes+for+the+%22unit+test+recipe%22.&author=Mageuzi&servings=4&prep-time=20+minutes&total-time=1+hour&rating=3&cuisine=Italian&type=Breakfast&ingredient%5Btype%5D%5B%5D=i&ingredient%5Bquantity%5D%5B%5D=1&ingredient%5Bunit%5D%5B%5D=head&ingredient%5Bname%5D%5B%5D=++++lettuce&ingredient%5Bpreparation%5D%5B%5D=++++torn+++++&ingredient%5Btype%5D%5B%5D=t&ingredient%5Btext%5D%5B%5D=Syrup&ingredient%5Btype%5D%5B%5D=i&ingredient%5Bquantity%5D%5B%5D=2&ingredient%5Bunit%5D%5B%5D=cups&ingredient%5Bname%5D%5B%5D=sugar&ingredient%5Bpreparation%5D%5B%5D=&directions%5Bseq%5D%5B%5D=&directions%5Btext%5D%5B%5D=Mix+well+and+bake+for+perfect+unit+testing.";

 		$curlResult = $this->doCurlPost("/api/1/recipes/0", $fields_string);
 		$this->message = $curlResult['body'];
 		$api_result = json_decode($curlResult['body']);

 		// Make sure recipe failed validation.
 		$this->_assert_equals($api_result->success, 0);
 		$this->_assert_not_empty($api_result->error);
 		$this->_assert_true(stripos($api_result->error, "The Recipe Title field is required") !== FALSE);

		// Missing ingredients
		$fields_string = "{\"id\":\"0\",\"recipe_img\":\"http://mageuzi.com/scratch/sample_remote_recipe_image.png\",\"title\":\"Unit test edit recipe\",\"comment\":\"These are more additional notes for the \\\"unit test recipe\\\".\",\"author\":\"Mageuzi\",\"servings\":\"4\",\"prep\":\"20 minutes\",\"total\":\"1 hour\",\"rating\":\"3\",\"cuisine\":\"Italian\",\"type\":\"Breakfast\",\"ingredients\":[],\"directions\":[\"Mix well and bake for perfect unit testing.\",\"Serve immediately.\"]}";
		//$fields_string = "recipe-id=0&recipe-img=&title=This+is+a+title&comment=These+are+additional+notes+for+the+%22unit+test+recipe%22.&author=Mageuzi&servings=4&prep-time=20+minutes&total-time=1+hour&rating=3&cuisine=Italian&type=Breakfast&directions%5Bseq%5D%5B%5D=&directions%5Btext%5D%5B%5D=Mix+well+and+bake+for+perfect+unit+testing.";

 		$curlResult = $this->doCurlPost("/api/1/recipes/0", $fields_string);
 		$this->message .= "<BR>" . $curlResult['body'];
 		$api_result = json_decode($curlResult['body']);

 		// Make sure recipe failed validation.
 		$this->_assert_equals($api_result->success, 0);
 		$this->_assert_not_empty($api_result->error);
 		$this->_assert_true(stripos($api_result->error, "Ingredients are required.") !== FALSE);

 		// Recipe not part of user's collection
		$fields_string = "{\"id\":\"201\",\"recipe_img\":\"http://mageuzi.com/scratch/sample_remote_recipe_image.png\",\"title\":\"Unit test edit recipe\",\"comment\":\"These are more additional notes for the \\\"unit test recipe\\\".\",\"author\":\"Mageuzi\",\"servings\":\"4\",\"prep\":\"20 minutes\",\"total\":\"1 hour\",\"rating\":\"3\",\"cuisine\":\"Italian\",\"type\":\"Breakfast\",\"ingredients\":[{\"type\":\"i\",\"name\":\"lettuce\",\"quantity\":\"1\",\"unit\":\"head\",\"prep\":\"torn\"},{\"type\":\"t\",\"text\":\"Syrup\"},{\"type\":\"i\",\"name\":\"sugar\",\"quantity\":\"2\",\"unit\":\"cups\",\"prep\":\"\"}],\"directions\":[\"Mix well and bake for perfect unit testing.\",\"Serve immediately.\"]}";
		//$fields_string = "recipe-id=201&recipe-img=&title=This+is+a+title&comment=These+are+additional+notes+for+the+%22unit+test+recipe%22.&author=Mageuzi&servings=4&prep-time=20+minutes&total-time=1+hour&rating=3&cuisine=Italian&type=Breakfast&ingredient%5Btype%5D%5B%5D=i&ingredient%5Bquantity%5D%5B%5D=1&ingredient%5Bunit%5D%5B%5D=head&ingredient%5Bname%5D%5B%5D=++++lettuce&ingredient%5Bpreparation%5D%5B%5D=++++torn+++++&ingredient%5Btype%5D%5B%5D=t&ingredient%5Btext%5D%5B%5D=Syrup&ingredient%5Btype%5D%5B%5D=i&ingredient%5Bquantity%5D%5B%5D=2&ingredient%5Bunit%5D%5B%5D=cups&ingredient%5Bname%5D%5B%5D=sugar&ingredient%5Bpreparation%5D%5B%5D=&directions%5Bseq%5D%5B%5D=&directions%5Btext%5D%5B%5D=Mix+well+and+bake+for+perfect+unit+testing.";

 		$curlResult = $this->doCurlPost("/api/1/recipes/201", $fields_string);
 		$this->message .= "<BR>" . $curlResult['body'];
 		$api_result = json_decode($curlResult['body']);

 		// Make sure recipe failed validation.
 		$this->_assert_equals($api_result->success, 0);
 		$this->_assert_not_empty($api_result->error);
 		$this->_assert_true(stripos($api_result->error, "Unable to access existing recipe") !== FALSE);

 		// Read-only session
		$this->establishReadOnlySession();
		$fields_string = "{\"id\":\"0\",\"recipe_img\":\"http://mageuzi.com/scratch/sample_remote_recipe_image.png\",\"title\":\"Unit test edit recipe\",\"comment\":\"These are more additional notes for the \\\"unit test recipe\\\".\",\"author\":\"Mageuzi\",\"servings\":\"4\",\"prep\":\"20 minutes\",\"total\":\"1 hour\",\"rating\":\"3\",\"cuisine\":\"Italian\",\"type\":\"Breakfast\",\"ingredients\":[{\"type\":\"i\",\"name\":\"lettuce\",\"quantity\":\"1\",\"unit\":\"head\",\"prep\":\"torn\"},{\"type\":\"t\",\"text\":\"Syrup\"},{\"type\":\"i\",\"name\":\"sugar\",\"quantity\":\"2\",\"unit\":\"cups\",\"prep\":\"\"}],\"directions\":[\"Mix well and bake for perfect unit testing.\",\"Serve immediately.\"]}";
		//$fields_string = "recipe-id=0&recipe-img=http://mageuzi.com/scratch/sample_remote_recipe_image.png&title=Unit+test+recipe&comment=These+are+additional+notes+for+the+%22unit+test+recipe%22.&author=Mageuzi&servings=4&prep-time=20+minutes&total-time=1+hour&rating=3&cuisine=Italian&type=Breakfast&ingredient%5Btype%5D%5B%5D=i&ingredient%5Bquantity%5D%5B%5D=1&ingredient%5Bunit%5D%5B%5D=head&ingredient%5Bname%5D%5B%5D=++++lettuce&ingredient%5Bpreparation%5D%5B%5D=++++torn+++++&ingredient%5Btype%5D%5B%5D=t&ingredient%5Btext%5D%5B%5D=Syrup&ingredient%5Btype%5D%5B%5D=i&ingredient%5Bquantity%5D%5B%5D=2&ingredient%5Bunit%5D%5B%5D=cups&ingredient%5Bname%5D%5B%5D=sugar&ingredient%5Bpreparation%5D%5B%5D=&directions%5Bseq%5D%5B%5D=&directions%5Btext%5D%5B%5D=Mix+well+and+bake+for+perfect+unit+testing.";

 		$curlResult = $this->doCurlPost("/api/1/recipes/201", $fields_string, $this->session_cookie_readonly);
 		$this->message .= "<BR>" . $curlResult['body'];
 		$api_result = json_decode($curlResult['body']);

 		// Make sure recipe failed validation.
 		$this->_assert_equals($api_result->success, 0);
 		$this->_assert_not_empty($api_result->error);
 		$this->_assert_true(stripos($api_result->error, "You must login before you can edit a recipe") !== FALSE);
	}

	function test_recipe_imageupload_success()
	{
		$this->establishSession();

		$file = realpath(dirname(__FILE__)) . "/recipeimg_normal.jpg";
		$curlResult = $this->doCurlBinaryPost("/api/1/recipes/image", $file);
		$this->message = $curlResult['body'];
		$api_result = json_decode($curlResult['body']);

		$this->_assert_true(file_exists(realpath(dirname(__FILE__)) . "/../../../temp/" . $api_result->result));
	}

	function test_recipe_parse_ingredients()
	{
		$this->establishSession();

 		$fields = array (
 			'content' => '<li><span class="ingredient">1 large head of cauliflower, cut into florets</span></li>
                      <li><span class="ingredient">3 tablespoons unsalted butter</span></li>
                      <li><span class="ingredient">1 medium shallot, diced (about 1/4 cup)</span></li>
                      <li><span class="ingredient">1–2 teaspoons sugar, divided</span></li>
                      <li><span class="ingredient">3 tablespoons flour</span></li>
                      <li><span class="ingredient">1/2 teaspoon freshly ground white pepper</span></li>
                      <li><span class="ingredient">1/2 teaspoon ground nutmeg</span></li>
                      <li><span class="ingredient">1/2 teaspoon ground cumin</span></li>
                      <li><span class="ingredient">2 1/4 cups whole milk, warm</span></li>
                      <li><span class="ingredient">1/4 cup heavy cream</span></li>
                      <li><span class="ingredient">3/4 cup grated pecorino Roman cheese, divided</span></li>
                      <li><span class="ingredient">1 1/2 teaspoons kosher salt</span></li>
                      <li><span class="ingredient">1/2 cup panko-style bread crumbs</span></li>
                      <li><span class="ingredient">2 tablespoon melted butter</span></li>
                      <li><span class="ingredient">2 tablespoons roughly chopped fresh parsley leaves</span></li>
                      <li><span class="ingredient">2 to 3 cloves (1 tbsp) garlic</span></li>
                      <li><span class="ingredient">1/2 small (1 cup) yellow onion, small dice</span></li>
                      <li><span class="ingredient">1 (28-ounce) can whole peeled tomatoes, crushed</span></li>
                      <li><span class="ingredient">1 fresh small hot chili (such as jalapeno, serrano, or Fresno), stems, seeds, and ribs removed</span></li>
                      <li><span class="ingredient">2 large cloves garlic, minced</span></li>
                      <li><span class="ingredient">salt and pepper</span></li>'
		);

		$curlResult = $this->doCurlPost("/recipe/parse/ingredients", $fields);
		$this->message = $curlResult['body'];
		$api_result = json_decode($curlResult['body']);

		$this->_assert_equals($api_result->success, 1);
		$this->_assert_equals(count($api_result->result), 21);

		$this->_assert_equals($api_result->result[0]->id, 1);
		$this->_assert_equals($api_result->result[0]->quantity, 1);
		$this->_assert_equals($api_result->result[0]->name, "of cauliflower");
		$this->_assert_equals($api_result->result[0]->unit, "large head");
		$this->_assert_equals($api_result->result[0]->preparation, "cut into florets");

		$this->_assert_equals($api_result->result[1]->id, 1);
		$this->_assert_equals($api_result->result[1]->quantity, 3);
		$this->_assert_equals($api_result->result[1]->name, "unsalted butter");
		$this->_assert_equals($api_result->result[1]->unit, "tablespoons");
		$this->_assert_equals($api_result->result[1]->preparation, "");

		$this->_assert_equals($api_result->result[2]->id, 1);
		$this->_assert_equals($api_result->result[2]->quantity, 1);
		$this->_assert_equals($api_result->result[2]->name, "shallot");
		$this->_assert_equals($api_result->result[2]->unit, "medium");
		$this->_assert_equals($api_result->result[2]->preparation, "diced (about 1/4 cup)");

		$this->_assert_equals($api_result->result[3]->id, 1);
		$this->_assert_equals($api_result->result[3]->quantity, "1–2");
		$this->_assert_equals($api_result->result[3]->name, "sugar");
		$this->_assert_equals($api_result->result[3]->unit, "teaspoons");
		$this->_assert_equals($api_result->result[3]->preparation, "divided");

		$this->_assert_equals($api_result->result[15]->id, 1);
		$this->_assert_equals($api_result->result[15]->quantity, "2 to 3");
		$this->_assert_equals($api_result->result[15]->name, "garlic");
		$this->_assert_equals($api_result->result[15]->unit, "cloves");
		$this->_assert_equals($api_result->result[15]->preparation, "(1 tbsp)");

		$this->_assert_equals($api_result->result[16]->id, 1);
		$this->_assert_equals($api_result->result[16]->quantity, "1/2");
		$this->_assert_equals($api_result->result[16]->name, "yellow onion");
		$this->_assert_equals($api_result->result[16]->unit, "small");
		$this->_assert_equals($api_result->result[16]->preparation, "small dice (1 cup)");

		$this->_assert_equals($api_result->result[17]->id, 1);
		$this->_assert_equals($api_result->result[17]->quantity, "1");
		$this->_assert_equals($api_result->result[17]->name, "(28-ounce) can whole peeled tomatoes");
		$this->_assert_equals($api_result->result[17]->unit, "");
		$this->_assert_equals($api_result->result[17]->preparation, "crushed");

		$this->_assert_equals($api_result->result[18]->id, 1);
		$this->_assert_equals($api_result->result[18]->quantity, "1");
		$this->_assert_equals($api_result->result[18]->name, "fresh small hot chili");
		$this->_assert_equals($api_result->result[18]->unit, "");
		$this->_assert_equals($api_result->result[18]->preparation, "(such as jalapeno, serrano, or Fresno), stems, seeds, and ribs removed");

		$this->_assert_equals($api_result->result[19]->id, 1);
		$this->_assert_equals($api_result->result[19]->quantity, "2");
		$this->_assert_equals($api_result->result[19]->name, "cloves garlic");
		$this->_assert_equals($api_result->result[19]->unit, "large");
		$this->_assert_equals($api_result->result[19]->preparation, "minced");

		$this->_assert_equals($api_result->result[20]->id, 1);
		$this->_assert_equals($api_result->result[20]->quantity, "");
		$this->_assert_equals($api_result->result[20]->name, "salt and pepper");
		$this->_assert_equals($api_result->result[20]->unit, "");
		$this->_assert_equals($api_result->result[20]->preparation, "");
	}

	function test_recipe_parse_directions()
	{
		$this->establishSession();

		$fields = array(
			'content' => '<li class="instruction"><div class="procedure-text"><p>Set the oven rack to the middle and preheat to 375°F. Pour 1/2 cup of water into the bottom of a pot fitted with a vegetable steamer. Cover and set over medium heat. Once the water boils, drop florets into the steamer, cover, and steam until tender but not mushy, about 10 minutes. Remove from heat.</p><p></p>
                      </div>
                    </li>
                    <li class="instruction">
                      <div class="procedure-number ">2</div>
                      <div class="procedure-text">
                        <p></p><p>Grease the bottom and sides of a small casserole dish and arrange the florets in a layer inside. </p><p></p>
                      </div>
                    </li>
                    <li class="instruction">
                      <div class="procedure-number ">3</div>
                      <div class="procedure-text">
                        <p></p><p>Add butter to a small heavy saucepan and melt over low heat. Add shallot and cook until gently browned, stirring occasionally, about 3 minutes. Whisk in flour, pepper, nutmeg, and cumin. Continue to cook, stirring constantly, until pale golden brown, about 3 minutes. Slowly whisk in milk and heavy cream and cook, stirring constantly, until the sauce comes to a boil and coats the back of a spoon. Remove from heat and stir in 1/2 cup of the pecorino. Season to taste with salt, then pour sauce over cauliflower in casserole dish.<br>
</p><p></p>
                      </div>
                    </li>
                    <li class="instruction">
                      <div class="procedure-number ">4</div>
                      <div class="procedure-text">
                        <p></p><p>In a small bowl combine bread crumbs, melted butter, and remaining pecorino. Stir well and sprinkle over the top of the florets. Bake until gently browned, about 20 minutes. Sprinkle with parsley and serve immediately, or alternatively, sprinkle with more cheese and broil briefly for a darker, crisper topping.</p></div></li>'
		);

		$curlResult = $this->doCurlPost("/recipe/parse/directions", $fields);
		$this->message = $curlResult['body'];
		$api_result = json_decode($curlResult['body']);

		$this->_assert_equals($api_result->success, 1);
		$this->_assert_equals(count($api_result->result), 7);

		$this->_assert_equals($api_result->result[0], "Set the oven rack to the middle and preheat to 375°F. Pour 1/2 cup of water into the bottom of a pot fitted with a vegetable steamer. Cover and set over medium heat. Once the water boils, drop florets into the steamer, cover, and steam until tender but not mushy, about 10 minutes. Remove from heat.");
		$this->_assert_equals($api_result->result[1], "2");
		$this->_assert_equals($api_result->result[2], "Grease the bottom and sides of a small casserole dish and arrange the florets in a layer inside.");
	}
	
	private function parse_time_string($time_string)
	{
		$hours = $minutes = 0;
		$hours_match_count = preg_match("/([0-9.]+)\s?h(ou)?r?s?/i", $time_string, $hour_matches);
		
		if ($hours_match_count > 0 && !empty($hour_matches[0]))
		{
			$hours = $hour_matches[1] * 60;
		}
		
		$minutes_match_count = preg_match("/([0-9.]+)\s?m(in)?(ute)?s?/i", $time_string, $minute_matches);

		if ($minutes_match_count > 0 && !empty($minute_matches[0]))
		{
			$minutes = $minute_matches[1];
		}

		return $hours + $minutes;		
	}
}
?>