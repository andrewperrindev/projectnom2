<!-- Import New Recipe Modal -->
<div class="modal fade bs-recipeimport-modal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header text-dark">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Recipe Import</h4>
			</div>
	    	<div class="modal-body text-dark">
				<div class="row instr-url">
					<div class="col-sm-12">
						<div class="alert alert-info" role="alert"><strong>Enter the URL for the recipe you'd like to import.</strong> We'll pull in as much information as possible automatically.</div>
					</div>
				</div>
				<div class="row instr-friend hidden">
					<div class="col-sm-12">
						<div class="alert alert-info" role="alert"><strong>This will add your friend's recipe to your collection.</strong> You'll be able to modify the recipe next, if you wish.</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12">
						<form method='post' id='import' action='/recipe/edit'>
							<input type="text" class="form-control import-recipe-url-display" placeholder="URL" data-default="focus">
							<input type="hidden" name='import-recipe-url' id="import-recipe-url">
							<input type="hidden" name="import-recipe-title" id="import-recipe-title">
						</form>
					</div>
				</div>
				<div class="row recipe-title hidden">
					<div class="col-sm-12">
						<div class="alert alert-success">
							<i class="fa fa-cutlery"></i>&nbsp;&nbsp; <span class="recipe-title"></span>
						</div>
					</div>
				</div>
	    	</div>
			<div class="modal-footer">
				<input type='hidden' id='upload-file-name'>
				<button type="button" class="btn btn-default btn-cancel" data-dismiss="modal">Cancel</button>
				<button type="submit" class="btn btn-primary btn-confirm extra-padding">Import</button>
	    	</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->