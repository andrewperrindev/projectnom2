    <!-- Intro Header -->
    <header class="intro">
        <div class="intro-body">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2 greeting">
	                    <h1 class='brand-heading'>Activate Account</h1>
	                </div>
                </div>
            </div>
        </div>
    </header>

    <section id="result" class="container content-section text-center text-dark">
		<div class="row">
			<div class="col-sm-10 col-sm-offset-1">
				<?php
					if ($valid_key)
					{
				?>
				<div class="alert alert-success" role="alert"><i class="fa fa-check"></i> Thank you for confirming your email address! Your account is now active. <a href="/">Click here to return to the home page and log in</a>.</div>
				<?php
					}
					else
					{
				?>
				<div class="alert alert-danger" role="alert">There was a problem activating your account. Please try again, or <a href="support@projectnom.com">email ProjectNom support</a> if you continue to receive this error.</div>
				<?php
					}
				?>
			</div>
		</div>
    </section>