<!-- Intro Header -->
<header class="intro">
    <div class="intro-body">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2 greeting">
                    <h1 class='brand-heading'>Shopping List</h1>
                </div>
            </div>
        </div>
    </div>
</header>

<!-- Shopping List Section -->
<section id="shoppingList" class="container content-section text-dark">
    <div class="container wait-spinner">
        <div class="row">
            <div class="col-sm-12 text-dark text-center wait">
                <i class="fa fa-circle-o-notch fa-spin"></i>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 text-dark">
                <div class="alert alert-danger invisible" role="alert"></div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-10 col-sm-offset-1">
            <div class='shopping-list'>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-10 col-sm-offset-1 footer invisible">
            <button class="btn btn-default pull-right reset" data-label="Clear All">Clear All</button>
        </div>
    </div>
</section>

<div class="row ingredient-list-item hidden">
    <div class="col-xs-1 handle-container">
        <span class='handle'><i class="fa fa-bars"></i></span>
    </div>
    <div class="col-xs-1 checkbox">
        <label>
            <input type="checkbox" class="checkbox add-ingredient pull-right" name="add-ingredient[]">
        </label>
    </div>
    <div class="col-xs-10">
        <input type="text" class="form-control input-lg ingredient-name" name="ingredient-name">
    </div>
</div>