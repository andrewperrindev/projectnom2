<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once(APPPATH . '/controllers/WebAppControllerBase.php');

class Home extends WebAppControllerBase
{
   public function __construct()
   {
        parent::__construct();
        /*
		$this->load->library('session');
		*/
		if ($this->session->userdata('username') === NULL)
		{
			$this->failureRedirect();
		}
   }

	public function index()
	{
		$data['title'] = "Home";
		$data['username'] = $this->session->userdata('username');
		add_js(array('home/home.js'));
		add_css('home/home.css');

		$this->load->view('templates/header', $data);
		$this->load->view('templates/nav_standard', $data);
		$this->load->view('pages/home', $data);
		$this->load->view('templates/footer', $data);	

	}
}