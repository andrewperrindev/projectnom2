<?php	
	if (isset($error))
	{
		$response = array
		(
			'success' => 0,
			'error' => $error
		);
		
		if (isset($title))
		{
			$response['title'] = $title;
		}
	}
	else
	{
		$response = array
		(
			'success' => 1,
			'result' => $result
		);
	}
	
	if (isset($url))
	{
		$response['url'] = $url;
	}
	
	print json_encode($response, JSON_UNESCAPED_UNICODE);
?>