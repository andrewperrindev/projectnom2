    <!-- Intro Header -->
    <header class="intro">
        <div class="intro-body">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2 greeting"></div>
                </div>
            </div>
        </div>
    </header>

	<!-- Search Section -->
	<section id="search" class="container content-section text-center text-dark">
		<div class="row">
			<div class="col-lg-10 col-lg-offset-1">
				<form class="form-inline recipe-search" role="search" action='/recipe/search' method='post'>
					<div class="form-group">
						<label class="sr-only" for="recipeSearch">Recipe Search</label>
						<div class="input-group">
							<div class="input-group-addon"><i class="fa fa-search"></i></div>
							<input type="text" class="form-control input-lg recipe-search" id="recipeSearch" name='search-query' placeholder="Search">
						</div>
					</div>
				</form>
			</div>
		</div>
	</section>

	<!-- Recipe Tabs Section -->
    <section id="overview" class="container content-section text-center text-dark">
	    <div class="error hidden">
	        <div class="row">
	            <div class="col-sm-12 text-dark">
	                <div class="alert alert-danger" role="alert"></div>
	            </div>
	        </div>
		</div>
		<div class="row">
			<div id="recipeTabs" class="col-lg-10 col-lg-offset-1"></div>
		</div>
	</section>