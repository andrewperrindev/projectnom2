<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
//require_once(APPPATH . "/models/recipe_class.php");

class ImageUtils
{
	function resizeImage($originalImage,$toWidth,$toHeight, $resizedImage = null)
	{
		// Get the original geometry and calculate scales
		list($width, $height, $imagetype) = getimagesize($originalImage);

		switch($imagetype)
		{
			case IMAGETYPE_JPEG:
				$imageTmp = imagecreatefromjpeg($originalImage); //jpeg file
				break;
			case IMAGETYPE_GIF:
				$imageTmp = imagecreatefromgif($originalImage); //gif file
				break;
			case IMAGETYPE_PNG:
				$imageTmp = imagecreatefrompng($originalImage); //png file
				break;
		}
		
		if ($width >= $toWidth || $height >= $toHeight)
		{
			$xscale=$width/$toWidth;
			$yscale=$height/$toHeight;
			
			// Recalculate new size with default ratio
			if ($yscale>$xscale){
				$new_width = round($width * (1/$yscale));
				$new_height = round($height * (1/$yscale));
			}
			else {
				$new_width = round($width * (1/$xscale));
				$new_height = round($height * (1/$xscale));
			}

			// Resize the original image
			$imageResized = imagecreatetruecolor($new_width, $new_height);

			imagecopyresampled($imageResized, $imageTmp, 0, 0, 0, 0, $new_width, $new_height, $width, $height);

			$orientation = $this->getOrientation($originalImage);
			if ($orientation !== FALSE)
			{			
				$imageRotated = imagerotate($imageResized, $orientation, 0);
				
				if ($imageRotated !== FALSE)
				{
					$imageResized = $imageRotated;
				}
			}
			
			if (!empty($resizedImage))
			{
				imagepng($imageResized, $resizedImage);
				return $resizedImage;
			}
			else
			{
				return $imageResized;
			}
		}
		else
		{
			return $imageTmp;
		}
	}
	
	function getOrientation($filename)
	{
		$orientation = FALSE;
	    $exif = @exif_read_data($filename);

	    if (!empty($exif['Orientation'])) 
	    {
	        switch ($exif['Orientation']) 
	        {
	            case 3:
	                $orientation = 180;
	                break;
	
	            case 6:
	                $orientation = -90;
	                break;
	
	            case 8:
	            	$orientation = 90;
	                break;
	        }
	    }
		
		return $orientation;
	}
}
?>