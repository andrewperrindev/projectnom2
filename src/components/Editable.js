var React = require('react');
var ReactDOM = require('react-dom');
var Textarea = require("react-autosize-textarea");

module.exports = React.createClass({
	inputSizes: {
		small: {fontSize: 12, padding: "5px 10px", height: 30},
		normal: {fontSize: 14, padding: "6px 12px", height: 34},
		large: {fontSize: 18, padding: "10px 16px", height: 46}
	},

	getInitialState: function() {		
		return {
			content: this.props.content,
			fontSize: this.isTextarea()
				? this.props.fontSize
				: 14,
			mutable: false,
			placeholder: this.props.placeholder
		};
	},

	value: function(newContent) {
		this.setState({
			height: undefined,
			content: newContent
		});
	},

	isInput: function() {
		return this.props.type == "input";
	},
	
	isTextarea: function() {
		return this.props.type == "textarea";
	},

	makeMutable: function(event) {
		this.setState({mutable: true});
	},
	
	makeImmutable: function(event) {
		this.setState({mutable: false});
	},
	
	saveContent: function(event) {
		this.setState({content: event.target.value});
	},

	getVisibleNode: function() {
		if (this.state.mutable) {
			if (this.isTextarea()) {
				return this.refs.input.getTextareaDOMNode();
			}
			else {
				return this.refs.input;
			}
		}
		else
		{
			return this.refs.displayText;
		}
	},
	
	checkForEnter: function(event) {
		if (event.keyCode == 13) {
			event.preventDefault();
			event.stopPropagation();
				
			this.makeImmutable(event);	
		}
	},

	componentDidUpdate: function(prevProps, prevState) {
		var visibleNode = this.getVisibleNode();
		
		if (this.state.mutable)
		{
			if (!prevState.mutable) {
				visibleNode.selectionStart = visibleNode.selectionEnd = visibleNode.value.length;
			}
			
			var newHeight = visibleNode.clientHeight + 2; // compensate for border

			if (prevState.height != newHeight)
			{
				this.setState({height: newHeight});
			}
		}
		
		if (prevState.mutable != this.state.mutable && this.props.onChangeCallback) {
			var e = {
				target: visibleNode,
				component: this,
				isMutable: this.state.mutable
			};
			this.props.onChangeCallback(e);
		}
	},

	render: function() {
		if (this.state.mutable)
		{
			var input;
			var classes = "form-control text-dark";
			
			if (this.props.editClasses) {
				classes += " " + this.props.editClasses;
			}
			
			if (this.isTextarea())
			{
				input = React.createElement(Textarea,
					{
						name: this.props.name,
						className: classes, 
						style: {fontSize: this.state.fontSize}, 
						autoFocus: true, 
						value: this.state.content, 
						onChange: this.saveContent, 
						onBlur: this.makeImmutable,
						onResize: this.props.onResize,
						ref: "input"
					}
				);
			}
			else
			{
				if (this.props.inputSize == "small") {
					classes += " input-sm";
				}
				else if (this.props.inputSize == "large") {
					classes += " input-lg";
				}
				input = React.DOM.input(
					{
						type: "text",
						name: this.props.name,
						className: classes, 
						autoFocus: true, 
						value: this.state.content, 
						onChange: this.saveContent, 
						onBlur: this.makeImmutable,
						onKeyDown: this.checkForEnter,
						ref: "input"
					}
				);				
			}
			
			return React.DOM.div({className: "editable"}, input);
		}
		else
		{
			var content = this.state.content 
				? this.state.content.trim() 
				: null;

			var classNames = "editable";
			var frameStyle = {padding: "6px 12px", height: this.state.height};
			var defaultStyle = {display: "inline", fontSize: this.state.fontSize, lineHeight: 1.42857143};
			var contentStyle = {borderBottom: "1px dotted #333"};
			
			if (this.props.displayClasses) {
				classNames += " " + this.props.displayClasses;
			}

			if (this.isInput())
			{
				var size = this.props.inputSize ? this.props.inputSize : "normal";
				frameStyle.height = this.state.height ? this.state.height : this.inputSizes[size].height;
				frameStyle.padding = this.inputSizes[size].padding;
				contentStyle.fontSize = this.inputSizes[size].fontSize;
			}
			
			if (content)
			{
				var result = [];
				content.split(new RegExp("\r?\n","g")).forEach(function(item, index, arr) {
					result.push(item);
					if (index < arr.length - 1) { result.push(React.DOM.br({key: index})); }
				});
				content = result;
			}
			else
			{
				content = this.state.placeholder ? this.state.placeholder : "Please fill in";
				contentStyle.fontStyle = "italic";
				contentStyle.color = "#aaa";
			}
			
			return React.DOM.div({className: classNames, style: frameStyle, onClick: this.makeMutable, onFocus: this.makeMutable, ref: "displayText", tabIndex: 0},
				React.DOM.p({className: "content", style: defaultStyle}, React.DOM.span({style: contentStyle}, content)),
				React.DOM.input({type: "hidden", name: this.props.name, className: this.props.editClasses, value: this.state.content})
			);
		}

	}
});
