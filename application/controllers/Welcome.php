<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once(APPPATH . '/controllers/WebAppControllerBase.php');

class Welcome extends WebAppControllerBase
{
	public function __construct()
	{
        parent::__construct();
        /*
		$this->load->library('session');
		*/
		if ($this->session->userdata('username') !== NULL)
		{
			header("Location: " . $this->getProtocol() . "://" . $_SERVER['SERVER_NAME'] . "/home");
			die();
		}
	}

	public function index()
	{
		$data['title'] = "Welcome";
		$data['returnUrl'] = $this->session->flashdata('returnUrl');
		$this->session->keep_flashdata('returnUrl');
		add_js(array('jqBootstrapValidation.js','welcome/welcome.js', 'login.js'));
		add_css('welcome/welcome.css');

		$this->load->view('templates/header', $data);
		$this->load->view('templates/nav_welcome', $data);
		$this->load->view('pages/welcome_message', $data);
		$this->load->view('templates/modal-login', $data);
		$this->load->view('templates/footer', $data);	
	}
	
	public function activate($activation_key = "")
	{
		$this->session->keep_flashdata('returnUrl');
		
		$this->load->model("user_model", "userService", TRUE);
		$data['title'] = "Activate Account";
		
		$this->load->view('templates/header', $data);
		$this->load->view('templates/nav_empty', $data);

		$data['valid_key'] = false;
		if (!empty($activation_key))
		{
			$data['valid_key'] = $this->userService->verifyActivationKey($activation_key);
		}

		$this->load->view("pages/activate", $data);
		$this->load->view('templates/footer', $data);	
	}
	
	public function forgot($key = null)
	{
		$this->load->model("user_model", "userService", TRUE);

		$data['title'] = "Forgot Password";
		$email = trim($this->input->post("email"));
		$password = $this->input->post("password");
		$confirmPassword = $this->input->post("confirmPassword");
		//add_js('welcome/forgot.js');
		
		$this->load->view('templates/header', $data);
		$this->load->view('templates/nav_empty', $data);

		$data['step'] = 1;
		if (!empty($email))
		{
			$data['step'] = 2;

			$newKey = $this->userService->addResetPasswordKey($email);
			
			if ($newKey !== FALSE)
			{
				$data['found'] = TRUE;
				$this->load->library("ProjectNomEmail");
				$this->projectnomemail->sendResetPasswordEmail($email, $newKey);
			}
		}
		else if (!empty($key))
		{
			if ($this->userService->verifyResetPasswordKey($key))
			{
				$data['step'] = 3;
				$data['key'] = $key;
				$data['badpassword'] = FALSE;
				
				if (strlen($password) >= 6 && $password == $confirmPassword)
				{
					$data['step'] = 4;
					$data['changeSuccess'] = $this->userService->changePasswordWithKey($key, $password);
				}
				else if (!empty($password))
				{
					$data['badpassword'] = TRUE;
				}
			}
			else
			{
				$data['step'] = 1;
				$data['badkey'] = TRUE;
			}
		}

		$this->load->view("pages/forgot", $data);
		$this->load->view('templates/footer', $data);	
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */