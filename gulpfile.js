var gulp = require('gulp'),
	changed = require('gulp-changed'),
	cssNano = require("gulp-cssnano"),
	uglify = require("gulp-uglify"),
	rename = require("gulp-rename"),
	jshint = require("gulp-jshint"),
	browserify = require("browserify"),
	envify = require("envify"),
	glob = require("glob"),
	source = require("vinyl-source-stream");
var exec = require('child_process').exec;
var packageJSON  = require('./package');

var jshintConfig = packageJSON.jshintConfig;
jshintConfig.lookup = false;

gulp.task('setup', function() {
	gulp.src(['./node_modules/react/dist/react.js','./node_modules/react-dom/dist/react-dom.js'])
	.pipe(gulp.dest('./js'));
});

gulp.task('minify-css', function () {
	gulp.src(['./css/**/*.min.css', '!./css/dist/**/*'])
	.pipe(changed('./css/dist'))
	.pipe(gulp.dest('./css/dist'));

	gulp.src(['./css/**/*.css', '!./css/dist/**/*', '!./css/**/*.min.css'])
	.pipe(changed('./css/dist'))
	.pipe(rename({suffix: ".min"}))
	.pipe(cssNano())
	.pipe(gulp.dest('./css/dist'));
});

gulp.task('js-lint', function () {
	gulp.src(['./src/**/*.js'])
	.pipe(jshint(jshintConfig))
	.pipe(jshint.reporter()); // Dump results
});

gulp.task('minify-js', ['browserify'], function () {
	gulp.src('./js/*.min.js')
	.pipe(changed('./js/dist'))
	.pipe(gulp.dest('./js/dist'));
	
	return gulp.src(['./js/**/*.js', '!./js/dist/**/*', '!./js/**/*.min.js', '!./js/test/**/*'])
	.pipe(changed('./js/dist'))
	.pipe(rename({suffix: ".min"}))
	.pipe(uglify())
	.pipe(gulp.dest('./js/dist'));
});

gulp.task('test', ['minify-js'], function(cb) {
	process.chdir('./js/test');
	exec('./RunAllTests.sh', function (err, stdout, stderr) {
		console.log(stdout);
		console.log(stderr);
		cb(err);
	});
});

gulp.task('browserify-global', function() {
	return browserify()
		.require(['react','react-dom',{file: './src/pn.js', expose: 'pn'}])
		.transform(envify)
		.bundle()
		.pipe(source('exports.js'))
		.pipe(gulp.dest('./js'));
});

gulp.task('browserify-custom', function() {
    return glob('./src/**/!(exports)*.js', function(err, files) {
        if(!err) {
	        var tasks = files.map(function(entry) {
		        //console.log(entry);
	            return browserify({ entries: [entry] })
					.external(['react','react-dom','pn'])
	                .bundle()
	                .pipe(source(entry))
	                .pipe(rename(function (path) {
						path.dirname = path.dirname.replace(/^src\/?/, "");
					}))
	                .pipe(gulp.dest('./js'));
	        });
		}
	});	
});

gulp.task('set-prod', function() {
	process.env.NODE_ENV = 'production';
});

gulp.task('browserify', ['browserify-global','browserify-custom']);
gulp.task('default', ['js-lint','browserify']);
gulp.task('predeploy', ['set-prod', 'default', 'minify-css', 'minify-js', 'test']);

gulp.task('watch', function() {
	gulp.watch(['./src/**/*.js'],
	['default']);
});

gulp.task('develop', ['default', 'watch']);