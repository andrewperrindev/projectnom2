<!-- Upload Modal -->
<div class="modal fade bs-upload-modal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header text-dark">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Upload Image</h4>
			</div>
	    	<div class="modal-body">
				<div class="row text-dark">
					<div class="col-sm-12">
						<div class="row row-error hidden">
							<div class='col-sm-10 col-sm-offset-1'>
								<div class="alert alert-danger" role="alert">
									<p class='text-center'><i class='fa fa-exclamation-triangle'></i></p>
									<span class='error-text'></span>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-10 col-sm-offset-1">
								<div class="file-upload dropzone" id="fileUpload">
									<div class='dz-message'>
										<h4><i class='fa fa-upload'></i>Drop Image File Here<br><small>(or click to choose)</small></h4>
									</div>
								</div>
								<div class="fallback hidden" id="fallback"></div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-10 col-sm-offset-1">
								<div class="upload-preview dropzone-previews">
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-8 col-sm-offset-2">
									<div id="uploadResult"></div>
							</div>
						</div>
					</div>
				</div>
	    	</div>
			<div class="modal-footer">
				<input type='hidden' id='upload-file-name'>
				<button type="submit" class="btn btn-primary btn-confirm">OK</button>
	    	</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class='upload-preview-template hidden'>
	<div id="preview-template" class="file-row">
		<div class="row">
			<div class='col-sm-10 col-sm-offset-1'>
				<div class="preview thumbnail">
					<img data-dz-thumbnail />
					<div class='caption text-center'>
						<h4 class='text-overflow'><span class="name" data-dz-name></span> <p class="size small" data-dz-size></p></h4>
						<div>
							<button data-dz-remove class="btn btn-danger delete">
								<i class="fa fa-trash-o"></i>
								<span>Remove</span>
							</button>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class='col-sm-10 col-sm-offset-1'>
				<div id="total-progress" class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
					<div class="progress-bar progress-bar-success" style="width:0%;" data-dz-uploadprogress></div>
				</div>
			</div>
		</div>
	</div>
</div>