    <!-- Intro Header -->
    <header class="intro">
        <div class="intro-body">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2 greeting">
                        <h1 class='brand-heading'><?php print $recipe->title; ?>
                        <?php
	                        if (!empty($recipe->author))
	                        {
		                        ?>
								<p class="text-center recipe-author"><small>by <?php print $recipe->author; ?></small></p>
								<?php
							}
						?></h1>
                    </div>
                </div>
            </div>
        </div>
    </header>

	<!-- Recipe Header Section -->
    <section id="header" class="container content-section text-dark">
		<div class="error hidden">
			<div class="row">
	        	<div class="col-sm-12 text-dark">
	            	<div class="alert alert-danger" role="alert"></div>
	        	</div>
	    	</div>
		</div>
		<div class="recipe-preview-header">
	        <div class="recipe-header">
	    		<div class="row recipe-intro">
	    			<?php
		    			if (!empty($recipe->image_url))
		    			{
			    			?>
							<div class="col-xs-6 col-xs-offset-3 col-sm-8 col-sm-offset-2 text-center recipe-img"><img src='/<?php print $recipe->image_url; ?>' class='img-thumbnail'></div>
							<?php
						}
					?>
	    			<div class="col-xs-12 text-center recipe-meta-details">
		    			<?php
			    			if (!empty($recipe->total_time) || !empty($recipe->prep_time))
			    			{
				    			$timeDetails = "";
								if (!empty($recipe->total_time))
								{
									$timeDetails .= " Total: " . $recipe->total_time;
								}
								if (!empty($recipe->prep_time))
								{
									if (!empty($timeDetails)) $timeDetails .= "; ";
									$timeDetails .= " Prep: " . $recipe->prep_time;
								}

				    			?>
								<div class="recipe-meta-detail time"><i class='fa fa-clock-o'></i> <span class="recipe-time"><?php print $timeDetails; ?></span></div>
								<?php
		    				}
		    			
		    				if (!empty($recipe->servings))
		    				{
			    				?>	
								<div class="recipe-meta-detail servings"><i class='fa fa-users'></i> <span class="recipe-servings"><?php print $recipe->servings;?></span></div>
								<?php
							}
						?>
	    			</div>
				</div>
	        </div>
		</div>
    </section>
    
	<section id="recipe-body" class="container content-section text-dark">
		<?php
		if (!empty($recipe->ingredients) && count($recipe->ingredients) > 0)
		{
			?>
			<div class="ingredient-frame col-xs-offset-1 col-xs-10 col-lg-offset-0 col-lg-5">
		        <div class="recipe-ingredients">
			        <div class="ingredient-view">
						<?php
						foreach ($recipe->ingredients as $ingredient)
						{
							$ingredientText = "";
							$ingredientClass = "text";
							
							if ($ingredient->id > 0)
							{
								if ($ingredient->quantity !== "")
								{
									$ingredientText .= $ingredient->quantity . " ";
								}
								if ($ingredient->unit !== "")
								{
									$ingredientText .= $ingredient->unit . " ";
								}
								if ($ingredient->name !== "")
								{
									$ingredientText .= $ingredient->name;
								}
								if ($ingredient->preparation !== "")
								{
									$ingredientText .= ", " . $ingredient->preparation;
								}
							}
							else
							{
								$ingredientText = $ingredient->name;
								$ingredientClass = "heading";
							}
	
							?>
							<div class='row ingredient-row'>
								<div class='col-xs-10 col-xs-offset-2 <?php print $ingredientClass; ?>'><?php print $ingredientText; ?></div>
							</div>
							<?php
						}
						?>
			        </div>
		        </div>
			</div>
			<?php
		}

		if (!empty($recipe->directions) && count($recipe->directions) > 0)
		{
			?>
			<div class="direction-frame col-xs-offset-1 col-xs-10 col-lg-offset-0 col-lg-7">
		        <div class="recipe-directions">
			        <div class="direction-view">
				        <?php
					    $direction_number = 0;
					    
					    foreach($recipe->directions as $direction)
					    {
						    $direction_number++;
						    
						    ?>
							<div class='row direction-row'>
								<div class='col-xs-1 col-xs-offset-1 col-sm-1 col-sm-offset-2 direction-label direction-number' id='direction-num'>
									<h1><?php print $direction_number; ?></h1>
								</div>
								<div class='col-xs-8 col-sm-7 recipe-direction'><?php print $direction; ?></div>
							</div>
							<?php
						}
						?>
			        </div>
		        </div>
			</div>
			<?php
		}
		?>
	</section>