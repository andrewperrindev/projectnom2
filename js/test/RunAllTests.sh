#!/bin/bash
./RunBrowserTestsOnly.sh

if [ $? -eq 0 ]
then
	for f in api/*.js
	do
		casperjs test --includes=common/common_login.js,common/common_eval.js $f
		
		if [ $? -ne 0 ]
		then
			echo "Tests failed!"
			exit 99
		fi
	done
fi