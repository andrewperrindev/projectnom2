<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once(APPPATH . '/controllers/WebAppControllerBase.php');

class Friends extends WebAppControllerBase 
{
   public function __construct()
   {
        parent::__construct();

		if ($this->session->userdata('username') === NULL)
		{
			$this->failureRedirect();
		}
   }

	public function index()
	{
		$data['username'] = $this->session->userdata('username');
		$data['title'] = "Friends";

		add_css(array('friends/friends.css'));
		add_js(array('friends/friends.js'));

		$this->load->view('templates/header', $data);
		$this->load->view('templates/nav_standard', $data);
		$this->load->view('pages/friends', $data);
		$this->load->view('templates/footer', $data);
	}


	public function messages()
	{
		$data['username'] = $this->session->userdata('username');
		$data['twitter'] = $this->session->userdata('twitter');
		$data['title'] = "Messages";

		add_css(array('friends/messagelist.css', 'recipe/modal-recipe-preview.css', 'recipe/dropzone-common.css','recipe/dropzone-message.css'));
		add_js(array('friends/messagelist.js', 'recipe/dropzone.js'));

		$this->load->view('templates/header', $data);
		$this->load->view('templates/nav_standard', $data);
		$this->load->view('pages/messagelist', $data);
		$this->load->view('templates/modal-recipe-preview', $data);
		$this->load->view('templates/footer', $data);
	}
}
?>