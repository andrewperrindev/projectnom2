<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once(APPPATH . '/controllers/WebAppControllerBase.php');

class Landing extends WebAppControllerBase 
{
	private $authenticated = false;
	
	public function __construct()
	{
		// Don't automatically close session since we may need to modify it here.
	    parent::__construct(FALSE);
	
		if ($this->session->userdata('username') !== NULL)
		{
			$this->authenticated = true;
		}
	}

	public function index()
	{
		// For now, there is no default view for this controller.
		// Redirect to root. Don't redirect back to this URL after login.
		$this->failureRedirect(false);
	}
	
	public function message($messageid)
	{
		$data["title"] = "Message";
		
		$this->load->model('Friends_model', 'friendsService', TRUE);
		
		$data['message'] = $this->friendsService->getMessage($messageid);
		
		if ($data['message'] != null)
		{
			if ($this->authenticated) 
			{ 
				$data['username'] = $this->session->userdata('username');
			}

			add_js("friends/landing.js");
			add_css(array('home/landing.css', 'recipe/modal-recipe-preview.css'));

			$this->load->view('templates/header', $data);

			($this->authenticated)
				? $this->load->view('templates/nav_standard', $data)
				: $this->load->view('templates/nav_empty', $data);

			$this->load->view('pages/landing', $data);

			if (!empty($data['message']['recipe_id']))
			{
				$this->load->model('Recipe_model', 'recipeService', TRUE);
				$this->load->helper('recipedisplay');
				try
				{
					$data['recipe'] = $this->recipeService->getRecipe(array(
						'username' => $this->authenticated ? $data['username'] : null,
						'recipeid' => $data['message']['recipe_id']
					));
				}
				catch (RecipeException $re)
				{
					$data['recipe'] = $re->baseRecipe;
				}

				$data['recipe']->title = getTitleForDisplay($data['recipe']->title, $data['recipe']->origin_url);
				$data['recipe']->displayTimes = getTimeForDisplay($data['recipe']->prep_time, $data['recipe']->total_time);
				$data['recipe']->displayIngredients = getIngredientsForDisplay($data['recipe']->ingredients);
				
				$this->load->view('templates/modal-recipe-preview', $data);
				
				if (!$this->authenticated)
				{
					$this->session->set_flashdata("returnUrl", $_SERVER["REQUEST_URI"]);
					session_write_close();
					$this->load->view('templates/modal-joinus');
				}
			}

			$this->load->view('templates/footer', $data);
		}
		else
		{
			// Invalid message.
			// Redirect to root. Don't redirect back to this URL after login.
			$this->failureRedirect(false);
		}
	}
}
?>