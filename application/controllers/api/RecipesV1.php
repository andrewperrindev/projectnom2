<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once(APPPATH . '/controllers/api/ApiControllerBase.php');
require_once(APPPATH . '/models/recipe_class.php');
require_once(APPPATH . '/models/ingredient_class.php');

class RecipesV1 extends ApiControllerBase 
{
	const DAYPART = 'daypart';
	const QUERY = 'query';
	const ALPHA = 'alpha';
	const FAVORITE = 'favorite';
	const TOTAL_TIME = "totalTime";
	const INGREDIENTS = "ingredientSort";
	const CUISINE = "cuisine";
	const TYPE = "recipeType";
	const RATING = "rating";
	const MOST_RECENT = 'recent';
	const DIFFERENT = 'different';
	const LIMIT = 'limit';
	const OFFSET = 'offset';

	const ERROR_TEXT = "<strong>Recipe retrieval error occurred.</strong> Please contact <a href='mailto:support@projectnom.com'>ProjectNom support</a> for assistance.";

   public function __construct()
   {
        parent::__construct();
   }

	public function index($id = 0, $aspect = "")
	{
		$this->load->model('Recipe_model', 'recipeService', TRUE);

		$data['username'] = $this->session->userdata('username');
		$data['readonly'] = $this->session->userdata('readonly');
		$data['recipeid'] = $id;

		if ($this->input->server('REQUEST_METHOD') == "GET")
		{
			if ($aspect == "photos")
			{
				$this->load->model('Friends_model', 'friendsService', TRUE);
				$data['result'] = array();
				
				try
				{
					$messages = $this->friendsService->getMessagesForRecipe(
						$data['username'],
						$id,
						true // only messages with photos
					);
					
					foreach($messages as $message)
					{
						$data["result"][] = array(
							"id" => $message["id"],
							"image" => str_replace("msgcache", "image", $message["image_url"])
						);
					}
				}
				catch (Exception $e)
				{
					log_message("error", $e->getMessage());
				}
				
				$this->load->view('api/simple', $data);
			}
			else
			{
				try
				{
					$recipe = $this->recipeService->getRecipe($data);
				}
				catch (RecipeException $re)
				{
					$recipe = $re->baseRecipe;
				}
				$data['recipes'] = array($recipe);
				$data['recipes']['rows'] = 1;
				$data["preview"] = $aspect == "preview" ? 1 : 0;
	
				$this->load->view('api/recipes_get', $data);
			}
		}
		else if ($this->input->server('REQUEST_METHOD') == "DELETE")
		{
			if ($data['readonly'])
			{
				$data['error'] = "You must login before you can edit a recipe.";
			}
			else if ($id > 0)
			{
				try
				{
					$success = $this->recipeService->deleteRecipe($data['recipeid'], $data['username']);
					
					if (!$success)
					{
						$data['error'] = "Unable to delete";
					}
				}
				catch (Exception $e)
				{
					$data['error'] = "There was a problem deleting this recipe.";
				}
			}
			else
			{
				$data['error'] = "Unable to delete";
			}
			
			$this->load->view('api/simple', $data);
		}
		else
		{
			//print_r($_POST);

			// Validation
			$this->load->library('form_validation');
		
			$this->form_validation->set_rules('title', 'Recipe Title', 'required|trim');
			$this->form_validation->set_rules('comment', 'Additional Notes', 'trim');

			$this->form_validation->set_rules('author', 'Author', 'trim');
			$this->form_validation->set_rules('servings', 'Servings', 'trim');
			$this->form_validation->set_rules('prep', 'Prep Time', 'trim');
			$this->form_validation->set_rules('total', 'Total Time', 'trim');
			$this->form_validation->set_rules('recipe_img', 'Recipe Image', 'trim');

			$this->form_validation->set_rules('rating', 'Rating', 'integer|trim');
			$this->form_validation->set_rules('cuisine', 'Cuisine', 'trim');
			$this->form_validation->set_rules('type', 'Recipe Type', 'trim');

			if (!is_array($this->input->post("ingredients")) || count($this->input->post("ingredients")) == 0)
			{
				$data['error'] = "<strong>Ingredients are required.</strong>";
			}
			else if ($this->form_validation->run())
			{
				$recipe = null;

				if ($data['readonly'])
				{
					$data['error'] = "You must login before you can edit a recipe.";
					$this->load->view('api/simple', $data);
					return;
				}
				if ($id > 0)
				{
					try
					{
						$recipe = $this->recipeService->getRecipe($data);
					}
					catch (Exception $e)
					{
						$data['error'] = "Unable to access existing recipe.";
						$this->load->view('api/simple', $data);
						return;
					}
				}
				else
				{
					$recipe = new RecipeDTO();
					$recipe->id = 0;
				}

				$recipe->title = $this->input->post("title");
				$recipe->comment = $this->input->post("comment");

				$recipe->author = $this->input->post("author");
				$recipe->servings = $this->input->post("servings");
				$recipe->prep_time = $this->input->post("prep");
				$recipe->total_time = $this->input->post("total");

				$recipe->rating = $this->input->post("rating");
				$recipe->cuisine = $this->input->post("cuisine");
				$recipe->type = $this->input->post("type");

				if ($this->input->post("origin_url") != "")
				{
					$recipe->origin_url = $this->input->post("origin_url");
				}
				if ($this->input->post("cache_url") != "")
				{
					$recipe->cache_url = ltrim($this->input->post("cache_url"), "/");
				}
				if ($this->input->post("recipe_img") != "")
				{
					$this->load->library('RecipeImport', array('units' => null));
					$recipe->image_url = 
						$this->recipeimport->parseImage($this->input->post("recipe_img"), md5($data['username']));
				}

				$text = $ingredient = 0;
				$allIngredients = $this->input->post("ingredients");
				$recipe->ingredients = array();
				for ($i = 0; $i < count($allIngredients); $i++)
				{
					$newIngredient = new Ingredient();

					if ($allIngredients[$i]["type"] == "i")
					{
						$newIngredient->quantity = trim($allIngredients[$i]["quantity"]);
						$newIngredient->unit = trim($allIngredients[$i]["unit"]);
						$newIngredient->name = trim($allIngredients[$i]["name"]);
						$newIngredient->preparation = trim($allIngredients[$i]["prep"]);
						$ingredient++;
					}
					else
					{
						$newIngredient->id = 0;
						$newIngredient->name = ($allIngredients[$i]["type"] == "p")
							? trim($allIngredients[$i]["rawtext"])
							: trim($allIngredients[$i]["text"]);
						$text++;	
					}

					if (!$newIngredient->isEmpty())
					{
						$recipe->ingredients[] = $newIngredient;
					}
				}

				$recipe->directions = array();
				$allDirections = $this->input->post("directions");
				if (is_array($allDirections))
				{
					for ($i = 0; $i < count($allDirections); $i++)
					{
						if (!empty($allDirections[$i]))
						{
							$recipe->directions[] = trim($allDirections[$i]);
						}
					}
				}

				$this->recipeService->saveRecipe($recipe, $data['username']);
				$data["result"] = $recipe->id;
			}
			else
			{
				$data['error'] = "<strong>Please fix the following:</strong>" . validation_errors();
			}

			$this->load->view('api/simple', $data);
		}
	}

	public function search($page = 1)
	{
		$this->load->model('Recipe_model', 'recipeService', TRUE);
		
		$data['username'] = $this->session->userdata('username');
		$ratings = $this->input->post("rating");

		if ($page < 1)
		{
			$page = 1;
		}
		
		if (is_array($ratings) && count($ratings) <= 6)
		{
			$ratings = array_map("intval", $ratings);
		}
		else
		{
			$ratings = array();
		}

		try
		{
			$data['recipes'] = $this->getRecipeData($data,
				array(
					self::ALPHA => ($this->input->post('sort') == 'title'),
					self::FAVORITE => ($this->input->post('sort') == 'rating'),
					self::TOTAL_TIME => ($this->input->post('sort') == 'time'),
					self::INGREDIENTS => ($this->input->post('sort') == 'ingredients'),
					self::CUISINE => $this->input->post('cuisine'),
					self::TYPE => $this->input->post('type'),
					self::RATING => $ratings,
					self::MOST_RECENT => ($this->input->post('sort') == 'new'),
					self::DIFFERENT => ($this->input->post('sort') == 'different'),
					self::LIMIT => 15,
					self::QUERY => $this->input->post('query'),
					self::OFFSET => 15*($page-1)
				)
			);
		}
		catch (Exception $e)
		{
			$data['error'] = self::ERROR_TEXT;
			//error_log($e->getMessage());
		}
		
		$this->load->view('api/recipes_get', $data);		
	}

	public function recent($dayPart = "")
	{
		//$this->load->library('session');
		$this->load->model('Recipe_model', 'recipeService', TRUE);
		
		$data['username'] = $this->session->userdata('username');

		try
		{
			$data['recipes'] = $this->getRandom(
				$this->getRecipeData($data,
					array(
						self::DAYPART => $dayPart,
						self::MOST_RECENT => TRUE,
						self::LIMIT => 10
					)
				),
				4);
		}
		catch (Exception $e)
		{
			$data['error'] = self::ERROR_TEXT;
			//error_log($e->getMessage());
		}
		
		$this->load->view('api/recipes_get', $data);
	}

	public function favorite($dayPart = "")
	{
		//$this->load->library('session');
		$this->load->model('Recipe_model', 'recipeService', TRUE);
		
		$data['username'] = $this->session->userdata('username');

		try
		{
			$data['recipes'] = $this->getRandom(
				$this->getRecipeData($data,
					array(
						self::DAYPART => $dayPart,
						self::FAVORITE => TRUE,
						self::LIMIT => 10,
						self::RATING => array(4,5)
					)
				),
				4);
		}
		catch (Exception $e)
		{
			$data['error'] = self::ERROR_TEXT;
			//error_log($e->getMessage());
		}
		
		$this->load->view('api/recipes_get', $data);
	}

	public function different($dayPart = "")
	{
		//$this->load->library('session');
		$this->load->model('Recipe_model', 'recipeService', TRUE);
		
		$data['username'] = $this->session->userdata('username');

		try
		{
			$data['recipes'] = $this->getRandom(
				$this->getRecipeData($data,
					array(
						self::DAYPART => $dayPart,
						self::DIFFERENT => TRUE,
						self::LIMIT => 10
					)
				),
				4);
		}
		catch (Exception $e)
		{
			$data['error'] = self::ERROR_TEXT;
			//error_log($e->getMessage());
		}
		
		$this->load->view('api/recipes_get', $data);
	}

	public function rating($id = 0)
	{
		$data['username'] = $this->session->userdata('username');
		$new_rating = $this->input->post('rating');

		if (isset($data['username']) && $id > 0)
		{
			$this->load->model('Recipe_model', 'recipeService', TRUE);

			if ($new_rating !== NULL)
			{
				$new_rating = intval($new_rating, 10);

				if (is_int($new_rating) && $new_rating >= 0 && $new_rating <= 5)
				{			
					try
					{
						$success = $this->recipeService->setRating($data, $id, $new_rating);

						if (!$success)
						{
							$data['error'] = "There was a problem updating the rating.";
						}
					}
					catch (Exception $e)
					{
						$data['error'] = self::ERROR_TEXT;
						//error_log($e->getMessage());
					}
				}
			}
			else
			{
				try
				{
					$data['recipeid'] = $id;
					$recipe = $this->recipeService->getRecipe($data);
					$data['result'] = $recipe->rating;
				}
				catch (Exception $e)
				{
					$data['error'] = self::ERROR_TEXT;
					//error_log($e->getMessage());
				}
			}
		}
		else
		{
			$data['result'] = array(1,2,3,4,5);
		}

		$this->load->view('api/simple', $data);
	}

	public function cuisine($id = 0)
	{
		$data['username'] = $this->session->userdata('username');
		$new_cuisine = $this->input->post('cuisine');
		$this->load->model('Recipe_model', 'recipeService', TRUE);

		if (isset($data['username']) && $id > 0)
		{
			if ($new_cuisine !== NULL)
			{
				if (trim($new_cuisine) == "")
				{
					$new_cuisine = null;
				}
				
				try
				{
					$success = $this->recipeService->setCuisine($data, $id, $new_cuisine);

					if (!$success)
					{
						$data['error'] = "There was a problem updating the cuisine.";
					}
				}
				catch (Exception $e)
				{
					$data['error'] = self::ERROR_TEXT;
					//error_log($e->getMessage());
				}
			}
			else
			{
				try
				{
					$data['recipeid'] = $id;
					$recipe = $this->recipeService->getRecipe($data);
					$data['result'] = $recipe->cuisine;
				}
				catch (Exception $e)
				{
					$data['error'] = self::ERROR_TEXT;
					//error_log($e->getMessage());
				}
			}
		}
		else
		{
			$data['result'] = $this->recipeService->getCuisines();
		}

		$this->load->view('api/simple', $data);
	}

	public function type($id = 0)
	{
		$data['username'] = $this->session->userdata('username');
		$new_type = $this->input->post('type');
		$this->load->model('Recipe_model', 'recipeService', TRUE);

		if (isset($data['username']) && $id > 0)
		{
			if ($new_type !== NULL)
			{
				if (trim($new_type) == "")
				{
					$new_type = null;
				}
				
				try
				{
					$success = $this->recipeService->setType($data, $id, $new_type);

					if (!$success)
					{
						$data['error'] = "There was a problem updating the type.";
					}
				}
				catch (Exception $e)
				{
					$data['error'] = self::ERROR_TEXT;
					//error_log($e->getMessage());
				}
			}
			else
			{
				try
				{
					$data['recipeid'] = $id;
					$recipe = $this->recipeService->getRecipe($data);
					$data['result'] = $recipe->type;
				}
				catch (Exception $e)
				{
					$data['error'] = self::ERROR_TEXT;
					//error_log($e->getMessage());
				}
			}
		}
		else
		{
			$data['result'] = $this->recipeService->getTypes();
		}

		$this->load->view('api/simple', $data);
	}

	public function image($id = 0)
	{
		if ($this->input->server('REQUEST_METHOD') == "GET")
		{
			header('HTTP/1.1 405 Method not allowed', true, 400);
			header('Content-Type: text/plain');
			print "This endpoint does not support GET.";
		}
		else
		{
			$data['username'] = $this->session->userdata('username');
			$recipe = null;

			if ($id > 0)
			{
				$this->load->model('Recipe_model', 'recipeService', TRUE);
				$data['recipeid'] = $id;

				try
				{
					$recipe = $this->recipeService->getRecipe($data);
				}
				catch (Exception $e)
				{
					$recipe = null;
				}
			}

			if (($recipe != null || $id <= 0) && $this->input->server('REQUEST_METHOD') == "POST")
			{
				$this->load->library('pnupload', $data);

				if ( !$this->pnupload->do_upload() )
				{
					//$error = array('error' => $this->upload->display_errors());

					header('HTTP/1.1 400 Bad Request', true, 400);
					header('Content-Type: text/html');
					print $this->pnupload->display_errors();
				}
				else
				{
					$upload_data = $this->pnupload->data();
					$data = array('result' => $upload_data['file_name']);

					$this->load->view('api/simple', $data);
				}
			}
			else
			{
				header('HTTP/1.1 400 Bad Request', true, 400);
				header('Content-Type: text/plain');
			}

		}
	}

	public function import()
	{
		if ($this->input->server('REQUEST_METHOD') == "POST")
		{
			$this->load->model('Recipe_model', 'recipeService', TRUE);
			$data['username'] = $this->session->userdata('username');
			$url = $this->input->post('url');
			if ($url[0] == "/") { $url = $this->getProtocol() . "://" . $_SERVER['SERVER_NAME'] . $url; }
			
			$existingRecipe = $this->recipeService->getRecipe(array_merge($data, array("origin_url" => $url)));
			
			if ($existingRecipe != null)
			{
				$data["result"] = $existingRecipe->id;
			}
			else
			{
				$this->load->library(
					"AutoImport", array("url" => $url, 'filenamehash' => md5($data['username'])));
				$url = $this->autoimport->getPageUrl();
	
				if ($this->autoimport->canImport())
				{
					$recipe = $this->autoimport->doImport();
					if (!empty($recipe))
					{
						//$data["error"] = print_r($recipe, true);
	
						$this->recipeService->saveRecipe($recipe, $data['username']);
						$data["result"] = $recipe->id;
					}
					else
					{
						$data['error'] = "There was a problem parsing the recipe.";
					}
				}
				else
				{
					$data['error'] = "Cannot autoimport this URL.";
					$data['title'] = $this->autoimport->getPageTitle();
				}
			}
			
			$data["url"] = $url;
		}

		$this->load->view('api/recipes_import', $data);
	}

	private function getRecipeData($data, $options)
	{
		return $this->recipeService->getRecipes(
			$data,	// userdata
			empty($options[self::QUERY]) ? '' : $options[self::QUERY],
			empty($options[self::DAYPART]) ? '' : $options[self::DAYPART],
			empty($options[self::ALPHA]) ? FALSE : $options[self::ALPHA],
			empty($options[self::FAVORITE]) ? FALSE : $options[self::FAVORITE],
			empty($options[self::TOTAL_TIME]) ? FALSE : $options[self::TOTAL_TIME],
			empty($options[self::INGREDIENTS]) ? FALSE : $options[self::INGREDIENTS],
			empty($options[self::CUISINE]) ? '' : $options[self::CUISINE],
			empty($options[self::TYPE]) ? '' : $options[self::TYPE],
			empty($options[self::RATING]) ? '' : $options[self::RATING],
			empty($options[self::MOST_RECENT]) ? FALSE : $options[self::MOST_RECENT],
			empty($options[self::DIFFERENT]) ? FALSE : $options[self::DIFFERENT],
			empty($options[self::LIMIT]) ? 0 : $options[self::LIMIT],
			empty($options[self::OFFSET]) ? 0 : $options[self::OFFSET]
			);
	}

	private function getRandom($recipes, $num = 1)
	{
		$rows = FALSE;
		
		if (array_key_exists("rows", $recipes))
		{
			$rows = $recipes["rows"];
			unset($recipes["rows"]);
		}
		
		$r_shuffled = $this->array_random($recipes, $num);
		
		if ($rows !== FALSE)
		{
			$r_shuffled["rows"] = $rows;
		}
		
		return $r_shuffled;
	}
}