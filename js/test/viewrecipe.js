casper.options.viewportSize = {
	width: 1024,
	height: 768
};

casper.test.begin('View Recipe Tests', 39,
function suite(test) 
{	
	// Login and navigate to recipe page. +4 test cases
	login_mageuzi(test, "home");

    casper.thenOpen(getServerName() + "recipe/215", function() {
	    this.waitForUrl(new RegExp(/recipe\/215/));
	});

	casper.then(function() {
		this.waitWhileSelector("div.wait-spinner");
	});

	// Check for correct View Recipe layout
	casper.then(function() {
		test.assertTitle("Blackberry Buttermilk Cake :: ProjectNom");
		
		test.assertSelectorHasText("h1.brand-heading", "Blackberry Buttermilk Cake", "Recipe Title Visible");
		test.assertSelectorHasText("div.recipe-comment", 'Buttermilk keeps this moist cake light and flavorful. Dust it with powdered sugar as it cools for a sweet, decorative finish.Special equipment: Use a 9"–10"-diameter springform pan.', "Recipe Comment Visible");
		test.assertSelectorHasText("p.recipe-author", "by Recipe Author", "Recipe Author Visible");
		test.assertSelectorHasText("div.servings-metadata span.servings", "8-10", "Recipe Servings Visible");
		test.assertSelectorHasText("div.time-metadata", " Total: 1 hour 45 minutes Prep: 20 minutes", "Recipe timings visible");

		test.assertEvalEquals(function() {
			return $("button#dropdownRating span.button-text").html();
		}, "<i class=\"fa fa-star\"></i>&nbsp;<i class=\"fa fa-star\"></i>&nbsp;<i class=\"fa fa-star\"></i>&nbsp;", "Recipe rating visible");
		test.assertSelectorHasText("button#dropdownCuisine span.button-text", "European", "Recipe cuisine visible");
		test.assertSelectorHasText("button#dropdownTypes span.button-text", "Dessert", "Recipe type visible");

		test.assertSelectorHasText("div.ingredient-row:not(.hidden) div.text", "3/4 cup unsalted butter (1 1/2 sticks), room temperature, plus more for pan and parchment2 1/3 cups cake flour (sifted, then measured) plus more for pan2 1/2 cups fresh blackberries (10 ounces)1/4 cup sugar, plus 1 1/3 cups1 1/2 teaspoons baking powder3/4 teaspoon salt1/2 teaspoon baking soda3 large eggs, room temperature2 teaspoons vanilla extract1 1/2 teaspoons finely grated orange zest1 cup buttermilk, well-shakenpowdered sugar (for dusting)", "Recipe ingredients visible");
		test.assertElementCount("div.ingredient-row:not(.hidden)", 12, "All ingredients visible");

		test.assertSelectorHasText("div.direction-row:not(.hidden) div.recipe-direction", "Position a rack in middle of oven and preheat to 350°. Butter pan; line bottom with a round of parchment paper. Butter parchment. Dust with flour; tap out excess. Arrange berries in a single layer in bottom of pan; sprinkle evenly with 1/4 cup sugar.Sift 2 1/3 cups flour, baking powder, salt, and baking soda into a medium bowl; set aside. Using an electric mixer, beat 3/4 cup butter and remaining 1 1/3 cups sugar in a large bowl at medium-high speed, occasionally scraping down sides of bowl, until pale and fluffy, about 2 minutes. Add eggs, one at a time, beating well after each addition. Beat in vanilla and zest. Reduce speed to low; beat in flour mixture in 3 additions, alternating with buttermilk in 2 additions, beginning and ending with flour mixture and beating just until incorporated. Pour batter over berries in pan; smooth top.Bake until cake is golden brown and a tester inserted into the center comes out clean, about 1 hour 25 minutes. Let cool in pan set on a wire rack for 15 minutes, then run a thin, sharp knife around edge of pan to loosen. Remove pan sides. Invert cake onto rack and remove pan bottom; peel off parchment. Dust top generously with powdered sugar and let cool completely.", "Recipe directions visible");
		test.assertElementCount("div.direction-row:not(.hidden)", 3, "All directions visible");

		test.assertExists("li.original-link:not(.hidden)", "View Original Recipe link visible");
		test.assertNotVisible("section#recipe-photos", "Photo section not visible");
	});
	
	// Test Rating button behavior
	casper.then(function() {
	   this.click("button#dropdownRating");
	   this.click("div.recipe-rating li.rating-item a.star-0");
		test.assertEvalEquals(function() {
			return $("button#dropdownRating span.button-text").html();
		}, "<i class=\"fa fa-star-o\"></i>&nbsp;", "Recipe rating unset");
	});
	casper.wait(1000, function() {
	   this.click("button#dropdownRating");
	   this.click("div.recipe-rating li.rating-item a.star-3");
		test.assertEvalEquals(function() {
			return $("button#dropdownRating span.button-text").html();
		}, "<i class=\"fa fa-star\"></i>&nbsp;<i class=\"fa fa-star\"></i>&nbsp;<i class=\"fa fa-star\"></i>&nbsp;", "Recipe rating updated to 3");
	});
	
	// Test Cuisine button behavior
	casper.then(function() {
	   this.click("button#dropdownCuisine");
	   this.click("div.recipe-cuisine li.cuisine-item a.empty");
	   test.assertSelectorHasText("button#dropdownCuisine span.button-text", "Cuisine", "Recipe cuisine unset");
	});
	casper.wait(1000, function() {
	   this.click("button#dropdownCuisine");
	   this.click("div.recipe-cuisine li.cuisine-item a.European");
	   test.assertSelectorHasText("button#dropdownCuisine span.button-text", "European", "Recipe cuisine updated to European");
	});
	
	// Test Type button behavior
	casper.then(function() {
	   this.click("button#dropdownTypes");
	   this.click("div.recipe-types li.type-item a.empty");
	   test.assertSelectorHasText("button#dropdownTypes span.button-text", "Recipe Type", "Recipe type unset");
	});
	casper.wait(1000, function() {
	   this.click("button#dropdownTypes");
	   this.click("div.recipe-types li.type-item a.Dessert");
	   test.assertSelectorHasText("button#dropdownTypes span.button-text", "Dessert", "Recipe type updated to Dessert");
	});

	// Shopping List
	casper.then(function() {
		this.click("#recipe-settings button");
		this.click("#recipe-settings a.list-add");
	});
	
	casper.wait(1500, function() {
		test.assertSelectorHasText("div.ingredient-list-item span.ingredient-name-label", "3/4 cup unsalted butter2 1/3 cups cake flour2 1/2 cups fresh blackberries1/4 cup sugar1 1/2 teaspoons baking powder3/4 teaspoon salt1/2 teaspoon baking soda3 large eggs2 teaspoons vanilla extract1 1/2 teaspoons finely grated orange zest1 cup buttermilkpowdered sugar (for dusting)", "All ingredients visible for shopping list");
		
		this.click("div.ingredient-list-item:first-of-type input.add-ingredient");
		this.click("div.ingredient-list-item:not(.hidden):last-of-type input.add-ingredient");
		this.click("div.bs-shopping-modal button.btn-confirm");
	});
	
	casper.wait(1500, function() {
		this.click("div.bs-shopping-modal button.btn-cancel");
	});
	
	// Post direct message
	casper.wait(1500, function() {
		this.click("#recipe-messages button");
		this.click("#recipe-messages a.send-message-friend");
	});
	
	casper.wait(1500, function() {
		test.assertVisible("div.bs-message-modal div.friend-list", "Friend list visible in friend message modal");
		test.assertNotVisible("div.bs-message-modal div.tweet-option", "Tweet option not visible in friend message modal");

		this.sendKeys("textarea#new-message", "This is a message to a friend (without an image)", {keepFocus: true});
		this.click("button.add-message");
	});

	casper.waitForSelector("div.bs-message-modal div.message-success:not(.hidden)", function() {
		this.click("div.bs-message-modal button.btn-cancel");
	});
		
	// Post Message
	casper.wait(1500, function() {
		this.click("#recipe-messages button");
		this.click("#recipe-messages a.send-message");
	});
	
	casper.wait(1500, function() {
		test.assertNotVisible("div.bs-message-modal div.friend-list", "Friend list not visible in post message modal");
		test.assertVisible("div.bs-message-modal div.tweet-option", "Tweet option visible in post message modal");
		
		var fs = require('fs');
		var currentFile = require('system').args[3];
		var curFilePath = fs.absolute(currentFile).split('/');
		if (curFilePath.length > 1) {
			curFilePath.pop();
			path = curFilePath.join('/');
			casper.page.uploadFile("input.dz-hidden-input", path + "/cornbread.png");
		}
	});
	casper.waitForSelector("#total-progress i.fa-check:not(.hidden)", function() {
		this.sendKeys("textarea#new-message", "This is a message with an image", {keepFocus: true});
		this.click("input#twitter");
		this.click("button.add-message");
	}, null, 30000);
	
	casper.waitForSelector("div.bs-message-modal div.message-success:not(.hidden)", function() {
		test.assertVisible("div.file-upload", "Image upload reset");
		test.assertDoesntExist("div.upload-preview div#preview-template", "Previous image upload removed");
	});
	
	casper.thenOpen(getServerName() + "friends/messages", function() {
	    this.waitForUrl(new RegExp(/friends\/messages/));
	});

	casper.then(function() {
		this.waitWhileSelector("section#message div.wait-spinner");
	});
	
	casper.then(function() {
	    test.assertVisible("div.message-list-item:first-of-type:not(.hidden) p.comment-icon", "Correct message type (public)");
	    test.assertSelectorHasText("div.message-list-item:first-of-type:not(.hidden) p.message", "This is a message with an image", "Public Message posted successfully");
	    this.click("div.message-list-item:first-of-type:not(.hidden) button.remove-message");
	});

	casper.wait(1500, function() {
	    test.assertVisible("div.message-list-item:first-of-type:not(.hidden) p.direct-icon", "Correct message type (direct)");
	    test.assertSelectorHasText("div.message-list-item:first-of-type:not(.hidden) p.message", "This is a message to a friend (without an image)", "Friend Message posted successfully");
	    this.click("div.message-list-item:first-of-type:not(.hidden) button.remove-message");
	});

	casper.wait(1500, function() {
		this.open(getServerName() + "shoppinglist");
	    this.waitForUrl(new RegExp(/shoppinglist/));
	});

	casper.then(function() {
		this.waitWhileSelector("div.wait-spinner");
	});

	casper.then(function() {
		test.assertField("ingredient-name0", "3/4 cup unsalted butter", "First item added to shopping list");
		test.assertField("ingredient-name1", "powdered sugar (for dusting)", "Second item added to shopping list");
		this.click("button.reset");
	});
	
	casper.wait(1500, function() {
		this.open(getServerName() + "recipe/152");
		this.waitForUrl(new RegExp(/recipe\/152/));
	});

	casper.then(function() {
		this.waitWhileSelector("div.wait-spinner");
	});
	
	casper.then(function() {
		this.capture("recipe.png");
		test.assertVisible("section#recipe-photos", "Photo section visible");
	});
	
	casper.run(function() {
        test.done();
    });
});
