<?php
class RecipeException extends Exception
{
    function __construct($recipe, $message)
    {
        parent::__construct($message);
        $this->baseRecipe = $recipe;
    }

	public $baseRecipe;	
}
?>