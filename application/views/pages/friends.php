<!-- Intro Header -->
<header class="intro">
    <div class="intro-body">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2 greeting">
                    <h1 class='brand-heading'>Friends</h1>
                </div>
            </div>
        </div>
    </div>
</header>

<!-- Add Friend Section -->
<section id="friends" class="container content-section text-dark">
    <div class="container wait-spinner">
        <div class="row">
            <div class="col-sm-12 text-dark text-center wait">
                <i class="fa fa-circle-o-notch fa-spin"></i>
            </div>
        </div>
    </div>
    <div class="container error hidden">
        <div class="row">
            <div class="col-sm-12 text-dark">
                <div class="alert alert-danger" role="alert"></div>
            </div>
        </div>
	</div>
    <div class="container add-friend hidden">
	    <div class="row">
	    	<form id="add-friend">
		        <div class="col-xs-12 col-sm-9">
		            <div class='form-group email-container'>
		            	<label class='sr-only' for='friend-email'>Friend's Email</label>
						<input type="email" class="form-control input-lg friend-email" id='friend-email' placeholder="Email" aria-describedby="helpBlock">
						<span id="helpBlock" class="help-block">Enter your friend's email to add them to your friend list.</span>
		            </div>
		        </div>
		        <div class="col-xs-12 col-sm-3">
		            <div class='add-container'>
						<input type="submit" class="btn btn-lg btn-default add-friend" value="Add Friend">
		            </div>
		        </div>
	        </form>
	    </div>
    </div>
</section>
<!-- Friend List Section -->
<section id="friendList" class="container content-section text-dark">
    <div class="container friends hidden">
	    <div class="row">
	        <div class="col-sm-11 col-sm-offset-1">
	            <div class='friend-list'>
	            </div>
	        </div>
	    </div>
    </div>
</section>

<div class="row friend-list-item hidden">
    <div class="col-xs-2 col-sm-1">
        <i class='fa fa-user'></i>
    </div>
    <div class="col-xs-8">
	    <input type="hidden" id="friendid">
        <p class="username"></p>
        <p class="email text-muted"></p>
    </div>
    <div class="col-xs-2 col-sm-1 text-right">
    	<button class="btn btn-danger remove-friend"><i class="fa fa-trash-o"></i></button>
    </div>
</div>