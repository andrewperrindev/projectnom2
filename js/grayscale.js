/*!
 * Start Bootstrap - Grayscale Bootstrap Theme (http://startbootstrap.com)
 * Code licensed under the Apache License v2.0.
 * For details, see http://www.apache.org/licenses/LICENSE-2.0.
 */

// jQuery to collapse the navbar on scroll
$(window).scroll(function() {
    if ($(".navbar").offset().top > 50) {
        $(".navbar-fixed-top").addClass("top-nav-collapse");
    } else {
        $(".navbar-fixed-top").removeClass("top-nav-collapse");
    }
});

// Closes the Responsive Menu on Menu Item Click
$('.navbar-collapse ul li a').click(function() 
{
    if (!$(this).hasClass("dropdown-toggle"))
    {
        $('.navbar-toggle:visible').click();
    }
});

//ShakeIt Plugin 
jQuery.fn.shakeit = function(intShakes, intDistance, intDuration) {
    this.each(function() {
        //$(this).css("position","relative");
        for (var x=1; x<=intShakes; x++) {
        $(this).animate({left:(intDistance*-1)}, (((intDuration/intShakes)/4)))
    .animate({left:intDistance}, ((intDuration/intShakes)/2))
    .animate({left:0}, (((intDuration/intShakes)/4)));
    }
  });
return this;
};

function initPage_base()
{
    // jQuery for page scrolling feature - requires jQuery Easing plugin
    $('a.page-scroll').bind('click', function(event) {
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: $($anchor.attr('href')).offset().top
        }, 1500, 'easeInOutExpo');
        event.preventDefault();
    });

    if ($('.nav-search').length)
    {
        $('.nav-search').on('shown.bs.dropdown', searchInputFocus);
        $('div.bs-recipeimport-modal .btn-confirm').on('click', doRecipeImport);
        $('div.bs-recipeimport-modal form').on('submit', startRecipeImport);
        $('a.logout').on('click', logout);
    }
    
    $("div.modal").on('shown.bs.modal', function (event) 
    {
	    $("input[data-default='focus']", this).focus();
	});
}

function searchInputFocus(event)
{
	if (!('ontouchstart' in window))
	{
	    $(".nav-search #search-query").focus();
	}
}

function getErrorAlert(alertMessage)
{
    return "<div class='alert alert-danger'>" +
        "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>" +
        alertMessage +
        "</div>";
}

function ajaxGet(url, data)
{
    return $.get(url, data, null, 'json')
		.fail(function(jqxhr, status, error)
		{
			// authentication failure
			if (jqxhr && jqxhr.status == 401)
			{
				document.location.reload(true);
			}
			else
			{
				$("div.error .alert")
					.html("<strong>Something went wrong.</strong> Please try again. If you keep seeing this, let us know.");
				$("div.error")
					.removeClass("hidden");
			}
		});
}

function ajaxPost(url, data)
{
	return $.post(url, data, null, 'json')
		.fail(function()
		{
			$("div.error .alert")
				.html("<strong>Something went wrong.</strong> Please try again. If you keep seeing this, let us know.");
			$("div.error")
				.removeClass("hidden");
			
		});
}

function ajaxPostJson(url, json)
{
    return $.ajax({
        type: "POST",
        url: url,
        data: json,
        dataType: 'json',
        contentType: 'application/json'
    });
}

function ajaxDelete(url, data)
{
    return $.ajax(
        {
            url: url,
            type: 'DELETE',
            dataType: 'text json'
        }
    );
}

function getDayPart()
{
    var d = new Date();
    var h = d.getHours();
    
    if (h >= 4 && h < 12)
    {
        return "morning";
    }
    else if (h >=12 && h < 16)
    {
        return "afternoon";
    }
    else if (h >= 16 && h < 21)
    {
        return "evening";
    }
    else if (h >= 21 || h < 4)
    {
        return "latenight";
    }
    else
    {
        return "undef";
    }
}

function convertDayPartToLabel(dayPart)
{
    if (dayPart == "morning")
    {
        dayLabel = "Breakfast";
    }
    else if (dayPart == "afternoon")
    {
        dayLabel = "Lunch";
    }
    else if (dayPart == "evening")
    {
        dayLabel = "Dinner";
    }
    else if (dayPart == "latenight")
    {
        dayLabel = "Late Night";
    }
    else
    {
        dayLabel = "All";
    }

    return dayLabel;
}

function getGreeting()
{
    var dayPart = getDayPart();
    
    if (dayPart == "morning")
    {
        return "<h1 class='brand-heading'>Good Morning. <small>Breakfast is the most important meal of the day.</small></h1>";
    }
    else if (dayPart == "afternoon")
    {
        return "<h1 class='brand-heading'>Good day. <small>Need a mid-day snack?</small></h1>";
    }
    else if (dayPart == "evening")
    {
        return "<h1 class='brand-heading'>Good evening. <small>No better way to end a day than a homecooked meal.</small></h1>";
    }
    else if (dayPart == "latenight")
    {
        return "<h1 class='brand-heading'>Burning the candle at both ends? <small>Perhaps you need a midnight snack.</small></h1>";
    }
}

function buttonWait(btn, waitOn)
{
    if (waitOn)
    {
	    if ($(btn).data("label") === undefined)
	    {
		    $(btn).data("label", $(btn).text());
	    }
	    
        $(btn).prop('disabled', true);
        $(btn).html("<i class='fa fa-circle-o-notch fa-spin'></i>");
    }
    else
    {
        $(btn).prop('disabled', false);
        $(btn).html($(btn).data("label"));
    }
}

function redirectTo(url)
{
	window.location.href = url;
}

function logout(event)
{
    event.preventDefault();

    ajaxGet("/api/1/user/logout")
        .done(function(data)
            {
                window.location.href = "/";
            });
}

String.prototype.repeat = function(count) 
{
	/* jslint -W030 */
    if (count < 1) return '';
    var result = '', pattern = this.valueOf();
    while (count > 1) {
        if (count & 1) result += pattern;
        count >>= 1, pattern += pattern;
    }
    return result + pattern;
};