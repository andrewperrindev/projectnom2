module.exports = function(config) {
	return {
		postMessage: addMessageBase,
		charCount: charCount,
		dropzoneConfig: { 
			url: "/api/1/messages/image",
			paramName: "userfile",
			maxFilesize: 4,
			uploadMultiple: false,
			maxFiles: 1,
			acceptedFiles: "image/jpeg,image/jpg,image/gif,image/png",
			previewsContainer: "div.upload-preview",
			previewTemplate: $('div.upload-preview-template').html(),
			thumbnailHeight: 75,
			init: config.initDropzone,
			
			fallback: function() {
				$("div#fileUpload").addClass("hidden");
				
				ReactDOM.render(
					React.createElement(BootstrapError,
						{
							visible: true,
							title: "Unfortunately, your browser is too old to support file uploads.", 
							detail: "You can still post a message, but you can't attach any images."
						}
					),
					document.getElementById("fallback")
				);
				
				$("div#fallback").removeClass("hidden");
			}
		}
	};
};

function addMessageBase(messageData)
{
	var errorText = "<strong>Something went wrong.</strong> Please try again. If you keep seeing this, let us know.";

	var newMessage =
	{
		message: messageData.message,
		type: messageData.type,
		tweet: messageData.tweet ? true : false
	};
	
	if (messageData.recipeid > 0)
	{
		newMessage.recipeid = messageData.recipeid;
	}
	if (messageData.to_user !== "")
	{
		newMessage.to_user = messageData.to_user;
	}
	if (messageData.image)
	{
		newMessage.image = messageData.image;
	}

	var defer = $.Deferred();

	ajaxPost("/api/1/messages", newMessage)
		.done(function(data)
		{
			if (data.success === 0)
			{				
				if (data.error == "Message too long.")
				{
					errorText = "<strong>Your message cannot exceed 255 characters.</strong>";
				}
				else if (data.error == "Required information missing.")
				{
					errorText = "<strong>Your message cannot be blank.</strong>";
				}
				
				defer.reject(errorText);
			}
			else
			{
				defer.resolve(data);
			}
		})
		.fail(function(xhr, error) {
			console.log(xhr.responseText);
			defer.reject(errorText);
		});
		
	return defer.promise();
}

function charCount(event)
{
	var $label = $("span.label.char-count");
	var remaining = (255 - $(this).val().length);
	
	if (remaining <= 0)
	{
		$(".btn.add-message").prop("disabled", true);
	}
	else
	{
		$(".btn.add-message").prop("disabled", false);
	}
		
	if (remaining < 25)
	{
		$label.removeClass("label-default label-warning").addClass("label-danger");
	}
	else if (remaining < 155)
	{
		$label.removeClass("label-default label-danger").addClass("label-warning");
	}
	else if (remaining >= 155)
	{
		$label.removeClass("label-warning label-danger").addClass("label-default");
	}
	
	$label.text(remaining);
}