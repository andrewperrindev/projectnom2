var React = require('react');

module.exports = React.createClass({
	render: function() {
		var classes = [(this.props.visible ? "visible" : "hidden")];
		var gridClasses = [];
		
		if (this.props.className)
		{
			$.each(this.props.className.split(" "), function categorize(index, item) {
				if (item.indexOf("col-") >= 0) {
					gridClasses.push(item);
				}
				else {
					classes.push(item);
				}
			});
		}
		
		if (gridClasses.length === 0) {
			gridClasses.push("col-xs-12");
		}
		
		return React.DOM.div({className: classes.join(" ")},
			React.DOM.div({className: "row"},
				React.DOM.div({className: "text-dark " + gridClasses.join(" ")},
					React.DOM.div({className: "alert alert-danger", role: "alert"},
						React.DOM.b({style: {paddingRight: 10}}, this.props.title),
						this.props.detail
					)
				)
			)
		);
	}
});
