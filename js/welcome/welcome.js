$(function() {
	initPage_base();
	initPage();
});

function initPage()
{
	$(".btn-login").on("click", welcomeLoginHandler);
	$(".login-modal").on("click", modalClickHandler);

	$("input,select,textarea").not("[type=submit]").jqBootstrapValidation({
        preventSubmit: true,
        submitError: function ($form, event, errors) { 
			$("section#signup .help-block").removeClass('invisible');
		},
        submitSuccess: createUserSubmit
    });
    
    if ($(".bs-login-modal input#returnUrl").val() !== "" && window.location.href.indexOf("#signup") == -1)
    {
	    modalClickHandler();
    }
}

function createUserSubmit($form, event)
{
    event.preventDefault(); // prevent default submit behaviour

	buttonWait($('#signupForm .btn-primary'), true);

    // get values from FORM
    var username = $("#signupForm #username").val();
    var email = $("#signupForm #email").val();
    var password = $("#signupForm #password").val();
    var password_confirm = $("#signupForm #password_confirm").val();

	var post_data = 
		{
            username: username,
            email: email,
            password: password,
            password_confirm: password_confirm
        };

    ajaxPost("/api/1/user/create", post_data)
		.done(function(data) { // success callback
            if (data.success == 1)
            {
                // Success message
                $('#success').html("<div class='alert alert-success'>");
                $('#success > .alert-success').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                    .append("</button>");
                $('#success > .alert-success')
                    .append("<p><strong>Your account has been created!</strong></p><p>We've sent an email to the address you provided. Please follow the instructions there to finish setting up your account.</p>");
                $('#success > .alert-success')
                    .append('</div>');

                //clear all fields
                $('#signupForm').trigger("reset");
            }
            else
            {
                // Fail message
                $('#success').html(getErrorAlert(data.error));
            }
		})
		.fail(function() 
		{
            // Fail message
            $('#success').html(getErrorAlert("<strong>Account creation error occurred.</strong> Please contact <a href='mailto:support@projectnom.com'>ProjectNom support</a> for assistance."));
		})
		.always(function()
		{
			buttonWait($('#signupForm .btn-primary'), false);
		});
}

function modalClickHandler()
{
	$(".bs-login-modal").modal('toggle');
	
}
function welcomeLoginHandler(event)
{
	modalLoginHandler(event)
		.done(function(data) { // success callback
			if (data.success == 1)
			{
				var returnUrl = $(".bs-login-modal input#returnUrl").val();
				window.location.href = (returnUrl !== "") ? returnUrl : "/home";
			}
		});
}