<?php
	$protocol = (!empty($_SERVER['HTTPS']) ? "https" : "http");
	$activation_link = "$protocol://" . $_SERVER['SERVER_NAME'] . "/welcome/forgot/$key";
?>
<html>
	<body>
		<table style="padding: 5px;">
			<tr>
				<td style="padding: 15px; text-align: center; font-size: 30px; background-color: #454545; background-image: url('<?php print $protocol; ?>://projectnom.com/img/email-bg.jpg'); background-repeat: no-repeat; color: white;">
					<b style="color: lightgray;">PROJECT</b>NOM
				</td>
			</tr>
			<tr>
				<td style="text-align: center; vertical-align: middle; padding: 15px;"><h2>Reset Password</h2></td>
			</tr>
			<tr>
				<td colspan='2' style="padding: 15px; font-size: 14pt;">
					<p>We received a request to reset your password at ProjectNom.</p>
					<p>If it was you, follow the link below to set a new password for your account. If it wasn't you, just ignore this email.</p>
					<p><a href="<?php print $activation_link; ?>"><?php print $activation_link; ?></a></p>
				</td>
			</tr>
		</table>
	</body>
</html>