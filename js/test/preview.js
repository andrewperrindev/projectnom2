casper.test.begin('ProjectNom properly displays recipe preview', 7, 
function suite(test) 
{
	var url = "preview/218";
	
	// Navigate to recipe preview
    casper.start(getServerName() + url, function() {
	    this.waitForUrl(new RegExp(url));
	});
	
	casper.then(function() {
	    test.assertTitle("ProjectNom", "Preview page title is the one expected");
	    test.assertSelectorHasText("h1.brand-heading", "Spinach Basil Pesto", "Recipe title loaded");
	    test.assertSelectorHasText("span.recipe-time", " Total: 10 min;  Prep: 10 min", "Total time loaded");
	    test.assertSelectorHasText("span.recipe-servings", "2 Cups", "Servings loaded");
	    
	    test.assertEval(function() {
            return __utils__.findAll("div.ingredient-row:not(.hidden)").length == 10;
        }, "10 Ingredients Displayed");

	    test.assertEval(function() {
            return __utils__.findAll("div.recipe-direction:not(.hidden)").length == 1;
        }, "1 Direction Displayed");
    });
    
    casper.thenOpen(getServerName() + "preview/236", function() {
	    this.waitForUrl(new RegExp(/bhg\.com/));
	    test.assertTitle("Sesame Garlic Beef Tacos", "Preview redirects to external site");
	});

    casper.run(function() {
        test.done();
    });
});