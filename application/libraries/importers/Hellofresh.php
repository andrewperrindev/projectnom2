<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Hellofresh
{
	private $CI;
	private $content;

	public function __construct($params)
    {
    	$this->CI =& get_instance();

    	if (!empty($params['content']))
    	{
    		$this->content = $params['content'];
    	}
    }

    public function getTitle()
    {
	    $title = $this->returnMatch("/<meta data-react-helmet=true property=og:title content=\"(.+)\">/imsU", $this->content);
		return $title;
    }

    public function getAuthor()
    {
		return "HelloFresh";
    }

    public function getTotalTime()
    {
	    return;
    }

    public function getPrepTime()
    {
		$prepTime = $this->returnMatch("/Preparation Time<\/span><\/div><div class=[^\s]+ data-reactid=[0-9]+>(.+)<\/div>/imsU", $this->content);
		return $prepTime;
    }

    public function getServings()
    {
		return "2 Servings";
    }

    public function getImage()
    {
    	$imgUrl = $this->returnMatch("/<meta data-react-helmet=true property=og:image content=\"(.+)\">/imsU", $this->content);
    	return $imgUrl;
    }

    public function getIngredients()
    {
    	$this->CI->load->helper("html2text");

    	if (!empty($this->content))
    	{
    		preg_match("/<div class=\"_dcijoc[^\"]+\" data-reactid=[0-9]+>(.+)<\/div><\/div><\/div>/imsU",
    			$this->content, $matches);
    		//return print_r($matches, true);

    		if (!empty($matches[1]))
    		{
    			$result = array();
    			preg_match_all("/<div class=[^\s]+ data-reactid=[0-9]+>(<p class=[^\s]+ data-reactid=[0-9]+>.+<\/p><p class=[^\s]+ data-reactid=[0-9]+>.+<\/p>)/imsU", trim($matches[1]), $ingredients);
				//return print_r($ingredients, true);
				
    			for ($i = 0; $i < count($ingredients[1]); $i++)
    			{
	    			$ingredient = $ingredients[1][$i];
	    			$ingredient = str_replace("</p><p", "</p>&nbsp;<p", $ingredient);
    				$result[] = strtolower(convert_html_to_text($ingredient));
    			}
    			return $result;
    		}
    	}
    }

    public function getDirections()
    {
        $this->CI->load->helper("html2text");
        $result = array();

        if (!empty($this->content))
        {
            preg_match("/<div class=\"_1yujhh8 [^\"]+\" data-reactid=[0-9]+>(.+)<\/div><\/div><\/div><\/div><\/div><\/div>/imsU", $this->content, $matches);
            //return print_r($matches, true);

            if (!empty($matches[1]))
            {
                preg_match_all("/<div class=[^\s]+ data-reactid=[0-9]+><span class=[^\s]+ data-reactid=[0-9]+><span class=[^\s]+ data-reactid=[0-9]+>[0-9]+<\/span><\/span>(<span class=[^\s]+ data-reactid=[0-9]+>.+<\/span>)/imsU", $matches[1], $directions);

                foreach ($directions[1] as $direction)
                {
                    $result[] = convert_html_to_text($direction);
                }
            }
        }

        return $result;
    }

    private function returnMatch($pattern, $content)
    {
    	if (!empty($content))
    	{
			preg_match($pattern, $content, $matches);

			if (!empty($matches[1]))
			{
				return trim($matches[1]);
			}
		}

		return "";
    }
}
?>