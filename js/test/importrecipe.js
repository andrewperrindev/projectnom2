casper.test.begin('Import Recipe Tests', 22,
function suite(test) 
{
	// Login and navigate to messages page. +4 test cases
	login_mageuzi(test, "home/");
	
	casper.then(function() {
		this.click("li#recipeNav > a");
		this.click("li#importNav > a");
		
		this.fillSelectors("form#import", {
			'input.import-recipe-url-display': 'http://food52.com/recipes/21599-salmon-with-a-thai-curry-sauce'
		}, true);
		
		this.waitForUrl(/\/recipe\/edit/);
	});

	casper.then(function() {
		this.waitForSelector("input.recipe-title");
	});
	
	casper.then(function() {
		test.assertField('title', 'Salmon with a Thai Curry Sauce Recipe on Food52', "Recipe title auto-imported");
		this.fillSelectors("form#editRecipeForm", {
			'input.recipe-title': 'Salmon with a Thai Curry Sauce'
		}, false);
		this.click("button.btn-importingredients");
		this.wait(1500);
	});
	
	// ingredients
	casper.thenEvaluate(function() {
		var iframe = document.getElementById("recipe");
		var range = rangy.createIframeRange(iframe);
		var ingredients = iframe.contentWindow.document.getElementsByClassName('recipe-list')[0];
		range.selectNodeContents(ingredients);
		var sel = rangy.getIframeSelection(iframe);
		sel.setSingleRange(range);
	});

	casper.then(function() {
		this.wait(1500);
	});
	
	casper.then(function() {
		this.click("div.bs-import-modal button.btn-confirm");
		this.wait(1500);
	});

	casper.then(function() {
		this.waitForSelector("div.ingredient-collection div.ingredient-row");
	});
	
	casper.then(function() {
		this.captureSelector("ingredients.png", "div.ingredient-collection");
		
		test.assertEval(function() {
            return __utils__.findAll("div.ingredient-collection div.ingredient-row").length === 17;
        }, "All ingredients successfully imported");

		test.assertEval(function() {
			var ingredient = "", fields = 0;
			
            $("input", $("div.ingredient-collection div.ingredient-row").first())
				.each(function(i) 
				{
					var id = $(this).attr("id");

					if (id == "quantity" || id == "unit" || id == "ingredient" || id == "preparation") {
						ingredient += $(this).val();
						fields++;
					}
				});

			return (ingredient == "1poundsalmon fillet(preferably wild caught), skin removed") && (fields == 4);
        }, "First ingredient imported correctly");
	});

	// directions
	casper.then(function() {
		this.click("button.btn-importdirections");
		this.wait(1500);
	});

	casper.thenEvaluate(function() {
		var iframe = document.getElementById("recipe");
		var range = rangy.createIframeRange(iframe);
		var directions = iframe.contentWindow.document.getElementsByTagName('ol')[0];
		range.selectNodeContents(directions);
		var sel = rangy.getIframeSelection(iframe);
		sel.setSingleRange(range);
		//console.log(sel.toString());
	});

	casper.then(function() {
		this.wait(1500);
	});
	
	casper.then(function() {
		this.click("div.bs-import-modal button.btn-confirm");
		this.wait(1500);
	});
	
	casper.then(function() {
		this.waitForSelector("div.direction-collection div.direction-row");
	});
	
	casper.then(function() {
		this.captureSelector("directions.png", "div.direction-collection");
		
		test.assertEval(function() {
            return __utils__.findAll("div.direction-collection div.direction-row").length === 5;
        }, "All directions successfully imported");
        
		test.assertEval(function() {
			var direction = $("textarea", $("div.direction-collection div.direction-row").first()).val();
			
			return (direction == "In a deep stovetop pan that will snugly fit the salmon (you may want to cut the fillet into two pieces), combine the wine with 1 cup water, the slice of ginger, chopped lemongrass, and 2 tablespoons of chopped green onion, plus a good pinch of salt. Bring to a boil, then turn down to a bare simmer. Slip in the salmon and cook until it is just barely cooked (it should be rare in the center). Remove the salmon from the poaching liquid and cut it into approximately 1-inch chunks. Set aside.");
        }, "First direction imported correctly");

	});
	
	// recipe comments
	/*
	casper.thenEvaluate(function() {
		$("textarea.recipe-comments").popover('show');
	});
	*/
	casper.then(function() {
		this.click("div#notesPlaceholder p.content");
	});
	
	casper.then(function() {
		this.wait(1500);
	});

	casper.then(function() {
		this.click("a#importmodalbtn");
	});

	casper.then(function() {
		this.wait(1500);
	});
	
	casper.thenEvaluate(function() {
		var iframe = document.getElementById("recipe");
		var range = rangy.createIframeRange(iframe);
		var comments = iframe.contentWindow.document.getElementsByClassName('recipe-note')[0];
		range.selectNodeContents(comments);
		var sel = rangy.getIframeSelection(iframe);
		sel.setSingleRange(range);
		//console.log(sel.toString());
	});

	casper.then(function() {
		this.wait(1500);
	});
	
	casper.then(function() {
		this.click("div.bs-import-modal button.btn-confirm");
		this.wait(1500);
	});
	
	casper.then(function() {
		test.assertField('comment', "Author Notes: This is another one of those lovely dishes, originally found in a magazine or newspaper, and adapted over the years to fit various cooks' tastes and needs. In spite of the slightly fussy fish poaching step, it's a remarkably simple dish that bursts with flavor. Sweet, spicy, salty, and funky, it has all the flavors you expect from Thai cuisine, but with salmon being the unexpected star. The flavorful sauce pairs marvelously with a fruity, citrusy Sauvignon Blanc.", "Recipe comment imported successfully");
		
		this.click("button.btn-addingredient");
		test.assertEval(function() {
            return __utils__.findAll("div.ingredient-collection div.ingredient-row").length === 18;
        }, "Add Ingredient button adds new ingredient row after import");

		this.click("button.btn-adddirection");
		test.assertEval(function() {
            return __utils__.findAll("div.direction-collection div.direction-row").length === 6;
        }, "Add Direction button adds new direction row after import");

		this.click("button#editRecipeSubmit");
		this.waitForUrl(/\/recipe\/[0-9]+/);
	});

	casper.then(function() {
		this.waitWhileSelector("div.wait-spinner");
	});

	casper.then(function() {
		this.echo("Check that recipe saved correctly: ");
		
		test.assertTitle("Salmon with a Thai Curry Sauce :: ProjectNom");
		
		test.assertSelectorHasText("h1.brand-heading", "Salmon with a Thai Curry Sauce", "Recipe Title Saved");
		test.assertSelectorHasText("div.recipe-comment", "Author Notes: This is another one of those lovely dishes, originally found in a magazine or newspaper, and adapted over the years to fit various cooks' tastes and needs. In spite of the slightly fussy fish poaching step, it's a remarkably simple dish that bursts with flavor. Sweet, spicy, salty, and funky, it has all the flavors you expect from Thai cuisine, but with salmon being the unexpected star. The flavorful sauce pairs marvelously with a fruity, citrusy Sauvignon Blanc.", "Recipe Comment Saved");

		test.assertDoesntExist("li.original-link.hidden", "View Original Recipe link is not hidden");
		test.assertExists("li.original-link", "View Original Recipe link exists");
		test.assertEquals(this.getElementAttribute("li.original-link a", "href"), "http://food52.com/recipes/21599-salmon-with-a-thai-curry-sauce",
			"Link back to original recipe is correct");
		
		test.assertElementCount("div.ingredient-row:not(.hidden)", 17, "All Ingredients saved");
		test.assertSelectorHasText("div.ingredient-row:not(.hidden):first-of-type", "1 pound salmon fillet (preferably wild caught), skin removed", "First ingredient saved");
		
		test.assertElementCount("div.direction-row:not(.hidden)", 5, "All directions saved");
		test.assertSelectorHasText("div.direction-row:not(.hidden):first-of-type", "In a deep stovetop pan that will snugly fit the salmon (you may want to cut the fillet into two pieces), combine the wine with 1 cup water, the slice of ginger, chopped lemongrass, and 2 tablespoons of chopped green onion, plus a good pinch of salt. Bring to a boil, then turn down to a bare simmer. Slip in the salmon and cook until it is just barely cooked (it should be rare in the center). Remove the salmon from the poaching liquid and cut it into approximately 1-inch chunks. Set aside.", "First direction saved");

		this.click("div#recipe-settings button.dropdown-toggle");
		this.click("div.panel ul.dropdown-menu a.edit");
		this.waitForUrl(/\/recipe\/edit\/[0-9]+/);
	});

	casper.then(function() {
		this.click("button#editRecipeDelete");
		this.click("div.bootbox button.btn-danger");
		this.waitForUrl(getServerName() + "home");
	});
	
    casper.run(function() {
        test.done();
    });
});                                                                              