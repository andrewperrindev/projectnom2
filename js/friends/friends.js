$(function() {
	initPage_base();
	initPage();
});

function initPage()
{
	$("form#add-friend").on("submit", addFriend);
	$("div.friend-list").on("click", "button.remove-friend", removeFriend);

    ajaxGet("/api/1/friends", null)
		.done(loadFriendList)
		.always(function()
		{
			$("div.wait-spinner").remove();
		});
}

function loadFriendList(data)
{
	if (data.success == 1)
	{
		$.each(data.result, function(index, value)
		{
			addFriendRow(value.username, value.email, value.id);
		});

		if (data.result.length === 0)
		{
			$("div.error .alert")
				.html("<strong>You don't have any ProjectNom friends yet.</strong> Adding friends allows you to see messages they post, and receive recipes they share.");
			$("div.error")
				.removeClass("hidden");
		}
		else
		{
			$("section#friends .footer").removeClass("invisible");
			//$("div.friend-list").height($("div.shopping-list").height());
		}
	}

	$("div.add-friend.hidden").removeClass("hidden");
	$("div.friends.hidden").removeClass("hidden");
}

function addFriend(event)
{
	event.preventDefault();
	var $emailInput = $("input.friend-email");
	$emailInput.prop("disabled", true);
	$("input.btn.add-friend").prop("disabled", true);
	var email = $emailInput.val();

	ajaxPost("/api/1/friends", {email: email})
		.done(function(data)
		{
			if (data.success == 1)
			{
				if (data.result.id > 0)
				{
					addFriendRow(data.result.username, email, data.result.id);
				}
				$emailInput.val("");
			}
			else
			{
				if (data.error == "Email does not exist.")
				{
					$("div.error .alert")
						.html("<strong>We couldn't find that email address.</strong> Tell your friend to join ProjectNom, and then try again!");
					$("div.error")
						.removeClass("hidden");
				}
			}
		})
		.always(function(data) 
		{
			$emailInput.prop("disabled", false);
			$("input.btn.add-friend").prop("disabled", false);
		});
}

function removeFriend(event)
{
	event.preventDefault();

	var $friendItem = $(this).closest("div.friend-list-item");
	var friendId = $("input#friendid", $friendItem).val();

	ajaxDelete("/api/1/friends/" + friendId, null)
		.done(function(data)
			{
				if (data.success == 1)
				{
					$friendItem.remove();
				}
			});
}

function addFriendRow(username, email, id)
{
	var $friend = $("div.friend-list-item.hidden").clone().removeClass("hidden");
	$("input#friendid", $friend).val(id);
	$("p.username", $friend).text(username);
	$("p.email", $friend).text(email);
	$("div.friend-list").append($friend);
}