<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

abstract class ApiControllerBase extends CI_Controller 
{
   	public function __construct()
   	{
        parent::__construct();
        
        if ($this->getProtocol() == "https")
        {
	        $this->output->set_header("Strict-Transport-Security: max-age=31536000; includeSubDomains; preload");
	    }

		$this->load->library('session');
		
		if ($this->session->userdata('username') === NULL)
		{
			header('HTTP/1.1 401 Unauthorized', true, 401);
			header('Content-Type: application/json');
			print json_encode(array(
				'success' => 0,
				'error' => 'You are not authorized to access this endpoint.'
			));
			die();
		}
		if (!empty($_SERVER["CONTENT_TYPE"]) && $_SERVER["CONTENT_TYPE"] == "application/json")
		{
			$_POST = array();
			$json_result = json_decode(file_get_contents("php://input"), true);
	
			if (!empty($json_result))
			{
				$_POST = $json_result;
			}
		}
   	}
	
	protected function getProtocol()
	{
		return (!empty($_SERVER['HTTPS']) ? "https" : "http");
	}

	protected function array_random($arr, $num = 1) 
	{
		if (!is_array($arr))
		{
			return $arr;
		}
		
	    shuffle($arr);
	    
	    if ($num >= count($arr))
	    {
		    return $arr;
	    }
	    
	    $r = array();
	    for ($i = 0; $i < $num; $i++) 
	    {
	    	$r[] = $arr[$i];
	    }
	    
	    return $num == 1 ? $r[0] : $r;
	}
}