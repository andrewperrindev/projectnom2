<?php	
	if (isset($error))
	{
		$response = array
		(
			'success' => 0,
			'error' => $error
		);
	}
	else
	{
		$response = array
		(
			'success' => 1
		);

		if (isset($message))
		{
			$response['message'] = $message;
		}
		if (isset($result))
		{
			$response['result'] = $result;
		}
	}
	
	print json_encode($response, JSON_UNESCAPED_UNICODE);
?>