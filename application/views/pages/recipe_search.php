    <!-- Intro Header -->
    <header class="intro">
        <div class="intro-body">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2 greeting">
                        <h1 class='brand-heading'>Recipes <?php if (!empty($query)) { print "<small>$query</small>"; }?></h1>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <!-- Filters -->
    <section id="search-filters" class="container content-section text-dark">
        <div class="row">
	        <div class="col-lg-10 col-lg-offset-1">
	        	<form action='/recipe/search' method="post" id="search-filters">
		        	<div class="form-group">
						<label class="sr-only" for="recipeSearch">Recipe Search</label>
						<div class="input-group">
							<div class="input-group-addon"><i class="fa fa-search"></i></div>
							<input type="text" class="form-control input-lg recipe-search" id="recipeSearch" name='search-query' placeholder="Search" value='<?php if (isset($query)) { print htmlentities($query, ENT_QUOTES); } ?>'>
						</div>
					</div>
                    <input type='hidden' id='search-sort' name='search-sort' value='<?php print $sort; ?>'>
                    <input type='hidden' id='search-cuisine' name='search-cuisine' value='<?php print $cuisine_filter; ?>'>
                    <input type='hidden' id='search-type' name='search-type' value='<?php print $type_filter; ?>'>
                    <input type='hidden' id='search-rating1' name='search-rating1' value='<?php print $rating_filter[1]; ?>'>
                    <input type='hidden' id='search-rating2' name='search-rating2' value='<?php print $rating_filter[2]; ?>'>
                    <input type='hidden' id='search-rating3' name='search-rating3' value='<?php print $rating_filter[3]; ?>'>
                    <input type='hidden' id='search-rating4' name='search-rating4' value='<?php print $rating_filter[4]; ?>'>
                    <input type='hidden' id='search-rating5' name='search-rating5' value='<?php print $rating_filter[5]; ?>'>
		        </form>
	        </div>
        </div>
        <div class="row filters">
	        <div class="col-md-2 col-md-offset-1 col-sm-3 col-xs-6">
		        <small class="filter-label text-muted">Sort</small>
		        <div class="btn-group">
					<button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						<span class='button-label'><?php 
							if ($sort == "title") { print "Title"; }
							else if ($sort == "ingredients") { print "Ingredients"; }
							else if ($sort == "time") { print "Total Time"; }
							else if ($sort == "rating") { print "Rating"; }
							else if ($sort == "new") { print "Recent"; }
							else if ($sort == "different") { print "Different"; }
							else { print "Sort"; }
						?></span> <span class="caret"></span>
					</button>
					<ul class="dropdown-menu">
                        <li><a href="#" class="sort-title" data-input="#search-sort" data-label="Title" data-value="title">Title</a></li>
                        <li><a href="#" class="sort-ingredients" data-input="#search-sort" data-label="Ingredients" data-value="ingredients">Number of Ingredients</a></li>
                        <li><a href="#" class="sort-time" data-input="#search-sort" data-label="Total Time" data-value="time">Total Time</a></option>
                        <li><a href="#" class="sort-rating" data-input="#search-sort" data-label="Rating" data-value="rating">Rating, High to Low</a></li>
                        <li><a href="#" class="sort-recent" data-input="#search-sort" data-label="Recent" data-value="new">Recently Added</a></li>
                        <li><a href="#" class="sort-different" data-input="#search-sort" data-label="Different" data-value="different">Different (Not Viewed Recently)</a></li>
					</ul>
				</div>
	        </div>
	        <div class="col-md-2 col-sm-3 col-xs-6">
		        <small class="filter-label text-muted">Cuisine</small>
		        <div class="btn-group">
					<button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						<span class='button-label'><?php 
							if (empty($cuisine_filter)) { print "Any"; }
							else { print $cuisine_filter; }
						?></span> <span class="caret"></span>
					</button>
					<ul class="dropdown-menu">
						<li><a href="#" data-input="#search-cuisine" data-label="Any" data-value="">Any</a></li>
                        <?php
                            foreach ($cuisines as $cuisine)
                            {
                            	?>
                            	<li><a href="#" data-input="#search-cuisine" data-label="<?php print $cuisine; ?>" data-value="<?php print $cuisine; ?>"><?php print $cuisine; ?></a></li>
                            	<?php
	                        }
	                    ?>
					</ul>
				</div>
	        </div>
	        <div class="col-md-2 col-sm-3 col-xs-6">
		        <small class="filter-label text-muted">Type</small>
		        <div class="btn-group">
					<button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						<span class='button-label'><?php 
							if (empty($type_filter)) { print "Any"; }
							else { print $type_filter; }
						?></span> <span class="caret"></span>
					</button>
					<ul class="dropdown-menu">
						<li><a href="#" data-input="#search-type" data-label="Any" data-value="">Any</a></li>
                        <?php
                            foreach ($types as $type)
                            {
                            	?>
                            	<li><a href="#" data-input="#search-type" data-label="<?php print $type; ?>" data-value="<?php print $type; ?>"><?php print $type; ?></a></li>
                            	<?php
	                        }
	                    ?>
					</ul>
				</div>
	        </div>
	        <div class="col-md-2 col-sm-3 col-xs-6">
		        <small class="filter-label text-muted">Rating</small>
		        <div class="btn-group">
					<button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						<span class='button-label'><?php
							$active_ratings = array_keys($rating_filter, "1");

							if (count($active_ratings) == 5)
							{
								print "All";
							}
							else if (count($active_ratings) == 0)
							{
								print "Not Rated";
							}
							else
							{								
								if (count($active_ratings) == 1)
								{
									print str_repeat("<i class='fa fa-star'></i>&nbsp;", $active_ratings[0]);
								}
								else
								{
									print "Multiple";
								}
							}
							?></span> <span class="caret"></span>
					</button>
					<ul class="dropdown-menu rating-filters">
                        <?php
                            for ($rating = 1; $rating <= 5; $rating++)
                            {
                                $rating_stars = str_repeat("<i class='fa fa-star'></i>&nbsp;", $rating);
                                //$rating_stars = $rating;
                            	?>
                            	<li><a href="#" class="rating$rating" data-input="#search-rating<?php print $rating; ?>" data-label="<?php print $rating_stars; ?>" data-value="1"><input type='checkbox' class='<?php print $rating; ?>' <?php if ($rating_filter[$rating] != "-1") { print "checked='checked'"; }?>>&nbsp; <?php print $rating_stars; ?></a></li>
                            	<?php
	                        }
	                    ?>
					</ul>
				</div>
	        </div>
	        <div class="col-md-2 col-sm-12 col-xs-12">
		        <small class="filter-label text-muted">&nbsp;</small>
		        <button type="button" class="btn btn-primary btn-sm filter-submit extra-padding pull-right" data-label="Apply">Apply</button>
	        </div>
        </div>
    </section>

	<!-- Search Section -->
    <section id="search" class="container content-section text-dark">
        <input type='hidden' id='current-query' value='<?php if (!empty($query)) { print htmlentities($query, ENT_QUOTES); }?>'>
	    <div class="wait-spinner">
	        <div class="row">
	            <div class="col-sm-12 text-dark text-center wait">
	                <i class="fa fa-circle-o-notch fa-spin"></i>
	            </div>
	        </div>
	    </div>
	    <div class="error hidden">
	        <div class="row">
	            <div class="col-sm-12 text-dark">
	                <div class="alert alert-danger" role="alert"></div>
	            </div>
	        </div>
		</div>
        <div class="search-results hidden">
    		<div class="row">
    			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 invisible">
                    <div class="col-lg-3 col-md-4 col-sm-3 col-xs-3 recipe-image">
                        <a href="#" class="thumbnail pull-right">
                            <img src="" alt="">
                        </a>
                    </div>
                    <div class="col-lg-9 col-md-8 col-sm-9 col-xs-9">
                        <div class="row">
                            <div class="col-lg-8 col-md-11 col-sm-11 col-xs-11 recipe-detail">
                                <h4>
                                    <a href=""></a>
                                </h4>
                                <p class='comment'></p>
                            </div>
                            <div class="col-lg-4 col-md-11 col-sm-11 col-xs-11 recipe-props">
	                            <div class="row">
		                            <div class="col-xs-12 col-sm-6 hidden">
										<span class="recipe-rating"></span>
		                            </div>
		                            <div class="col-xs-12 col-sm-6 hidden">
										<span class="recipe-servings"></span>
		                            </div>
		                            <div class="col-xs-12 col-sm-6 hidden">
										<span class="recipe-time"></span>
		                            </div>
		                            <div class="col-xs-12 col-sm-6 hidden">
										<span class="recipe-ingredients"></span>
		                            </div>
	                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Pagination -->
    <section id="search-pagination" class="container content-section text-dark">
        <div class="pagination-bar text-center">
            <input type='hidden' id='current-page' value='<?php print $page; ?>'>
            <nav>
                <ul class="pagination pagination-lg">
                </ul>
            </nav>
        </div>
    </section>