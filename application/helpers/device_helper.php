<?php
	function getDevice()
	{
		if (stripos($_SERVER['HTTP_USER_AGENT'],"iPod"))
		{
			return "ipod";
		}
		else if (stripos($_SERVER['HTTP_USER_AGENT'],"iPhone"))
		{
			return "iphone";
		}
		else if (stripos($_SERVER['HTTP_USER_AGENT'],"iPad"))
		{
			return "ipad";
		}
		else if (stripos($_SERVER['HTTP_USER_AGENT'],"Android"))
		{
			return "android";
		}
		
		return "other";
	}
	
	function isIosDevice()
	{
		$device = getDevice();
		return ($device == "ipod" || $device == "iphone" || $device == "ipad");
	}
?>