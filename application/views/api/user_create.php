<?php
	if (isset($error))
	{
		$result = array
		(
			'success' => 0,
			'error' => $error
		);
	}
	else
	{
		$result = array
		(
			'success' => 1,
			'username' => $username,
			'email' => $email
		);
	}
	
	print json_encode($result, JSON_UNESCAPED_UNICODE);
?>