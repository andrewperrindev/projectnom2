function doRecipeImport()
{
	var $urlInput = $('div.bs-recipeimport-modal input#import-recipe-url');
	var $urlDisplay = $('div.bs-recipeimport-modal input.import-recipe-url-display');
	$urlInput.val($urlDisplay.val());
    buttonWait($('div.bs-recipeimport-modal .btn-primary'), true);
    $urlDisplay.prop("readonly", true);

    var post_data = 
    {
        url: $urlDisplay.val()
    };

    ajaxPost("/api/1/recipes/import", post_data)
        .done(function(data)
            {
                if (data.success == 1)
                {
                    $('div.bs-recipeimport-modal form').attr("action", "/recipe/edit/" + data.result);
                }
                else if (data.title !== "")
                {
	                $("div.bs-recipeimport-modal input#import-recipe-title").val(data.title);
                }
                
                if (data.url !== "")
                {
					$urlInput.val(data.url);
                }

				$('div.bs-recipeimport-modal form').off('submit');
                $('div.bs-recipeimport-modal form').submit();
            })
        .fail(function(data)
			{
				buttonWait($('div.bs-recipeimport-modal .btn-primary'), false);
			    $urlDisplay.prop("readonly", false);
	        });
}

function startRecipeImport(event)
{
	event.preventDefault();
	$('div.bs-recipeimport-modal .btn-confirm').click();
}