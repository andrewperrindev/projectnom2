var pnHome = (function($) {
	var React = require('react');
	var ReactDOM = require('react-dom');
	var PN = require('pn');
	
	// Custom React Component
	var RecipeTabs = require("../components/RecipeTabs.js");

	var publicApi = {
		init: initPage
	};

	return publicApi;

	// ---------------------
	
	function initPage()
	{
		$("div.greeting").html(getGreeting());
		$("form.recipe-search input").focus();
	
		ReactDOM.render(
		  React.createElement(RecipeTabs, {dayParts: PN.dayParts}),
		  document.getElementById('recipeTabs')
		);
	}
})(jQuery);

$(function() {
	initPage_base();
	pnHome.init();
});