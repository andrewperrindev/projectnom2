    <!-- Intro Header -->
    <header class="intro">
        <div class="intro-body">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2 greeting">
                        <h1 class='brand-heading'><?php print ($recipeid > 0) ? "Edit" : "New"; ?> Recipe</h1>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <!-- Recipe Header Section -->
    <form action="/api/1/recipes/<?php print $recipeid; ?>" id='editRecipeForm' method='post' data-return="/recipe/">
    <section id="header" class="container content-section text-dark">
        <input type='hidden' id='recipe-id' name='recipe-id' value='<?php print $recipeid; ?>'>
        <input type='hidden' id='recipe-img' name='recipe-img' value=''>
        <div class="container wait-spinner">
            <div class="row">
                <div class="col-sm-12 text-dark text-center wait">
                    <i class="fa fa-circle-o-notch fa-spin"></i>
                </div>
            </div>
        </div>
        <?php
    	if ($recipeid > 0 && $is_new)
    	{
	    ?>
	    <div class="saved-msg hidden">
	    	<div class="row">
		    	<div class="col-sm-12 text-dark">
			    	<div class="alert alert-success" role="alert"><i class='fa fa-check'></i> &nbsp;<strong>Recipe successfully imported and saved!</strong></div>
		    	</div>
	    	</div>
	    </div>
	    <?php
		}
	    if ($is_import)
	    {
		?>
    	<div class="import-msg hidden">
			<div class="row">
            	<div class="col-sm-12 text-dark">
                	<div class="alert alert-info" role="alert"><p><i class='fa fa-info-circle'></i> &nbsp;<strong>Details missing or incorrect?</strong> Click the field you'd like to modify, then click 'Import'.</p><?php
	                	if (!empty($page_title))
	                	{
		                	?><p><small>Want to help make importing recipes easier? <a href="mailto:feedback@projectnom.com">Send us the URL for this recipe</a>, and we'll make sure more details get filled in automatically next time!</small></p><?php
			            }?>
			    	</div>
            	</div>
        	</div>
		</div>
		<?php		    
	    }
	    ?>
    	<div class="error<?php if (empty($error)) {?> hidden<?php } ?>">
			<div class="row">
            	<div class="col-sm-12 text-dark">
                	<div class="alert alert-danger" role="alert"><?php if (!empty($error)) { print $error; }?></div>
            	</div>
        	</div>
		</div>
        <div class="recipe-header invisible">
	        <div class="row">
		        <div class="col-xs-12 col-md-offset-2 col-md-8">
	                <div id="titlePlaceholder" data-pagetitle="<?php if (!empty($page_title)) { print $page_title; } ?>"></div>
	                <div id="byAuthor" class="text-center">
	                	<span style="padding-left: 12px;">by</span>
	                	<div id="authorPlaceholder" style="display: inline-block;"></div>
	                </div>
		        </div>
	        </div>
            <div class="row">
                <div class="col-xs-12 col-md-offset-3 col-md-6 recipe-img">
                    <div class="thumbnail">
                        <img src='/img/edit-placeholder.png' class='recipe-thumb'>
                        <div class='caption actions'>
                            <button class="btn btn-default upload-modal" type="button" data-toggle="modal" data-target="div.bs-upload-modal">Upload</button>
                            <?php
                            if ($is_import)
                            {
                                ?>
                                <button type='button' class="btn btn-default import-image-modal" data-toggle="modal" data-target="div.bs-import-image-modal"><i class='fa fa-import'></i>&nbsp;Import...</button>
                                <?php
                            }
                            ?>
                        </div>
                    </div>
                </div>
                <!--
                <div class="col-xs-12 col-md-offset-1 col-md-5 recipe-comment">
                    <textarea type="text" name="title" class="recipe-title form-control text-dark has-popup" rows='2' placeholder="Recipe Title" data-label="Title"></textarea>
                    <textarea type="text" name="comment" class="recipe-comments form-control text-dark has-popup" placeholder="Additional Notes" data-label="Notes"></textarea>
                </div>
                -->
            </div>
            
            <div class="row">
    			<div class="col-xs-12 col-sm-6 col-sm-offset-1 recipe-stats">
	    			<div class="panel panel-default">
		    			<div class="panel-heading">
							<h6>At a Glance</h6>
      					</div>
		  				<div class="panel-body">
			  				<div class="row">
				  				<div class="col-xs-12 col-lg-12">
  					                <div id="notesPlaceholder"></div>
				  				</div>
			  				</div>
			  				<div class="row">
			  					<div class="col-xs-12 servings-metadata">
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class='fa fa-pie-chart'></i> Yield</div>
                                        <!--<input type="text" class="form-control servings has-popup" id="servings" name="servings" placeholder="Servings" data-label="Servings">-->
                                        <div id="yieldPlaceholder" class="label-addon"></div>
                                    </div>
				  				</div>
			  					<div class="col-xs-12 time-metadata">
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class='fa fa-clock-o'></i> Prep</div>
                                        <div id="prepPlaceholder" class="label-addon"></div>
                                        <!--<input type="text" class="form-control preptime has-popup" id="prep-time" name="prep-time" placeholder="Prep" data-label="Prep Time">-->
                                    </div>
                                    <div class="input-group">
	                                    <div class="input-group-addon"><i class='fa fa-clock-o'></i> Total</div>
	                                    <div id="totalPlaceholder" class="label-addon"></div>
                                        <!--<input type="text" class="form-control total has-popup" id="total-time" name="total-time" placeholder="Total" data-label="Total Time">-->
                                    </div>
							  	</div>
	      					</div>
	  					</div>
	  				</div>
    			</div>
				<div class="col-xs-12 col-sm-4 recipe-tags">
					<div class='panel panel-default'>
		    			<div class="panel-heading">
							<h6>Tags</h6>
      					</div>
						<div class='panel-body'>
							<div class="row">
								<div class="col-xs-12 recipe-user-metadata">
                                    <input type='hidden' name='rating' id='inputRating'>
                                    <input type='hidden' name='cuisine' id='inputCuisine'>
                                    <input type='hidden' name='type' id='inputRecipeType'>
									<div class="col-xs-12 text-center">
										<div class='dropdown recipe-rating'>
											<button class='btn btn-default dropdown-toggle' type='button' id='dropdownRating' data-toggle='dropdown' aria-expanded='false'>
												<span class='button-text'><i class='fa fa-star-o'></i>&nbsp;</span>
												<span class='caret'></span>
											</button>
											<ul class='dropdown-menu' role='menu' aria-labelledby='dropdownRating'>
												<li role='presentation' class='rating-item'>
													<a role='menuitem' class='star-0' data-value='0' tabindex='-1' href='#'><i class='fa fa-star-o'></i>&nbsp;</a>
												</li>
											<?php
												for($i = 1; $i <= 5; $i++)
												{
											?>
													<li role='presentation' class='rating-item'><a role='menuitem' class='star-<?php print $i; ?>' data-value='<?php print $i; ?>' tabindex='-1' href='#'>
													<?php print str_repeat("<i class='fa fa-star'></i>&nbsp;", $i); ?>
													</a></li>
											<?php
												}
											?>
											</ul>
										</div>
									</div>
									<div class="col-xs-12 text-center">
										<div class='dropdown recipe-cuisine'>
											<button class='btn btn-default dropdown-toggle' type='button' id='dropdownCuisine' data-toggle='dropdown' aria-expanded='false'>
												<span class='button-icon'><i class='fa fa-globe'></i>&nbsp;</span>
												<span class='button-text'><em>Cuisine</em></span>
												<span class='caret'></span>
											</button>
											<ul class='dropdown-menu' role='menu' aria-labelledby='dropdownCuisine'>
												<li role='presentation' class='cuisine-item'>
													<a role='menuitem' class='empty' data-value='' tabindex='-1' href='#'>&nbsp;</a>
												</li>
											<?php
												for($i = 0; $i < count($cuisines); $i++)
												{
											?>
													<li role='presentation' class='cuisine-item'><a role='menuitem' class='<?php print $cuisines[$i]; ?>' data-value='<?php print $cuisines[$i]; ?>' tabindex='-1' href='#'>
													<?php print $cuisines[$i]; ?>
													</a></li>
											<?php
												}
											?>
											</ul>
										</div>
									</div>
									<div class="col-xs-12 text-center">
										<div class='dropdown recipe-types'>
											<button class='btn btn-default dropdown-toggle' type='button' id='dropdownTypes' data-toggle='dropdown' aria-expanded='false'>
												<span class='button-icon'><i class='fa fa-tag'></i>&nbsp;</span>
												<span class='button-text'><em>Recipe Type</em></span>
												<span class='caret'></span>
											</button>
											<ul class='dropdown-menu' role='menu' aria-labelledby='dropdownTypes'>
												<li role='presentation' class='type-item'>
													<a role='menuitem' class='empty' data-value='' tabindex='-1' href='#'>&nbsp;</a>
												</li>
											<?php
												for($i = 0; $i < count($types); $i++)
												{
											?>
													<li role='presentation' class='type-item'><a role='menuitem' class='<?php print $types[$i]; ?>' data-value='<?php print $types[$i]; ?>' tabindex='-1' href='#'>
													<?php print $types[$i]; ?>
													</a></li>
											<?php
												}
											?>
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
            
        </div>
    </section>

    <section id="ingredients" class="container text-dark sortable invisible">
        <div class="ingredient-collection">
            <div class="fixed-placeholder"></div>
        </div>
        <div class="row">
            <div class="col-sm-12 text-center section-controls">
                <!--<input type="hidden" class="ingredient-seq" value='0'>-->
                <button type='button' class="btn btn-default btn-addingredient"><i class='fa fa-plus'></i>&nbsp;Add Ingredient</button>
                <?php
                if ($is_import)
                {
                    ?>
                    <button type='button' class="btn btn-default btn-importingredients" data-parent="ingredients" data-label="Ingredients"><i class='fa fa-import'></i>&nbsp;Import Ingredients...</button>
                    <?php
                }
                ?>
            </div>
        </div>
    </section>

    <section id="directions" class="container content-section text-dark sortable invisible">
        <div class="direction-collection">
            <div class="fixed-placeholder"></div>
        </div>
        <div class="row">
            <div class="col-sm-12 text-center section-controls">
                <!--<input type="hidden" class="ingredient-seq" value='0'>-->
                <button type='button' class="btn btn-default btn-adddirection"><i class='fa fa-plus'></i>&nbsp;Add Direction</button>
                <?php
                if ($is_import)
                {
                    ?>
                    <button type='button' class="btn btn-default btn-importdirections" data-parent="directions" data-label="Directions"><i class='fa fa-import'></i>&nbsp;Import Directions...</button>
                    <?php
                }
                ?>
            </div>
        </div>
    </section>

    <section id="editControls" class="container content-section text-dark invisible">
        <div class="row">
            <div class="col-xs-12">
				<div class='panel panel-default'>
					<div class='panel-body'>
						<!--
						<button type="button" id="editRecipeDelete" class="btn btn-lg btn-danger extra-padding pull-left" data-label="Delete">
							<i class="fa fa-trash visible-xs-inline-block"></i>
							<span class="hidden-xs">Delete</span>
						</button>
						-->
						<div id="deletePlaceholder"></div>
						<div id="savePlaceholder"></div>
						<!--
						<button type="submit" id="editRecipeSubmit" class="btn btn-lg btn-primary extra-padding pull-right" data-label="Save">
							<i class="fa fa-check visible-xs-inline-block"></i>
							<span class="hidden-xs">Save</span>
						</button>
						-->
						<button type="button" id="editRecipeCancel" class="btn btn-lg btn-default extra-padding pull-right" data-label="Save">Cancel</button>
					</div>
				</div>
            </div>
        </div>
    </section>
    </form>

    <div class="ingredient-row full hidden">
        <div class="row">
            <div class="hidden-xs col-sm-1 text-center">
                <input type="hidden" id="type" name="ingredient[type][]" value='i'>
                <span class='handle'><i class="fa fa-bars"></i></span>
            </div>
            <div class="col-xs-10">
                <div class='row'>
                    <div class='col-xs-2'>
                        <input type="text" placeholder="Quantity" id="quantity" name="ingredient[quantity][]" class="form-control quantity" value=''>
                    </div>
                    <div class="col-xs-2">
                        <input type="text" placeholder="Unit" id="unit" name="ingredient[unit][]" class="form-control unit" value=''>
                    </div>
                    <div class="col-xs-4">
                        <input type="text" placeholder="Ingredient" id="ingredient" name="ingredient[name][]" class="form-control ingredient-name" value=''>
                    </div>
                    <div class="col-xs-4">
                        <input type="text" placeholder="Preparation" id="preparation" name="ingredient[preparation][]" class="form-control prep" value=''>
                    </div>
                </div>
            </div>
            <div class="col-xs-1">
                <div class="dropdown">
                    <button class='btn btn-default dropdown-toggle trash' id="ingredientSettings" data-toggle="dropdown" aria-expanded="true">
                        <i class="fa fa-gear"></i>
                        <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu dropdown-menu-right" role="menu" aria-labelledby="ingredientSettings">
                        <li role="presentation"><a role="menuitem" tabindex="-1" class='trash' href="#"><i class="fa fa-trash"></i>&nbsp;Remove</a></li>
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="#" class='to-text'>Convert to Text</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="ingredient-row text-only hidden">
        <div class="row">
            <div class="hidden-xs col-sm-1 text-center">
                <input type="hidden" id="type" name="ingredient[type][]" value='t'>
                <span class='handle'><i class="fa fa-bars"></i></span>
            </div>
            <div class="col-xs-10">
                <input type="text" placeholder="Text" id="text" name="ingredient[text][]" class="form-control text" value='' />
            </div>
            <div class="col-xs-1">
                <div class="dropdown">
                    <button class='btn btn-default dropdown-toggle trash' id="ingredientSettings" data-toggle="dropdown" aria-expanded="true">
                        <i class="fa fa-gear"></i>
                        <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu dropdown-menu-right" role="menu" aria-labelledby="ingredientSettings">
                        <li role="presentation"><a role="menuitem" tabindex="-1" class='trash' href="#"><i class="fa fa-trash"></i>&nbsp;Remove</a></li>
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="#" class='to-ingredient'>Convert to Ingredient</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="ingredient-row parsable hidden">
        <div class="row">
            <div class="hidden-xs col-sm-1 text-center drag-handle">
                <input type="hidden" id="type" name="ingredient[type][]" value='p'>
                <span class='handle'><i class="fa fa-bars"></i></span>
            </div>
            <div class="col-xs-10">
                <input type="text" placeholder="Ingredient" id="rawtext" name="ingredient[rawtext][]" class="form-control raw" value='' />
            </div>
            <div class="col-xs-1 menu">
                <div class="dropdown">
                    <button class='btn btn-default dropdown-toggle trash' id="ingredientSettings" data-toggle="dropdown" aria-expanded="true">
                        <i class="fa fa-gear"></i>
                        <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu dropdown-menu-right" role="menu" aria-labelledby="ingredientSettings">
                        <li role="presentation"><a role="menuitem" tabindex="-1" class='trash' href="#"><i class="fa fa-trash"></i>&nbsp;Remove</a></li>
                    </ul>
                </div>
                <span class="spinner hidden">
                	<i class='fa fa-circle-o-notch fa-spin' style='padding: 10px 12px;'></i>
                </span>
            </div>
        </div>
    </div>

    <div class="direction-row hidden">
        <div class="row">
            <div class="hidden-xs col-sm-2">
                <input type="hidden" id="direction-seq" name="directions[seq][]" value=''>
                <div class='row'>
                    <div class='col-xs-2 col-sm-offset-1'>
                        <span class='handle'><i class="fa fa-bars"></i></span>
                    </div>
                    <div class='col-xs-5 col-sm-offset-3 col-lg-offset-4'>
                        <div class="direction-label direction-number" id="direction-num">
                            <h1 class="direction-text"></h1>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-10">
                <div class="row">
                    <div class="col-xs-10">
                        <textarea class="recipe-direction form-control" rows="3" name="directions[text][]"></textarea>
                    </div>
                    <div class="col-xs-2">
                        <button class='btn btn-danger trash'><i class="fa fa-trash"></i></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
