(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
var React = require('react');

module.exports = React.createClass({
	render: function() {
		var classes = [(this.props.visible ? "visible" : "hidden")];
		var gridClasses = [];
		
		if (this.props.className)
		{
			$.each(this.props.className.split(" "), function categorize(index, item) {
				if (item.indexOf("col-") >= 0) {
					gridClasses.push(item);
				}
				else {
					classes.push(item);
				}
			});
		}
		
		if (gridClasses.length === 0) {
			gridClasses.push("col-xs-12");
		}
		
		return React.DOM.div({className: classes.join(" ")},
			React.DOM.div({className: "row"},
				React.DOM.div({className: "text-dark " + gridClasses.join(" ")},
					React.DOM.div({className: "alert alert-danger", role: "alert"},
						React.DOM.b({style: {paddingRight: 10}}, this.props.title),
						this.props.detail
					)
				)
			)
		);
	}
});

},{"react":"react"}],2:[function(require,module,exports){
var pnMessages = (function($) {
	var React = require('react');
	var ReactDOM = require('react-dom');

	// Custom React Component
	var BootstrapError = require("../components/BootstrapError.js");

	var messageUtils = require("./messageutils.js")({initDropzone: initDropzone});

	var publicApi = {
		init: initPage
	};

	return publicApi;

	// ---------------------

	function initDropzone()
	{
		this
		.on("addedfile", function(file) 
			{
				$('.btn-confirm').prop('disabled', true);
			}
		)
		.on("maxfilesreached", function(file) 
			{
				$(".dropzone").addClass("hidden");
			}
		)
		.on("removedfile", function(file) 
			{
				$(".dropzone").removeClass("hidden");
				$('input#upload-file-name').val("");
				$("#total-progress i.fa-check").addClass("hidden");
				resetUploadError();
			}
		)
		.on("success", function(file, response)
			{
				resetUploadError();
				var result = JSON.parse(response);
	
				if (result.success == 1)
				{
					$('input#upload-file-name').val(result.result);
					$("#total-progress").removeClass("progress-striped active");
					$("#total-progress i.fa-check").removeClass("hidden");
				}
				else
				{
					$('div.message-error .alert').html(result.error);
					showUploadError();
				}
			}
		)
		.on("error", function(file, errorMessage)
			{
				this.removeAllFiles();
				$('div.message-error .alert').html(errorMessage);
				showUploadError();
			}
		)
		.on("queuecomplete", function(progress) 
			{
				$('.btn-confirm').prop('disabled', false);
			}
		);
	}

	function showUploadError()
	{
		$('div.message-error').removeClass('hidden');
	}
	
	function resetUploadError()
	{
		$('div.message-error').addClass('hidden');
	}

	function initPage()
	{
		Dropzone.options.fileUpload = messageUtils.dropzoneConfig;
		
		$("form#add-message").on("submit", addMessage);
		$("div.message-list").on("click", "button.remove-message", removeMessage);
		$("div.message-list").on("click", "button.view-tweet", viewTweet);
		//$("div.message-list").on("click", "button.import", importRecipeAttachment);
		$("textarea.new-message").on("change keyup paste", messageUtils.charCount);
	
		$('div.bs-recipe-preview-modal').on('show.bs.modal', function (event) 
		{
			var button = $(event.relatedTarget); // Button that triggered the modal
			var recipeId = button.data('recipeid');
		    ajaxGet("/api/1/recipes/" + recipeId + "/preview", null)
				.done(loadRecipePreview)
				.always(function()
				{
					$("div.bs-recipe-preview-modal div.wait-spinner").addClass("hidden");
				});
		});
		$('div.bs-recipe-preview-modal').on('hide.bs.modal', function (event) 
		{
			$("div.bs-recipe-preview-modal div.wait-spinner").removeClass("hidden");
			$("div.bs-recipe-preview-modal div.recipe-header").addClass("hidden");
			$("div.bs-recipe-preview-modal div.recipe-ingredients").addClass("hidden");
			$("div.bs-recipe-preview-modal div.recipe-directions").addClass("hidden");
		});
		
		$("div.bs-recipeimport-modal").on("show.bs.modal", function(event)
		{
			var button = $(event.relatedTarget); // Button that triggered the modal
			var recipeId = button.data('recipeid');
			if (recipeId > 0)
			{
				var $recipeTitle = $("span.attachment-content a", $(button).closest("div.attachment-section"));
			
				$("div.instr-url", this).addClass("hidden");
				$("div.instr-friend", this).removeClass("hidden");
				$("input.import-recipe-url-display", this)
					.addClass("hidden")
					.val("/recipe/" + recipeId);
				$("div.recipe-title span.recipe-title", this).text($recipeTitle.text());
				$("div.recipe-title", this).removeClass("hidden");
			}
		});
		$("div.bs-recipeimport-modal").on("hide.bs.modal", function(event)
		{
			$("div.instr-url", this).removeClass("hidden");
			$("div.instr-friend", this).addClass("hidden");
			$("input.import-recipe-url-display", this)
				.removeClass("hidden")
				.val("");
			$("div.recipe-title", this).addClass("hidden");
		});
		
		loadMessages(0);
	}

	function clearMessageAlerts()
	{
		$("div.message-error").addClass("hidden");
	}
	
	function addMessage(event)
	{
		event.preventDefault();
		clearMessageAlerts();

		var $messageInput = $("textarea.new-message");
		$messageInput.prop("disabled", true);
		$(".btn.add-message").prop("disabled", true);

		var messageData = {
			message: $messageInput.val(),
			type: $messageInput.data("type"),
			image: $("input#upload-file-name").val()
		};
	
		if ($("div.tweet-option").not(".hidden").length)
		{
			messageData.tweet = $("#twitter").prop("checked");
		}
	
		messageUtils.postMessage(messageData)
			.done(function(data)
			{
				if (data.success == 1)
				{
					addMessageRow(data.result, 1);
					$messageInput.val("");
					$messageInput.trigger("paste");
					Dropzone.forElement("div.file-upload").removeAllFiles();
					
					if (data.result && data.result.twitter_error) {
						$("div.message-error .alert")
							.html("<strong>There was a problem posting your message to Twitter.</strong> Your message was still posted to ProjectNom.");
						$("div.message-error")
							.removeClass("hidden");						
					}
				}
			})
			.fail(function(error)
			{
				$("div.message-error .alert")
					.html(error);
				$("div.message-error")
					.removeClass("hidden");
			})
			.always(function(data) 
			{
				$messageInput.prop("disabled", false);
				$(".btn.add-message").prop("disabled", false);
			});
	}
	
	function loadMessages(page)
	{	
	    ajaxGet("/api/1/messages/page/" + page, null)
			.done(loadMessageList)
			.always(function()
			{
				$("section#message div.wait-spinner").remove();			
				$(".row.show-more p").text("Show More");
			});
	}
	
	function loadMessageList(data)
	{
		if (data.success == 1)
		{
			var page = parseInt($(".show-more input#current-page").val());
			
			$.each(data.result, function(index, value)
			{
				addMessageRow(value);
			});
	
			if (data.result.length === 0)
			{
				if (page <= 0)
				{
					$("div.error .alert")
						.html("<strong>No messages yet.</strong> Try adding more friends to see their messages -- or post one of your own!");
					$("div.error")
						.removeClass("hidden");
				}
				else
				{
					$(".show-more").addClass("hidden");
				}
			}
			else
			{
				if (data.result.length >= 10)
				{
					$(".row.show-more")
						.removeClass("hidden")
						.on("click", showMore);
				}
				else
				{
					$(".show-more").addClass("hidden");
				}
				
				$("section#friends .footer").removeClass("invisible");
				$(".show-more input#current-page").val(page+1);
			}
		}
	
		$("div.add-message.hidden").removeClass("hidden");
		$("div.messages.hidden").removeClass("hidden");
	}
	
	function showMore(event)
	{
		$(this).off("click");
		$("div.row.show-more p").html("<i class='fa fa-circle-o-notch fa-spin'></i>");
		
		page = $("input#current-page", this).val();
		
		if (page === "" || page < 0)
		{
			page = 0;
		}
	
		loadMessages(page);
	}
	
	function removeMessage(event)
	{
		event.preventDefault();
	
		var $messageItem = $(this).closest("div.message-list-item");
		var messageid = $(this).data("messageid");
	
		if (messageid > 0)
		{
			ajaxDelete("/api/1/messages/" + messageid, null)
				.done(function(data)
					{
						if (data.success == 1)
						{
							$messageItem.remove();
						}
					});
		}
	}
	
	function viewTweet(event) 
	{
		event.preventDefault();
		
		var tweetId = $(event.currentTarget).data("tweetid");
		
		if (tweetId > 0)
		{
			window.location = "http://twitter.com/auser/status/" + tweetId;
		}
	}
	
	function addMessageRow(message, atTop)
	{
		atTop = atTop || 0;
		
		var $message = $("div.message-list-item.hidden").clone().removeClass("hidden");
		$("p.username", $message).text(message.author);
		$("p.message", $message).html(message.message.replace(/(?:\r\n|\r|\n)/g, '<br>'));
		$("p.timestamp", $message).text(message.timestamp);
		
		if (message.is_current_user == 1)
		{
			$("button.remove-message", $message)
				.data("messageid", message.id)
				.removeClass("hidden");
			$("div.header", $message).addClass("author");
			$("div.message-type", $message).addClass("pull-right text-right");
			$("div.message-delete", $message).removeClass("text-right");
		}
		
		if (message.tweet_id) 
		{
			$("button.view-tweet", $message)
				.data("tweetid", message.tweet_id)
				.removeClass("hidden");
		}
		
		if (message.to_user) {
			$("p.to_username", $message).text("to " + (message.is_current_user == 1 ? message.to_user : "you"));
			$("div.message-type p.comment-icon", $message).addClass("hidden");
			$("div.message-type p.direct-icon", $message).removeClass("hidden");
		}
		
		if (typeof message.recipe_title !== "undefined")
		{
			$("div.row.attachment button.preview", $message).data("recipeid", message.recipe_id);
			$("div.row.attachment button.import", $message).data("recipeid", message.recipe_id);
			$("div.row.attachment .attachment-content", $message)
				.html("<a href='" + message.recipe_url + "'>" + message.recipe_title + "</a>");
			$("div.row.attachment", $message).removeClass("hidden");
		}
		else
		{
			$("div.row.attachment", $message).remove();
		}
		
		if (message.image_url) {
			$("div.row.attachment-img img.img-thumbnail", $message).attr("src", "/" + message.image_url);
			$("div.row.attachment-img", $message).removeClass("hidden");
		}
		
		if (atTop === 1)
		{
			$("div.message-list").prepend($message);
		}
		else
		{
			$("div.message-list").append($message);
		}
	}
	
	function importRecipeAttachment(event)
	{
		$recipeUrl = $("span.attachment-content a", $(this).closest("div.attachment-section"));
		$(".bs-recipeimport-modal input.import-recipe-url-display").val($recipeUrl.attr("href"));
		$(".bs-recipeimport-modal").modal("show");
	}
	
	function loadRecipePreview(data)
	{
		if (data.success === 0)
		{
			var $recipeAlert = $("div.bs-recipe-preview-modal div.error");
			var alertText = "<strong>There was a problem retrieving this recipe.</strong>";
			$("div.alert", $recipeAlert).html(alertText);
			$recipeAlert.removeClass('hidden');
		}
		else
		{
			var recipeData = data.recipes[0];
	
			var $recipeHeader = $("div.recipe-preview-header > div.recipe-header").detach();
			$recipeHeader.removeClass('hidden');
			$("h2.recipe-title", $recipeHeader).html(recipeData.title);
			//$recipeHeader.empty();
	
			if (recipeData.image_url !== null && recipeData.image_url !== "")
			{
				$(".recipe-img > img.img-thumbnail",$recipeHeader).attr("src", "/" + recipeData.image_url);
				$(".recipe-img", $recipeHeader).removeClass("hidden");
			}
			else
			{
				$(".recipe-img",$recipeHeader).addClass("hidden");
			}
			
			if (recipeData.author)
			{
				$(".recipe-author", $recipeHeader)
					.html("<p><small><i>by " + recipeData.author + "</i></small></p>")
					.removeClass("hidden");
			}
			else
			{
				$(".recipe-author", $recipeHeader).addClass("hidden");
			}
			
			if (recipeData.total_time || recipeData.prep_time)
			{
				var details = "";
				if (recipeData.total_time)
				{
					details += " Total: " + recipeData.total_time;
				}
				if (recipeData.prep_time)
				{
					if (recipeData.total_time) details += "; ";
					details += " Prep: " + recipeData.prep_time;
				}
	
				$(".recipe-time", $recipeHeader).html(details);
				$(".recipe-meta-detail.time", $recipeHeader).removeClass("hidden");
			}
			else
			{
				$(".recipe-meta-detail.time", $recipeHeader).addClass("hidden");
			}
			
			if (recipeData.servings)
			{
				$(".recipe-servings", $recipeHeader)
					.html(recipeData.servings);
				$(".recipe-meta-detail.servings", $recipeHeader).removeClass("hidden");
			}
			else
			{
				$(".recipe-meta-detail.servings", $recipeHeader).addClass("hidden");
			}
	
			$recipeHeader.appendTo("div.recipe-preview-header");
			
			var $ingredientSection = $("div.recipe-preview-ingredients > div.recipe-ingredients").detach();
			$("div.ingredient-row", $ingredientSection).not(".hidden").remove();
			$ingredientSection.removeClass("hidden");
			for (var i = 0; i < recipeData.ingredients.length; i++) 
			{ 
				var ingredient = recipeData.ingredients[i];
				var ingredientText = "";
				var $ingredientRow = $("div.ingredient-row.hidden", $ingredientSection).clone();
				$ingredientRow.removeClass("hidden");
	
				if (ingredient.id > 0)
				{				
					if (ingredient.quantity !== "")
					{
						ingredientText += ingredient.quantity + " ";
					}
					if (ingredient.unit !== "")
					{
						ingredientText += ingredient.unit + " ";
					}
					if (ingredient.name !== "")
					{
						ingredientText += ingredient.name;
					}
					if (ingredient.preparation !== "")
					{
						ingredientText += ", " + ingredient.preparation;
					}
				
					$(".text", $ingredientRow).html(ingredientText);
				}
				else
				{
					$(".text", $ingredientRow)
						.removeClass("text")
						.addClass("heading")
						.html(ingredient.name);
				}
	
				$ingredientSection.append($ingredientRow);
			}
	
			$("div.recipe-preview-ingredients").append($ingredientSection);
	
			var $directionSection = $("div.recipe-preview-directions > div.recipe-directions").detach();
			$("div.direction-row", $directionSection).not(".hidden").remove();
			$directionSection.removeClass("hidden");
			for (i = 0; i < recipeData.directions.length; i++)
			{
				var direction = recipeData.directions[i];
				var directionText = "";
				var $directionRow = $("div.direction-row.hidden", $directionSection).clone();
				$directionRow.removeClass("hidden");
	
				$("h1", $directionRow).text(i+1);
				$(".recipe-direction", $directionRow).html(direction);
	
				$directionSection.append($directionRow);
			}
	
			$("div.recipe-preview-directions").append($directionSection);
			
			$('div.bs-recipe-preview-modal').data('bs.modal').handleUpdate();
		}
	}
})(jQuery);

$(function() {
	initPage_base();
	pnMessages.init();
});

},{"../components/BootstrapError.js":1,"./messageutils.js":3,"react":"react","react-dom":"react-dom"}],3:[function(require,module,exports){
module.exports = function(config) {
	return {
		postMessage: addMessageBase,
		charCount: charCount,
		dropzoneConfig: { 
			url: "/api/1/messages/image",
			paramName: "userfile",
			maxFilesize: 4,
			uploadMultiple: false,
			maxFiles: 1,
			acceptedFiles: "image/jpeg,image/jpg,image/gif,image/png",
			previewsContainer: "div.upload-preview",
			previewTemplate: $('div.upload-preview-template').html(),
			thumbnailHeight: 75,
			init: config.initDropzone,
			
			fallback: function() {
				$("div#fileUpload").addClass("hidden");
				
				ReactDOM.render(
					React.createElement(BootstrapError,
						{
							visible: true,
							title: "Unfortunately, your browser is too old to support file uploads.", 
							detail: "You can still post a message, but you can't attach any images."
						}
					),
					document.getElementById("fallback")
				);
				
				$("div#fallback").removeClass("hidden");
			}
		}
	};
};

function addMessageBase(messageData)
{
	var errorText = "<strong>Something went wrong.</strong> Please try again. If you keep seeing this, let us know.";

	var newMessage =
	{
		message: messageData.message,
		type: messageData.type,
		tweet: messageData.tweet ? true : false
	};
	
	if (messageData.recipeid > 0)
	{
		newMessage.recipeid = messageData.recipeid;
	}
	if (messageData.to_user !== "")
	{
		newMessage.to_user = messageData.to_user;
	}
	if (messageData.image)
	{
		newMessage.image = messageData.image;
	}

	var defer = $.Deferred();

	ajaxPost("/api/1/messages", newMessage)
		.done(function(data)
		{
			if (data.success === 0)
			{				
				if (data.error == "Message too long.")
				{
					errorText = "<strong>Your message cannot exceed 255 characters.</strong>";
				}
				else if (data.error == "Required information missing.")
				{
					errorText = "<strong>Your message cannot be blank.</strong>";
				}
				
				defer.reject(errorText);
			}
			else
			{
				defer.resolve(data);
			}
		})
		.fail(function(xhr, error) {
			console.log(xhr.responseText);
			defer.reject(errorText);
		});
		
	return defer.promise();
}

function charCount(event)
{
	var $label = $("span.label.char-count");
	var remaining = (255 - $(this).val().length);
	
	if (remaining <= 0)
	{
		$(".btn.add-message").prop("disabled", true);
	}
	else
	{
		$(".btn.add-message").prop("disabled", false);
	}
		
	if (remaining < 25)
	{
		$label.removeClass("label-default label-warning").addClass("label-danger");
	}
	else if (remaining < 155)
	{
		$label.removeClass("label-default label-danger").addClass("label-warning");
	}
	else if (remaining >= 155)
	{
		$label.removeClass("label-warning label-danger").addClass("label-default");
	}
	
	$label.text(remaining);
}
},{}]},{},[2]);
