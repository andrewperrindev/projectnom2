<?php
include_once("Base_model.php");

class Shoppinglist_model extends Base_model
{
    function __construct()
    {
        parent::__construct();
    }

    function getList($username)
    {
    	$result = array();

    	$this->db->from('shoppinglist_ingredients');
    	$this->db->join('user_shoppinglists', 'user_shoppinglists.list_id = shoppinglist_ingredients.list_id');
    	$this->db->join('users', 'users.id = user_shoppinglists.user_id');
    	$this->db->where('username', $username);
    	$this->db->order_by('seq');
    	$query = $this->db->get();

    	foreach ($query->result() as $row)
		{
			$result[] = $row->item;
		}

		return $result;
    }

    function saveItem($item, $username, $seq = 1)
    {
    	$userid = $this->getIdForUsername($username);

    	if ($userid > 0)
		{
			$query = $this->db->get_where("user_shoppinglists", array('user_id' => $userid));

			$listid = 1;
			if ($query->num_rows() > 0)
			{
				$listid = $query->row()->list_id;

				$this->db->select_max('seq');
				$query = $this->db->get('shoppinglist_ingredients');
				if ($query->num_rows() > 0)
				{
					$row = $query->row();
					if ($row->seq > 0)
					{
						$seq = $row->seq + 1;
					}
				}
			}
			else
			{
				$this->db->select_max('list_id');
				$query = $this->db->get('user_shoppinglists');
				$row = $query->row();
				if ($row->list_id > 0)
				{
					$listid = $row->list_id + 1;
				}

				$data = array(
					'list_id' => $listid,
					'user_id' => $userid,
					'name' => 'Default'
				);
				$this->db->insert('user_shoppinglists', $data);
			}

			if ($listid > 0)
			{
				$data = array(
					'list_id' => $listid,
					'item' => $item,
					'seq' => $seq
				);
				$this->db->insert('shoppinglist_ingredients', $data);
				return true;
			}
		}

		return false;
    }

    function clearList($username)
    {
    	$this->db->from('user_shoppinglists');
    	$this->db->join('users', 'users.id = user_shoppinglists.user_id');
    	$this->db->where('username', $username);
    	$query = $this->db->get();

    	if ($query->num_rows() > 0)
    	{
    		$listid = $query->row()->list_id;
    		$this->db->delete('shoppinglist_ingredients', array('list_id' => $listid));
    		return true;
    	}

    	return false;
    }
}
?>