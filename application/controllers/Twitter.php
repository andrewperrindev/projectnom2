<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once(APPPATH . '/controllers/WebAppControllerBase.php');

class Twitter extends WebAppControllerBase 
{
   public function __construct()
   {
	    // Don't automatically close session since we may need to modify it here.
        parent::__construct(FALSE);

		if ($this->session->userdata('username') === NULL)
		{
			$this->failureRedirect();
		}
   }

	public function index()
	{
		$this->load->library('pntwitter', array());
		$request_token = $this->pntwitter->getRequestToken($this->getProtocol() . "://" . $_SERVER['SERVER_NAME'] . "/twitter/callback");
		
		$this->session->set_flashdata('oauth_token', $request_token['oauth_token']);
		$this->session->set_flashdata('oauth_token_secret', $request_token['oauth_token_secret']);
		
		$url = $this->pntwitter->getAuthorizeUrl($request_token['oauth_token']);
		session_write_close();
		
		header("Location: $url");
		die();
	}
	
	public function callback()
	{
		$request_token = array(
			'oauth_token' => $this->session->flashdata('oauth_token'),
			'oauth_token_secret' => $this->session->flashdata('oauth_token_secret')
		);

		if (!isset($_GET['oauth_token']))
		{
			$data['error'] = "No response received from Twitter.";
		}
		else if ($this->session->userdata('readonly'))
		{
			$data['error'] = "Please log in to ProjectNom before adding your Twitter account.";
		}
		else if (isset($_GET['oauth_token']) && $request_token['oauth_token'] !== $_GET['oauth_token']) 
		{
			log_message("error", "Session oauth_token (" . $request_token['oauth_token'] . 
				") doesn't match what we received from Twitter (" . $_GET['oauth_token'] . ")!");
			$data['error'] = "The response from Twitter isn't what we expected.";
		}
		else
		{
			$this->load->library('pntwitter', $request_token);
			$access_token = $this->pntwitter->getAccessToken($_GET['oauth_verifier']);
			
			if (!empty($access_token))
			{
				if($this->session->userdata('username') !== NULL)
				{
					$this->load->model("user_model", "userService", TRUE);
					
					try
					{
						$this->userService->setTwitterAccount($this->session->userdata('username'), $access_token['oauth_token'], 
							$access_token['oauth_token_secret'], $access_token['screen_name']);
						$this->session->set_userdata(array('twitter' => $access_token['screen_name']));
						session_write_close();
	
						header("Location: " . $this->getProtocol() . "://" . $_SERVER['SERVER_NAME'] . "/account");
						die();
					}
					catch(Exception $e)
					{
						log_message("error", print_r($access_token, TRUE) . "\n" . $e->getMessage());
						$data['error'] = "Could not update your account with Twitter details.";
					}					
				}
			}
		}

		$data['title'] = "Error";
		$data['alert_title'] = "'An error has occurred.'";
		$data['detail'] = <<<React
this.React.DOM.span(null,
	"Something went wrong while authenticating with Twitter. ({$data['error']}) You can ",
	this.React.DOM.a({href: "/home"}, "try again"),
	", or ",
	this.React.DOM.a({href: "mailto:support@projectnom.com"}, "email us for assistance"),
	"."
)
React;
		
		add_js('error.js');

		$this->load->view('templates/header', $data);
		$this->load->view('templates/nav_empty', $data);
		$this->load->view('pages/error', $data);
		$this->load->view('templates/footer', $data);
	}
}
?>