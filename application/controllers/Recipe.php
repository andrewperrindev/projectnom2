<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once(APPPATH . '/controllers/WebAppControllerBase.php');

class Recipe extends WebAppControllerBase 
{
   public function __construct()
   {
        parent::__construct();
        /*
		$this->load->library('session');
		*/
		if ($this->session->userdata('username') === NULL)
		{
			if (preg_match("/recipe\/([0-9]+)/i", $_SERVER["REQUEST_URI"], $matches))
			{
				header("Location: " . $this->getProtocol() . "://" . $_SERVER['SERVER_NAME'] . "/preview/" . $matches[1]);
				die();
			}
			$this->failureRedirect();
		}
   }

	public function index($id = 0)
	{
		$this->load->model('Recipe_model', 'recipeService', TRUE);

		$data['username'] = $this->session->userdata('username');
		$data['twitter'] = $this->session->userdata('twitter');
		$data['recipeid'] = $id;
		$data['cuisines'] = $this->recipeService->getCuisines();
		$data['types'] = $this->recipeService->getTypes();

		add_js(array('recipe/recipe.js','recipe/recipe-common.js', 'recipe/dropzone.js'));
		add_css(array('recipe/recipe.css','recipe/dropzone-common.css','recipe/dropzone-message.css'));

		$this->load->view('templates/header', $data);
		$this->load->view('templates/nav_standard', $data);
		$this->load->view('pages/recipe', $data);
		$this->load->view('templates/modal-shoppinglist', $data);
		$this->load->view('templates/modal-message', $data);
		$this->load->view('templates/footer', $data);
	}

	public function edit($id = 0)
	{
		$this->load->model('Recipe_model', 'recipeService', TRUE);
		
		$data['username'] = $this->session->userdata('username');
		$data['readonly'] = $this->session->userdata('readonly');
		$data['recipeid'] = $id;
		$data['is_import'] = $data['is_new'] = false;
		$data['cuisines'] = $this->recipeService->getCuisines();
		$data['types'] = $this->recipeService->getTypes();
		
		if ($this->input->post("import-recipe-url") != "")
		{
			$data['origin_url'] = $this->input->post("import-recipe-url");
			$data['page_title'] = $this->input->post("import-recipe-title");
			$data['cache_url'] = "";
			$data['is_import'] = $data['is_new'] = true;
		}
		else if ($id > 0)
		{
			try
			{
				$importUrls = $this->recipeService->getImportUrls($data);
				$data['origin_url'] = $importUrls['origin_url'];
				$data['cache_url'] = $importUrls['cache_url'];

				if (!empty($data['origin_url']))
				{
					$data['is_import'] = true;
				}
				if ($this->input->server('REQUEST_METHOD') == "POST")
				{
					$data['is_new'] = true;
				}
			}
			catch (Exception $e)
			{
				log_message("error", "Unable to get import URLs for recipe: " . $e->getMessage());
			}
		}

		$js = array('recipe/edit.js','autogrow.min.js','recipe/dropzone.js','recipe/jquery-ui.js','recipe/recipe-common.js','bootbox.min.js','jquery.ui.touch-punch.min.js');
		$css = array('recipe/edit.css','recipe/dropzone-common.css','recipe/dropzone-recipe.css', 'recipe/jquery-ui.min.css','recipe/jquery-ui.structure.min.css');

		if ($data['is_import'])
		{
			$this->load->helper("recipe");

			if (empty($data['cache_url']) || !file_exists(APPPATH . "/../" . $data['cache_url']))
			{
				try
				{
					$data['cache_url'] = CreateCachedRecipe($data['origin_url'], $data['cache_url'], md5($data['username']));
				}
				catch (Exception $e)
				{
					$data['error'] = "<strong>We had trouble importing your recipe.</strong> You can try again, or you can enter your recipe manually.";
					$data['is_import'] = false;
				}
				//$this->recipeService->saveRecipeCacheUrl($id, $data['cache_url']);
			}
			if (empty($data['error']))
			{
				$js = array_merge($js, array('recipe/modal-import.js','recipe/modal-import-image.js','recipe/rangy-core.js'));
				$css[] = 'recipe/modal-import.css'; 
			}
		}

		add_css($css);

		$this->load->view('templates/header', $data);
		$this->load->view('templates/nav_standard', $data);
		$this->load->view('pages/recipe_edit', $data);
		$this->load->view('templates/modal-upload', $data);
		if ($data['is_import'])
		{
			if (!empty($data['cache_url']) && stripos($data['cache_url'], 'recipecache') !== FALSE)
			{
				$data['import_image_urls'] = GetRecipeImages($data['origin_url'], $data['cache_url']);

				$this->load->view('templates/modal-import', $data);
				$this->load->view('templates/modal-import-image', $data);
			}
			else
			{
				$data['is_import'] = false;
			}
		}
		if ($data['readonly'])
		{
			$this->load->view('templates/modal-login', $data);
			$js = array_merge($js, array('login.js'));
		}

		add_js($js);

		$this->load->view('templates/footer', $data);	
	}

	public function parse($type)
	{
		$this->load->helper("html2text");
		$content = $this->input->post("content");

		if (!empty($content) && !empty($type))
		{
			$content = str_replace("&nbsp;", " ", $content);
			
			if ($type == "ingredients")
			{
				$this->load->model('Recipe_model', 'recipeService', TRUE);
				$ingredients = array();
				$ingredients_str = convert_html_to_text($content);

				$units = $this->recipeService->getUnits();
				$this->load->library('RecipeImport', array('units' => $units));

				foreach (explode("\n", $ingredients_str) as $ingredient_str)
				{
					if (!empty($ingredient_str))
					{
						$ingredientData = $this->recipeimport->parseIngredient($ingredient_str);

						if (is_array($ingredientData))
						{
							$ingredient = new Ingredient();
							$ingredient->id = 1;
							$ingredient->name = $ingredientData['name'];
							$ingredient->preparation = $ingredientData['preparation'];
							$ingredient->unit = $ingredientData['unit'];
							$ingredient->quantity = $ingredientData['quantity'];

							if (rtrim(strrchr($ingredient->name, ":")) === ":")
							{
								$ingredient->id = 0;
							}

							$ingredients[] = $ingredient;
						}
					}
				}

				$data["result"] = $ingredients;
				$this->load->view('api/simple', $data);
			}
			else if ($type == "directions")
			{
				$directions = array();
				$directions_str = convert_html_to_text($content);

				foreach (explode("\n", $directions_str) as $direction_str)
				{
					$direction_str = trim($direction_str);
					if ($direction_str != "")
					{
						$directions[] = $direction_str;
					}
				}

				$data["result"] = $directions;
				$this->load->view('api/simple', $data);
			}
			else
			{
				$data["result"] =  convert_html_to_text(trim(urldecode($content)));
				$this->load->view('api/simple', $data);
			}
		}
		else {
			$this->load->view('api/simple');
		}
	}

	public function search($page = 1, $filter = "")
	{
		$this->load->model('Recipe_model', 'recipeService', TRUE);

		if ($filter == "new")
		{
			$sort = "new";
		}
		else if ($filter == "different")
		{
			$sort = "different";
		}
		else if ($filter == "favorite")
		{
			$sort = "rating";
			$rating1_filter = $rating2_filter = $rating3_filter = -1;
			$rating4_filter = $rating5_filter = 1;
		}
		else
		{
			$sort = $this->input->post("search-sort");
			$rating1_filter = $this->input->post("search-rating1");
			$rating2_filter = $this->input->post("search-rating2");
			$rating3_filter = $this->input->post("search-rating3");
			$rating4_filter = $this->input->post("search-rating4");
			$rating5_filter = $this->input->post("search-rating5");
		}

		$cuisine_filter = $this->input->post("search-cuisine");
		$type_filter = $this->input->post("search-type");

		$data['title'] = "Search";
		$data['username'] = $this->session->userdata('username');
		$data['cuisines'] = $this->recipeService->getCuisines();
		$data['types'] = $this->recipeService->getTypes();

		$data['query'] = $this->input->post("search-query");
		$data['page'] = $page;
		$data['sort'] = empty($sort) ? "title" : $sort;
		$data['cuisine_filter'] = isset($cuisine_filter) ? $cuisine_filter : "";
		$data['type_filter'] = isset($type_filter) ? $type_filter : "";
		$data['rating_filter'] = array
			(
				1 => empty($rating1_filter) ? "1" : intval($rating1_filter),
				2 => empty($rating2_filter) ? "1" : intval($rating2_filter),
				3 => empty($rating3_filter) ? "1" : intval($rating3_filter),
				4 => empty($rating4_filter) ? "1" : intval($rating4_filter),
				5 => empty($rating5_filter) ? "1" : intval($rating5_filter)
			);

		add_js('recipe/search.js');
		add_css('recipe/search.css');

		$this->load->view('templates/header', $data);
		$this->load->view('templates/nav_standard', $data);
		$this->load->view('pages/recipe_search', $data);
		$this->load->view('templates/footer', $data);
	}
}