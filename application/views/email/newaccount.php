<?php
	$protocol = (!empty($_SERVER['HTTPS']) ? "https" : "http");
	$activation_link = "$protocol://" . $_SERVER['SERVER_NAME'] . "/welcome/activate/$activation_key";
?>
<html>
	<body>
		<table style="padding: 5px;">
			<tr>
				<td style="padding: 15px; text-align: center; font-size: 30px; background-color: #454545; background-image: url('<?php print $protocol; ?>://projectnom.com/img/email-bg.jpg'); background-repeat: no-repeat; color: white;">
					<b style="color: lightgray;">PROJECT</b>NOM
				</td>
			</tr>
			<tr>
				<td style="text-align: center; vertical-align: middle; padding: 15px;"><h2>New Account</h2></td>
			</tr>
			<tr>
				<td colspan='2' style="padding: 15px; font-size: 14pt;">
					<p>Welcome to ProjectNom!</p>
					<p>To get started, please click on the following link to confirm your email and activate your account:
					<p><a href="<?php print $activation_link; ?>"><?php print $activation_link; ?></a></p>
				</td>
			</tr>
		</table>
	</body>
</html>