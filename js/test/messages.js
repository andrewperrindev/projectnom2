casper.test.begin('ProjectNom properly displays user messages', 38, 
function suite(test) 
{
	// Login and navigate to messages page. +4 test cases
	login_mageuzi(test, "friends/messages");

	casper.then(function() {
		this.waitWhileSelector("section#message div.wait-spinner");
	});

	// Check for all messages loaded
	casper.then(function() {
        test.assertEval(function() {
            return __utils__.findAll("div.message-list-item:not(.hidden)").length == 10;
        }, "10 Messages Displayed");	    
    });
    
    // At least one message has recipe attached
    casper.then(function() {
	   test.assertExists("div.attachment > div.attachment-section", "At least one recipe attachment exists");
	    
	   test.assertEvalEquals(function() {
		    return document.querySelector("div.attachment .attachment-content a").textContent;		    
	   }, "Spinach Basil Pesto", "First recipe is Spinach Basil Pesto");
	    
	   this.echo("Make sure recipe preview loads:");
	   this.click("div.attachment-section button.preview");
	   this.waitForSelector('div.bs-recipe-preview-modal div.wait-spinner.hidden');
    });
    
    casper.then(function() {
	   test.assertEquals(this.fetchText("div.bs-recipe-preview-modal h2.recipe-title"), "Spinach Basil Pesto", "Recipe title loaded");
	   test.assertEquals(this.fetchText("div.bs-recipe-preview-modal div.recipe-author"), "by Jade Vance", "Recipe author loaded");
	   test.assertEquals(this.fetchText("div.bs-recipe-preview-modal span.recipe-time"), " Total: 10 min;  Prep: 10 min", "Recipe time loaded");
	   test.assertEquals(this.fetchText("div.bs-recipe-preview-modal span.recipe-servings"), "2 Cups", "Recipe servings loaded");
       test.assertEval(function() {
            return __utils__.findAll("div.bs-recipe-preview-modal div.ingredient-row:not(.hidden)").length == 10;
        }, "10 Ingredients loaded");
       test.assertEval(function() {
            return __utils__.findAll("div.bs-recipe-preview-modal div.direction-row:not(.hidden)").length == 1;
        }, "1 direction loaded");

	   this.click("div.bs-recipe-preview-modal button.btn-cancel");
	   this.waitWhileSelector('div.bs-recipe-preview-modal div.wait-spinner.hidden');
    });

    // Look for message with external recipe attachment
    casper.then(function() {
	    this.echo("Make sure external recipe preview loads:");
		test.assertEvalEquals(function() {
			var result = false;
			$("div.message-list-item").each(function(index)
			{
				if ($("p.message", this).text() == "Post from a completely different user lorem ipsum")
				{
					result = true;
		
					// mark preview button so we can click it later
					$("button.preview", this).addClass("casperjs-preview-external");
				}
			});
		
		return result;
	   }, true, "Message with External Recipe Attachment Found");
    });

    casper.then(function() {
	   this.click("div.attachment-section button.preview.casperjs-preview-external");
	   this.waitForSelector('div.bs-recipe-preview-modal div.wait-spinner.hidden');
	});
	
	casper.then(function() {
	   test.assertEquals(this.fetchText("div.bs-recipe-preview-modal h2.recipe-title"), "The Right Bank Courtesy of www.bonappetit.com", "Recipe title loaded");
	   test.assertEquals(this.fetchText("div.bs-recipe-preview-modal div.recipe-author"), "by Kristin Almy", "Recipe author loaded");
	   test.assertEquals(this.fetchText("div.bs-recipe-preview-modal span.recipe-time"), " Total: 5min;  Prep: 5 min", "Recipe time loaded");
	   test.assertEquals(this.fetchText("div.bs-recipe-preview-modal span.recipe-servings"), "4 rocks glasses", "Recipe servings loaded");
       test.assertEval(function() {
            return __utils__.findAll("div.bs-recipe-preview-modal div.ingredient-row:not(.hidden)").length == 7;
        }, "7 Ingredients loaded");

	   this.click("div.bs-recipe-preview-modal button.btn-cancel");
	   this.waitWhileSelector('div.bs-recipe-preview-modal div.wait-spinner.hidden');
	});
	
    casper.then(function() {
		this.echo("Make sure new message posts:");
        test.assertExists('form#add-message', "Post message form is found");
        this.fillSelectors('form#add-message', {
            "textarea#new-message": "test message",
            "input#twitter": false
        }, false);
        this.click("input.add-message");
        
        // Wait until message finishes posting
        //this.waitForSelector(".btn.add-message:disabled");
        this.waitWhileSelector(".btn.add-message:disabled");
	});
	
	// Check to make sure the message just posted appears on page
	casper.then(function() {
		test.assertEvalEquals(function() {
			var result = false;
			$("div.message-list-item").each(function(index)
			{
				if ($("p.message", this).text() == "test message")
				{
					result = true;
		
					// mark delete button so we can click it later
					$("button.remove-message", this).addClass("casperjs-delete");
				}
			});
		
		return result;
	   }, true, "New Message Posted Successfully");		
	});
	
	casper.then(function() {
		this.echo("Make sure message deletes:");
		this.click("button.casperjs-delete");
		this.waitWhileSelector("button.casperjs-delete");
	});

    casper.then(function() {
		test.assertDoesntExist("button.casperjs-delete", "Message deleted successfully");

		this.echo("Make sure new message with image posts:");
        test.assertExists('form#add-message', "Post message form is found");
		var fs = require('fs');
		var currentFile = require('system').args[3];
		var curFilePath = fs.absolute(currentFile).split('/');
		if (curFilePath.length > 1) {
			curFilePath.pop();
			path = curFilePath.join('/');
			casper.page.uploadFile("input.dz-hidden-input", path + "/cornbread.png");
		}
	});
	casper.waitForSelector("#total-progress i.fa-check:not(.hidden)", function() {
        this.fillSelectors('form#add-message', {
            "textarea#new-message": "test message with image",
            "input#twitter": false
        }, false);
        this.click("input.add-message");
        
        // Wait until message finishes posting
        //this.waitForSelector(".btn.add-message:disabled");
        this.waitWhileSelector(".btn.add-message:disabled");
	});
	
	// Check to make sure the message just posted appears on page
	casper.then(function() {
		test.assertEvalEquals(function() {
			var result = false;
			$("div.message-list-item").each(function(index)
			{
				if ($("p.message", this).text() == "test message with image"
					&& $("div.attachment-img", this).not(".hidden").length)
				{
					result = true;
		
					// mark delete button so we can click it later
					$("button.remove-message", this).addClass("casperjs-delete");
				}
			});
		
		return result;
	   }, true, "New Message with Image Posted Successfully");		
	});
	
	casper.then(function() {
		this.echo("Make sure message deletes:");
		this.click("button.casperjs-delete");
		this.waitWhileSelector("button.casperjs-delete");
	});

	casper.then(function() {
		test.assertDoesntExist("button.casperjs-delete", "Message deleted successfully");
		
		this.click("div.show-more");
		this.waitWhileSelector("div.show-more i.fa-circle-o-notch");
	});
	
	casper.then(function() {
		this.echo("Make sure Show More loads additional messages:");
		test.assertEval(function() {
            return __utils__.findAll("div.message-list-item:not(.hidden)").length == 18;
        }, "8 Messages Loaded after Show More");
	});

	// find the first recipe to test import
	casper.then(function() {
		test.assertEvalEquals(function() {
			var result = false;
			$("div.message-list-item").each(function(index)
			{
				if ($("p.message", this).text() == "Bleep bloop")
				{
					result = true;
		
					// mark import button so we can click it later
					$("button.import", this).addClass("casperjs-import");
				}
			});
		
		return result;
	   }, true, "Internal recipe found");
	});

	casper.then(function() {
		this.echo("Make sure internal recipe imports:");
		this.click("button.casperjs-import");
		this.waitUntilVisible("div.bs-recipeimport-modal div.recipe-title");
	});
	
	casper.then(function() {
		test.assertVisible("div.bs-recipeimport-modal div.recipe-title", "Import Modal Visible");
		test.assertSelectorHasText("div.modal-body div.recipe-title span.recipe-title", "Spinach Basil Pesto", "Recipe Title Visible");
		this.click("div.bs-recipeimport-modal button.btn-confirm");
		this.waitForSelector("div.saved-msg");
	});

	casper.then(function() {
		this.waitWhileSelector("div.wait-spinner");
	});
	
	casper.then(function() {
		test.assertVisible("div.saved-msg", "Import success message visible");
		test.assertSelectorHasText("div#titlePlaceholder p.content", "Spinach Basil Pesto", "Recipe title imported");
		test.assertNotVisible("button.import-image-modal", "No import option provided");
		this.click("button#editRecipeDelete");
		this.waitUntilVisible("div.bootbox button.btn-danger");
	});
	
	casper.then(function() {
		this.click("div.bootbox button.btn-danger");
		this.waitForUrl(getServerName() + "home");
	});

	// find the second recipe to test import	
	casper.thenOpen(getServerName() + "friends/messages");

	casper.then(function() {
		this.waitWhileSelector("section#message div.wait-spinner");
	});
	
	casper.then(function() {
		test.assertEvalEquals(function() {
			var result = false;
			$("div.message-list-item").each(function(index)
			{
				if ($("p.message", this).text() == "Blah baaah")
				{
					result = true;
		
					// mark import button so we can click it later
					$("button.import", this).addClass("casperjs-import");
				}
			});
		
		return result;
	   }, true, "External recipe found");
	});

	casper.then(function() {
		this.echo("Make sure external recipe imports:");
		this.click("button.casperjs-import");
		this.waitUntilVisible("div.bs-recipeimport-modal div.recipe-title");
	});
	
	casper.then(function() {
		test.assertVisible("div.bs-recipeimport-modal div.recipe-title", "Import Modal Visible");
		test.assertSelectorHasText("div.modal-body div.recipe-title span.recipe-title", "Fruit-Apathy Blueberry Tart", "Recipe Title Visible");
		this.click("div.bs-recipeimport-modal button.btn-confirm");
		this.waitForSelector("div.saved-msg");
	});
	
	casper.then(function() {
		test.assertVisible("div.saved-msg", "Import success message visible");
		test.assertSelectorHasText("div#titlePlaceholder p.content", "Fruit-Apathy Blueberry Tart", "Recipe title imported");
		test.assertVisible("button.import-image-modal", "Import option provided");
		this.click("button#editRecipeDelete");
		this.waitUntilVisible("div.bootbox button.btn-danger");
	});
	
	casper.then(function() {
		this.click("div.bootbox button.btn-danger");
		this.waitForUrl(getServerName() + "home");
	});
	
    casper.run(function() {
        test.done();
    });
});