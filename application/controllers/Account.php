<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once(APPPATH . '/controllers/WebAppControllerBase.php');

class Account extends WebAppControllerBase
{
   public function __construct()
   {
        parent::__construct();
        /*
		$this->load->library('session');
		*/
		if ($this->session->userdata('username') === NULL)
		{
			$this->failureRedirect();
		}
   }

	public function index()
	{
		$data['email'] = $data['twitter'] = "";
		$data['title'] = "Account";
		$data['username'] = $this->session->userdata('username');
		$data['readonly'] = $this->session->userdata('readonly');
		add_css('account/account.css');
		$js = array('account/account.js','bootbox.min.js');

		$this->load->model('User_model', 'userService', TRUE);
		$userDetails = $this->userService->getAccountDetails($data['username']);
		
		if (!empty($userDetails) && is_array($userDetails))
		{
			$data['email'] = $userDetails['email'];
			$data['twitter'] = $userDetails['twitter'];
		}
		
		$this->load->view('templates/header', $data);
		$this->load->view('templates/nav_standard', $data);
		$this->load->view('pages/account', $data);
		if ($data['readonly'])
		{
			$this->load->view('templates/modal-login', $data);
			$js = array_merge($js, array('login.js'));
		}
		add_js($js);
		$this->load->view('templates/footer', $data);	
	}
}