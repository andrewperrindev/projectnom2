    <!-- Intro Header -->
    <header class="intro">
        <div class="intro-body">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <h1 class="brand-heading"><span class="pn-gray">PROJECT</span><span class="light">NOM</span></h1>
                        <p class="intro-text">A free, intuitive way to organize your recipe collection.</p>
                        <a href="#about" class="btn btn-circle page-scroll">
                            <i class="fa fa-angle-double-down animated"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <!-- About Section -->
    <section id="about" class="container content-section text-center text-dark">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2">
                <h2>About ProjectNom</h2>
                <p>Have you ever wished you could store all your recipes in one place? Do you have recipe collections scattered across multiple web sites? The goal of ProjectNom is to bring all that under one easy-to-use, clutter-free site.</p>
				<div class="row">
				  <div class="col-xs-10 col-xs-offset-1 col-sm-4 col-sm-offset-0">
				    <div class="thumbnail no-border">
				      <p><i class="fa fa-cutlery"></i></p>
				      <div class="caption">
				        <h3>Recipe Library</h3>
				        <p>Quickly import recipes from across the web, giving you one-stop access to the recipes you love!</p>
				      </div>
				    </div>
				  </div>
				  <div class="col-xs-10 col-xs-offset-1 col-sm-4 col-sm-offset-0">
				    <div class="thumbnail no-border">
				      <p><i class="fa fa-shopping-cart"></i></p>
				      <div class="caption">
				        <h3>Shopping List</h3>
				        <p>Once you've found the perfect recipe, select the ingredients you need for easy shopping.</p>
				      </div>
				    </div>
				  </div>
				  <div class="col-xs-10 col-xs-offset-1 col-sm-4 col-sm-offset-0">
				    <div class="thumbnail no-border">
				      <p><i class="fa fa-comments-o"></i></p>
				      <div class="caption">
				        <h3>Share</h3>
				        <p>Add your friends, share recipes and brag about your latest culinary adventures!</p>
				      </div>
				    </div>
				  </div>
				</div>
            </div>
        </div>
    </section>

    <!-- Sign up Section -->
    <section id="signup" class="content-section text-center">
        <div class="signup-section">
			<div class="container login-form">
				<h2>Sign Up</h2>
				
				<h4>Please provide the following details to create an account</h4>
				<div class="row">
					<div class="col-lg-12">
						<form name='signupForm' id='signupForm'>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label for="username">User name</label>
										<input type="text" class="form-control" id="username" name="username" tabindex='1' required />
										<p class="help-block text-danger invisible"></p>
									</div>
									<div class="form-group">
										<label for="email">Email</label>
										<input type="email" class="form-control" id="email" name="email" tabindex='2' data-validation-email-message='Please enter a valid email.' data-validation-required-message='Email address is required.' required />
										<p class="help-block text-danger invisible"></p>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="password">Password</label>
										<input type="password" class="form-control" id="password" name="password" placeholder="Must be at least 6 characters" minlength='6' data-validation-minlength-message='Password must be at least 6 characters.' tabindex='3' required />
										<p class="help-block text-danger invisible"></p>
									</div>
									<div class="form-group">
										<label for="password_confirm">Confirm Password</label>
										<input type="password" class="form-control" id="password_confirm" name="password_confirm" tabindex='4' data-validation-match-match='password' data-validation-match-message='Please make sure your password is correct in both password boxes.' required />
										<p class="help-block text-danger invisible"></p>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-12 text-center">
									<div id="success"></div>
									<button class="btn btn-xl btn-primary extra-padding" type="submit" tabindex='5' data-label='Create Account'>Create Account</button>
								</div>
							</div>
						</form>
					</div>
				</div>
				
			</div>
        </div>
    </section>

    <!-- Contact Section -->
    <section id="contact" class="container content-section text-center text-dark">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2">
                <h2>Contact ProjectNom</h2>
                <p>Questions, comments, feedback? Send us a note, we'd love to hear from you!</p>
                <p><a href="mailto:feedback@projectnom.com">feedback@projectnom.com</a>
                </p>
                <ul class="list-inline banner-social-buttons">
                    <li>
                        <a href="https://twitter.com/ProjectNom" class="btn btn-default btn-lg"><i class="fa fa-twitter fa-fw"></i> <span class="network-name">Twitter</span></a>
                    </li>
                </ul>
            </div>
        </div>
    </section>