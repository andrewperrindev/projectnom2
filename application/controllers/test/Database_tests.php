<?php
require_once(APPPATH . '/controllers/test/ProjectNom_Toast.php');

class Database_tests extends ProjectNom_Toast
{
	function __construct()
	{
		parent::__construct(__FILE__); // Remember this
	}

	function test_database_error_handling()
	{
		$this->load->database();

		try
		{
			$query = $this->db->get('badtable');
			$this->_fail("Database exception was not caught.");
		}
		catch (Exception $e)
		{
			$this->message = $e->getMessage();
			$this->_assert_true(stripos($e->getMessage(), "Error Number: 1146") !== FALSE);
		}
	}
}
?>