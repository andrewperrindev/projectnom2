<?php
require_once(APPPATH . '/controllers/test/ProjectNom_Toast.php');

class User_tests extends ProjectNom_Toast
{
	function __construct()
	{
		parent::__construct(__FILE__); // Remember this
		
		$this->load->database();
	}

	function test_user_login_logout_success()
	{
 		$fields = array (
 			'username' => 'mageuzi',
 			'password' => 'jessiekat');

 		$curlResult = $this->doCurlPost("/api/1/user/login", $fields);
		$this->message = $curlResult['body'] . "<BR>" . $curlResult['headers'][0]['Set-Cookie']['pn_session'];
		$api_result = json_decode($curlResult['body']);

		$this->_assert_equals($api_result->success, 1);
		$this->_assert_equals($api_result->username, 'mageuzi');
		$this->_assert_true(isset($curlResult['headers'][0]['Set-Cookie']['pn_session']));

 		$fields = "{\"username\": \"mageuzi\",\"password\": \"jessiekat\"}";

 		$curlResult = $this->doCurlPost("/api/1/user/login", $fields);
		$this->message .= "<BR>" . $curlResult['body'] . "<BR>" . $curlResult['headers'][0]['Set-Cookie']['pn_session'];
		$api_result = json_decode($curlResult['body']);

		$this->_assert_equals($api_result->success, 1);
		$this->_assert_equals($api_result->username, 'mageuzi');
		$this->_assert_true(isset($curlResult['headers'][0]['Set-Cookie']['pn_session']));

 		$curlResult = $this->doCurlPost("/api/1/user/logout", $fields);
		$this->message .= "<BR>" . $curlResult['body'];
		$api_result = json_decode($curlResult['body']);

		$this->_assert_equals($api_result->success, 1);
	}

	function test_user_login_failure()
	{
 		$fields = array (
 			'username' => 'mageuzi',
 			'password' => 'incorrect');

 		$curlResult = $this->doCurlPost("/api/1/user/login", $fields);
		$this->message = $curlResult['body'];
		$api_result = json_decode($curlResult['body']);

		$this->_assert_equals($api_result->success, 0);
		$this->_assert_true(isset($api_result->error));
		$this->_assert_false(isset($curlResult['headers'][0]['Set-Cookie']['pn_session']));
	}

	function test_user_login_invalid()
	{
 		$curlResult = $this->doCurlPost("/api/1/user/login", "{\"username\": \"'; SELECT * FROM USERS --\"}");
		$this->message = $curlResult['body'];
		$api_result = json_decode($curlResult['body']);

		$this->_assert_equals($api_result->success, 0);
	}

	function test_user_login_locked()
	{
 		$curlResult = $this->doCurlPost("/api/1/user/login", "{\"username\": \"FlatFootFox\"}");
		$this->message = $curlResult['body'];
		$api_result = json_decode($curlResult['body']);

		$this->_assert_equals($api_result->success, 0);
		$this->_assert_true(stripos($api_result->error, 'Account is locked.') !== FALSE);
	}

	function test_user_create_validation_failure_username()
	{
		$fields = $this->getTestUser();
		$fields['username'] = '';

		$curlResult = $this->doCurlPost("/api/1/user/create", $fields);
		$this->message = $curlResult['body'];
		$api_result = json_decode($curlResult['body']);

		$this->_assert_equals($api_result->success, 0);
		$this->_assert_true(isset($api_result->error));
		$this->_assert_true(stripos($api_result->error, 'username field is required') !== FALSE);

		$fields['username'] = 'mageuzi';

		$curlResult = $this->doCurlPost("/api/1/user/create", $fields);
		$this->message .= "<BR>" . $curlResult['body'];
		$api_result = json_decode($curlResult['body']);

		$this->_assert_equals($api_result->success, 0);
		$this->_assert_true(isset($api_result->error));
		$this->_assert_true(stripos($api_result->error, 'user name or email already used'));
	}

	function test_user_create_validation_failure_email()
	{
		$fields = $this->getTestUser();
		$fields['email'] = '';

		$curlResult = $this->doCurlPost("/api/1/user/create", $fields);
		$this->message = $curlResult['body'];
		$api_result = json_decode($curlResult['body']);

		$this->_assert_equals($api_result->success, 0);
		$this->_assert_true(isset($api_result->error));
		$this->_assert_true(stripos($api_result->error, 'email field is required'));

		$fields['email'] = 'not_a_valid_email';

		$curlResult = $this->doCurlPost("/api/1/user/create", $fields);
		$this->message .= "<br>" . $curlResult['body'];
		$api_result = json_decode($curlResult['body']);

		$this->_assert_equals($api_result->success, 0);
		$this->_assert_true(isset($api_result->error));
		$this->_assert_true(stripos($api_result->error, 'email field must contain a valid email address'));

		$fields['email'] = 'mageuzi@gmail.com';

		$curlResult = $this->doCurlPost("/api/1/user/create", $fields);
		$this->message .= "<br>" . $curlResult['body'];
		$api_result = json_decode($curlResult['body']);

		$this->_assert_equals($api_result->success, 0);
		$this->_assert_true(isset($api_result->error));
		$this->_assert_true(stripos($api_result->error, 'user name or email already used'));
	}

	function test_user_create_validation_failure_password()
	{
		$fields = $this->getTestUser();
		$fields['password'] = '';

		$curlResult = $this->doCurlPost("/api/1/user/create", $fields);
		$this->message = $curlResult['body'];
		$api_result = json_decode($curlResult['body']);

		$this->_assert_equals($api_result->success, 0);
		$this->_assert_true(isset($api_result->error));
		$this->_assert_true(stripos($api_result->error, 'password field is required'));

		$fields['password'] = 'pswd';

		$curlResult = $this->doCurlPost("/api/1/user/create", $fields);
		$this->message .= "<BR>" . $curlResult['body'];
		$api_result = json_decode($curlResult['body']);

		$this->_assert_equals($api_result->success, 0);
		$this->_assert_true(isset($api_result->error));
		$this->_assert_true(stripos($api_result->error, 'password field does not match the password confirmation field'));

		$fields['password_confirm'] = 'pswd';

		$curlResult = $this->doCurlPost("/api/1/user/create", $fields);
		$this->message .= "<BR>" . $curlResult['body'];
		$api_result = json_decode($curlResult['body']);

		$this->_assert_equals($api_result->success, 0);
		$this->_assert_true(isset($api_result->error));
		$this->_assert_true(stripos($api_result->error, 'password field must be at least 6 characters in length'));

		$fields['password_confirm'] = '';

		$curlResult = $this->doCurlPost("/api/1/user/create", $fields);
		$this->message .= "<BR>" . $curlResult['body'];
		$api_result = json_decode($curlResult['body']);

		$this->_assert_equals($api_result->success, 0);
		$this->_assert_true(isset($api_result->error));
		$this->_assert_true(stripos($api_result->error, 'password confirmation field is required'));
	}

	function test_user_create_success()
	{
		$fields = $this->getTestUser();

		$curlResult = $this->doCurlPost("/api/1/user/create", $fields);
		$this->message = $curlResult['body'];
		$api_result = json_decode($curlResult['body']);
		
		$this->_assert_equals($api_result->success, 1);
		$this->_assert_equals($api_result->username, $fields['username']);
		$this->_assert_equals($api_result->email, $fields['email']);
	}
	
	function test_user_activate_success()
	{
		$this->db->where("task", "ActivationKey");
		$query = $this->db->get("users_accesskeys");
		$this->message = $this->db->last_query();
		$key = $query->row()->key;
		
		$curlResult = $this->doCurlGet("/welcome/activate/$key", null);
		//$this->message = $curlResult['body'];
		$this->_assert_true(strpos($curlResult['body'], "Thank you for confirming your email address! Your account is now active."));

		$this->db->where("task", "ActivationKey");
		$query = $this->db->get("users_accesskeys");
		$this->_assert_equals($query->num_rows(), 0);
	}

	function test_user_resetpassword_success()
	{
		$fields = array("email" => "testuser@projectnom.com");
		$curlResult = $this->doCurlPost("/welcome/forgot", json_encode($fields, JSON_FORCE_OBJECT));
		//$this->message = $curlResult['body'];
		$this->_assert_true(strpos($curlResult['body'], "You should receive an email shortly."));

		$this->db->where("task", "ResetPassword");
		$query = $this->db->get("users_accesskeys");
		$this->message = $this->db->last_query();
		$key = $query->row()->key;
		
		$curlResult = $this->doCurlGet("/welcome/forgot/$key", null);
		//$this->message = $curlResult['body'];
		$this->_assert_true(strpos($curlResult['body'], "Enter a new password here."));

		// INVALID: Passwords don't match
		$fields = array("password" => "P@ssw0rd!2", "confirmPassword" => "P@ssw0rd!3");
		$curlResult = $this->doCurlPost("/welcome/forgot/$key", json_encode($fields, JSON_FORCE_OBJECT));
		//$this->message = $curlResult['body'];
		$this->_assert_true(strpos($curlResult['body'], "Invalid password"));

		// INVALID: Password too short
		$fields = array("password" => "moo", "confirmPassword" => "moo");
		$curlResult = $this->doCurlPost("/welcome/forgot/$key", json_encode($fields, JSON_FORCE_OBJECT));
		//$this->message = $curlResult['body'];
		$this->_assert_true(strpos($curlResult['body'], "Invalid password"));

		$fields = array("password" => "P@ssw0rd!2", "confirmPassword" => "P@ssw0rd!2");
		$curlResult = $this->doCurlPost("/welcome/forgot/$key", json_encode($fields, JSON_FORCE_OBJECT));
		//$this->message = $curlResult['body'];
		$this->_assert_true(strpos($curlResult['body'], "Your password has been reset."));

		$this->db->where("task", "ResetPassword");
		$query = $this->db->get("users_accesskeys");
		$this->_assert_equals($query->num_rows(), 0);

		// INVALID: Bad key
		$fields = array("password" => "P@ssw0rd!", "confirmPassword" => "P@ssw0rd!");
		$curlResult = $this->doCurlPost("/welcome/forgot/$key", json_encode($fields, JSON_FORCE_OBJECT));
		//$this->message = $curlResult['body'];
		$this->_assert_true(strpos($curlResult['body'], "Invalid key"));
	}

	function test_user_update_failure_auth()
	{
		$curlResult = $this->doCurlGet("/api/1/user");
		$this->message = $curlResult['body'];
		$api_result = json_decode($curlResult['body']);

		$this->_assert_equals($api_result->success, 0);
		$this->_assert_equals($api_result->error, "You are not authorized to access this endpoint.");
	}

	function test_user_update_failure_readonly()
	{
		$this->establishReadOnlySession();
		
		$curlResult = $this->doCurlPost("/api/1/user", null, $this->session_cookie_readonly);
		$this->message = $curlResult['body'];
		$api_result = json_decode($curlResult['body']);

		$this->_assert_equals($api_result->success, 0);
		$this->_assert_equals($api_result->error, "You must login before you can edit your account.");
	}
	
	function test_user_update_emailonly_success()
	{
		$this->establishSession(array('username'=>'testuser', 'password'=>'P@ssw0rd!2'));
		
		$fields = array(
			'username' => 'testuser',
			'email' => 'testuserupdate@projectnom.com'
		);
		
		$curlResult = $this->doCurlPost("/api/1/user", $fields);
		$this->message = $curlResult['body'];
		$api_result = json_decode($curlResult['body']);
		
		$this->_assert_equals($api_result->success, 1);

		$curlResult = $this->doCurlGet("/api/1/user");
		$this->message .= "<BR>" . $curlResult['body'];
		$api_result = json_decode($curlResult['body']);

		$this->_assert_equals($api_result->success, 1);
		$this->_assert_equals($api_result->result->email, "testuserupdate@projectnom.com");
	}

	function test_user_update_emailonly_failure()
	{
		$this->establishSession(array('username'=>'testuser', 'password'=>'P@ssw0rd!2'));
		
		$fields = array(
			'username' => 'testuser',
			'email' => 'testuserupdate@'
		);
		
		$curlResult = $this->doCurlPost("/api/1/user", $fields);
		$this->message = $curlResult['body'];
		$api_result = json_decode($curlResult['body']);
		$this->_assert_equals($api_result->success, 0);
		$this->_assert_true(stripos($api_result->error, "The Email field must contain a valid email address."));
	}

	function test_user_update_emailonly_duplicate_failure()
	{
		$this->establishSession(array('username'=>'testuser', 'password'=>'P@ssw0rd!2'));
		
		$fields = array(
			'username' => 'testuser',
			'email' => 'mageuzi@gmail.com'
		);
		
		$curlResult = $this->doCurlPost("/api/1/user", $fields);
		$this->message = $curlResult['body'];
		$api_result = json_decode($curlResult['body']);
		$this->_assert_equals($api_result->success, 0);
		$this->_assert_true(stripos($api_result->error, "This email is used by another account."));
	}

	function test_user_update_emailandpassword_success()
	{
		$this->establishSession(array('username'=>'testuser', 'password'=>'P@ssw0rd!2'));
		
		$fields = array(
			'username' => 'testuser',
			'email' => 'testuserupdate@projectnom.com',
			'password' => 'N3wP@ssw0rd!',
			'password_confirm' => 'N3wP@ssw0rd!'
		);
		
		$curlResult = $this->doCurlPost("/api/1/user", $fields);
		$this->message = $curlResult['body'];
		$api_result = json_decode($curlResult['body']);
		
		$this->_assert_equals($api_result->success, 1);

		$curlResult = $this->doCurlGet("/api/1/user");
		$this->message .= "<BR>" . $curlResult['body'];
		$api_result = json_decode($curlResult['body']);

		$this->_assert_equals($api_result->success, 1);
		$this->establishSession(array('username'=>'testuser', 'password'=>'N3wP@ssw0rd!'));
		$this->message .= "<BR>" . $this->session_cookie;
		$this->_assert_true(strlen($this->session_cookie) > strlen("pn_session="));
	}

	function test_user_update_emailandpassword_failure()
	{
		$this->establishSession(array('username'=>'testuser', 'password'=>'N3wP@ssw0rd!'));
		
		$fields = array(
			'username' => 'testuser',
			'email' => 'testuserupdate@projectnom.com',
			'password' => 'N3wP@ssw0rd!',
			'password_confirm' => 'P@ssw0rd!'
		);
		
		$curlResult = $this->doCurlPost("/api/1/user", $fields);
		$this->message = $curlResult['body'];
		$api_result = json_decode($curlResult['body']);
		
		$this->_assert_equals($api_result->success, 0);
		$this->_assert_true(stripos($api_result->error, "The Password field does not match the Password Confirmation field."));
	}

	private function getTestUser()
	{
		$this->db->delete("users_accesskeys", array("task" => "ActivationKey"));
		$this->db->delete("users_accesskeys", array("task" => "ResetPassword"));
		$this->db->delete('users', array('username' => 'testuser'));

		return array (
			'username' => 'testuser',
			'email' => 'testuser@projectnom.com',
			'password' => 'P@ssw0rd!',
			'password_confirm' => 'P@ssw0rd!');
	}
}
?>