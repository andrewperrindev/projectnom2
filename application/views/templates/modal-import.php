<!-- Import Modal -->
<div class="modal fade bs-import-modal">
	<div class="modal-content">
		<div class="container">
			<div class="row text-dark">
				<div class="col-sm-12">
    				<div class="modal-body">
    					<table>
    						<tr>
    							<td class='header'>
									<button type="button" class="close btn-lg" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
									<h3 class="modal-title">Import <span class="import-type"></span></h3>
								</td>
							</tr>
							<tr>
								<td>
									<div class="alert alert-info" role="alert">
										<strong>Here is the recipe you chose to import.</strong> Click and drag to select the text you'd like to use, then click Import.
									</div>
								</td>
							</tr>
    						<tr>
    							<td class='content'>
	    							<div style="overflow: auto; -webkit-overflow-scrolling: touch;">
										<iframe id="recipe" src="<?php print "/$cache_url"; ?>" width="100%" height="95%" data-origin="<?php print $origin_url; ?>"></iframe>
	    							</div>
									<input type="hidden" class="captured-text" />
								</td>
							</tr>
							<tr>
								<td class='footer'>
									<button type="submit" class="btn btn-default btn-cancel" data-dismiss="modal">Cancel</button>
									<span id="importPlaceholder"></span>
								</td>
							</tr>
						</table>
					</div>
				</div>
			</div>
    	</div>
	</div><!-- /.modal-content -->
</div><!-- /.modal -->
