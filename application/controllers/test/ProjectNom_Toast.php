<?php
require_once(APPPATH . '/controllers/test/Toast.php');

abstract class ProjectNom_Toast extends Toast
{
	private $rememberme_cookie = "remember_accesskey=mageuzi%26{TOKEN}; expires=Thu, 08-May-2025 04:32:52 GMT; path=/; domain=.projectnom2.dev";

	protected $session_cookie;
	protected $session_cookie_readonly;

	function __construct($test_file)
	{
		parent::__construct($test_file); // Remember this
	}

	protected function establishSession($fields = null, $force = false)
	{
		if (!isset($this->session_cookie) || $force)
		{
			if (empty($fields))
			{
		 		$fields = array (
		 			'username' => 'mageuzi',
		 			'password' => 'jessiekat');
		 	}

	 		$curlResult = $this->doCurlPost("/api/1/user/login", $fields);
			$this->session_cookie = "pn_session=" . $curlResult['headers'][0]['Set-Cookie']['pn_session'];
			//$this->message .= "<BR>" . print_r($curlResult['headers'], true);
		}
	}

	protected function establishReadOnlySession()
	{
		if (!isset($this->session_cookie_readonly))
		{
			$this->load->database();
			$query = $this->db->get_where('users_accesskeys', array('user_id' => 1, 'task' => 'RememberMe'));
			$accesskey = $query->row()->key;
			$this->session_cookie_readonly = str_replace("{TOKEN}", $accesskey, $this->rememberme_cookie);

 			$curlResult = $this->doCurlGet("/", $this->session_cookie_readonly);
 			//$this->message = print_r($curlResult, true);
			$this->session_cookie_readonly = "pn_session=" . $curlResult['headers'][0]['Set-Cookie']['pn_session'];
		}
	}

	protected function doCurlGet($url_segment, $cookie = null)
	{
		$url = 'http://' . $_SERVER['SERVER_NAME'] . $url_segment;
		$ch = curl_init($url);

		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_HEADER, TRUE);
		curl_setopt($ch, CURLOPT_VERBOSE, TRUE);

		if (!empty($cookie) || isset($this->session_cookie))
		{
			if (empty($cookie))
			{
				$cookie = $this->session_cookie;
			}

			curl_setopt($ch, CURLOPT_COOKIE, $cookie);
		}

		$response = $this->getCurlResponse($ch);

		curl_close($ch);

		return $response;
	}

	protected function doCurlPost($url_segment,$post_data, $cookie = null)
	{
		$url = 'http://' . $_SERVER['SERVER_NAME'] . $url_segment;
		$header = array('Content-Type: application/x-www-form-urlencoded');
		$ch = curl_init($url);
 
 		//url-ify the data for the POST
 		$fields_string = "";
 		if (is_array($post_data))
 		{
			foreach($post_data as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
			rtrim($fields_string, '&');
		}
		else
		{
			$fields_string = $post_data;
			if (substr($post_data, 0, 1) == "{")
			{
				$header = array('Content-Type: application/json');
			}
		}

		curl_setopt($ch, CURLOPT_POST, TRUE);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_HEADER, TRUE);
		curl_setopt($ch, CURLOPT_VERBOSE, TRUE);

		if (!empty($cookie) || isset($this->session_cookie))
		{
			if (empty($cookie))
			{
				$cookie = $this->session_cookie;
			}

			curl_setopt($ch, CURLOPT_COOKIE, $cookie);
		}
 
		$response = $this->getCurlResponse($ch);

		curl_close($ch);

		return $response;
	}

	protected function doCurlDelete($url_segment, $cookie = null)
	{
		$url = 'http://' . $_SERVER['SERVER_NAME'] . $url_segment;
		$ch = curl_init($url);

		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_HEADER, TRUE);
		curl_setopt($ch, CURLOPT_VERBOSE, TRUE);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");

		if (!empty($cookie) || isset($this->session_cookie))
		{
			if (empty($cookie))
			{
				$cookie = $this->session_cookie;
			}

			curl_setopt($ch, CURLOPT_COOKIE, $cookie);
		}

		$response = $this->getCurlResponse($ch);

		curl_close($ch);

		return $response;
	}

	protected function doCurlBinaryPost($url_segment, $file, $cookie = null)
	{
		$url = 'http://' . $_SERVER['SERVER_NAME'] . $url_segment;
		$header = array('Content-Type: multipart/form-data');
		$fields = array('userfile' => "@$file");
 
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_POST, TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
		curl_setopt($ch, CURLOPT_HEADER, TRUE);
		curl_setopt($ch, CURLOPT_VERBOSE, TRUE);

		if (!empty($cookie) || isset($this->session_cookie))
		{
			if (empty($cookie))
			{
				$cookie = $this->session_cookie;
			}

			curl_setopt($ch, CURLOPT_COOKIE, $cookie);
		}

		$response = $this->getCurlResponse($ch);
		curl_close($ch);

		return $response;
	}

	protected function getCurlResponse($curlHandle)
	{
		$response = curl_exec($curlHandle);

		$header_size = curl_getinfo($curlHandle, CURLINFO_HEADER_SIZE);
		$header = substr($response, 0, $header_size);
		$body = substr($response, $header_size);

		return array('headers'=>$this->parseHeaders($header), 'body'=>$body);		
	}

	protected function parseHeaders($headerContent)
	{
	    $headers = array();

	    // Split the string on every "double" new line.
	    $arrRequests = explode("\r\n\r\n", $headerContent);

	    // Loop of response headers. The "count() -1" is to 
	    //avoid an empty row for the extra line break before the body of the response.
	    for ($index = 0; $index < count($arrRequests) -1; $index++) {

	        foreach (explode("\r\n", $arrRequests[$index]) as $i => $line)
	        {
	            if ($i === 0)
	                $headers[$index]['http_code'] = $line;
	            else
	            {
	                list ($key, $value) = explode(': ', $line);
	                if ($key == 'Set-Cookie')
	                {
	                	list ($cookie_name, $cookie_value) = explode('=', $value, 2);
	                	$headers[$index][$key][$cookie_name] = $cookie_value;
	                }
	                else
	                {
		                $headers[$index][$key] = $value;
	                }
	            }
	        }
	    }

	    return $headers;
	}	
}
?>