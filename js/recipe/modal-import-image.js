var ImportImage =
{
	init: (function() {
		return function()
		{
			$(".bs-import-image-modal .import-recipe-image").on("click", selectImage);
			$(".bs-import-image-modal .btn-confirm").on("click", confirmImage);

			$("div.bs-import-image-modal")
				.on("hide.bs.modal", function(event)
				{
					$(".bs-import-image-modal img").removeClass("selected");
					$(".bs-import-image-modal .chosen-image-url").val("");
				});
		};
	})()
};

function selectImage(event)
{
	if ($(this).hasClass("selected"))
	{
		$(this).removeClass("selected");
		$(".bs-import-image-modal .chosen-image-url").val("");
	}
	else
	{
		$(this).addClass("selected");
		$(".bs-import-image-modal .chosen-image-url").val($(this).attr("src"));
	}
}

function confirmImage(event)
{
	event.preventDefault();
	var chosen_image = $(".bs-import-image-modal .chosen-image-url").val();

	if (chosen_image !== "")
	{
		$("img.recipe-thumb").attr("src", chosen_image);
		$("input#recipe-img").val(chosen_image);
	}

	$(".bs-import-image-modal").modal("hide");
}