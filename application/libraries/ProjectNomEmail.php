<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class ProjectNomEmail 
{
	private $CI;

	public function __construct()
    {
    	$this->CI =& get_instance();
    }
    
    public function sendNewAccountEmail($to, $activation_key)
    {
	    $data['activation_key'] = $activation_key;
	    $message = $this->CI->load->view("email/newaccount", $data, TRUE);
	    
	    $this->sendEmail($to, "Welcome to ProjectNom!", $message);
    }

    public function sendResetPasswordEmail($to, $key)
    {
	    $data['key'] = $key;
	    $message = $this->CI->load->view("email/resetpassword", $data, TRUE);
	    
	    $this->sendEmail($to, "Reset Password Request", $message);
    }
    
    private function sendEmail($to, $subject, $message)
    {
		$this->CI->load->library('email');
		
		$this->CI->email->from('support@projectnom.com', 'ProjectNom');
		$this->CI->email->to($to); 
		
		$this->CI->email->subject($subject);
		$this->CI->email->message($message);	
		
		$this->CI->email->send();
		//echo $this->CI->email->print_debugger();
    }
}
?>