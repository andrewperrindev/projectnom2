var React = require('react');

module.exports = React.createClass({
/*
	defaultClick: function() {
		setTimeout(function() {
			this.setState({isBusy: true});
		}, 1000);
	},
*/
	getInitialState: function() {
		return {
			busy: this.props.initialBusy,
			disabled: this.props.initialDisabled
		};
	},

	busy: function() {
		this.setState({busy: true});
	},
	
	activate: function() {
		this.setState({busy: false});
	},
	
	disable: function() {
		this.setState({disabled: true});
	},
	
	enable: function() {
		this.setState({disabled: false});
	},

	render: function() {
		var buttonClasses = "btn";
		buttonClasses += this.props.style ? " btn-" + this.props.style : " btn-default";
		
		if (this.props.classes && this.props.classes.button) {
			buttonClasses += " " + this.props.classes.button;
		}
				
		if (this.state.busy)
		{
			var busyLabel = this.props.busyLabel
				? React.DOM.span({style: {paddingLeft: 10}}, this.props.busyLabel)
				: "";

			return React.DOM.button({className: buttonClasses, style: {minWidth: 75}, disabled: "disabled"},
				React.DOM.i({className: "fa fa-circle-o-notch fa-spin"}),
				busyLabel
			);
		}
		else
		{
			var icon = "";
			var labelClasses = this.props.classes && this.props.classes.label
				? this.props.classes.label
				: "";
			var padding = this.props.iconPadding !== undefined
				? this.props.iconPadding
				: 10;

			if (this.props.icon) {
				var iconClasses = "fa fa-" + this.props.icon;
				if (this.props.classes && this.props.classes.icon) {
					iconClasses += " " + this.props.classes.icon;
				}
				icon = React.DOM.i({className: iconClasses, style: {paddingRight: padding}});
			}
			
			var attributes = {className: buttonClasses, type: this.props.type, id: this.props.id, onClick: this.props.onClick};
			
			if (this.state.disabled) {
				attributes.disabled = "disabled";
			}

			return React.DOM.button(attributes,
				icon,
				React.DOM.span({className: labelClasses}, this.props.label)
			);
		}
	}
});
