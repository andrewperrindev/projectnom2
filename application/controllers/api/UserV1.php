<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class UserV1 extends CI_Controller 
{
	const ERROR_TEXT = "Please contact <a href='mailto:support@projectnom.com'>ProjectNom support</a> for assistance.";

	public function __construct()
	{
		parent::__construct();
		
		if (!empty($_SERVER["CONTENT_TYPE"]) && $_SERVER["CONTENT_TYPE"] == "application/json")
		{
			$_POST = array();
			$json_result = json_decode(file_get_contents("php://input"), true);
	
			if (!empty($json_result))
			{
				$_POST = $json_result;
			}
		}
	}

	public function index()
	{
		$this->load->library('session');
		
		if ($this->session->userdata('username') !== NULL)
		{
			$data = array();
			$data['username'] = $this->session->userdata("username");
			$data['readonly'] = $this->session->userdata('readonly');
			$this->load->model('User_model', 'userService', TRUE);

			if ($this->input->server('REQUEST_METHOD') == "GET")
			{
				$userDetails = $this->userService->getAccountDetails($data['username']);
				
				if (!empty($userDetails) && is_array($userDetails))
				{
					$data['result'] = array(
						'username' => $userDetails['username'],
						'email' => $userDetails['email']
					);
				}
				else
				{
					$data['error'] = "User profile could not be found.";
				}
			}
			if ($this->input->server('REQUEST_METHOD') == "POST")
			{
				if ($data['readonly'])
				{
					$data['error'] = "You must login before you can edit your account.";
				}
				else
				{
					$this->load->library('form_validation');

					$this->form_validation->set_rules('email', 'Email', 'required|valid_email|trim');
					
					if (trim($this->input->post("password")) != "")
					{
						$this->form_validation->set_rules('password', 'Password', 'matches[password_confirm]|min_length[6]|trim');
						$this->form_validation->set_rules('password_confirm', 'Password Confirmation', 'trim');
					}

					try
					{
						if ($this->form_validation->run())
						{
							$newUserData = array(
								'username' => $data['username'],
								'email' => $this->input->post("email"),
								'password' => $this->input->post("password"),
								'removeTwitter' => $this->input->post("remove_twitter") == 1
							);
							$this->userService->updateAccount($newUserData);
							
							if ($newUserData['removeTwitter'])
							{
								$this->session->set_userdata(array('twitter' => null));
							}
							
							session_write_close();
						}
						else
						{
							$data['error'] = "<strong>Please fix the following:</strong>" . validation_errors();
						}
					}
					catch (Exception $e)
					{
						$errorDetail = self::ERROR_TEXT;
						
						if ($e->getMessage() == "Email exists.")
						{
							$errorDetail = "This email is used by another account.";
						}
						
						$data['error'] = "<strong>Profile update failed.</strong> $errorDetail";
						log_message("error", $e->getMessage());
					}

				}
			}
						
			$this->load->view('api/simple', $data);
		}
		else
		{
			$this->returnUnauthorized();
		}
	}
	
	public function create()
	{
		$this->load->library('form_validation');
		$this->load->model('User_model', 'userService', TRUE);
		
		$this->form_validation->set_rules('username', 'Username', 'required|trim');
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email|trim');
		$this->form_validation->set_rules('password', 'Password', 'required|matches[password_confirm]|min_length[6]|trim');
		$this->form_validation->set_rules('password_confirm', 'Password Confirmation', 'required|trim');

		$data['username'] = $this->input->post("username");
		$data['email'] = $this->input->post("email");
		$data['password'] = $this->input->post("password");
		$password_confirm = $this->input->post("password_confirm");

		try
		{
			if ($this->form_validation->run())
			{
				$activation_key = $this->userService->createAccount($data);
				
				$this->load->library("ProjectNomEmail");
				$this->projectnomemail->sendNewAccountEmail($data['email'], $activation_key);
			}
			else
			{
				$data['error'] = "<strong>Please fix the following:</strong>" . validation_errors();
			}
		}
		catch (Exception $e)
		{
			if ($e->getMessage() == "Username or email exists.")
			{
				$data['error'] = "<strong>User name or email already used.</strong> Please try another user name.";
			}
			else
			{
				$data['error'] = "<strong>Account creation error occurred.</strong> " . self::ERROR_TEXT;
				//error_log($e->getMessage());
			}
		}
		
		$this->load->view('api/user_create', $data);
	}
	
	public function login()
	{
		$this->load->model('User_model', 'userService', TRUE);

		$data['username'] = $this->input->post("username");
		$data['password'] = $this->input->post("password");

		try
		{
			if ($this->userService->validateAccount($data))
			{
				$this->load->library('session');
				$this->session->set_userdata(array('username' => $data['username']));
				$this->session->set_userdata(array('readonly' => FALSE));
				$this->session->set_userdata(array('twitter' => $data['twitter']));

				if ($this->input->post("rememberme") == 1)
				{
					$new_access_key = $this->userService->setAccessKey($data['username']);

					if ($new_access_key !== FALSE)
					{
						$this->input->set_cookie(
							array(
							    'name'   => 'remember_accesskey',
							    'value'  => $new_access_key,
							    'expire' => 60*60*24*365*10,
							    'domain' => '.' . $_SERVER['SERVER_NAME']
							)
						);
					}
				}
				
				session_write_close();
			}
		}
		catch (Exception $e)
		{
			if ($e->getMessage() == "Incorrect password." || $e->getMessage() == "Account does not exist in database.")
			{
				$data['error'] = "<strong>Incorrect user name or password specified.</strong> Please try again.";
			}
			else if ($e->getMessage() == "Account is locked.")
			{
				$data['error'] = "<strong>Account is locked.</strong> Please check your email for instructions on how to activate your account.";
			}
			else
			{
				log_message('error', print_r($e, TRUE));
				$data['error'] = "<h4>Login error occurred.</h4> " . self::ERROR_TEXT;
			}
		}
		
		$this->load->view('api/user_login', $data);
	}

	public function logout()
	{
		$this->load->helper('cookie');
		$this->load->library('session');
		$this->session->unset_userdata("username");
		$this->session->unset_userdata("readonly");
		$this->session->unset_userdata("rememberme");
		delete_cookie("remember_accesskey", '.' . $_SERVER['SERVER_NAME']);
		$this->session->sess_destroy();
		session_write_close();

		$this->load->view('api/simple');
	}
	
	private function returnUnauthorized()
	{
		header('HTTP/1.1 401 Unauthorized', true, 401);
		header('Content-Type: application/json');
		print json_encode(array(
			'success' => 0,
			'error' => 'You are not authorized to access this endpoint.'
		));
		die();
	}
}