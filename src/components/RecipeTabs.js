var React = require('react');

var fakewaffle = require('../lib/responsive-tabs.js');

var BootstrapError = require('./BootstrapError.js');
var RecipeGroup =require('./RecipeGroup.js');

module.exports = React.createClass({
	getInitialState: function() {
		var dayPart = this.props.dayParts.reduce(function getCurrent(prev, curr) {
				if (prev === "") {
					return curr.isCurrent() ? curr.id : prev;
				}
				
				return prev;
			}, 
			"");
		
		return {
			dayPart: dayPart,
			current: 0,
			tabs: [
				{
					name: "New",
					endpoint: "/api/1/recipes/recent",
					data: [],
					loaded: false,
					more: "recipe/search/1/new"
				},
				{
					name: "Favorites",
					endpoint: "/api/1/recipes/favorite",
					data: [],
					loaded: false,
					more: "recipe/search/1/favorite"
				},
				{
					name: "Different",
					endpoint: "/api/1/recipes/different",
					data: [],
					loaded: false,
					more: "recipe/search/1/different"
				}
			]
		};
	},
		
	fetchRecipeData: function fetchData() {
		/*
			So this is kind of messy. What we're doing here is sending off an AJAX request
			for each item in our tabs array. The problem with that is we don't know which
			request will finish first. How do we synchronize our updates to state?
			
			Thankfully, setState has a variant which takes a callback. This callback is
			guaranteed to be syncronous, so when we update our state with new information,
			we can be assured that we're not pummeling data from another callback.
		*/
		this.state.tabs.map(function loadTabs(tab) {
			ajaxGet(tab.endpoint + "/" + this.state.dayPart, null)
				.done(function(data)
				{
					this.setState(function modifyState(oldState) {
						return {
							tabs: oldState.tabs.map(function(oldTab) {
								if (oldTab.name == tab.name) {
									return {
										name: oldTab.name,
										endpoint: oldTab.endpoint,
										data: data.recipes,
										loaded: true
									};
								}
								else {
									return oldTab;
								}
							})
						};
					});
				}.bind(this));			
		}.bind(this));		
	},
	
	dayPartChange: function dayPartChange(event) {
		this.setState(
			{
				dayPart: event.target.value,
				tabs: this.state.tabs.map(function(oldTab) {
					return {
						name: oldTab.name,
						endpoint: oldTab.endpoint,
						data: [],
						loaded: false
					};
				})
			}, 
			this.fetchRecipeData);
	},
	
	tabChange: function tabChange(event) {
		//$("div.see-more").text(event.target.id);
		$.each(this.state.tabs, function(index, tab) {
			if ("tab" + tab.name == event.target.id) 
			{
				this.setState({current: index});
				$(this.refs.moreUrl)
					.attr("href", tab.more);
				//	.show();
				return false;
			}
		}.bind(this));
	},

	componentDidMount: function() {
		this.fetchRecipeData();
		
		if (this.state.tabs.length > 0)
		{
			$("a[data-toggle='tab']").on("shown.bs.tab", this.tabChange);
			fakewaffle.responsiveTabs(['xs']);
			$("#tab" + this.state.tabs[0].name).tab('show');
		}
	},

	render: function() {
		var currentTab = this.state.tabs[this.state.current];
		
		// Tab Options
		var tabs = this.state.tabs.map(function getTabs(tab) {
			var t = tab.name.toLowerCase();
			return React.DOM.li({role: "presentation", key: t},
				React.DOM.a({href: "#" + t, "aria-controls": t, role: "tab", "data-toggle": "tab", id: "tab" + tab.name}, tab.name));
		});
		var tabList = React.DOM.ul({className: "nav nav-tabs responsive", role: "tablist", id: "tabs"}, tabs);

		// Tab Content
		var tabpanels = this.state.tabs.map(function getTabPanels(tab) {
			var t = tab.name.toLowerCase();
			return React.DOM.div({className: "tab-pane content-pane", id: t, key: t, role: "tabpanel"},
				React.createElement(BootstrapError, {visible: tab.data.length === 0 && tab.loaded, 
					className: "retrieve-error col-sm-10 col-sm-offset-1",
					title: "No matching recipes found."}),
				React.createElement(RecipeGroup, {recipes: tab.data, loaded: tab.loaded}));
		});
		var tabContent = React.DOM.div({className: "tab-content responsive"}, tabpanels);
		
		// Day Part Selection
		var dayPartOption = React.DOM.div({className: "tab-detail form-inline"},
			React.DOM.div({className: "row"},
				React.DOM.div({className: "col-xs-12 col-sm-6 col-sm-push-6 part-select"},
					"Show ",
					React.DOM.select(
						{
							className: "form-control", 
							value: this.state.dayPart, 
							onChange: this.dayPartChange, 
							style: {display: "inline-block", width: "auto"}
						},
						this.props.dayParts.map(function populateDayParts(dayPart) {
							return React.DOM.option({key: dayPart.id, value: dayPart.id}, dayPart.name);
						})
					),
					" recipes"
				),
				React.DOM.div({className: "col-xs-12 col-sm-6 col-sm-pull-6 see-more", style: {height: 34, paddingTop: 8}, ref: "seeMore"},
					React.DOM.a(
						{ref: "moreUrl", style: {visibility: currentTab.data.length === 0 ? "hidden" : "visible"}}, 
						"More Recipes Like This"))
			)
		);
		
		return React.DOM.div({},
			React.DOM.div({className: "tabpanel"}, tabList, tabContent),
			dayPartOption);
	}
});
