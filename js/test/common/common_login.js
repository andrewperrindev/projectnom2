function getServerName() {
	return "http://projectnom.dev/";
}

casper.options.waitTimeout = 20000;

casper.test.on("fail", function(failure) {
	casper.exit(1);
});

casper.on("page.error", function(msg, trace) {
    this.echo("PhantomJS Page Error: " + msg);
});

casper.on('remote.message', function(message) {
    this.echo('PhantomJS console.log: ' + message);
});

function login_mageuzi(test, url)
{
    casper.start(getServerName() + url, function() {
        test.assertTitle("Welcome :: ProjectNom", "Public homepage title is the one expected");
        test.assertExists('form#signupForm', "Sign up form is found");
        test.assertEval(function() {
            return __utils__.findAll("ul.navbar-nav li:not(.hidden)").length == 3;
        }, "Nav Bar Shows 3 Options");

		login_mageuzi_credentials(test, this);
        this.echo("Waiting for login to process...");
	    this.waitForUrl(new RegExp(url));
    });	
}

function login_mageuzi_credentials(test, page)
{
    test.assertExists('form#loginForm', "Login form is found");
    page.fill('form#loginForm', {
        username: "mageuzi",
        password: "jessiekat"
    }, false);
    page.click("button.btn-login");	
}

function login_wolf_credentials(test, page)
{
    test.assertExists('form#loginForm', "Login form is found");
    page.fill('form#loginForm', {
        username: "Wolf",
        password: "jessiekat",
        rememberme: true
    }, false);
    page.click("button.btn-login");	
}

// PhantomJS 1.x doesn't support .bind()
// This is a polyfill workaround, stolen from:
// http://stackoverflow.com/questions/25359247/casperjs-bind-issue
casper.on( 'page.initialized', function(){
    this.evaluate(function(){
        var isFunction = function(o) {
          return typeof o == 'function';
        };

        var bind,
          slice = [].slice,
          proto = Function.prototype,
          featureMap;

        featureMap = {
          'function-bind': 'bind'
        };

        function has(feature) {
          var prop = featureMap[feature];
          return isFunction(proto[prop]);
        }

        // check for missing features
        if (!has('function-bind')) {
          // adapted from Mozilla Developer Network example at
          // https://developer.mozilla.org/en/JavaScript/Reference/Global_Objects/Function/bind
          bind = function bind(obj) {
            var args = slice.call(arguments, 1),
              self = this,
              nop = function() {
              },
              bound = function() {
                return self.apply(this instanceof nop ? this : (obj || {}), args.concat(slice.call(arguments)));
              };
            nop.prototype = this.prototype || {}; // Firefox cries sometimes if prototype is undefined
            bound.prototype = new nop();
            return bound;
          };
          proto.bind = bind;
        }
    });
});