#!/bin/bash
for f in *.js
do
	casperjs test --includes=common/common_login.js,common/common_eval.js $f
	
	if [ $? -ne 0 ]
	then
		echo "FAIL: Previous tests failed!"
		exit 99
	fi
done
