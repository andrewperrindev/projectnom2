casper.test.begin('Landing page behaves correctly', 40, 
function suite(test) 
{
	casper.start(getServerName() + "landing/message/12", function() {
		this.echo("Make sure landing page visible without login:");
		test.assertTitle("Message :: ProjectNom", "Page title is correct");
		
		test.assertDoesntExist("a[href=#about]", "About nav not available");
		test.assertDoesntExist("a[href=#signup]", "Sign Up nav not available");
		test.assertDoesntExist("a.login-modal", "Login nav not available");
		
		test.assertDoesntExist("li.nav-search", "Search nav not available");
		test.assertDoesntExist("li.dropdown a[title=Friends]", "Friends nav not available");
		test.assertDoesntExist("li.dropdown a[title='Shopping List']", "Shopping List nav not available");
		test.assertDoesntExist("li.dropdown#recipeNav", "Recipe nav not available");
		test.assertDoesntExist("li.dropdown i.fa-user", "User nav not available");
		
		test.assertSelectorHasText("div.alert-success", "Welcome to ProjectNom! We help cooks organize and share recipes.", "Welcome message visible");
		test.assertSelectorHasText("p.username", "jadieladie", "Message username correct");
		test.assertSelectorHasText("p.message", "Post from a completely different user lorem ipsum", "Message content correct");
		
		test.assertSelectorHasText("div.attachment#recipe span.attachment-content", "The Right Bank", "Recipe title visible");
		test.assertVisible("div.attachment#recipe button.preview", "Recipe preview button visible");
		test.assertVisible("div.attachment#recipe button.import", "Recipe import button visible");
		
		test.assertVisible("div.attachment#image img", "Image attachment visible");
	});

    casper.then(function() {
	   this.click("div.attachment-section button.preview");
	   this.waitUntilVisible('div.bs-recipe-preview-modal h2.recipe-title');
	});
	
	casper.then(function() {
	   test.assertEquals(this.fetchText("div.bs-recipe-preview-modal h2.recipe-title"), "The Right Bank Courtesy of www.bonappetit.com", "Recipe title loaded");
	   test.assertEquals(this.fetchText("div.bs-recipe-preview-modal div.recipe-author"), "by Kristin Almy", "Recipe author loaded");
	   test.assertEquals(this.fetchText("div.bs-recipe-preview-modal span.recipe-time"), " Total: 5min;  Prep: 5 min", "Recipe time loaded");
	   test.assertEquals(this.fetchText("div.bs-recipe-preview-modal span.recipe-servings"), "4 rocks glasses", "Recipe servings loaded");
       test.assertEval(function() {
            return __utils__.findAll("div.bs-recipe-preview-modal div.ingredient-row:not(.hidden)").length == 7;
        }, "7 Ingredients loaded");

	   this.click("div.bs-recipe-preview-modal button.btn-cancel");
	   this.waitWhileVisible('div.bs-recipe-preview-modal h2.recipe-title');
	});

    casper.then(function() {
	   this.click("div.attachment-section button.import");
	   this.waitUntilVisible('div.bs-recipeimport-modal div.modal-body');
	});

	casper.then(function() {
		test.assertSelectorHasText("div.bs-recipeimport-modal div.modal-body", "Ready to start your recipe collection? Sign up now for a ProjectNom account and you can save this recipe -- as well as any other recipes you find online! They'll all be safe and sound in one place, ready for you to cook something awesome.", "Non-account Import Modal Text Visible");
		this.echo("Make sure Sign Up button redirects properly");
		this.click("button#signUpRedirect");
		this.waitForUrl(getServerName() + "#signup");
	});
	
	casper.thenOpen(getServerName() + "landing/message/12", function() {
	   this.click("div.attachment-section button.import");
	   this.waitUntilVisible('div.bs-recipeimport-modal div.modal-body');
	});

	casper.then(function() {
		this.echo("Make sure Log In button redirects properly:");
		this.click("button#logInRedirect");
		this.waitForUrl(getServerName());
	});
	
	casper.waitUntilVisible('form#loginForm', function() {
		this.capture("loginform.png");
		login_mageuzi_credentials(test, this);
		this.waitForUrl(getServerName() + "landing/message/12");
	});
	
	casper.then(function() {
		this.echo("Make sure landing page correct after login:");
		test.assertTitle("Message :: ProjectNom", "Page title is correct");
		
		test.assertDoesntExist("a[href=#about]", "About nav not available");
		test.assertDoesntExist("a[href=#signup]", "Sign Up nav not available");
		test.assertDoesntExist("a.login-modal", "Login nav not available");
		
		test.assertExists("li.nav-search", "Search nav not available");
		test.assertExists("li.dropdown a[title=Friends]", "Friends nav not available");
		test.assertExists("li.dropdown a[title='Shopping List']", "Shopping List nav not available");
		test.assertExists("li.dropdown#recipeNav", "Recipe nav not available");
		test.assertExists("li.dropdown i.fa-user", "User nav not available");
		
		test.assertSelectorDoesntHaveText("div.alert-success", "Welcome to ProjectNom! We help cooks organize and share recipes.", "Welcome message not needed");
		test.assertSelectorHasText("p.username", "jadieladie", "Message username correct");
		test.assertSelectorHasText("p.message", "Post from a completely different user lorem ipsum", "Message content correct");
	});

	casper.then(function() {
		this.echo("Make sure recipe imports:");
		this.click("button.import");
		this.waitUntilVisible("div.bs-recipeimport-modal div.recipe-title");
	});
	
	casper.then(function() {
		test.assertVisible("div.bs-recipeimport-modal div.recipe-title", "Import Modal Visible");
		test.assertSelectorHasText("div.modal-body div.recipe-title span.recipe-title", "The Right Bank", "Recipe Title Visible");
		this.click("div.bs-recipeimport-modal button.btn-confirm");
		this.waitForSelector("div.saved-msg");
	});
	
	casper.then(function() {
		test.assertVisible("div.saved-msg", "Import success message visible");
		test.assertSelectorHasText("div#titlePlaceholder p.content", "The Right Bank", "Recipe title imported");
		test.assertVisible("button.import-image-modal", "Import option provided");
		this.click("button#editRecipeDelete");
		this.waitUntilVisible("div.bootbox button.btn-danger");
	});
	
	casper.then(function() {
		this.click("div.bootbox button.btn-danger");
		this.waitForUrl(getServerName() + "home");
	});
		
    casper.run(function() {
        test.done();
    });
});