casper.test.begin('Add New Recipe Tests', 83,
function suite(test) 
{
	// Login and navigate to messages page. +4 test cases
	login_mageuzi(test, "recipe/edit");

	// Check for correct Add Recipe layout
	casper.then(function() {
		this.echo("Check for correct Add Recipe layout: ");
		test.assertEquals(this.fetchText("h1.brand-heading"), "New Recipe", "Page title loaded");
		
		test.assertExists("button.upload-modal", "Upload image option exists");
		test.assertDoesntExist("button.import-image-modal", "Import image option doesn't exist");

		test.assertExists("div#titlePlaceholder p.content", "Recipe title exists");
		test.assertExists("div#titlePlaceholder input.recipe-title", "Recipe title hidden input exists");
		test.assertFieldCSS("div#titlePlaceholder input.recipe-title", "", "Recipe title input empty");
		this.click("div#titlePlaceholder p.content");
		test.assertExists("textarea.recipe-title", "Recipe title textarea exists after click");

		test.assertExists("div#notesPlaceholder p.content", "Recipe comment exists");
		test.assertExists("div#notesPlaceholder input.recipe-comments", "Recipe comment hidden input exists");
		test.assertFieldCSS("div#notesPlaceholder input.recipe-comments", "", "Recipe comment input empty");
		this.click("div#notesPlaceholder p.content");
		test.assertExists("textarea.recipe-comments", "Recipe comment textarea exists after click");

		test.assertExists("div#authorPlaceholder p.content", "Recipe author exists");
		test.assertExists("div#authorPlaceholder input.author[type=hidden]", "Recipe author hidden input exists");
		test.assertFieldCSS("div#authorPlaceholder input.author[type=hidden]", "", "Recipe author input empty");
		this.click("div#authorPlaceholder p.content");
		test.assertExists("input.author[type=text]", "Recipe author input exists after click");
		
		test.assertExists("div#yieldPlaceholder p.content", "Recipe yield exists");
		test.assertExists("div#yieldPlaceholder input.servings[type=hidden]", "Recipe yield hidden input exists");
		test.assertFieldCSS("div#yieldPlaceholder input.servings[type=hidden]", "", "Recipe yield input empty");
		this.click("div#yieldPlaceholder p.content");
		test.assertExists("input.servings[type=text]", "Recipe yield input exists after click");

		test.assertExists("div#prepPlaceholder p.content", "Recipe prep exists");
		test.assertExists("div#prepPlaceholder input.preptime[type=hidden]", "Recipe prep hidden input exists");
		test.assertFieldCSS("div#prepPlaceholder input.preptime[type=hidden]", "", "Recipe prep input empty");
		this.click("div#prepPlaceholder p.content");
		test.assertExists("input.preptime[type=text]", "Recipe prep input exists after click");
		
		test.assertExists("div#totalPlaceholder p.content", "Recipe total exists");
		test.assertExists("div#totalPlaceholder input.total[type=hidden]", "Recipe total hidden input exists");
		test.assertFieldCSS("div#totalPlaceholder input.total[type=hidden]", "", "Recipe total input empty");
		this.click("div#totalPlaceholder p.content");
		test.assertExists("input.total[type=text]", "Recipe total input exists after click");

		test.assertExists("input#inputRating", "Recipe rating input exists");
		test.assertExists("button#dropdownRating", "Recipe rating dropdown exists");
		test.assertFieldCSS("input#inputRating", "", "Recipe rating input empty");

		test.assertExists("input#inputCuisine", "Recipe cuisine input exists");
		test.assertExists("button#dropdownCuisine", "Recipe cuisine dropdown exists");
		test.assertFieldCSS("input#inputCuisine", "", "Recipe cuisine input empty");

		test.assertExists("input#inputRecipeType", "Recipe type input exists");
		test.assertExists("button#dropdownTypes", "Recipe type dropdown exists");
		test.assertFieldCSS("input#inputRecipeType", "", "Recipe type input empty");
		
		test.assertExists("button.btn-addingredient", "Add Ingredient Button exists");
		test.assertDoesntExist("button.btn-importingredients", "Import ingredient button doesn't exist");
		test.assertEval(function() {
            return __utils__.findAll("div.ingredient-collection div.ingredient-row").length === 0;
        }, "No ingredients by default");

		test.assertExists("button.btn-adddirection", "Add Direction Button exists");
		test.assertDoesntExist("button.btn-importdirections", "Import Direction button doesn't exist");
		test.assertEval(function() {
            return __utils__.findAll("div.direction-collection div.direction-row").length === 0;
        }, "No directions by default");
    });
    
    // Fill in details; check for expected behavior in certain fields.
    casper.then(function() {
	   this.echo("Fill in form & check for expected form behavior: ");
	   
	   this.fillSelectors("form#editRecipeForm", {
		   "input.recipe-title": "Casper.js Test Recipe",
		   "input.recipe-comments": "Some comments for this recipe.",
		   "input.author": "Phantom.js",
		   "input.servings": "Five Large Servings",
		   "input.preptime": "25 minutes 4 seconds",
		   "input.total": "45 minutes"
	   }, false); 

	   this.click("button#dropdownRating");
	   this.click("div.recipe-rating li.rating-item a.star-1");
	   test.assertFieldCSS("input#inputRating", "1", "Recipe rating set to 1");

	   this.click("button#dropdownCuisine");
	   this.click("div.recipe-cuisine li.cuisine-item a.Spanish");
	   test.assertFieldCSS("input#inputCuisine", "Spanish", "Recipe cuisine set to Spanish");
	   this.click("button#dropdownCuisine");
	   this.click("div.recipe-cuisine li.cuisine-item a.empty");
	   test.assertFieldCSS("input#inputCuisine", "", "Recipe cuisine set to blank");
	   test.assertSelectorHasText("div.recipe-cuisine span.button-text", "Cuisine", "Recipe cuisine button text is default");
	   this.click("button#dropdownCuisine");
	   this.click("div.recipe-cuisine li.cuisine-item a.Indian");
	   test.assertFieldCSS("input#inputCuisine", "Indian", "Recipe cuisine set to Indian");

	   this.click("button#dropdownTypes");
	   this.click("div.recipe-types li.type-item a.Salad");
	   test.assertFieldCSS("input#inputRecipeType", "Salad", "Recipe type set to Salad");
	   this.click("button#dropdownTypes");
	   this.click("div.recipe-types li.type-item a.empty");
	   test.assertFieldCSS("input#inputRecipeType", "", "Recipe type set to blank");
	   test.assertSelectorHasText("div.recipe-types span.button-text", "Recipe Type", "Recipe type button text is default");
	   this.click("button#dropdownTypes");
	   this.click("div.recipe-types li.type-item a.Snack");
	   test.assertFieldCSS("input#inputRecipeType", "Snack", "Recipe type set to Snack");
	   
	   this.click("button.btn-addingredient");
	   test.assertExists("div.ingredient-collection input.raw", "Add ingredient button adds raw input field");
	   this.sendKeys("div.ingredient-collection input.raw", "2/3 cup water", {keepFocus: true});
	   this.sendKeys("div.ingredient-collection input.raw", casper.page.event.key.Enter, {keepFocus:true});

	   this.waitForSelector("div.ingredient-collection input.quantity");
    });
    
    casper.then(function() {
	   test.assertExists("div.ingredient-collection input.raw", "Enter key adds raw input field");
	   test.assertExists("div.ingredient-collection input.quantity", "First ingredient parsed");
	   test.assertFieldCSS("div.ingredient-collection input.quantity", "2/3", "Quantity parsed");
	   test.assertFieldCSS("div.ingredient-collection input.unit", "cup", "Unit parsed");
	   test.assertFieldCSS("div.ingredient-collection input.ingredient-name", "water", "Ingredient name parsed");
	   test.assertFieldCSS("div.ingredient-collection input.prep", "", "Prep was parsed");
	   this.click("div.ingredient-collection button#ingredientSettings");
	   this.click("div.ingredient-collection a.trash");
	   this.sendKeys("div.ingredient-collection input.raw", "2 carrots, diced", {keepFocus: true});
	   this.sendKeys("div.ingredient-collection input.raw", casper.page.event.key.Tab, {keepFocus:true});

	   this.waitForSelector("div.ingredient-collection input.quantity");
    });

    casper.then(function() {
	   test.assertDoesntExist("div.ingredient-collection input.raw", "Tab key doesn't add raw input field");
	   test.assertExists("div.ingredient-collection input.quantity", "Second ingredient parsed");
	   test.assertFieldCSS("div.ingredient-collection input.quantity", "2", "Quantity parsed");
	   test.assertFieldCSS("div.ingredient-collection input.unit", "", "Unit parsed");
	   test.assertFieldCSS("div.ingredient-collection input.ingredient-name", "carrots", "Ingredient name parsed");
	   test.assertFieldCSS("div.ingredient-collection input.prep", "diced", "Prep was parsed");
	   
	   for (var i = 0; i < 10; i++) {
		   this.click("button.btn-addingredient");
	   }
	});
	
	casper.then(function() {
		this.click("button.btn-adddirection");
		test.assertExists("div.direction-collection textarea.recipe-direction", "Add direction button adds direction field");
		this.sendKeys("div.direction-collection textarea.recipe-direction", "This is a new direction for this recipe. It is the first and last.");
		this.click("button#editRecipeSubmit");
		this.waitForUrl(/\/recipe\/[0-9]+/);
	});
	
	casper.then(function() {
		this.waitWhileSelector("div.wait-spinner");
	});
	
	// Check that recipe saved correctly by analyzing the result page
	casper.then(function() {
		this.echo("Check that recipe saved correctly: ");
		
		test.assertTitle("Casper.js Test Recipe :: ProjectNom");
		
		test.assertSelectorHasText("h1.brand-heading", "Casper.js Test Recipe", "Recipe Title Saved");
		test.assertSelectorHasText("div.recipe-comment", "Some comments for this recipe.", "Recipe Comment Saved");
		test.assertSelectorHasText("p.recipe-author", "Phantom.js", "Recipe Author saved");
		test.assertSelectorHasText("div.servings-metadata span.servings", "Five Large Servings", "Recipe Servings saved");
		test.assertSelectorHasText("div.time-metadata", "Total: 45 minutes Prep: 25 minutes 4 seconds", "Recipe timings saved");
		test.assertEvalEquals(function() {
			return $("button#dropdownRating span.button-text").html();
		}, "<i class=\"fa fa-star\"></i>&nbsp;", "Recipe rating saved");
		test.assertSelectorHasText("button#dropdownCuisine span.button-text", "Indian", "Recipe cuisine saved");
		test.assertSelectorHasText("button#dropdownTypes span.button-text", "Snack", "Recipe type saved");
		
		test.assertSelectorHasText("div.ingredient-row:not(.hidden)", "2 carrots, diced", "Recipe ingredient saved");
		test.assertElementCount("div.ingredient-row:not(.hidden)", 1, "Only one ingredient saved");
		test.assertSelectorHasText("div.direction-row:not(.hidden)", "This is a new direction for this recipe. It is the first and last.", "Recipe direction saved");
		test.assertElementCount("div.direction-row:not(.hidden)", 1, "Only one direction saved");
		
		test.assertExists("li.original-link.hidden", "View Original Recipe link not visible");
		this.click("div#recipe-settings button.dropdown-toggle");
		this.click("div.panel ul.dropdown-menu a.edit");
		this.waitForUrl(/\/recipe\/edit\/[0-9]+/);
	});

	casper.then(function() {
		this.click("button#editRecipeDelete");
		this.click("div.bootbox button.btn-danger");
		this.waitForUrl(getServerName() + "home");
	});

    casper.run(function() {
        test.done();
    });
});                                                                              