<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class WebAppControllerBase extends CI_Controller 
{
   public function __construct($autoCloseSession = TRUE)
   {
        parent::__construct();
        
        if ($this->getProtocol() == "https")
        {
	        $this->output->set_header("Strict-Transport-Security: max-age=31536000; includeSubDomains; preload");
	    }
        
		$this->load->library('session');
		
		if ($this->session->userdata('username') === NULL)
		{
			$access_key = $this->input->cookie('remember_accesskey');

			if ($access_key !== NULL)
			{
				$this->load->model('User_model', 'userService', TRUE);
				$key_components = explode("&", $access_key);
				$username = $key_components[0];

				$user_match = $this->userService->getAccountDetails($username, $key_components[1]);

				if (isset($user_match) && !empty($user_match['username']) && $user_match['username'] == $username)
				{
					$this->session->set_userdata(array('username' => $username));
					$this->session->set_userdata(array('readonly' => TRUE));
					$this->session->set_userdata(array('twitter' => $user_match['twitter']));

					$new_access_key = $this->userService->setAccessKey($username, $key_components[1]);

					if ($new_access_key !== FALSE)
					{
						$this->input->set_cookie(
							array(
							    'name'   => 'remember_accesskey',
							    'value'  => $new_access_key,
							    'expire' => 60*60*24*365*10,
							    'domain' => '.' . $_SERVER['SERVER_NAME']
							)
						);
					}
					
					if ($autoCloseSession) { session_write_close(); }
				}
			}
		}
	}

	protected function failureRedirect($showLogin = true)
	{
		if ($showLogin)
		{
			$this->session->set_flashdata('returnUrl', $_SERVER['REQUEST_URI']);
			session_write_close();
		}
		header("Location: " . $this->getProtocol() . "://" . $_SERVER['SERVER_NAME'] . "/");
		die();		
	}
	
	protected function getProtocol()
	{
		return (!empty($_SERVER['HTTPS']) ? "https" : "http");
	}
}