<div class="row message-list-item">
	<div class="col-xs-12 col-md-offset-1 col-md-10 col-lg-offset-2 col-lg-8">
		<div class="thumbnail">
			<div class="row header">
				<div class="col-sm-10 col-xs-9 message-type">
					<p><i class='fa fa-comment'></i></p>
					<p class="username"><?php if (!empty($message["author"])) { print $message["author"]; } ?></p>
	    		</div>
	    		<div class="col-sm-2 col-xs-3 text-right message-delete">
		    		<button class="btn btn-danger remove-message hidden" data-messageid=""><i class="fa fa-trash-o"></i></button>
	    		</div>
			</div>
			<div class="row body">
				<div class="col-xs-12 message-section">
					<p class="message"><?php if (!empty($message["message"])) { print $message["message"]; } ?></p>
				</div>
			</div>
			<div class="row attachment <?php if (empty($message["recipe_id"])) {?>hidden<?php } ?>" id="recipe">
				<div class="col-xs-10 col-xs-offset-1 attachment-section">
					<div class="alert alert-info">
						<div class="row">
							<div class="col-xs-8 col-sm-7 attachment-content-section">
								<div class="col-xs-3 col-sm-2">
									<i class="fa fa-cutlery"></i>
								</div>
								<div class="col-xs-9 col-sm-10">
									<span class="attachment-content"><?php if (!empty($message["recipe_title"])) { print $message["recipe_title"]; }?></span>
								</div>
							</div>
							<div class="col-xs-4 col-sm-5">
								<div class="col-xs-12 col-sm-6">
									<button class="btn btn-default btn-block preview" data-toggle="modal" data-target="div.bs-recipe-preview-modal" title="Preview"><i class="fa fa-search-plus"></i><span class="hidden-xs">&nbsp;View</span></button>
								</div>
								<div class="col-xs-12 col-sm-6">
									<button class="btn btn-default btn-block import" data-toggle="modal" data-target="div.bs-recipeimport-modal" title="Import" data-id="<?php if (!empty($message["recipe_id"])) { print $message["recipe_id"]; } ?>" <?php if (!empty($username) && $username == $message["author"]) { ?>disabled='disabled' <?php } ?>>
										<i class="fa fa-plus"></i><span class="hidden-xs">&nbsp;Save</span>
									</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row attachment <?php if (empty($message["image_url"])) {?>hidden<?php } ?>" id="image">
				<div class="col-xs-10 col-xs-offset-1 attachment-section">
					<div class="alert alert-info">
						<div class="row">
							<div class="col-xs-1">
								<i class="fa fa-paperclip"></i>
							</div>
							<div class="col-xs-10 attachment-content-section">
								<span class="attachment-content">
									<img src="<?php if (!empty($message["image_url"])) { print "/" . $message["image_url"]; }?>" class="img-thumbnail">
								</span>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row footer">
				<div class="col-xs-12">
					<p class="timestamp text-muted"><?php if (!empty($message["timestamp"])) { print $message["timestamp"]; } ?></p>
				</div>
			</div>
		</div>
    </div>
</div>