<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
require_once(APPPATH . "/models/recipe_class.php");
require_once(APPPATH . "/models/ingredient_class.php");

class AutoImport 
{
	private $CI;
	private $url;
	private $fileNameHash;
	private $pageTitle;

	public function __construct($params)
    {
    	$this->CI =& get_instance();
		$this->CI->load->model('Recipe_model', 'recipeService', TRUE);
		$this->CI->load->helper("remoteretrieve");
    	$this->pageTitle = "";

    	if (!empty($params['url']))
    	{
    		$this->url = $params['url'];
    		$this->getImporter();
    	}

    	if (!empty($params['filenamehash']))
    	{
    		$this->fileNameHash = $params['filenamehash'];
    	}
    	else
    	{
    		$this->fileNameHash = md5("unknown");
    	}
    }

	public function getPageUrl()
	{
		return $this->url;
	}

	public function getPageTitle()
	{
		return $this->pageTitle;
	}

    public function canImport()
    {
    	return !empty($this->CI->importer);
    }

    public function doImport()
    {
    	$recipe = new RecipeDTO();

    	if ($this->canImport())
		{
			$units = $this->CI->recipeService->getUnits();
			$this->CI->load->library('RecipeImport', array('units' => $units));

			if ($this->url == null)
			{
				$recipe->origin_url = $this->CI->importer->getOrigin();
				$recipe->cache_url = $this->CI->importer->getCache();
			}
			else
			{
				$recipe->origin_url = $this->url;
			}
			$recipe->title = $this->CI->importer->getTitle();
			$recipe->author = $this->CI->importer->getAuthor();
			$recipe->total_time = $this->CI->importer->getTotalTime();
			$recipe->prep_time = $this->CI->importer->getPrepTime();
			$recipe->servings = $this->CI->importer->getServings();

			$recipe->image_url = 
				$this->CI->recipeimport->parseImage($this->CI->importer->getImage(), $this->fileNameHash);

			$rawIngredients = $this->CI->importer->getIngredients();
			//$recipe->comment = $rawIngredients;

			if (is_array($rawIngredients))
			{
				foreach ($rawIngredients as $rawIngredient)
				{
					$ingredientData = $this->CI->recipeimport->parseIngredient($rawIngredient);
	
					if (is_array($ingredientData))
					{
						$ingredient = new Ingredient();
						$ingredient->name = $ingredientData['name'];
						$ingredient->preparation = $ingredientData['preparation'];
						$ingredient->unit = $ingredientData['unit'];
						$ingredient->quantity = $ingredientData['quantity'];
	
						if (rtrim(strrchr($ingredient->name, ":")) === ":")
						{
							$ingredient->id = 0;
						}
	
						$recipe->ingredients[] = $ingredient;
					}
				}
			}
			
			$rawDirections = $this->CI->importer->getDirections();
			//$recipe->comment = $rawDirections;
			$recipe->directions = $rawDirections;
		}

		return $recipe;
    }

    private function getImporter()
    {
    	$parsed_url = parse_url($this->url);

    	if ($parsed_url === FALSE || empty($parsed_url['host'])) return;
    	
		if (stripos($parsed_url['host'], "projectnom") !== FALSE || stripos($parsed_url['host'], "localhost") !== FALSE)
		{
			$recipe = $this->getProjectNomRecipe($parsed_url['path']);
			
			if (!empty($recipe))
			{
				$this->url = null;
				$this->CI->load->library('importers/projectnom', array('content' => $recipe, 'filenamehash' => $this->fileNameHash), 'importer');
			}
			
			return;
		}

    	$this->url = explode("?", $this->url)[0];
    	$content = $this->stripComments(file_get_contents_curl($this->url));
    	
    	if ($content === FALSE) return;

    	if (stripos($parsed_url['host'], "foodnetwork.com") !== FALSE)
    	{
        	$this->CI->load->library('importers/foodnetwork', array('content' => $content, 'filenamehash' => $this->fileNameHash), 'importer');
    	}
    	else if (stripos($parsed_url['host'], "seriouseats.com") !== FALSE)
    	{
        	$this->CI->load->library('importers/seriouseats', array('content' => $content, 'filenamehash' => $this->fileNameHash), 'importer');
    	}
    	else if (stripos($parsed_url['host'], "epicurious.com") !== FALSE)
    	{
        	$this->CI->load->library('importers/epicurious', array('content' => $content, 'filenamehash' => $this->fileNameHash), 'importer');
    	}
    	else if (stripos($parsed_url['host'], "allrecipes.com") !== FALSE)
    	{
        	$this->CI->load->library('importers/allrecipes', array('content' => $content, 'filenamehash' => $this->fileNameHash), 'importer');
    	}
    	else if (stripos($parsed_url['host'], "bonappetit.com") !== FALSE)
    	{
        	$this->CI->load->library('importers/bonappetit', array('content' => $content, 'filenamehash' => $this->fileNameHash), 'importer');
    	}
    	else if (stripos($parsed_url['host'], "blueapron.com") !== FALSE)
    	{
        	$this->CI->load->library('importers/blueapron', array('content' => $content, 'filenamehash' => $this->fileNameHash), 'importer');
    	}
    	else if (stripos($parsed_url['host'], "hellofresh.com") !== FALSE)
    	{
        	$this->CI->load->library('importers/hellofresh', array('content' => $content, 'filenamehash' => $this->fileNameHash), 'importer');
    	}
    	else
    	{
	    	if (!empty($content))
	    	{
		    	preg_match("/<title>(.*)<\/title>/i", $content, $matches);

				if (!empty($matches[1]))
				{
					$this->pageTitle = $matches[1];
		    	}
		    }
    	}
    }
    
    private function getProjectNomRecipe($urlPath)
    {
	    $path_components = explode("/", $urlPath);
	    
	    if (is_array($path_components) && $path_components[1] == "recipe")
	    {
		    $recipe = null;
		    $recipe_id = $path_components[2];
		    try
		    {
			    $recipe = $this->CI->recipeService->getRecipe(array("recipeid" => $recipe_id, "username" => ""));
			}
			catch (RecipeException $re)
			{
				$recipe = $re->baseRecipe;
			}

			if (!empty($recipe))
			{
				return $recipe;
			}
	    }
	    
	    return FALSE;
    }
    
    private function stripComments($html)
    {
		return preg_replace("/<!--.+-->/imsU", "",	$html);
    }
}

?>