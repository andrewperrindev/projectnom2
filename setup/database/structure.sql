-- Generated using
-- MySQL Server version: 5.6.25
-- PHP Version: 5.5.26

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

CREATE TABLE IF NOT EXISTS `ci3_sessions` (
        `id` varchar(128) NOT NULL,
        `ip_address` varchar(45) NOT NULL,
        `timestamp` int(10) unsigned DEFAULT 0 NOT NULL,
        `data` blob NOT NULL,
        KEY `ci_sessions_timestamp` (`timestamp`)
);

-- --------------------------------------------------------

--
-- Table structure for table `ingredients`
--

CREATE TABLE IF NOT EXISTS `ingredients` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `name_plural` varchar(255) DEFAULT NULL,
  `category` varchar(255) DEFAULT NULL,
  `primary_ind` tinyint(4) NOT NULL DEFAULT '0',
  `variation_of` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `recipes`
--

CREATE TABLE IF NOT EXISTS `recipes` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `author` varchar(155) DEFAULT NULL,
  `origin_url` varchar(500) DEFAULT NULL,
  `cache_url` varchar(500) DEFAULT NULL,
  `image_url` varchar(255) DEFAULT NULL,
  `prep_time` varchar(25) DEFAULT NULL,
  `total_time` varchar(25) DEFAULT NULL,
  `servings` varchar(50) DEFAULT NULL,
  `cookbook_title` varchar(100) DEFAULT NULL,
  `cookbook_page` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `originurl_idx` (`origin_url`(333))
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `recipe_bookmarks`
--

CREATE TABLE IF NOT EXISTS `recipe_bookmarks` (
  `userid` int(11) NOT NULL,
  `recipe_url` varchar(300) NOT NULL,
  `recipe_title` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`userid`,`recipe_url`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `recipe_directions`
--

CREATE TABLE IF NOT EXISTS `recipe_directions` (
  `recipe_id` int(11) NOT NULL,
  `seq` int(11) NOT NULL,
  `instructions` mediumtext NOT NULL,
  `section` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`recipe_id`,`seq`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `recipe_ingredients`
--

CREATE TABLE IF NOT EXISTS `recipe_ingredients` (
  `recipe_id` int(11) NOT NULL,
  `ingredient_id` int(11) NOT NULL,
  `seq` int(11) NOT NULL,
  `quantity` varchar(10) DEFAULT NULL,
  `unit` varchar(50) DEFAULT NULL,
  `preparation` varchar(255) DEFAULT NULL,
  `descr_text` varchar(255) DEFAULT NULL,
  KEY `recipe_ingredient_idx` (`recipe_id`,`ingredient_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `shoppinglist_ingredients`
--

CREATE TABLE IF NOT EXISTS `shoppinglist_ingredients` (
  `list_id` int(11) NOT NULL,
  `item` varchar(150) NOT NULL,
  `complete_ind` tinyint(4) NOT NULL DEFAULT '0',
  `seq` int(11) NOT NULL,
  PRIMARY KEY (`list_id`,`seq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `shoppinglist_recipes`
--

CREATE TABLE IF NOT EXISTS `shoppinglist_recipes` (
  `list_id` int(11) NOT NULL,
  `recipe_id` int(11) NOT NULL,
  PRIMARY KEY (`list_id`,`recipe_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) NOT NULL,
  `email` varchar(150) NOT NULL,
  `password` varchar(150) NOT NULL,
  `profile` varchar(500) DEFAULT NULL,
  `is_locked` tinyint(4) NOT NULL DEFAULT '1',
  `is_admin` tinyint(4) NOT NULL DEFAULT '0',
  `twitter_id` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ;

-- --------------------------------------------------------

--
-- Table structure for table `users_accesskeys`
--

CREATE TABLE IF NOT EXISTS `users_accesskeys` (
  `user_id` int(11) NOT NULL,
  `key` varchar(100) NOT NULL,
  `task` varchar(255) NOT NULL,
  PRIMARY KEY (`user_id`,`key`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `users_friends`
--

CREATE TABLE IF NOT EXISTS `users_friends` (
  `user_id` int(11) NOT NULL,
  `friend_user_id` int(11) NOT NULL,
  PRIMARY KEY (`user_id`,`friend_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users_messages`
--

CREATE TABLE IF NOT EXISTS `users_messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `author_userid` int(11) NOT NULL,
  `to_userid` int(11) DEFAULT NULL,
  `message_type` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `message` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `recipeid` int(11) DEFAULT NULL,
  `timestamp` datetime NOT NULL,
  `image_url` varchar(155) COLLATE utf8mb4_bin DEFAULT NULL,
  `tweet_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

ALTER TABLE `users_messages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_author` (`author_userid`);

-- --------------------------------------------------------

--
-- Table structure for table `users_twitter`
--

CREATE TABLE IF NOT EXISTS `users_twitter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `oauth_token` varchar(500) NOT NULL,
  `oauth_token_secret` varchar(500) NOT NULL,
  `username` varchar(15) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_token` (`oauth_token`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ;

-- --------------------------------------------------------

--
-- Table structure for table `user_recipebox`
--

CREATE TABLE IF NOT EXISTS `user_recipebox` (
  `userid` int(11) NOT NULL,
  `recipeid` int(11) NOT NULL,
  `comment` text,
  `type` varchar(255) DEFAULT NULL,
  `cuisine` varchar(255) DEFAULT NULL,
  `rating` tinyint(4) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL DEFAULT '1900-01-01 00:00:00',
  `date_lastviewed` datetime DEFAULT NULL,
  PRIMARY KEY (`userid`,`recipeid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `user_shoppinglists`
--

CREATE TABLE IF NOT EXISTS `user_shoppinglists` (
  `list_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`list_id`),
  UNIQUE KEY `list_id` (`list_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ;
