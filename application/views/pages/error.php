    <!-- Intro Header -->
    <header class="intro">
        <div class="intro-body">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2 greeting">
	                    <h1 class='brand-heading'>Problem</h1>
	                </div>
                </div>
            </div>
        </div>
    </header>

    <section id="result" class="container content-section text-center text-dark">
		<div class="row">
			<div id="error">
				<script>
					var pnError = pnError || {};
					pnError.renderError = function renderError() {
						this.ReactDOM.render(
							this.React.createElement(this.BootstrapError, 
								{
									visible: true, 
									className: "col-sm-10 col-sm-offset-1",
									title: <?php print $alert_title; ?>,
									detail: <?php print $detail; ?>
								}),
							document.getElementById('error')
						);
					}
				</script>
			</div>
		</div>
    </section>