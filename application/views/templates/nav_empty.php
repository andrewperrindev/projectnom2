    <!-- Navigation -->
    <nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-main-collapse">
                    <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand page-scroll" href="/">
                    <i class="fa fa-cutlery"></i>  <span class="pn-gray">PROJECT</span><span class="light">NOM</span>
                </a>
            </div>
        </div>
        <!-- /.container -->
    </nav>