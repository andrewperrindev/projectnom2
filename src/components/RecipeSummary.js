var React = require('react');

module.exports = React.createClass({
	getInitialState: function() {
		return {
			isTouch: ('ontouchstart' in window)
		};
	},
	
	componentDidMount: function() {
		if (!this.state.isTouch) {
			$('[data-toggle="tooltip"]', this.refs.recipe).tooltip(
				{
					html: true, 
					placement: 'bottom', 
					title: $("div.caption-detail", this.refs.recipe).html()
				}
			);
		}
	},
	
	render: function() {
		var r = this.props.recipe;
		var recipeUrl = "recipe/" + r.id;
		var containerProps = {className: "thumbnail no-border", ref: "recipe"},
			captionProps = {className: "caption-detail"};

		var image = React.DOM.a(
			{href: recipeUrl},
			React.DOM.img({src: "/" + this.getImage(r), alt: r.title})
		);
		
		var captionLink = React.DOM.a(
			{href: recipeUrl, "data-toggle": "tooltip"},
			React.DOM.h4(null, r.title)
		);
		var caption = React.DOM.div({className: "caption"}, captionLink);

		if (this.state.isTouch) {
			containerProps.className += " thumbnail-touch";
		}
		else {
			captionProps.className += " hidden";
		}

		var captionDetail = React.DOM.div(captionProps, this.getDetails(r));

		var container = React.DOM.div(containerProps, image, caption, captionDetail);
		return container;
	},
	
	getImage: function(r) {
		if (r.image_url)
		{
			return  r.has_thumb == 1 ? r.image_url + ".thumb" : r.image_url;
		}
		else
		{
			return "img/place-setting.png";
		}		
	},
	
	getDetails: function(r) {
		var details = [];
		
		if (r.servings) {
			details.push(
				React.DOM.span(
					{className: "recipe-detail", key: "servings"},
					React.DOM.i({className: "fa fa-users", style: {paddingRight: "5px"}}),
					r.servings
				)
			);
		}
		if (r.total_time || r.prep_time)
		{
			var time = r.total_time ? r.total_time + " total" : r.prep_time + " prep";
			details.push(
				React.DOM.span(
					{className: "recipe-detail", key: "time"},
					React.DOM.i({className: "fa fa-clock-o", style: {paddingRight: "5px"}}),
					time
				)
			);
		}
		if (r.rating > 0)
		{
			var stars = [];
			for (var i = 0; i < r.rating; i++) {
				stars.push(React.DOM.i({className: "fa fa-star", style: {paddingRight: "5px"}, key: i}));
			}
			details.push(
				React.DOM.span({className: "recipe-detail", key: "rating"}, stars)
			);
		}
		if (r.ingredients.length > 0)
		{
			details.push(
				React.DOM.span(
					{className: "recipe-detail", key: "ingredients"},
					React.DOM.span({className: "glyphicon glyphicon-apple", "aria-hidden": "true", style: {paddingRight: "5px"}}),
					r.ingredients.length + " ingredients"
				)
			);
		}

		return details;
	}
});