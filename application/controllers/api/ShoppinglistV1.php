<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once(APPPATH . '/controllers/api/ApiControllerBase.php');

class ShoppinglistV1 extends ApiControllerBase 
{
   public function __construct()
   {
        parent::__construct();
   }

	public function index()
	{
		$this->load->model('Shoppinglist_model', 'shoppingListService', TRUE);

		$data['username'] = $this->session->userdata('username');

		if ($this->input->server('REQUEST_METHOD') == "GET")
		{
			$data['result'] = $this->shoppingListService->getList($data['username']);
		}
		else if ($this->input->server('REQUEST_METHOD') == "DELETE")
		{
			$success = $this->shoppingListService->clearList($data['username']);

			if (!$success)
			{
				$data['error'] = "There was a problem clearing your shopping list.";
			}
		}
		else
		{
			$shoppingList = json_decode($this->input->post("shoppinglist"));

			if ($shoppingList != null)
			{
				foreach ($shoppingList as $item)
				{
					$this->shoppingListService->saveItem($item, $data['username']);
					$data["result"][] = $item;
				}
			}
			/*
			for ($i = 0; $i < count($this->input->post("add-ingredient")); $i++)
			{
				$id = $this->input->post("add-ingredient")[$i];

				if (!empty($id))
				{
					//$data["result"] = print_r($_POST, true);
					$this->shoppingListService->saveItem($this->input->post("ingredient-name$id"), $data['username']);
					$data["result"][] = $this->input->post("ingredient-name$id");
				}
			}
			*/
		}

		$this->load->view('api/simple', $data);
	}
}
?>