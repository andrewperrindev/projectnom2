<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once(APPPATH . '/controllers/WebAppControllerBase.php');

class Shoppinglist extends WebAppControllerBase 
{
   public function __construct()
   {
        parent::__construct();
        /*
		$this->load->library('session');
		*/
		if ($this->session->userdata('username') === NULL)
		{
			$this->failureRedirect();
		}
   }

	public function index()
	{
		$data['username'] = $this->session->userdata('username');
		$data['title'] = "Shopping List";

		add_css(array('shoppinglist/shoppinglist.css','recipe/jquery-ui.min.css','recipe/jquery-ui.structure.min.css'));
		add_js(array('shoppinglist/shoppinglist.js', 'recipe/jquery-ui.js'));

		$this->load->view('templates/header', $data);
		$this->load->view('templates/nav_standard', $data);
		$this->load->view('pages/shoppinglist', $data);
		$this->load->view('templates/footer', $data);
	}
}
?>