<!-- Shopping List Modal -->
<div class="modal fade bs-shopping-modal">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header text-dark">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Add to Shopping List</h4>
			</div>
	    	<div class="modal-body text-dark">
				<div class="row">
					<div class="col-sm-12">
						<div class="alert alert-info" role="alert"><strong>Select the ingredients you would like to add to your shopping list.</strong> You will be able to edit the details later.</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-10 col-sm-offset-1">
						<form method='post' id='shopping-list' action='/api/1/shoppinglist'>
						</form>
					</div>
				</div>
	    	</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default btn-cancel" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-primary btn-confirm" data-label='View My Shopping List'>Add to Shopping List</button>
	    	</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="row ingredient-list-item hidden">
	<div class="col-sm-2">
		<input type="checkbox" class="checkbox add-ingredient pull-right" name="add-ingredient[]">
	</div>
	<div class="col-sm-10">
		<input type="hidden" class="ingredient-name" name="ingredient-name">
		<span class="ingredient-name-label"></span>
	</div>
</div>