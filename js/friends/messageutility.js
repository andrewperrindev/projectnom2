function addMessageBase(messageData)
{
	var newMessage =
	{
		message: messageData.message,
		type: messageData.type,
		tweet: messageData.tweet ? true : false
	};
	
	if (messageData.recipeid > 0)
	{
		newMessage.recipeid = messageData.recipeid;
	}
	if (messageData.to_user !== "")
	{
		newMessage.to_user = messageData.to_user;
	}
	if (messageData.attached_img)
	{
		newMessage.image = messageData.attached_img;
	}

	var defer = $.Deferred();

	ajaxPost("/api/1/messages", newMessage)
		.done(function(data)
		{
			if (data.success === 0)
			{
				var errorText = "<strong>Something went wrong.</strong> Please try again. If you keep seeing this, let us know.";
				
				if (data.error == "Message too long.")
				{
					errorText = "<strong>Your message cannot exceed 255 characters.</strong>";
				}
				else if (data.error == "Required information missing.")
				{
					errorText = "<strong>Your message cannot be blank.</strong>";
				}
				
				defer.reject(errorText);
			}
			else
			{
				defer.resolve(data);
			}
		});
		
	return defer.promise();
}

function charCount(event)
{
	var $label = $("span.label.char-count");
	var remaining = (255 - $(this).val().length);
	
	if (remaining <= 0)
	{
		$(".btn.add-message").prop("disabled", true);
	}
	else
	{
		$(".btn.add-message").prop("disabled", false);
	}
		
	if (remaining < 25)
	{
		$label.removeClass("label-default label-warning").addClass("label-danger");
	}
	else if (remaining < 155)
	{
		$label.removeClass("label-default label-danger").addClass("label-warning");
	}
	else if (remaining >= 155)
	{
		$label.removeClass("label-warning label-danger").addClass("label-default");
	}
	
	$label.text(remaining);
}