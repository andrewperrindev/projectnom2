function countFailures() {
	return $("div.err").length;
}

function getFailedTestNames() {
	return $("div.err span.test-name")
		.map(function() {
			return $(this).text();
		})
		.get()
		.join("\n")
	;
}