<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

require 'twitteroauth/autoload.php';
use Abraham\TwitterOAuth\TwitterOAuth;

class PNTwitter 
{	
	const CONSUMER_KEY = "NtGZ8cg4Xiqe3HBitI6t22HSn";
	const CONSUMER_SECRET = "bPW6VnXeKMN1yfEbtYLHUp7aQw7Ax3nQEryxUXoyqDtopRMAKp";
	const OAUTH_CALLBACK = "https://projectnom.com/twitter/callback";

	private $CI;
	private $connection;
	private $twitterConfig;

	public function __construct($params)
	{
		$this->CI =& get_instance();
		
		if (isset($params) && !empty($params['oauth_token']) && !empty($params['oauth_token_secret']))
		{
			$this->connection = new TwitterOAuth(self::CONSUMER_KEY, self::CONSUMER_SECRET, $params['oauth_token'], $params['oauth_token_secret']);
			
			$this->CI->load->driver('cache');
			if ( !$this->twitterConfig = $this->CI->cache->file->get('twitterConfig'))
			{
				log_message('error', "Cache miss!");
				$configResult = $this->getConfiguration();
				
				if (!empty($configResult) && !empty($configResult->short_url_length))
				{
					$this->twitterConfig = $configResult;
					
					// Save into the cache for 24 hours
					$this->CI->cache->file->save('twitterConfig', $this->twitterConfig, (60*60)*24);
				}
			}

		}
		else
		{
			$this->connection = new TwitterOAuth(self::CONSUMER_KEY, self::CONSUMER_SECRET);
		}
	}
	
	public function getRequestToken($callback = self::OAUTH_CALLBACK)
	{
		return $this->connection->oauth('oauth/request_token', array('oauth_callback' => $callback));
	}
	
	public function getAuthorizeUrl($oauth_token)
	{
		return $this->connection->url('oauth/authorize', array('oauth_token' => $oauth_token));
	}
	
	public function getAccessToken($oauth_verifier)
	{
		return $this->connection->oauth("oauth/access_token", array("oauth_verifier" => $oauth_verifier));
	}
	
	public function postStatus($status, $media_path = "")
	{
		$parameters = [
		    'status' => $status
		];
		
		if (!empty($media_path))
		{
			$media = $this->connection->upload('media/upload', ['media' => $media_path]);
			$parameters["media_ids"] = $media->media_id_string;
		}
		
		$postResult = $this->connection->post('statuses/update', $parameters);
		if ($this->connection->getLastHttpCode() != 200) {
			log_message("error", "ERROR: Could not post Twitter status update: " . json_encode($postResult));
		}
		
		return $postResult;
	}
	
	public function getMediaCharLength()
	{
		if (!empty($this->twitterConfig) && !empty($this->twitterConfig->characters_reserved_per_media))
		{
			return $this->twitterConfig->characters_reserved_per_media;
		}
		
		return 0;
	}
	
	public function getShortUrlLength()
	{
		if (!empty($this->twitterConfig) && !empty($this->twitterConfig->short_url_length))
		{
			return $this->twitterConfig->short_url_length;
		}
		
		return 0;		
	}
	
	/* Private */
	private function getConfiguration()
	{
		return $this->connection->get('help/configuration');
	}
}
?>