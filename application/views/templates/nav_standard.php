    <!-- Navigation -->
    <nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-main-collapse">
                    <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand page-scroll" href="/">
                    <i class="fa fa-cutlery"></i>  <span class="pn-gray">PROJECT</span><span class="light">NOM</span>
                </a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-right navbar-main-collapse">
                <ul class="nav navbar-nav">
                    <!-- Hidden li included to remove active class from about link when scrolled up past about section -->
                    <li class="hidden">
                        <a href="#page-top"></a>
                    </li>

                    <li class="dropdown nav-search">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-search"></i> <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li>
                                <form class="navbar-form navbar-left" role="search" action='/recipe/search' method='post'>
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Search" id='search-query' name='search-query'>
                                    </div>
                                </form>
                            </li>
                        </ul>
                    </li>

                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" title="Friends"><i class="fa fa-users"></i> <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="/friends">Friend List</a></li>
                            <li id="nav-messages"><a href="/friends/messages">Messages</a></li>
                        </ul>
                    </li>

                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" title="Shopping List"><i class="fa fa-shopping-cart"></i> <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="/shoppinglist">Shopping List</a></li>
                        </ul>
                    </li>

                    <li class="dropdown" id="recipeNav">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Recipes <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="/recipe/search">Browse</a></li>
                            <li class="divider"></li>
                            <li><a href="/recipe/edit">Add</a></li>
                            <li id="importNav"><a href="#" data-toggle='modal' data-target='div.bs-recipeimport-modal'>Import...</a></li>
                        </ul>
                    </li>
                    <!--
                    <li>
                    	<a class="page-scroll" href="#about">About</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#signup">Sign up</a>
                    </li>
                    -->
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-user"></i> <?php print $username; ?> <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="/account">My Account</a></li>
                            <li class="divider"></li>
                            <li><a href="#" class='logout'>Log Off</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>