$(function() {
	initPage_base();
	initPage();
});

function initPage()
{
	$('#filters').on('show.bs.collapse', function () 
		{
			$(".fa-angle-double-right").removeClass("fa-angle-double-right").addClass("fa-angle-double-down");
		}
	);
	$('#filters').on('hide.bs.collapse', function () 
		{
			$(".fa-angle-double-down").removeClass("fa-angle-double-down").addClass("fa-angle-double-right");
		}
	);

	var $recipeTemplateRaw = $("section#search > div.search-results");
	$("div.row > div.invisible",$recipeTemplateRaw).removeClass("invisible");

	var recipeData = { recipeTemplate: $("div", $recipeTemplateRaw)[0] };

	$("div.filters a").on('click', onFilterChange);
	$("div.filters .filter-submit").on('click', filterResults);
	doSearch(recipeData);
}

function filterResults(filterEvent)
{
	buttonWait($(this), true);
	$("form#search-filters").submit();
}

function onFilterChange(filterChangeEvent)
{
	filterChangeEvent.preventDefault();
	
	var $searchForm = $('form#search-filters');
	var label = $(this).data("label");
	var value = $(this).data("value");
	var backingInput = $(this).data("input");
	
	if ($("input", this).length > 0)
	{
		var $checkbox = $("input", this);
			
		// Invert whatever value is currently set for the checkbox.	
		var isChecked = ($(backingInput).val() == "-1");
		value = isChecked ? "1" : "-1";
		
		setTimeout(function() 
				{ 
					$checkbox.prop('checked', isChecked);
					
					var labeltext = "";
					var $checkedRatings = $("ul.rating-filters input:checked");
					if ($checkedRatings.length == 5)
					{
						labeltext = "All";
					}
					else if ($checkedRatings.length == 1)
					{
						labeltext = $($checkedRatings[0].closest("a")).data("label");
					}
					else if ($checkedRatings.length === 0)
					{
						labeltext = "Not Rated";
					}
					else
					{
						labeltext = "Multiple";
					}

					$("button .button-label", $checkbox.closest(".btn-group")).html(labeltext);
				},
				0);
		
		filterChangeEvent.stopPropagation();
	}
	else
	{
		$("button .button-label", $(this).closest(".btn-group")).text(label);
	}

	$(backingInput).val(value);
}

function doSearch(recipeData)
{
	var currentPage = $("#current-page").val();
	var ratings = [];
	
	if ($("form#search-filters input#search-rating1").val() != "-1")
	{
		ratings.push(1);
	}
	if ($("form#search-filters input#search-rating2").val() != "-1")
	{
		ratings.push(2);
	}
	if ($("form#search-filters input#search-rating3").val() != "-1")
	{
		ratings.push(3);
	}
	if ($("form#search-filters input#search-rating4").val() != "-1")
	{
		ratings.push(4);
	}
	if ($("form#search-filters input#search-rating5").val() != "-1")
	{
		ratings.push(5);
	}
	if (ratings.length === 0 || ratings.length == 5)
	{
		ratings.push(0);
	}

	var post_data = 
				{
                    query: $("form#search-filters input#recipeSearch").val(),
                    sort: $("form#search-filters input#search-sort").val(),
                    cuisine: $("form#search-filters input#search-cuisine").val(),
                    type: $("form#search-filters input#search-type").val(),
                    rating: ratings
                };

    ajaxPost("/api/1/recipes/search/" + currentPage, post_data)
		.done(function(data) // success callback
		{ 
			if (data.recipes.length > 0)
			{
				var $allRecipes = $("section#search > div.search-results").detach().removeClass("hidden");
				$allRecipes.empty();
	
				for (var i = 0; i < data.recipes.length; i++) 
				{ 
					$recipe = $(recipeData.recipeTemplate).clone();
					$recipe = populateRecipeDetails($recipe, data.recipes[i]);
					$allRecipes.append($recipe);
				}
	
				$allRecipes.appendTo("section#search");
	
				var page_nav = populatePaginationNav(Math.ceil(data.total / 15), currentPage);
				$("section#search-pagination ul.pagination").html(page_nav);
				$('a.pagination-link').on('click', recipeData, onPageChange);
			}
			else
			{
				$("div.error")
					.removeClass("hidden")
					.find(".alert").text("No recipes found.");
			}
		})
		.always(function()
		{
			$("div.wait-spinner").remove();
		});	
}

function onPageChange(pageEvent)
{
	$("#current-page").val(this.id);
	doSearch(pageEvent.data);
}

function populateRecipeDetails($recipeTemplate, recipeData)
{
	$("div.recipe-image > a", $recipeTemplate).prop("href", "/recipe/"+recipeData.id);
	
	if (recipeData.image_url && recipeData.has_thumb == 1)
	{
		recipeData.image_url += ".thumb";
	}

	$("div.recipe-image > a > img", $recipeTemplate)
		.prop("src", (recipeData.image_url ? "/" + recipeData.image_url : "/img/place-setting.png"))
		.prop("alt", recipeData.title);
	$("div.recipe-detail > h4 > a", $recipeTemplate).prop("href", "/recipe/"+recipeData.id);
	$("div.recipe-detail > h4 > a", $recipeTemplate).html(recipeData.title);

	if (recipeData.comment)
	{
		$("div.recipe-detail > p.comment", $recipeTemplate).html(recipeData.comment);
	}
	if (recipeData.rating > 0)
	{
		var rating = "<i class='fa fa-star'></i>&nbsp;".repeat(recipeData.rating);
		$("div.recipe-props span.recipe-rating", $recipeTemplate)
			.html(rating)
			.parent().removeClass("hidden");
	}
	if (recipeData.servings)
	{
		var servings = "<i class='fa fa-users'></i> " + recipeData.servings;
		$("div.recipe-props span.recipe-servings", $recipeTemplate)
			.html(servings)
			.parent().removeClass("hidden");
	}
	if (recipeData.total_time)
	{
		var total_time = "<i class='fa fa-clock-o'></i> " + recipeData.total_time;
		$("div.recipe-props span.recipe-time", $recipeTemplate)
			.html(total_time)
			.parent().removeClass("hidden");
	}
	if (recipeData.ingredients.length > 0)
	{
		var ingredients = "<span class='glyphicon glyphicon-apple'></span> " + recipeData.ingredients.length + " ingredients";
		$("div.recipe-props span.recipe-ingredients", $recipeTemplate)
			.html(ingredients)
			.parent().removeClass("hidden");
	}

	return $recipeTemplate;
}

function populatePaginationNav(pages,current)
{
	var result = "";

	if (current > 1)
	{
		result += "<li>";
		result += "<a href='#' class='pagination-link' id='" + (current-1) + "' aria-label='Previous'><span aria-hidden='true'>&laquo;</span></a>";
	    result += "</li>";
	}

    for(var i = 1; i<=pages; i++)
    {
		var element = "li";
		if (current == i) { element += " class='active'"; }
		result += "<" + element + "><a href='#' class='pagination-link' id='" + i + "'>" + i + "</a></li>"; //search/" + i + "
    }

    if (current < pages)
    {
	    result += "<li>";
	    result += "<a href='#' class='pagination-link' id='" + (parseInt(current,10)+1) + "' aria-label='Next'><span aria-hidden='true'>&raquo;</span></a>";
	    result += "</li>";
    }

    return result;
}

