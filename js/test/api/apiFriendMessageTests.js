casper.test.begin('WebAPI Tests: Friend/Message Endpoints',
function suite(test) 
{
	var url = "test/Friends_tests";
	
	// Navigate to unit test page
    casper.start("http://projectnom2.dev/" + url, function() {
	    this.waitForUrl(new RegExp(url));
	});
	
	casper.thenBypassIf(function() {
		return casper.evaluate(countFailures) === 0; 
	}, 1);
	
	// Print failures
	casper.then(function() {
		this.echo("[FAILURES] " + casper.evaluate(getFailedTestNames), "ERROR");
		test.fail("Test failures!");
	});

	casper.thenBypassUnless(function () {
		return casper.evaluate(countFailures) === 0;
	}, 1);
	
	// Verify passes
	casper.then(function() {
		this.echo("No failures!", "INFO");
		test.assertElementCount("div.pas", 24, "All tests passed");		
	});
	
	casper.run(function() {
        test.done();
    });
});