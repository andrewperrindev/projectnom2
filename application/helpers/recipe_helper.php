<?php
	include_once("simple_html_dom_helper.php");

	function CreateCachedRecipe($remoteUrl, $localFile = "", $filenameHash = "unknown")
	{
		if (!empty($remoteUrl))
		{
			if (empty($localFile))
			{
				$localFile = "recipecache/$filenameHash" . time() . ".html";
			}

			$CI =& get_instance();
			$CI->load->helper("remoteretrieve");

			$recipe_content = file_get_contents_curl($remoteUrl);
			
			if ($recipe_content === FALSE)
			{
				throw new Exception("Could not retrieve remote URL");
			}
			
			// Strip out all JavaScript
			$recipe_content = preg_replace('/<script(.*?)>(.*?)<\/script>/is', '', $recipe_content);
			// Expose any "noscript" content since JS won't be executing
			$recipe_content = str_ireplace(array("<noscript>", "</noscript>"), "", $recipe_content);
			// Some pages hide their entire content until after load (???), so this should undo that.
			$recipe_content .= "<style>html {visibility: visible; }</style>";

			$recipe_html = str_get_html($recipe_content);
			$element = $recipe_html->find("head", 0);
			$element->outertext = '<head>' . "<base href='$remoteUrl'>" . $element->innertext . '</head>';
			$recipe_content = $recipe_html->save();
			
			$recipe_html = str_get_html($recipe_content);

			/*
				We can't load HTTP content from an HTTPS page, and that can cause issues with loading CSS.
				To work around this, we look for all <link>'d stylesheets and load their content on the server.
				We then replace the <link> tag with the styles directly, inline with the HTML.
			*/
			foreach($recipe_html->find('link') as $link)
			{
				// We only want stylesheet links, and only stylesheets meant for screen display.
				if ($link->rel == "stylesheet" && (empty($link->media) || strtolower($link->media) != "print"))
				{
					$style_href = $link->href;
					
					// If the href to the CSS is relative, we need to handle it appropriately.
					if (substr($style_href, 0, 4) != "http")
					{
						$parsed_href = parse_url($remoteUrl);

						if (substr($style_href, 0, 2) == "//")
						{
							$style_href = $parsed_href["scheme"] . ":$style_href";
						}
						else
						{
							if ($style_href[0] != "/")
							{
								$path_href = pathinfo($parsed_href["path"], PATHINFO_DIRNAME);
								$style_href = "/$path_href/$style_href";
							}
							
							$style_href = $parsed_href["scheme"] . "://" . $parsed_href["host"] . $style_href;
						}
					}
					
					$style_content = file_get_contents_curl($style_href);
					
					if ($style_content !== FALSE)
					{
						$link->outertext = '<style type="text/css">' . $style_content . '</style>';
					}
				}
			}
			
			$recipe_content = $recipe_html->save();

			$recipe_file = fopen(APPPATH . "/../" . $localFile, "w");
			fwrite($recipe_file, $recipe_content);
			fclose($recipe_file);

			return $localFile;
		}
	}

	function GetRecipeImages($remoteUrl, $localFile)
	{
		$result = array();
		$source_url_components = parse_url($remoteUrl);
		$recipe_content = file_get_contents(APPPATH . "/../" . $localFile);
		$recipe_html = str_get_html($recipe_content);

		// Find all image links
		$image_links = $recipe_html->find('a');
		$total_tags = count($image_links);
		$count = 0;

		foreach($image_links as $element)
		{
			if (strripos($element->href, ".jpg") > 0 || strripos($element->href, ".gif") > 0 || strripos($element->href, ".png") > 0)
			{
				$count++;
				$img_src = $element->href;

				if (is_string($img_src) && $img_src[0] == "/")
				{
					$img_src = $source_url_components["scheme"] . "://" . $source_url_components["host"] . $img_src;
				}
	
				if ($img_src != "")
				{
					$result[] = $img_src;
				}				
				//$result[] = $element->href;
				//print "<li class='image$count'><a href='#' class='thumbnail'><img id='image$count' alt='" . $element->alt . "' src='" . $element->href . "' onClick='selectThumb(\"image$count\")' onLoad='ThumbTest(\"image$count\")' /></a></li>";
			}
		}

		// Find all images
		$image_tags = $recipe_html->find('img');
		$total_tags = count($image_tags);

		foreach($image_tags as $element)
		{
			$count++;
			$img_src = $element->src;

			if (is_string($img_src))
			{
				if (stripos($img_src, "//") === 0)
				{
					$img_src = "http:$img_src";
				}
				if ($img_src[0] == "/")
				{
					$img_src = $source_url_components["scheme"] . "://" . $source_url_components["host"] . $img_src;
				}
			}
			
			if ($img_src != "")
			{
				$result[] = $img_src;
				//print "<li class='image$count'><a href='#' class='thumbnail'><img height='" . $element->height . "' id='image$count' alt='" . $element->alt . "' src='" . $img_src . "' onClick='selectThumb(\"image$count\")' onLoad='ThumbTest(\"image$count\")' /></a></li>";
			}
		}

		return $result;
	}
?>