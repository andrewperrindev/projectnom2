<?php
class RecipeDTO
{
	public $id = -1;

	public $title;
	public $author;

	public $servings;
	public $prep_time;
	public $total_time;
	
	public $origin_url;
	public $cache_url;
	public $image_url;
	public $has_thumb;
	
	public $ingredients = array();
	public $directions = array();
	
	public $comment;
	public $cuisine;
	public $type;
	public $rating;

	public $cookbook_title;
	public $cookbook_page;

	public $editable = true;

	public static function sort_bytitle($recipe1, $recipe2)
	{
		$title1 = str_ireplace(array('a ', 'the '), '', $recipe1->title);
		$title2 = str_ireplace(array('a ', 'the '), '', $recipe2->title);
		return strcmp($title1, $title2);
	}
	public static function sort_byrating($recipe1, $recipe2)
	{
		if ($recipe1->rating == $recipe2->rating)
		{
			return self::sort_bytitle($recipe1, $recipe2);
		}
		
		return ($recipe1->rating > $recipe2->rating) ? -1 : 1;
	}
	public static function sort_byingredients($recipe1, $recipe2)
	{
		$ingredient_count1 = $ingredient_count2 = 0;

		if (is_array($recipe1->ingredients))
		{
			$ingredient_count1 = count($recipe1->ingredients);
		}
		if (is_array($recipe2->ingredients))
		{
			$ingredient_count2 = count($recipe2->ingredients);
		}

		if ($ingredient_count1 == $ingredient_count2)
		{
			return 0;
		}

		return ($ingredient_count1 < $ingredient_count2) ? -1 : 1;
	}
	public static function sort_bytotaltime($recipe1, $recipe2)
	{
		if (!empty($recipe1->total_time))
		{
			$time1 = self::parse_time_string($recipe1->total_time);
		}
		else if (!empty($recipe1->prep_time))
		{
			$time1 = self::parse_time_string($recipe1->prep_time);
		}
		else
		{
			$time1 = 999;
			if (is_array($recipe1->ingredients))
			{
				$time1 *= count($recipe1->ingredients);
			}
		}

		if (!empty($recipe2->total_time))
		{
			$time2 = self::parse_time_string($recipe2->total_time);
		}
		else if (!empty($recipe2->prep_time))
		{
			$time2 = self::parse_time_string($recipe2->prep_time);
		}
		else
		{
			$time2 = 999;
			if (is_array($recipe2->ingredients))
			{
				$time2 *= count($recipe2->ingredients);
			}
		}

		if ($time1 == $time2)
		{
			return 0;
		}

		return ($time1 < $time2) ? -1 : 1;
	}

	private static function parse_time_string($time_string)
	{
		$hours = $minutes = 0;
		$hours_match_count = preg_match("/([0-9.]+)\s?h(ou)?r?s?/i", $time_string, $hour_matches);
		
		if ($hours_match_count > 0 && !empty($hour_matches[0]))
		{
			$hours = $hour_matches[1] * 60;
		}
		
		$minutes_match_count = preg_match("/([0-9.]+)\s?m(in)?(ute)?s?/i", $time_string, $minute_matches);

		if ($minutes_match_count > 0 && !empty($minute_matches[0]))
		{
			$minutes = $minute_matches[1];
		}

		return $hours + $minutes;		
	}
}
?>