<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once(APPPATH . '/controllers/WebAppControllerBase.php');

class Image extends WebAppControllerBase 
{
	const ORIGINAL = "original";
	const SMALL = "small";
	const MEDIUM = "medium";
	
	public function __construct()
	{
		parent::__construct();

		if ($this->session->userdata('username') === NULL)
		{
			$this->failureRedirect();
		}
	}

	public function _remap($method, $params = array())
	{
		if (trim($method) == "index" || trim($method) == "")
		{
			header('HTTP/1.1 404 File not found', true, 404);
			header('Content-Type: text/plain');
			print "Directory listing not allowed.";
		}
		else
		{
			$size = self::ORIGINAL;
			
			if (count($params) > 0)
			{
				$params[0] = strtolower($params[0]);

				if ($params[0] == self::SMALL)
				{
					$size = self::SMALL;
				}
				else if ($params[0] == self::MEDIUM)
				{
					$size = self::MEDIUM;
				}
			}
			
    		$this->index($method, $size);
		}
	}

	public function index ($filename, $size = self::ORIGINAL)
	{
		$file_path = $ctype = "";
		$file_extension = strtolower(substr(strrchr($filename,"."),1));

		switch( $file_extension ) {
		    case "gif": $ctype="image/gif"; break;
		    case "png": $ctype="image/png"; break;
		    case "jpeg":
		    case "jpg": $ctype="image/jpeg"; break;
		    default:
	    }
	    
	    switch( $size ) {
		    case self::SMALL: $height=200; break;
		    case self::MEDIUM: $height=400; break;
		    default: $height = 600;
	    }

		if (file_exists(APPPATH . "/../temp/$filename"))
		{
			$file_path = APPPATH . "/../temp/$filename";
		}
		else if (file_exists(APPPATH . "/../recipecache/$filename"))
		{
			$file_path = APPPATH . "/../recipecache/$filename";
		}
		else if (file_exists(APPPATH . "/../msgcache/$filename"))
		{
			$file_path = APPPATH . "/../msgcache/$filename";
		}
		else
		{
			header('HTTP/1.1 404 File not found', true, 404);
			header('Content-Type: text/plain');
			print "The file requested was not found on this server.";
			die();
		}

		header('Content-Type: image/png');
		
		$this->load->library("ImageUtils");
		$image = $this->imageutils->resizeImage($file_path, 800, $height);
		imagepng($image);
	}
}
?>