    <!-- Intro Header -->
    <header class="intro">
        <div class="intro-body">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2 greeting">
	                    <h1 class='brand-heading'>Account Profile</h1>
	                </div>
                </div>
            </div>
        </div>
    </header>

	<section id="profile" class="container content-section text-dark">
    	<div class="container error hidden">
			<div class="row">
            	<div class="col-sm-12 text-dark">
                	<div class="alert alert-danger" role="alert"></div>
            	</div>
        	</div>
		</div>
    	<div class="container success hidden">
			<div class="row">
            	<div class="col-sm-12 text-dark">
                	<div class="alert alert-success" role="alert"></div>
            	</div>
        	</div>
		</div>
		<div class="row">
			<div class="col-xs-12 col-sm-8 col-sm-offset-1">
				<form class="form-horizontal" id="formUpdateProfile" action='/api/1/user' method='post'>
					<div class="form-group">
						<label for="inputUsername" class="col-xs-4 control-label">Username</label>
						<div class="col-xs-8">
							<input type="text" class="form-control" id="inputUsername" placeholder="Username" value='<?php print $username; ?>' disabled>
						</div>
					</div>
					<div class="form-group">
						<label for="inputEmail" class="col-xs-4 control-label">Email</label>
						<div class="col-xs-8">
							<input type="email" class="form-control" id="inputEmail" placeholder="Email" value='<?php print $email; ?>'>
    					</div>
					</div>
					<div class="form-group">
						<label for="inputTwitter" class="col-xs-4 control-label">Twitter</label>
						<div class="col-xs-8">
							<div class="twitter" id="twitterConnect"><?php print $twitter; ?></div>
    					</div>
					</div>
					<hr>
					<div class="form-group">
						<label for="newPassword" class="col-xs-4 control-label">New Password</label>
						<div class="col-xs-8">
							<input type="password" class="form-control" id="newPassword" aria-describedby="helpPassword">
						</div>
					</div>
					<div class="form-group">
						<label for="confirmPassword" class="col-xs-4 control-label">Confirm New Password</label>
						<div class="col-xs-8">
							<input type="password" class="form-control" id="confirmPassword">
						</div>
						<div class="col-xs-8">
							<span id="helpPassword" class="help-block">If you don't want to change your password, leave these blank.</span>
						</div>
					</div>
					<div class="form-group">
						<div class="col-xs-offset-4 col-xs-8" id="saveButton">
							<button type="submit" class="btn btn-primary" id='editProfileSubmit'>Save</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</section>