var pnLanding = (function($) {
	var React = require('react');
	var ReactDOM = require('react-dom');
	
	// Custom React Component
	var BusyButton = require("../components/BusyButton.js");
	
	var signupButton = null;
	var loginButton = null;
	
	var publicApi = {
		init: initPage
	};

	return publicApi;

	// ---------------------
	
	function signUpRedirect()
	{
		signupButton.busy();
		window.location = "/#signup";
	}

	function logInRedirect()
	{
		loginButton.busy();
		window.location = "/";
	}
	
	// Needs to get wrapped into a React component
	function importSetup()
	{
		// TODO: Source from React component state
		var message = window.PN.messageJson;
		
		$("div.bs-recipeimport-modal").on("show.bs.modal", function(event)
		{
			var modal = event.target;
			var button = $(event.relatedTarget); // Button that triggered the modal

			if (button.data("id") > 0 && message.recipe_id > 0 && button.data("id") == message.recipe_id)
			{
				//var $recipeTitle = $("span.attachment-content a", $(button).closest("div.attachment-section"));
			
				$("div.instr-url", modal).addClass("hidden");
				$("div.instr-friend", modal).removeClass("hidden");
				$("input.import-recipe-url-display", modal)
					.addClass("hidden")
					.val("/recipe/" + message.recipe_id);
				$("div.recipe-title span.recipe-title", modal).text(message.recipe_title);
				$("div.recipe-title", modal).removeClass("hidden");
			}
		});
		$("div.bs-recipeimport-modal").on("hide.bs.modal", function(event)
		{
			var modal = event.target;
			
			$("div.instr-url", modal).removeClass("hidden");
			$("div.instr-friend", modal).addClass("hidden");
			$("input.import-recipe-url-display", modal)
				.removeClass("hidden")
				.val("");
			$("div.recipe-title", modal).addClass("hidden");
		});
	}
	
	function initPage()
	{
		if ($("#signup").length)
		{
			signupButton = ReactDOM.render(
				React.createElement(BusyButton,
					{type: "submit", style: "primary", id: "signUpRedirect", label: "Sign Up", onClick: signUpRedirect}
				),
				document.getElementById('signup')
			);

			loginButton = ReactDOM.render(
				React.createElement(BusyButton,
					{type: "submit", style: "default", id: "logInRedirect", label: "Log In", onClick: logInRedirect}
				),
				document.getElementById('login')
			);
		}
		else if ($("div.bs-recipeimport-modal").length)
		{
			importSetup();
		}
	}
})(jQuery);

$(function() {
	initPage_base();
	pnLanding.init();
});