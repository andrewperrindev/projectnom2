<?php
	
class Base_model extends CI_Model 
{
    function __construct()
    {
        parent::__construct();
    }

	function getIdForUsername($username) {
		$query = $this->db->get_where('users', array('username' => $username));
		
    	if ($query->num_rows() > 0)
    	{
			return $query->row()->id;
		}
		
		return -1;
	}
}

?>