var pnRecipeEdit = (function($) {
	var React = require('react');
	var ReactDOM = require('react-dom');

	// Custom React Component
	var BootstrapError = require("../components/BootstrapError.js");
	var Editable = require("../components/Editable.js");
	var BusyButton = require("../components/BusyButton.js");

	var dropzoneConfig = { 
		url: "/api/1/recipes/image/",
		paramName: "userfile",
		maxFilesize: 4,
		uploadMultiple: false,
		maxFiles: 1,
		acceptedFiles: "image/jpeg,image/jpg,image/gif,image/png",
		previewsContainer: "div.upload-preview",
		previewTemplate: $('div.upload-preview-template').html(),
		init: initDropzone,

		fallback: function() {
			$("div#fileUpload").addClass("hidden");
			$("button.upload-modal").addClass("hidden");
			
			ReactDOM.render(
				React.createElement(BootstrapError,
					{
						visible: true,
						title: "Unfortunately, your browser is too old to support file uploads.", 
						detail: "Please upgrade or switch browsers if you wish to upload an image."
					}
				),
				document.getElementById("fallback")
			);
			
			$("div#fallback").removeClass("hidden");
		}
	};

	var reactComponents = {
		recipeTitle: ReactDOM.render(
			React.createElement(Editable,
				{
					componentRef: "recipeTitle",
					name: "title",
					type: "textarea",
					fontSize: 35,
					placeholder: "Recipe Title",
					onChangeCallback: handleEditableChange,
					onResize: redrawPopover,
					label: "Title",
					displayClasses: "text-center",
					editClasses: "recipe-title"
				}
			),
			document.getElementById("titlePlaceholder")
		),
	
		recipeAuthor: ReactDOM.render(
			React.createElement(Editable,
				{
					componentRef: "recipeAuthor",
					name: "author",
					type: "input",
					inputSize: "large",
					placeholder: "Author",
					onChangeCallback: handleEditableChange,
					label: "Author",
					editClasses: "author"
				}
			),
			document.getElementById("authorPlaceholder")
		),

		recipeNotes: ReactDOM.render(
			React.createElement(Editable,
				{
					componentRef: "recipeNotes",
					name: "comment",
					type: "textarea",
					fontSize: 14,
					placeholder: "Notes",
					onChangeCallback: handleEditableChange,
					label: "Notes",
					editClasses: "recipe-comments"
				}
			),
			document.getElementById("notesPlaceholder")
		),
		
		recipeYield: ReactDOM.render(
			React.createElement(Editable,
				{
					componentRef: "recipeYield",
					name: "servings",
					type: "input",
					// inputSize: "large",
					placeholder: "Yield",
					onChangeCallback: handleEditableChange,
					label: "Yield",
					editClasses: "servings"
				}
			),
			document.getElementById("yieldPlaceholder")
		),
		
		recipePrepTime: ReactDOM.render(
			React.createElement(Editable,
				{
					componentRef: "recipePrepTime",
					name: "prep-time",
					type: "input",
					placeholder: "Prep Time",
					onChangeCallback: handleEditableChange,
					label: "Prep Time",
					editClasses: "preptime"
				}
			),
			document.getElementById("prepPlaceholder")
		),

		recipeTotalTime: ReactDOM.render(
			React.createElement(Editable,
				{
					componentRef: "recipeTotalTime",
					name: "total-time",
					type: "input",
					placeholder: "Total Time",
					onChangeCallback: handleEditableChange,
					label: "Total Time",
					editClasses: "total"
				}
			),
			document.getElementById("totalPlaceholder")
		),
		
		save: ReactDOM.render(
			React.createElement(BusyButton,
				{
					id: "editRecipeSubmit",
					type: "submit",
					style: "primary",
					icon: "check",
					label: "Save",
					classes: {
						button: "btn-lg pull-right extra-padding",
						icon: "visible-xs-inline-block",
						label: "hidden-xs"
					},
					iconPadding: 0
				}
			),
			document.getElementById("savePlaceholder")
		),
		
		delete: ReactDOM.render(
			React.createElement(BusyButton,
				{
					id: "editRecipeDelete",
					type: "button",
					style: "danger",
					icon: "trash",
					label: "Delete",
					classes: {
						button: "btn-lg pull-left extra-padding",
						icon: "visible-xs-inline-block",
						label: "hidden-xs"
					},
					iconPadding: 0
				}
			),
			document.getElementById("deletePlaceholder")
		)
	};

	function initDropzone()
	{
		this
		.on("addedfile", function(file) 
			{
				$('.btn-confirm').prop('disabled', true);
			}
		)
		.on("maxfilesreached", function(file) 
			{
				$(".dropzone").addClass("hidden");
			}
		)
		.on("removedfile", function(file) 
			{
				$(".dropzone").removeClass("hidden");
				$('input#upload-file-name').val("");
				resetUploadError();
			}
		)
		. on("success", function(file, response)
			{
				resetUploadError();
				var result = JSON.parse(response);
	
				if (result.success == 1)
				{
					$('input#upload-file-name').val(result.result);
					$("#total-progress").removeClass("progress-striped active");
				}
				else
				{
					$('div.row-error span.error-text').html(result.error);
					showUploadError();
				}
			}
		)
		.on("error", function(file, errorMessage)
			{
				this.removeAllFiles();
				$('div.row-error span.error-text').html(errorMessage);
				showUploadError();
			}
		)
		.on("queuecomplete", function(progress) 
			{
				$('.btn-confirm').prop('disabled', false);
			}
		);
	}

	function loadRecipeForEdit(data)
	{
		if (data.success === 0 || data.readonly == 1)
		{
			var $recipeAlert = $("section#header > div.error");
			var alertText = "<strong>It looks like you don't have access to this recipe!</strong>";
	
			$("div.alert", $recipeAlert).html(alertText);
			$recipeAlert.removeClass('hidden');
		}
		else
		{
			var recipeData = data.recipes[0];
			document.title = recipeData.title + " :: " + document.title;
	
			var $recipeHeader = $("section#header > div.recipe-header").detach();
			
			if (recipeData.image_url !== null && recipeData.image_url !== "")
			{
				$("img.recipe-thumb", $recipeHeader).attr("src", "/" + recipeData.image_url);
			}
			
			reactComponents.recipeTitle.value(recipeData.title);
			reactComponents.recipeAuthor.value(recipeData.author);
			reactComponents.recipeNotes.value(recipeData.comment);
			reactComponents.recipeYield.value(recipeData.servings);
			reactComponents.recipePrepTime.value(recipeData.prep_time);
			reactComponents.recipeTotalTime.value(recipeData.total_time);

			$("input#inputRating", $recipeHeader).val(recipeData.rating);
			$("input#inputCuisine", $recipeHeader).val(recipeData.cuisine);
			$("input#inputRecipeType", $recipeHeader).val(recipeData.type);
			setRatingDropdown(recipeData.rating, $('button#dropdownRating > span.button-text', $recipeHeader));
			setCuisineDropdown(recipeData.cuisine, $('button#dropdownCuisine > span.button-text', $recipeHeader));
			setTypeDropdown(recipeData.type, $('button#dropdownTypes > span.button-text', $recipeHeader));
	
			$recipeHeader.appendTo("section#header");
	
			var $ingredientSection = $("div.ingredient-collection").detach();
			for (var i = 0; i < recipeData.ingredients.length; i++) 
			{ 
				var ingredient = recipeData.ingredients[i];
				var $newRow = getIngredientRow(ingredient);
				$($ingredientSection).append($newRow);
			}
	
			$ingredientSection.prependTo("section#ingredients");
	
			var $directionSection = $("div.direction-collection").detach();
			for (i = 0; i < recipeData.directions.length; i++)
			{
				var direction = recipeData.directions[i];
				var $directionRow = $("div.direction-row.hidden").clone();
				$directionRow.removeClass("hidden");
	
				$("h1", $directionRow).text(i+1);
				$("textarea.recipe-direction", $directionRow).html(direction);
	
				$directionSection.append($directionRow);
			}
	
			$directionSection.prependTo("section#directions");
			$("div.direction-collection .recipe-direction").autogrow();
	
			showPageContent();
		}
	}

	function recipeMetadataSelect(itemEvent)
	{
		itemEvent.preventDefault();
		$selectedItem = $(this);
		$parentButton = $selectedItem.closest("div.dropdown");
		$parentButtonLabel = $("span.button-text", $parentButton);
		var selectedValue = $selectedItem.data("value");
	
		if ($("button", $parentButton).attr('id') == 'dropdownRating')
		{
			if (selectedValue != $parentButtonLabel.attr("id"))
			{
				$("input#inputRating").val(selectedValue);
				setRatingDropdown(selectedValue, $parentButtonLabel);
			}
		}
		else if ($("button", $parentButton).attr('id') == 'dropdownCuisine')
		{
			if (selectedValue != $parentButtonLabel.attr("id"))
			{
				$("input#inputCuisine").val(selectedValue);
				setCuisineDropdown(selectedValue, $parentButtonLabel);
			}
		}
		else if ($("button", $parentButton).attr('id') == 'dropdownTypes')
		{
			if (selectedValue != $parentButtonLabel.attr("id"))
			{
				$("input#inputRecipeType").val(selectedValue);
				setTypeDropdown(selectedValue, $parentButtonLabel);
			}
		}
	}

	function getBlankIngredientRow()
	{
		$newRow = $("div.ingredient-row.full.hidden").clone();
		$newRow.removeClass("hidden");
		return $newRow;
	}
	
	function getIngredientRow(ingredient)
	{
		var $newRow;
	
		if (ingredient.id > 0)
		{
			$newRow = getBlankIngredientRow();
			$("input.quantity", $newRow).val(ingredient.quantity);
			$("input.unit", $newRow).val(ingredient.unit);
			$("input.ingredient-name", $newRow).val(ingredient.name);
			$("input.prep", $newRow).val(ingredient.preparation);
		}
		else
		{
			$newRow = $("div.ingredient-row.text-only.hidden").clone();
			$newRow.removeClass("hidden");
			$("input.text", $newRow).val(ingredient.name);
		}
	
		return $newRow;
	}

	function addBlankIngredientRow(event)
	{
		$newRow = $("div.ingredient-row.parsable.hidden").clone();
		$newRow.removeClass("hidden");
		
		if ($("div.ingredient-row").not(".hidden").length === 0)
		{
			$("input.raw", $newRow).tooltip({
				trigger: 'focus',
				title: 'Tip: Press Enter to quickly add more ingredients'
			});
		}
		
		addIngredientToDom($newRow);
		$("input.raw", $newRow).focus();
	}
	
	function addIngredientToDom(ingredient)
	{
		$("section#ingredients").removeAttr('style');
		$("div.ingredient-collection").append(ingredient);
		setStaticSize("section#ingredients");
	}
	
	function getBlankDirectionRow()
	{
		$newRow = $("div.direction-row.hidden").clone();
		$newRow.removeClass("hidden");
		return $newRow;
	}
	
	function getDirectionRow(direction)
	{
		var $newRow = getBlankDirectionRow();
		$("textarea.recipe-direction", $newRow).html(direction);
		return $newRow;
	}
	
	function addBlankDirectionRow()
	{
		$newRow = getBlankDirectionRow();
		addDirectionToDom($newRow);
		$("textarea.recipe-direction", $newRow).focus();
	}
	
	function addDirectionToDom(direction)
	{
		$("section#directions").removeAttr('style');
		$("h1.direction-text",direction).text($("div.direction-collection .direction-row").length + 1);
		$("div.direction-collection").append(direction);
		$(".recipe-direction", direction).autogrow();
		$("textarea", direction).trigger("paste");
	}

	function removeRow(event)
	{
		event.preventDefault();
	
		var $recipeRow = $(this).parents("div.ingredient-row,div.direction-row");
		var isDirection = $recipeRow.hasClass("direction-row");
		$recipeRow.remove();
	
		if (isDirection)
		{
			enumerateDirections();
		}
		else
		{
			$("section#ingredients").removeAttr('style');
			setStaticSize("section#ingredients");
		}
	}
	
	function convertIngredientToText(event)
	{
		event.preventDefault();
		var $thisRow = $(this).parents("div.ingredient-row");
	
		var fullText = $.trim($(".quantity",$thisRow).val() + " " + $(".unit",$thisRow).val() + " " + $(".ingredient-name",$thisRow).val() + " " + $(".prep",$thisRow).val());
		var $textRow = $("div.ingredient-row.text-only.hidden").clone();
		$thisRow.html($textRow.html());
		$("input.text",$thisRow).val(fullText);
	}
	
	function convertTextToIngredient(event)
	{
		event.preventDefault();
		var $thisRow = $(this).parents("div.ingredient-row");
	
		var fullText = $.trim($(".text",$thisRow).val());
		var $ingredientRow = $("div.ingredient-row.full.hidden").clone();
		$thisRow.html($ingredientRow.html());
		$("input.ingredient-name",$thisRow).val(fullText);
	}
	
	function parseIngredient(event)
	{
		var $thisRow = $(event.target).parents("div.ingredient-row");
		var rawIngredient = $.trim($(event.target).val());
		
		if (rawIngredient !== "")
		{
			setStaticSize($thisRow);
			
			$("div.drag-handle", $thisRow).css("visibility", "hidden");
			$("div.menu", $thisRow).hide();
			$("span.spinner", $thisRow).show();
			
			var params = 
			{
				content: rawIngredient
			};
	
			ajaxPost("/recipe/parse/ingredients", params)
				.done(function(data) // success callback
					{ 
						var $ingredientRow = getIngredientRow(data.result[0]);
						$("div.row", $thisRow).remove();
						$thisRow.append($("> div.row", $ingredientRow));
						$thisRow.removeAttr("style");
						$thisRow
							.removeClass("parsable")
							.addClass("parsed");
					}
				)
				.fail(function(data)
					{
						$("span.spinner", $thisRow).hide();
						$("div.drag-handle", $thisRow).css("visibility", "visible");
						$("div.menu", $thisRow).show();
					}
				);
		}
	}

	function enumerateDirections()
	{
		$("div.direction-collection .direction-row").each(
			function(index)
			{
				$("h1.direction-text", this).text(index + 1);
			}
		);
	
	}
	
	function importIngredient(ingredient) {
		var $newRow = getIngredientRow(ingredient);
		addIngredientToDom($newRow);
	}
	
	function importDirection(direction) {
		var $newRow = getDirectionRow(direction);
		addDirectionToDom($newRow);
	}

	function deleteRecipeHandler(event)
	{
		reactComponents.delete.busy();
		
		bootbox.confirm({
			message: "Are you sure you want to delete this recipe? It can't be recovered.",
			title: "Delete Recipe",
			className: "text-dark",
			buttons: {
				cancel: {
				  label: "Nevermind",
				  className: "btn-default"
				},
				confirm: {
				  label: "Yes, delete",
				  className: "btn-danger"
				}
			},
			callback: function(result) { 
				if (result)
				{
					deleteRecipe()
						.done(function(data) {
							if (data.success == 1)
							{
								window.location.href = "/home";
							}
							else
							{
								if (data.error == "You must login before you can edit a recipe.")
								{
									$(".bs-login-modal").modal('show');
								}
								else
								{
									$("section#header > div.error div.alert")
										.text("Unable to delete this recipe.");
									$("section#header > div.error")
										.removeClass("hidden");
								}
								
								reactComponents.delete.activate();
							}
						});
				}
				else
				{
					reactComponents.delete.activate();
				}
			}
		});
	}

	function deleteRecipe()
	{
		var recipeId = $("#recipe-id").val();
		return ajaxDelete('/api/1/recipes/' + recipeId, null);
	}
	
	function submitRecipe(event)
	{
		event.preventDefault();
		$form = $(this);
		reactComponents.save.busy();
	
		// Filter out any blank raw ingredient rows,
		// then wait for any conversions that haven't completed yet.
		$parsableIngredients = $("div.ingredient-row.parsable").not(".hidden");
		
		$parsableIngredients.each(function (index) {
			$rawInput = $("input.raw", this);
			
			if ($rawInput.length > 0 && $.trim($rawInput.val()) === "") {
				$(this).remove();
			}
		});
		
		if ($parsableIngredients.length > 0 || $("div.ingredient-row").length != $("div.ingredient-row > div.row").length) {
			setTimeout(function delayedSubmit() { submitRecipe.call($form, event); }, 1000);
			return;
		}
	
		var submitUrl = $(this).attr("action");
		var returnUrl = $(this).data("return");
	
		var parsedResult = {
			id: $("input#recipe-id", this).val(),
			recipe_img: $("input#recipe-img", this).val(),
			title: $("input.recipe-title", this).val(),
			comment: $("input.recipe-comments", this).val(),
			author: $("input.author", this).val(),
			servings: $("input.servings", this).val(),
			prep: $("input.preptime", this).val(),
			total: $("input.total", this).val(),
			rating: $("input#inputRating", this).val(),
			cuisine: $("input#inputCuisine", this).val(),
			type: $("input#inputRecipeType", this).val(),
			ingredients: [],
			directions: []
		};
		
		if ($("iframe#recipe").length > 0)
		{
			parsedResult.cache_url = $("iframe#recipe").attr("src");
			parsedResult.origin_url = $("iframe#recipe").data("origin");
		}
	
		$("div.ingredient-collection .ingredient-row").each(function(index)
		{
			var ingredient = {
				type: $("input#type", this).val()
			};
	
			if (ingredient.type == 'i')
			{
				ingredient.name = $("input.ingredient-name", this).val();
				ingredient.quantity = $("input.quantity", this).val();
				ingredient.unit = $("input.unit", this).val();
				ingredient.prep = $("input.prep", this).val();
			}
			else
			{
				ingredient.text = $("input.text", this).val();
			}
	
			parsedResult.ingredients.push(ingredient);
		});
	
		$("div.direction-collection .direction-row").each(function(index)
		{
			parsedResult.directions.push($("textarea.recipe-direction", this).val());
		});
	
		var parsedResultJson = JSON.stringify(parsedResult);
	
	    ajaxPostJson(submitUrl, parsedResultJson)
			.done(function(data) {
				if (data.success == 1)
				{
					window.location.href = returnUrl + data.result;
				}
				else
				{
					if (data.error == "You must login before you can edit a recipe.")
					{
						$(".bs-login-modal").modal('show');
					}
					else
					{
						showSaveError(data);
					}
					
					reactComponents.save.activate();
				}
			})
			.fail(function(data) {
				showSaveError(data);
				reactComponents.save.activate();
			});
	}

	function showSaveError(response) {
		var errorText = "There was a problem saving your recipe. ";
		if (response.error) {
			errorText += response.error;
		}
		else {
			errorText += "Please try again, or <a href='mailto:support@projectnom.com'>email support</a> if you continue to have trouble.";
		}
		
		$("section#header > div.error div.alert")
			.html(errorText);
		$("section#header > div.error")
			.removeClass("hidden");		
	}

	function modalUploadHandler(event)
	{
		var filename = $('input#upload-file-name').val();
	
		if (filename !== "")
		{
			$("input#recipe-img").val(filename);
			$("img.recipe-thumb").attr("src", "/image/" + filename);
		}
		/*
		Replace with RESET button underneath image
		else
		{
			$("input.recipe-img").val("");
			$("img.recipe-thumb").attr("src", "/img/edit-placeholder.png");		
		}
		*/
	
		$(".bs-upload-modal").modal('toggle');
	}
	
	function showUploadError()
	{
		$('div.row-error').removeClass('hidden');
		$("#total-progress").removeClass("progress-striped active");
		$('div.progress-bar')
			.removeClass('progress-bar-success')
			.addClass('progress-bar-danger');
	}
	
	function resetUploadError()
	{
		$('div.row-error').addClass('hidden');
		$("#total-progress").addClass("progress-striped active");
		$('div.progress-bar')
			.addClass('progress-bar-success')
			.removeClass('progress-bar-danger');
	}
	
	function cancelEdit(event)
	{
		var recipeId = $("#recipe-id").val();
		var redirect = (recipeId > 0) ? "/recipe/" + recipeId : "/home";
			
		redirectTo(redirect);
	}
	
	function setStaticSize(section)
	{
		$(section).height($(section).height());
	}
	
	function showPageContent()
	{
		$("section.invisible,div.recipe-header.invisible").removeClass("invisible");
		$("div.saved-msg").removeClass("hidden");
		$("textarea").trigger("paste");
		setStaticSize("section#ingredients");
		// Delaying when the size is set for the directions section
		// due to the unpredictable nature of when the textboxes will finish resizing.
		$(".handle").mouseover(function()
		{
			setStaticSize("section#directions");
		});
	
		if (typeof Import !== 'undefined')
		{
			Import.init({
				importIngredient: importIngredient,
				importDirection: importDirection,
				importMetadata: importMetafield
			},
			{
				react: React,
				reactDOM: ReactDOM,
				busyButton: BusyButton
			});
			$("div.import-msg").removeClass("hidden");
		}
		if (typeof ImportImage !== 'undefined')
		{
			ImportImage.init();
		}
	}
	
	function recipeLoginHandler(event)
	{
		modalLoginHandler(event)
			.done(function(data) { // success callback
				if (data.success == 1)
				{
					$(".bs-login-modal").modal('hide');
				}
			});
	}

	function scrolling(e, c) {
		var pos = $(e).offset();
		window.scrollTo(0, pos.top - 100);
//		e.scrollIntoView();
//		if (c < 5) { setTimeout(this.scrolling, 300, e, c+1); }
	}
	
	function handleEditableChange(e) {
		// ensure input is visible on touch devices
		if ('ontouchstart' in window) {
			setTimeout(scrolling, 300, e.target, 0);
		}
		
		if (typeof Import !== 'undefined' && e.isMutable)
		{
			Import.showPopover(e.target, e.component.props.componentRef, e.component.props.label);
		}
	}
	
	function redrawPopover(event) {
		if (typeof Import !== 'undefined') {
			Import.reshowPopover(event);
		}
	}
		
	function importMetafield(field, content) {
		reactComponents[field].value(content);
	}
	
	function initPage()
	{
		var recipeId = $("#recipe-id").val();
		
		$("div.recipe-user-metadata a").on("click", recipeMetadataSelect);
	
		$(".btn-addingredient").on("click", addBlankIngredientRow);
		$("div.ingredient-collection").on("click", "a.trash", removeRow);
		$("div.ingredient-collection").on("click", "a.to-text", convertIngredientToText);
		$("div.ingredient-collection").on("click", "a.to-ingredient", convertTextToIngredient);
		$("div.ingredient-collection").on("blur", "input#rawtext", parseIngredient);
		$("div.ingredient-collection").on("keydown", "input#rawtext", function parseAndAdd(event) {			
			if (event.keyCode == 13) {
				event.preventDefault();
				event.stopPropagation();

				parseIngredient(event);
				addBlankIngredientRow(event);
			}
		});
	
		$(".btn-adddirection").on("click", addBlankDirectionRow);
		$("div.direction-collection").on("click", "button.trash", removeRow);
	
		$(".bs-upload-modal .btn-confirm").on("click", modalUploadHandler);
		
		$("form#editRecipeForm").on("submit", submitRecipe);
		$("button#editRecipeDelete").on("click", (recipeId > 0) ? deleteRecipeHandler : cancelEdit);
		$("button#editRecipeCancel").on("click", cancelEdit);
	
	    $( "div.ingredient-collection" ).sortable({
			items: "> div.ingredient-row",
			handle: ".handle",
			containment: $("section#ingredients"),
			axis: "y",
			placeholder: "sortable-placeholder",
			start: function(e, ui){
				ui.placeholder.height(ui.item.height());
			}
		});
	
	    $( "div.direction-collection" ).sortable({
			items: "> div.direction-row",
			handle: ".handle",
			axis: "y",
			placeholder: "sortable-placeholder",
			start: function(e, ui) {
				ui.placeholder.height(ui.item.height());
				setStaticSize("section#directions");
			},
			update: function(e, ui) {
				enumerateDirections();
			}
		});

		dropzoneConfig.url += recipeId;
		Dropzone.options.fileUpload = dropzoneConfig;

		if (recipeId > 0)
		{
		    ajaxGet("/api/1/recipes/" + recipeId, null)
				.done(loadRecipeForEdit)
				.always(function()
				{
					$("div.wait-spinner").remove();
				});
		}
		else
		{
			document.title = "New Recipe :: " + document.title;
			reactComponents.recipeTitle.value($("div#titlePlaceholder").data("pagetitle"));
			showPageContent();
			$("div.wait-spinner").remove();
		}
	
		$(".btn-login").on("click", recipeLoginHandler);
		$(".bs-login-modal").modal('show');
	}


	// ---------------------
	
	var publicApi = {
		init: initPage,
		
		getIngredientRow: getIngredientRow,
		addIngredientToDom: addIngredientToDom,
		
		getDirectionRow: getDirectionRow,
		addDirectionToDom: addDirectionToDom
	};

	return publicApi;

})(jQuery);

$(function() {
	initPage_base();
	pnRecipeEdit.init();
});
