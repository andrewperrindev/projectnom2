function modalLoginFocus()
{
	$("#loginForm #username").focus();
}
function modalLoginHandler(event)
{
    event.preventDefault(); // prevent default submit behaviour

    buttonWait($('.btn-login'), true);

    // get values from FORM
    var username = $("#loginForm #username").val();
    var password = $("#loginForm #password").val();
    var rememberme = ($("#loginForm #rememberme")[0].checked) ? 1 : 0;

	var post_data = 
		{
            username: username,
            password: password,
            rememberme: rememberme
        };

	return ajaxPost("/api/1/user/login", post_data)
		.done(function(data) { // default success callback
			if (data.success != 1)
			{
				$('#loginForm').trigger("reset");
				$('#loginResult').html(getErrorAlert(data.error));
				$(".bs-login-modal .modal-content").shakeit(3,15,400);
				buttonWait($('.btn-login'), false);
				modalLoginFocus();
			}
		})
		.fail(function()
		{
			// Fail message
			$('#loginResult').html(getErrorAlert("<strong>Login error occurred.</strong> Please contact <a href='mailto:support@projectnom.com'>ProjectNom support</a> for assistance."));
			buttonWait($('.btn-login'), false);
		});
}
$('.bs-login-modal').on('shown.bs.modal', modalLoginFocus);