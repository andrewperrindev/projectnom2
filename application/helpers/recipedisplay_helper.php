<?php
	function getTitleForDisplay($title, $originUrl)
	{
		if (!empty($originUrl))
		{
			$url_parsed = parse_url($originUrl);
			
			$title = "$title <p class='text-muted'><small><a href='$originUrl'>"
				. "Courtesy of " . $url_parsed["host"] . "</a></small></p>";
		}

		return $title;
	}
	
	function getTimeForDisplay($prepTime, $totalTime)
	{
		$timeDetails = "";

		if (!empty($totalTime) || !empty($prepTime))
		{
			if (!empty($totalTime))
			{
				$timeDetails .= " Total: " . $totalTime;
			}
			if (!empty($prepTime))
			{
				if (!empty($timeDetails)) $timeDetails .= "; ";
				$timeDetails .= " Prep: " . $prepTime;
			}
		}
		
		return $timeDetails;
	}
	
	function getIngredientsForDisplay($ingredients)
	{
		$parsedIngredients = array();
		
		foreach ($ingredients as $ingredient)
		{
			$ingredientText = "";
			$ingredientType = "text";
			
			if ($ingredient->id > 0)
			{
				if ($ingredient->quantity !== "")
				{
					$ingredientText .= $ingredient->quantity . " ";
				}
				if ($ingredient->unit !== "")
				{
					$ingredientText .= $ingredient->unit . " ";
				}
				if ($ingredient->name !== "")
				{
					$ingredientText .= $ingredient->name;
				}
				if ($ingredient->preparation !== "")
				{
					$ingredientText .= ", " . $ingredient->preparation;
				}
			}
			else
			{
				$ingredientText = $ingredient->name;
				$ingredientType = "heading";
			}
			
			$parsedIngredients[] = array(
				"text" => $ingredientText,
				"type" => $ingredientType
			);
		}
		
		return $parsedIngredients;
	}
	
?>