<!-- Import Image Modal -->
<div class="modal fade bs-import-image-modal">
	<div class="modal-content">
		<div class="container">
			<div class="row text-dark">
				<div class="col-sm-12">
    				<div class="modal-body">
    					<table>
    						<tr>
    							<td class='header'>
									<button type="button" class="close btn-lg" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
									<h3 class="modal-title">Import Image</h3>
								</td>
							</tr>
							<tr>
								<td>
									<div class="alert alert-info" role="alert">
										<strong>Here are the images we could find in your recipe.</strong> Click to select an image, and then click the Import button.
									</div>
								</td>
							</tr>
    						<tr>
    							<td class='content'>
    								<?php
    									$image_count = 0;
    									foreach($import_image_urls as $image_url)
    									{
    										if ($image_count == 0)
    										{
    											?>
			    								<div class="row text-dark">
			    								<?php
			    							}

			    							$image_count++;

    										?>
    										<div class="col-sm-3">
    											<img src="<?php print $image_url; ?>" class="thumbnail img-responsive import-recipe-image">
    										</div>
    										<?php

    										if ($image_count == 4)
    										{
    											$image_count = 0;
    											?>
    											</div>
    											<?php
    										}
    									}

    								?>
								</td>
							</tr>
							<tr>
								<td class='footer'>
									<input type="hidden" class="chosen-image-url">
									<button type="submit" class="btn btn-default btn-cancel" data-dismiss="modal">Cancel</button>
									<button type="submit" class="btn btn-primary btn-confirm">Import</button>
								</td>
							</tr>
						</table>
					</div>
				</div>
			</div>
    	</div>
	</div><!-- /.modal-content -->
</div><!-- /.modal -->
