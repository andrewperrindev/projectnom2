<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Foodnetwork
{
	private $CI;
	private $content;

	public function __construct($params)
    {
    	$this->CI =& get_instance();

    	if (!empty($params['content']))
    	{
    		$this->content = $params['content'];
    	}
    }

    public function getTitle()
    {
		return $this->returnMatch("/<div class=\".+title\">\s+<h1 itemprop=\"?name\"?>(.+)<\/h1>/", $this->content);
    }

    public function getAuthor()
    {
		return $this->returnMatch("/<h6>Recipe courtesy of <a href=\".+\" itemprop=\"?url\"?><span itemprop=\"?name\"?>(.+)<\/span><\/a><\/h6>/", $this->content);
    }

    public function getTotalTime()
    {
		return $this->returnMatch("/<dt class=\"?total\"?>Total Time:<\/dt><dd class=\"?total\"?>(.+)<\/dd>/", $this->content);
    }

    public function getPrepTime()
    {
		return $this->returnMatch("/<dt>Prep:<\/dt><dd>(.+)<\/dd>/", $this->content);
    }

    public function getServings()
    {
		return $this->returnMatch("/<dt>Yield:<\/dt>\s+<dd>(.+)<\/dd>/imsU", $this->content);
    }

    public function getImage()
    {
    	return $this->returnMatch("/<img title=\".+\" itemprop=\"image\" height=\"[0-9]+\" alt=\".+\" width=\"[0-9]+\" src=\"(.+)\" \/>/", $this->content);
    }

    public function getIngredients()
    {
    	$this->CI->load->helper("html2text");

    	if (!empty($this->content))
    	{
    		preg_match("/<div class=\"col8 ingredients responsive\">\s+<h6>Ingredients<\/h6>\s+<ul>(.+)<\/ul>\s+<\/div>/imsU",
    			$this->content, $matches);

    		if (!empty($matches[1]))
    		{
    			$result = array();
    			preg_match_all("/<li (class|itemprop)=\"?ingredients\"?>(.+)<\/li>/imsU", trim($matches[1]), $ingredients);

    			foreach ($ingredients[2] as $ingredient)
    			{
    				$result[] = convert_html_to_text($ingredient);
    			}
    			return $result;
    		}
    	}
    }

    public function getDirections()
    {
        $this->CI->load->helper("html2text");
        $result = array();

        if (!empty($this->content))
        {
            //preg_match("/<h6>Directions<\/h6>\s+(.+)?\s+(<p>(.+)<\/p>)+?/imsU", $this->content, $matches);
            preg_match("/<h6>Directions<\/h6>(.+)<hr>/imsU", $this->content, $matches);
            //return print_r($matches, true);

            if (!empty($matches[1]))
            {
                preg_match_all("/<p>(.+)<\/p>/imsU", $matches[1], $directions);

                foreach ($directions[1] as $direction)
                {
                    $result[] = convert_html_to_text($direction);
                }
            }
        }

        return $result;
    }

    private function returnMatch($pattern, $content)
    {
    	if (!empty($content))
    	{
			preg_match($pattern, $content, $matches);

			if (!empty($matches[1]))
			{
				return $matches[1];
			}
		}

		return "";
    }
}
?>