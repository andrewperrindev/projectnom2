<?php
include_once("recipe_class.php");
include_once("ingredient_class.php");
include_once("RecipeException.php");
include_once("Base_model.php");

class Recipe_model extends Base_model
{
    function __construct()
    {
        parent::__construct();
    }
    
    public function getRecipe($userData)
    {
    	$recipe = null;
    	
    	$this->db->select('recipes.*,user_recipebox.`rating`,user_recipebox.`type`,user_recipebox.`cuisine`,'
    		. 'user_recipebox.`comment`, CASE WHEN user_recipebox.`userid` > 0 THEN 1 ELSE 0 END AS editable', FALSE);
    	$this->db->from("recipes");
	    $this->db->join('user_recipebox', 'recipes.id = user_recipebox.recipeid');
	    $this->db->join('users', 'user_recipebox.userid = users.id');

	    $query = $this->db->where('username', $userData['username']);
	    if (!empty($userData['origin_url']))
	    {
		    $query = $this->db->where('recipes.origin_url', $userData['origin_url']);
		}
		else
		{
			$query = $this->db->where('recipes.id', $userData['recipeid']);
		}
	    $query = $this->db->get();

	    if ($query->num_rows() > 0)
	    {
		    $recipe = $this->toRecipe($query->row());
		    $recipe = $this->getIngredients($recipe);
		    $recipe = $this->getDirections($recipe);

			// Since CI3 doesn't support joined tables in UPDATEs anymore, we have to write our own query.
		    $this->db->query("UPDATE `user_recipebox` as ur, `users` as u "
		    	. " SET ur.date_lastviewed = " . $this->db->escape(date("Y-m-d H:i:s"))
		    	. " WHERE ur.recipeid = " . $this->db->escape($recipe->id)
		    	. " AND u.username = " . $this->db->escape($userData["username"])
		    	. " AND ur.userid = u.id");
		}
		else if (!empty($userData["recipeid"]) && $userData["recipeid"] > 0)
		{
			$this->db->select("recipes.*, 0 AS editable", FALSE);
			$this->db->from("recipes");
    		$query = $this->db->where("id", $userData['recipeid']);
    		$query = $this->db->get();

    		if ($query->num_rows() > 0)
    		{
    			$recipe = $this->toRecipe($query->row());
    			$recipe = $this->getIngredients($recipe);
    			$recipe = $this->getDirections($recipe);
    		}

    		$exception = new RecipeException($recipe, "You do not have permission to this recipe.");
			throw $exception;
		}

	    return $recipe;
    }

    public function getRecipes($userData, $searchQuery = '', $dayPart='', $alpha = FALSE, $favorite = FALSE, $totalTime = false, $ingredientSort = FALSE, $cuisine = '', $type = '', $rating = '', $mostRecent = FALSE, $different = FALSE, $limit = 0, $offset = 0)
    {
    	$recipes = array();

    	$this->db->select('SQL_CALC_FOUND_ROWS null as rows, recipes.*,user_recipebox.`rating`,user_recipebox.`type`,'
    		. 'user_recipebox.`cuisine`,user_recipebox.`comment`,1 AS editable,'
    		. 'IFNULL(user_recipebox.`date_added`, "1900-01-01") AS `date_added`,'
    		. 'IFNULL(user_recipebox.`date_lastviewed`, "1900-01-01") AS `date_lastviewed`', FALSE);
	    $this->db->from('recipes');
	    $this->db->join('user_recipebox', 'recipes.id = user_recipebox.recipeid');
	    $this->db->join('users', 'users.id = user_recipebox.userid');

	    if ($mostRecent)
	    {
	    	$this->db->order_by("date_added", "desc");
	    }
	    else if ($different)
	    {
	    	$this->db->order_by("date_lastviewed", "asc");
	    }
	    else if ($favorite)
	    {
	    	$this->db->order_by("rating", "desc");
	    }
	    else if ($alpha)
	    {
	    	$this->db->order_by("title", "asc");
	    }
	    if ($limit > 0 && !$totalTime && !$ingredientSort)
	    {
		    // Don't apply a SQL limit for time or ingredient sorts, as we need all recipes to do the sort
	    	$this->db->limit($limit,$offset);
		}

    	//$this->db->order_by("title", "asc");

		if (!empty($dayPart) && !is_array($dayPart))
		{
			$recipeTypesByDaypart = $this->getRecipeTypesByDaypart();
			$query = $this->db->where_in('type', $recipeTypesByDaypart[$dayPart]);
		}
		if (!empty($cuisine) && !is_array($cuisine))
		{
			$query = $this->db->where("cuisine", $cuisine);
		}
		if (!empty($type) && !is_array($type))
		{
			$query = $this->db->where("type", $type);
		}

		if (isset($rating) && is_array($rating) && count($rating) <= 5)
		{
			$query = $this->db->where_in("rating", $rating);
		}
		
		if (!empty($searchQuery) && !is_array($searchQuery))
		{
			$query = $this->db->like('title', $searchQuery);
		}
	    $query = $this->db->where('username', $userData['username']);		
	    $query = $this->db->get();
	    foreach ($query->result() as $row)
	    {
	    	$recipes[] = $this->toRecipe($row);
	    }
	    
	    $recipe_total = $this->db->query('SELECT FOUND_ROWS() count;')->row()->count;

	    $recipes = $this->getIngredients($recipes);

		if ($totalTime || $ingredientSort)
		{
			if ($totalTime)
			{
				usort($recipes, "RecipeDTO::sort_bytotaltime");
			}
			else
			{
				usort($recipes, "RecipeDTO::sort_byingredients");
			}
			$recipes = array_slice($recipes, $offset, $limit);
		}

	    // If a limit is active, also return the total number of rows.
	    if ($limit > 0)
	    {
	    	$recipes['rows'] = $recipe_total;
	    }

	    return $recipes;

	    //return TRUE;
    }

    public function getImportUrls($userData)
    {
    	$this->db->select('recipes.`origin_url`, recipes.`cache_url`', FALSE);
    	$this->db->from("recipes");
	    $this->db->join('user_recipebox', 'recipes.id = user_recipebox.recipeid');
	    $this->db->join('users', 'users.id = user_recipebox.userid');

	    $query = $this->db->where('username', $userData['username']);
	    $query = $this->db->where('recipes.id', $userData['recipeid']);
	    $query = $this->db->get();

	    if ($query->num_rows() > 0)
	    {
	    	$result = array();
	    	$result['origin_url'] = $query->row()->origin_url;
	    	$result['cache_url'] = $query->row()->cache_url;
	    	return $result;
		}
		else
		{
			throw new Exception("No data available.");
		}    	
    }

    public function saveRecipe($recipe, $username)
    {
    	if ($recipe != null)
    	{
	    	// This is to remove any initial encoding sent to the server (e.g. &amp;).
	    	// We only want to save a single pass of encoding to the db (e.g. to prevent &amp;amp;).
	    	$recipe = $this->decodeText($recipe);
	    	
			$recipe_data = array (
				"title" => htmlentities($recipe->title, ENT_QUOTES, "UTF-8"),
				"author" => htmlentities($recipe->author, ENT_QUOTES, "UTF-8"),
				"image_url" => $recipe->image_url,
				"prep_time" => htmlentities($recipe->prep_time, ENT_QUOTES, "UTF-8"),
				"total_time" => htmlentities($recipe->total_time, ENT_QUOTES, "UTF-8"),
				"servings" => htmlentities($recipe->servings, ENT_QUOTES, "UTF-8")
			);

			if (!empty($recipe->origin_url))
			{
				$recipe_data["origin_url"] = $recipe->origin_url;
			}
			if (!empty($recipe->cache_url))
			{
				$recipe_data["cache_url"] = $recipe->cache_url;
			}

			$userrecipe_data = array (
				"comment" => htmlentities($recipe->comment, ENT_QUOTES, "UTF-8"),
				"type" => htmlentities($recipe->type, ENT_QUOTES, "UTF-8"),
				"cuisine" => htmlentities($recipe->cuisine, ENT_QUOTES, "UTF-8"),
				"rating" => is_numeric($recipe->rating) ? $recipe->rating : 0,
			);

    		if ($recipe->id <= 0)
    		{
	    		$this->db->select_max('id');
	    		$query = $this->db->get('recipes');

	    		$recipe->id = 1;

	    		if ($query->num_rows() > 0)
				{
					$recipe->id = $query->row()->id + 1;
				}

				$recipe_data["id"] = $recipe->id;
				$this->db->insert('recipes', $recipe_data);

				$userid = $this->getIdForUsername($username);
				$userrecipe_data["userid"] = $userid;
				$userrecipe_data["recipeid"] = $recipe->id;
				$userrecipe_data["date_added"] = date("Y-m-d H:i:s");
				$this->db->insert('user_recipebox', $userrecipe_data);
			}
			else
			{
				$this->db->where('id', $recipe->id);
				$this->db->update('recipes', $recipe_data); 

				$userid = $this->getIdForUsername($username);

		    	$this->db->where('rb.userid', $userid);
    			$this->db->where('rb.recipeid', $recipe->id);
    			$this->db->update('user_recipebox rb', $userrecipe_data);
			}

			$this->db->delete('recipe_ingredients', array('recipe_id' => $recipe->id));

			$seq = 1;
			foreach ($recipe->ingredients as $ingredient)
			{
				$ingredient->quantity = htmlentities($ingredient->quantity, ENT_QUOTES, "UTF-8");
				$ingredient->unit = htmlentities($ingredient->unit, ENT_QUOTES, "UTF-8");
				$ingredient->name = htmlentities($ingredient->name, ENT_QUOTES, "UTF-8");
				$ingredient->preparation = htmlentities($ingredient->preparation, ENT_QUOTES, "UTF-8");

				if (!isset($ingredient->id))
				{
					$query = $this->db->get_where('ingredients', array('name' => $ingredient->name));

		    		if ($query->num_rows() > 0)
					{
						$ingredient->id = $query->row()->id;
					}
					else
					{
			    		$this->db->select_max('id');
			    		$query = $this->db->get('ingredients');

			    		$ingredient->id = 1;

			    		if ($query->num_rows() > 0)
						{
							$ingredient->id = $query->row()->id + 1;
						}

						$ingredient_data = array (
							"id" => $ingredient->id,
							"name" => $ingredient->name
						);

						$this->db->insert('ingredients', $ingredient_data);
					}
				}

				$ingredient_data = array (
					"recipe_id" => $recipe->id,
					"ingredient_id" => $ingredient->id,
					"seq" => $seq,
					"quantity" => $ingredient->quantity,
					"unit" => $ingredient->unit,
					"preparation" => $ingredient->preparation,
					"descr_text" => ($ingredient->id == 0) ? $ingredient->name : NULL
				);
				$this->db->insert("recipe_ingredients", $ingredient_data);
				$seq++;
			}

			$this->db->delete('recipe_directions', array('recipe_id' => $recipe->id));

			$seq = 1;
			foreach ($recipe->directions as $direction)
			{
				$direction_data = array (
					"recipe_id" => $recipe->id,
					"seq" => $seq,
					"instructions" => htmlentities($direction, ENT_QUOTES, "UTF-8")
				);
				$this->db->insert("recipe_directions", $direction_data);
				$seq++;
			}


    	}
    }

    public function saveRecipeCacheUrl($recipeid, $cache_url)
    {
    	if (intval($recipeid) > 0)
    	{
			if (!empty($cache_url))
			{
				$recipe_data["cache_url"] = $cache_url;

				$this->db->where('id', $recipeid);
				$this->db->update('recipes', $recipe_data); 
			}
		}
	}
	
    public function setRating($userData, $id, $rating)
    {
    	$rating_data = array(
    		'rb.rating' => $rating
		);

    	return $this->setRecipeMetadata($rating_data, $id, $userData['username']);
    }

    public function setCuisine($userData, $id, $cuisine)
    {
    	$cuisine_data = array(
    		'rb.cuisine' => $cuisine
		);

		return $this->setRecipeMetadata($cuisine_data, $id, $userData['username']);
    }

    public function setType($userData, $id, $type)
    {
    	$type_data = array(
    		'rb.type' => $type
		);

		return $this->setRecipeMetadata($type_data, $id, $userData['username']);
    }

    private function setRecipeMetadata($update_data, $id, $username)
    {
		$userid = $this->getIdForUsername($username);

    	$this->db->where('rb.userid', $userid);
    	$this->db->where('rb.recipeid', $id);
    	$this->db->update('user_recipebox rb', $update_data);

    	return ($this->db->affected_rows() == 1) ? TRUE : FALSE;    	
    }

	public function deleteRecipe($recipe, $username)
	{
		$userid = $this->getIdForUsername($username);
		
		$this->db->where("recipeid", $recipe);
		$this->db->where("userid", $userid);
		$this->db->delete("user_recipebox");
		
		if ($this->db->affected_rows() > 0)
		{
			$this->db->where("recipeid", $recipe);
			$this->db->from("user_recipebox");
			$recipe_count = $this->db->count_all_results();
			
			if ($recipe_count == 0)
			{
				$this->db->where("recipe_id", $recipe);
				$this->db->delete("recipe_ingredients");

				$this->db->where("recipe_id", $recipe);
				$this->db->delete("recipe_directions");

				$this->db->where("id", $recipe);
				$this->db->delete("recipes");
			}
			
			return TRUE;
		}
		
		return FALSE;
	}

    public function getCuisines()
    {
    	return array('American', 'Asian', 'Chinese', 'European', 'French', 'Fusion', 'Indian', 'Italian', 'Japanese', 'Korean', 'Mexican', 'Spanish', 'Unique', 'Nothing');
    }

	public function getTypes()
	{
		$result = array();
		foreach($this->getRecipeTypesByCategory() as $arr)
		{
		    $result = array_merge($result, $arr);
		}

		sort($result);
		return $result;
	}

    public function getUnits()
    {
    	$units = array();

    	$this->db->select('unit');
    	$this->db->where("IFNULL(`unit`, '') !=", "''", FALSE);
    	$this->db->distinct();
    	$this->db->order_by('unit', 'desc');
    	$query = $this->db->get('recipe_ingredients');

	    foreach ($query->result() as $row)
	    {
	    	$units[] = $row->unit;
	    }

	    return $units;
    }

	private function getRecipeTypesByCategory()
	{
		return array(
				"Main Dish" => array("Breakfast", "Brunch", "Lunch", "Dinner"),
				"Side Dish" => array("Side Dish", "Salad"),
				"Snack" => array("Snack", "Appetizer", "Fourthmeal"),
				"Dessert" => array("Dessert"),
				"Other" => array("Dressing/Sauce", "Drink")
			);
	}

	private function getRecipeTypesByDaypart()
	{
		return array(
				"morning" => array('Breakfast', 'Brunch'),
				"afternoon" => array('Brunch', 'Lunch', 'Snack'),
				"evening" => array('Dinner', 'Appetizer', 'Side Dish'),
				"latenight" => array('Dessert', 'Fourthmeal')
			);
	}

    private function toRecipe($dbResult)
    {
    	$new_recipe = new RecipeDTO();

		$new_recipe->id = $dbResult->id;
		$new_recipe->title = stripslashes($dbResult->title);
		$new_recipe->author = stripslashes($dbResult->author);
		//$new_recipe->cookbook_title = stripslashes($result_array["cookbook_title"]);
		//$new_recipe->cookbook_page = $result_array["cookbook_page"];
		$new_recipe->origin_url = $dbResult->origin_url;
		$new_recipe->cache_url = $dbResult->cache_url;
		$new_recipe->prep_time = $dbResult->prep_time;
		$new_recipe->total_time = $dbResult->total_time;
		if (!empty($dbResult->servings))
		{
			$new_recipe->servings = stripslashes($dbResult->servings);
		}
		
		if (!empty($dbResult->image_url))
		{
			$new_recipe->image_url = $dbResult->image_url;
			if (file_exists(APPPATH . "../" . $dbResult->image_url . ".thumb"))
			{
				$new_recipe->has_thumb = 1;
			}
		}

		if ($dbResult->editable == 1)
		{
			$new_recipe->type = $dbResult->type;
			$new_recipe->cuisine = $dbResult->cuisine;
			$new_recipe->comment = stripslashes($dbResult->comment);
			$new_recipe->rating = $dbResult->rating;
		}
		else
		{
			$new_recipe->editable = false;
		}

		return $this->decodeText($new_recipe);
    }
    
    private function decodeText($recipe) 
    {
	    $recipe->title = html_entity_decode($recipe->title, ENT_QUOTES, "UTF-8");
	    $recipe->author = html_entity_decode($recipe->author, ENT_QUOTES, "UTF-8");
	    $recipe->prep_time = html_entity_decode($recipe->prep_time, ENT_QUOTES, "UTF-8");
	    $recipe->total_time = html_entity_decode($recipe->total_time, ENT_QUOTES, "UTF-8");
	    $recipe->servings = html_entity_decode($recipe->servings, ENT_QUOTES, "UTF-8");
	    $recipe->comment = html_entity_decode($recipe->comment, ENT_QUOTES, "UTF-8");
	    
	    return $recipe;
    }

    private function getIngredients($recipe)
    {
	    $recipes = array();
	    $array_context = is_array($recipe);
	    
	    if (!$array_context)
	    {
		    $recipe = array($recipe);
	    }

		foreach ($recipe as &$r)
	    {
		    if ($r->id > 0 && count($r->ingredients) == 0)
		    {
		    	$recipes[$r->id] =& $r;
		    }
	    }

		if (count($recipes) > 0)
		{
			$this->db->select('recipe_ingredients.recipe_id, IFNULL(ingredients.id,0) AS id, recipe_ingredients.quantity, recipe_ingredients.unit,'
				. 'ingredients.name, recipe_ingredients.preparation,recipe_ingredients.descr_text', FALSE);
			$this->db->from('recipe_ingredients');
			$this->db->join('ingredients', 'recipe_ingredients.ingredient_id = ingredients.id', 'left outer');
			$this->db->order_by('recipe_ingredients.recipe_id');
			$this->db->order_by('recipe_ingredients.seq');

			$query = $this->db->where_in('recipe_ingredients.recipe_id', array_keys($recipes));
			$query = $this->db->get();

			foreach ($query->result() as $row)
			{
				$ingredient = new Ingredient();
				$ingredient->id = $row->id;

				if ($ingredient->id > 0)
				{
					$ingredient->name = html_entity_decode(stripslashes($row->name));
				}
				else
				{
					$ingredient->name = html_entity_decode(stripslashes($row->descr_text));
				}

				$ingredient->quantity = html_entity_decode($row->quantity);
				$ingredient->unit = html_entity_decode(stripslashes($row->unit));
				$ingredient->preparation = html_entity_decode(stripslashes($row->preparation));

				array_push($recipes[$row->recipe_id]->ingredients, $ingredient);
			}
		}

		if (!$array_context)
		{
			return $recipe[0];
		}
		else
		{
			return $recipe;
		}
    }
    
    private function getDirections($recipe)
	{
		if ($recipe->id > 0 && count($recipe->directions) == 0)
		{
			$this->db->select('instructions');
			$this->db->from('recipe_directions');
			$this->db->order_by('seq');

			$query = $this->db->where('recipe_id', $recipe->id);
			$query = $this->db->get();

			foreach($query->result() as $row)
			{
				array_push($recipe->directions, html_entity_decode(stripslashes($row->instructions)));
			}
		}

		return $recipe;
	}
}
?>