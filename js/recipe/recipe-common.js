function setRatingDropdown(rating, target)
{
	var ratingDropdown = "";

	if (rating > 0)
	{
		ratingDropdown += "<i class='fa fa-star'></i>&nbsp;".repeat(rating);
	}
	else
	{
		ratingDropdown += "<i class='fa fa-star-o'></i>&nbsp;";
	}

	target
		.attr("id", rating)
		.html(ratingDropdown);
}

function setCuisineDropdown(cuisine, target)
{
	var cuisineDropdown = "";

	if (cuisine)
	{
		cuisineDropdown += cuisine;
	}
	else
	{
		cuisineDropdown += "<em>Cuisine</em>";
	}

	target
		.attr("id", cuisine)
		.html(cuisineDropdown);
}

function setTypeDropdown(type, target)
{
	var typeDropdown = "";

	if (type)
	{
		typeDropdown += type;
	}
	else
	{
		typeDropdown += "<em>Recipe Type</em>";
	}

	target
		.attr("id", type)
		.html(typeDropdown);
}