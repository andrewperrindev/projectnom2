<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Projectnom
{
	private $CI;
	private $recipe;

	public function __construct($params)
    {
    	$this->CI =& get_instance();

    	if (!empty($params['content']))
    	{
    		$this->recipe = $params['content'];
    	}
    }

    public function getTitle()
    {
		return $this->recipe->title;
    }

    public function getAuthor()
    {
		return $this->recipe->author;
    }

    public function getTotalTime()
    {
		return $this->recipe->total_time;
    }

    public function getPrepTime()
    {
		return $this->recipe->prep_time;
    }

    public function getServings()
    {
		return $this->recipe->servings;
    }

    public function getImage()
    {
    	$imgUrl = $this->recipe->image_url;
    	
    	if (!empty($imgUrl))
    	{
		    $imgUrl = (!empty($_SERVER['HTTPS']) ? "https" : "http") . "://" . $_SERVER['SERVER_NAME'] . "/$imgUrl";
			return $imgUrl;
    	}
    	
    	return "";
    }

    public function getIngredients()
    {
    	$result = array();
    	
    	foreach ($this->recipe->ingredients as $ingredient)
    	{
	    	$result[] = $ingredient->quantity . " " . $ingredient->unit . " " . $ingredient->name . ", " . $ingredient->preparation;
    	}
    	
    	return $result;
    }

    public function getDirections()
    {
	    return $this->recipe->directions;
    }
    
    public function getOrigin()
    {
	    return $this->recipe->origin_url;
    }
    
    public function getCache()
    {
	    return $this->recipe->cache_url;
    }
}
?>