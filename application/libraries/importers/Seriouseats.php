<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Seriouseats
{
	private $CI;
	private $content;

	public function __construct($params)
    {
    	$this->CI =& get_instance();

    	if (!empty($params['content']))
    	{
    		$this->content = $params['content'];
    	}
    }

    public function getTitle()
    {
		return $this->returnMatch("/<h1 class=\"recipe-title fn\" itemprop=\"name\">(.+) Recipe<\/h1>/", $this->content);
    }

    public function getAuthor()
    {
		return $this->returnMatch("/<span class=\"author-name\">\s+<a href=\".+\" class=\"name\">(.+)<\/a>/", $this->content);
    }

    public function getTotalTime()
    {
		return $this->returnMatch("/<span class=\"label\">Total time:<\/span><span class=\"info\">(.+)<\/span>/", $this->content);
    }

    public function getPrepTime()
    {
		return $this->returnMatch("/<span class=\"label\">Active time:<\/span>\s*<span class=\"info\">(.+)<\/span>/", $this->content);
    }

    public function getServings()
    {
		return $this->returnMatch("/<span class=\"info yield\" itemprop=\"recipeYield\">(.+)<\/span>/", $this->content);
    }

    public function getImage()
    {
    	return $this->returnMatch("/<img src=\"(.+)\" class=\"photo\" itemprop=\"image\" alt=\".+\" \/>/", $this->content);
    }

    public function getIngredients()
    {
    	$this->CI->load->helper("html2text");

    	if (!empty($this->content))
    	{
    		preg_match("/<div class=\"recipe-ingredients\">\s+<h4 class=\"title\">Ingredients<\/h4>\s+<ul>(.+)<\/ul>\s+<\/div>/imsU",
    			$this->content, $matches);

    		if (!empty($matches[1]))
    		{
    			$result = array();
    			preg_match_all("/<li class=\"ingredient\" itemprop=\"ingredients\">(.+)<\/li>/", trim($matches[1]), $ingredients);

    			foreach ($ingredients[1] as $ingredient)
    			{
    				$result[] = convert_html_to_text($ingredient);
    			}
    			return $result;
    		}
    	}
    }

    public function getDirections()
    {
        $this->CI->load->helper("html2text");
        $result = array();

        if (!empty($this->content))
        {
            preg_match("/<div class=\"recipe-procedures\">\s+<h4 class=\"title\">Directions<\/h4>\s+<ol class=\"recipe-procedures-list instructions\" itemprop=\"recipeInstructions\">\s+(.+)\s+<\/ol>\s+<\/div>/imsU", $this->content, $matches);
            //return print_r($matches, true);

            if (!empty($matches[1]))
            {
                preg_match_all("/<li class=\"recipe-procedure\">\s+<div class=\"recipe-procedure-number (checked)?\">\d+\.?<\/div>\s+(.+)?\s+<div class=\"recipe-procedure-text\">\s+<p><p>(.+)<\/p><\/p>\s+<\/div>/imsU", $matches[0], $directions);

                foreach ($directions[3] as $direction)
                {
                    $result[] = convert_html_to_text($direction);
                }
            }
        }

        return $result;
    }

    private function returnMatch($pattern, $content)
    {
    	if (!empty($content))
    	{
			preg_match($pattern, $content, $matches);

			if (!empty($matches[1]))
			{
				return $matches[1];
			}
		}

		return "";
    }
}
?>