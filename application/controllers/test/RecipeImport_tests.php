<?php
require_once(APPPATH . '/controllers/test/ProjectNom_Toast.php');

class RecipeImport_tests extends ProjectNom_Toast
{
/*
	To test recipe auto importer regular expressions:
	1. Return raw recipe object in RecipesV1->import
	2. Return raw regexp match in site-specific importer
	3. For ingredients and directions, set raw regexp match as $recipe->comment
*/

	function __construct()
	{
		parent::__construct(__FILE__); // Remember this
	}
	
	function test_import_projectnom_recipe_local()
	{
		$this->establishSession();
		
 		$fields = array (
 			'url' => 'http://projectnom2.dev/recipe/218'
		);

 		$curlResult = $this->doCurlPost("/api/1/recipes/import", $fields);
		$this->message = $curlResult['body'];
		$api_result = json_decode($curlResult['body']);

		$this->_assert_equals($api_result->success, 1);
		$this->_assert_true($api_result->result > 0);
		$this->_assert_true(empty($api_result->url));
		$recipe = $this->getRecipe($api_result->result);

		$this->_assert_equals($recipe->title, "Spinach Basil Pesto");
		
		$this->deleteRecipe($recipe->id);
	}

	function test_import_projectnom_recipe_remote()
	{
		$this->establishSession();
		
 		$fields = array (
 			'url' => 'http://projectnom2.dev/recipe/236'
		);

 		$curlResult = $this->doCurlPost("/api/1/recipes/import", $fields);
		$this->message = $curlResult['body'];
		$api_result = json_decode($curlResult['body']);

		/*
		$this->_assert_equals($api_result->success, 0);
		$this->_assert_equals($api_result->error, "Cannot autoimport this URL.");
		$this->_assert_equals($api_result->url, "http://www.recipe.com/sesame-garlic-beef-tacos/");
		$this->_assert_equals($api_result->title, "Sesame Garlic Beef Tacos - Recipe.com");
		*/
		$this->_assert_equals($api_result->success, 1);
		$this->_assert_true($api_result->result > 0);
		$this->_assert_true(empty($api_result->url));
		$recipe = $this->getRecipe($api_result->result);

		$this->_assert_equals($recipe->title, "Sesame Garlic Beef Tacos");
		
		$this->deleteRecipe($recipe->id);
	}
	
	function test_import_existing_remote_recipe()
	{
		$this->establishSession();
		
		$fields = array (
			'url' => "http://www.foodnetwork.com/recipes/blueberry-crumble-recipe/index.html"
		);

 		$curlResult = $this->doCurlPost("/api/1/recipes/import", $fields);
		$this->message = $curlResult['body'];
		$api_result = json_decode($curlResult['body']);

		$this->_assert_equals($api_result->success, 1);
		$this->_assert_equals($api_result->result, 214);
		$this->_assert_false(empty($api_result->url));
		$recipe = $this->getRecipe($api_result->result);

		$this->_assert_equals($recipe->title, "Blueberry Crumble");
	}
	
	function test_import_foodnetwork_recipe()
	{
		$this->establishSession();

		// Recipe 1
 		$fields = array (
 			'url' => 'http://www.foodnetwork.com/recipes/ellie-krieger/pasta-with-escarole-white-beans-and-chicken-sausage-recipe.html'
		);

 		$curlResult = $this->doCurlPost("/api/1/recipes/import", $fields);
		$this->message = $curlResult['body'];
		$api_result = json_decode($curlResult['body']);

		$this->_assert_equals($api_result->success, 1);
		$this->_assert_true($api_result->result > 0);
		$recipe = $this->getRecipe($api_result->result);
		//$this->message = print_r($recipe, TRUE);

		$this->_assert_true(stripos($recipe->image_url, "recipecache") !== FALSE);
		$this->_assert_equals($recipe->title, "Pasta with Escarole, White Beans and Chicken Sausage");
		$this->_assert_equals($recipe->author, "Ellie Krieger");
		$this->_assert_equals($recipe->servings, "4 servings (serving size 2 cups)");
		$this->_assert_equals($recipe->prep_time, "10 min");
		$this->_assert_equals($recipe->total_time, "25 min");

		$this->_assert_equals(count($recipe->ingredients), 13);
		$this->_assert_equals($recipe->ingredients[0]->name, "whole-wheat bowtie (or other shape) pasta");
		
		$this->_assert_equals(count($recipe->directions), 4);
		$this->_assert_equals($recipe->directions[0], "Cook the pasta according to the directions on the package.");

		$this->deleteRecipe($recipe->id);


		// Recipe 2
 		$fields = array (
 			'url' => 'http://www.foodnetwork.com/recipes/gazpacho1.html'
		);

 		$curlResult = $this->doCurlPost("/api/1/recipes/import", $fields);
		$this->message .= "<BR>" . $curlResult['body'];
		$api_result = json_decode($curlResult['body']);

		$this->_assert_equals($api_result->success, 1);
		$this->_assert_true($api_result->result > 0);
		$recipe = $this->getRecipe($api_result->result);

		$this->_assert_true(stripos($recipe->image_url, "recipecache") !== FALSE);
		$this->_assert_equals($recipe->title, "Gazpacho");
		$this->_assert_equals($recipe->servings, "6 to 8 servings");
		$this->_assert_equals($recipe->prep_time, "10 min");
		$this->_assert_equals($recipe->total_time, "1 hr 10 min");
		
		$this->_assert_equals(count($recipe->ingredients), 9);
		$this->_assert_equals($recipe->ingredients[0]->name, "tomatoes");
		
		$this->_assert_equals(count($recipe->directions), 3);
		$this->_assert_equals($recipe->directions[0], "Add the tomatoes to a food processor with a pinch of salt and puree until smooth. Combine the onions, bell peppers and cucumbers with the tomato puree in a large bowl. Chill at least 1 hour.");

		$this->deleteRecipe($recipe->id);


		// Recipe 3
 		$fields = array (
 			'url' => 'http://www.foodnetwork.com/recipes/alton-brown/the-chewy-recipe.html'
		);

 		$curlResult = $this->doCurlPost("/api/1/recipes/import", $fields);
		$this->message .= "<BR>" . $curlResult['body'];
		$api_result = json_decode($curlResult['body']);

		$this->_assert_equals($api_result->success, 1);
		$this->_assert_true($api_result->result > 0);
		$recipe = $this->getRecipe($api_result->result);

		$this->_assert_true(stripos($recipe->image_url, "recipecache") !== FALSE);
		$this->_assert_equals($recipe->title, "The Chewy");
		$this->_assert_equals($recipe->author, "Alton Brown");
		$this->_assert_equals($recipe->servings, "2 dozen cookies");
		$this->_assert_equals($recipe->prep_time, "20 min");
		$this->_assert_equals($recipe->total_time, "1 hr 50 min");
		
		$this->_assert_equals(count($recipe->ingredients), 11);
		$this->_assert_equals($recipe->ingredients[0]->name, "unsalted butter");
		
		$this->_assert_equals(count($recipe->directions), 7);
		$this->_assert_equals($recipe->directions[0], "Melt the butter in a 2-quart saucepan over low heat. Set aside to cool slightly.");

		$this->deleteRecipe($recipe->id);
	}
	
	function test_import_seriouseats_recipe()
	{
		$this->establishSession();

		// Recipe 1
 		$fields = array (
 			'url' => 'http://www.seriouseats.com/recipes/2015/07/teriyaki-burgers-recipe.html'
		);

 		$curlResult = $this->doCurlPost("/api/1/recipes/import", $fields);
		$this->message = $curlResult['body'];
		$api_result = json_decode($curlResult['body']);

		$this->_assert_equals($api_result->success, 1);
		$this->_assert_true($api_result->result > 0);
		$recipe = $this->getRecipe($api_result->result);
		//$this->message .= "<BR>" . print_r($recipe, true);

		$this->_assert_true(stripos($recipe->image_url, "recipecache") !== FALSE);
		$this->_assert_equals($recipe->title, "Teriyaki Burgers");
		$this->_assert_equals($recipe->author, "J. Kenji López-Alt");
		$this->_assert_equals($recipe->servings, "Serves 4");
		$this->_assert_equals($recipe->prep_time, "1 hour");
		$this->_assert_equals($recipe->total_time, "1 hour");

		$this->_assert_equals(count($recipe->ingredients), 8);
		$this->_assert_equals($recipe->ingredients[0]->name, "fresh ground beef chuck");

		$this->_assert_equals(count($recipe->directions), 4);
		$this->_assert_equals($recipe->directions[0], "Form beef into 4 patties, about 1/2 inch wider than the burger buns, with a slight depression in the center to account for bulging as they cook. Season generously with salt and togarashi and refrigerate until ready to cook. Sprinkle scallions evenly over a large plate or a cutting board.");

		$this->deleteRecipe($recipe->id);


		// Recipe 2
 		$fields = array (
 			'url' => 'http://www.seriouseats.com/recipes/2015/05/soft-scrambled-eggs-recipe.html'
		);

 		$curlResult = $this->doCurlPost("/api/1/recipes/import", $fields);
		$this->message .= "<BR>" . $curlResult['body'];
		$api_result = json_decode($curlResult['body']);

		$this->_assert_equals($api_result->success, 1);
		$this->_assert_true($api_result->result > 0);
		$recipe = $this->getRecipe($api_result->result);
		//$this->message .= "<BR>" . print_r($recipe, true);
		
		$this->_assert_true(stripos($recipe->image_url, "recipecache") !== FALSE);
		$this->_assert_equals($recipe->title, "Soft-Scrambled Eggs");
		$this->_assert_equals($recipe->author, "Daniel Gritzer");
		$this->_assert_equals($recipe->servings, "Serves 1");
		$this->_assert_equals($recipe->prep_time, "5 minutes");
		$this->_assert_equals($recipe->total_time, "5 minutes");
		
		$this->_assert_equals(count($recipe->ingredients), 5);
		$this->_assert_equals($recipe->ingredients[0]->name, "eggs");
		
		$this->_assert_equals(count($recipe->directions), 1);
		$this->_assert_equals($recipe->directions[0], "In a nonstick skillet, stir together eggs, salt, butter, and milk, if using, and stir. Set over medium heat and cook, stirring with a silicon spatula, until eggs are moist and just beginning to set. Remove from heat and continue to cook, stirring, until eggs have just set into soft curds; if necessary, return to heat briefly if eggs need to cook slightly more. Season with pepper and serve right away.");

		$this->deleteRecipe($recipe->id);


		// Recipe 3
 		$fields = array (
 			'url' => 'http://www.seriouseats.com/recipes/2015/07/crispy-caramel-chicken-skewers-recipe.html'
		);

 		$curlResult = $this->doCurlPost("/api/1/recipes/import", $fields);
		$this->message .= "<BR>" . $curlResult['body'];
		$api_result = json_decode($curlResult['body']);

		$this->_assert_equals($api_result->success, 1);
		$this->_assert_true($api_result->result > 0);
		$recipe = $this->getRecipe($api_result->result);
		//$this->message .= "<BR>" . print_r($recipe, true);
		
		$this->_assert_true(stripos($recipe->image_url, "recipecache") !== FALSE);
		$this->_assert_equals($recipe->title, "Crispy Caramel Chicken Skewers");
		$this->_assert_equals($recipe->author, "Morgan Eisenberg");
		$this->_assert_equals($recipe->servings, "Serves 4 to 6");
		$this->_assert_equals($recipe->prep_time, "1 hour");
		$this->_assert_equals($recipe->total_time, "3 hours");

		$this->_assert_equals(count($recipe->ingredients), 19);
		$this->_assert_equals($recipe->ingredients[0]->name, "For the Marinated Chicken:");
		
		$this->_assert_equals(count($recipe->directions), 5);
		$this->_assert_equals($recipe->directions[0], "For the Marinated Chicken: In a large zipper-lock bag, combine chicken, fish sauce, brown sugar, and orange juice. Shake well to coat. Refrigerate for at least 2 hours or up to overnight.");

		$this->deleteRecipe($recipe->id);


		// Recipe 4
 		$fields = array (
 			'url' => 'http://www.seriouseats.com/recipes/2012/03/drinking-in-season-rhubarb-sparkler-gin-cocktail-recipe.html'
		);

 		$curlResult = $this->doCurlPost("/api/1/recipes/import", $fields);
		$this->message .= "<BR>" . $curlResult['body'];
		$api_result = json_decode($curlResult['body']);

		$this->_assert_equals($api_result->success, 1);
		$this->_assert_true($api_result->result > 0);
		$recipe = $this->getRecipe($api_result->result);
		//$this->message .= "<BR>" . print_r($recipe, true);
		
		$this->_assert_true(stripos($recipe->image_url, "recipecache") !== FALSE);
		$this->_assert_equals($recipe->title, "Rhubarb Sparkler");
		$this->_assert_equals($recipe->author, "Kelly Carámbula");
		$this->_assert_equals($recipe->servings, "makes 1 cocktail");
		$this->_assert_equals($recipe->prep_time, "5 minutes");
		$this->_assert_equals($recipe->total_time, "5 minutes");

		$this->_assert_equals(count($recipe->ingredients), 10);
		$this->_assert_equals($recipe->ingredients[0]->name, "For the rhubarb syrup:");
		
		$this->_assert_equals(count($recipe->directions), 3);
		$this->_assert_equals($recipe->directions[0], "For the rhubarb syrup: Combine rhubarb, water, and sugar in a small saucepan. Heat over high heat until boiling, stirring to dissolve sugar. Reduce to a simmer and continue to cook until rhubarb breaks down, about 20 minutes. Remove from heat and let cool. Strain through a fine mesh strainer. Rhubarb syrup will keep for one week in a sealed container in the refrigerator.");

		$this->deleteRecipe($recipe->id);


		// Recipe 5
 		$fields = array (
 			'url' => 'http://www.seriouseats.com/recipes/2016/10/chicken-cacciatore-mushrooms-recipe.html'
		);

 		$curlResult = $this->doCurlPost("/api/1/recipes/import", $fields);
		$this->message .= "<BR>" . $curlResult['body'];
		$api_result = json_decode($curlResult['body']);

		$this->_assert_equals($api_result->success, 1);
		$this->_assert_true($api_result->result > 0);
		$recipe = $this->getRecipe($api_result->result);
		//$this->message .= "<BR>" . print_r($recipe, true);
		
		$this->_assert_true(stripos($recipe->image_url, "recipecache") !== FALSE);
		$this->_assert_equals($recipe->title, "Chicken Cacciatore With Mushrooms, Tomato, and Onion");
		$this->_assert_equals($recipe->author, "Daniel Gritzer");
		$this->_assert_equals($recipe->servings, "Serves 4 to 6");
		$this->_assert_equals($recipe->prep_time, "40 minutes");
		$this->_assert_equals($recipe->total_time, "1 hour 10 minutes");

		$this->_assert_equals(count($recipe->ingredients), 12);
		$this->_assert_equals($recipe->ingredients[0]->quantity, "3 to 4");
		$this->_assert_equals($recipe->ingredients[0]->unit, "pounds");
		$this->_assert_equals($recipe->ingredients[0]->name, "bone-in, skin-on chicken legs");
		$this->_assert_equals($recipe->ingredients[0]->preparation, "thighs and drumsticks split (1.4 to 1.8kg)");
		
		$this->_assert_equals(count($recipe->directions), 5);
		$this->_assert_equals($recipe->directions[0], "Preheat oven to 350°F (175°C). Season chicken all over with salt and pepper. Pour a roughly 1/2-inch layer of flour into a wide, shallow bowl. Dredge each chicken piece in flour and tap off excess.");

		$this->deleteRecipe($recipe->id);
	}
	
	function test_import_epicurious_recipe()
	{
		$this->establishSession();

		// Recipe 1
 		$fields = array (
 			'url' => 'http://www.epicurious.com/recipes/food/views/classic-smashed-cheeseburger-51261810'
		);

 		$curlResult = $this->doCurlPost("/api/1/recipes/import", $fields);
		$this->message = $curlResult['body'];
		$api_result = json_decode($curlResult['body']);

		$this->_assert_equals($api_result->success, 1);
		$this->_assert_true($api_result->result > 0);
		$recipe = $this->getRecipe($api_result->result);
		//$this->message .= "<BR>" . print_r($recipe, true);

		$this->_assert_true(stripos($recipe->image_url, "recipecache") !== FALSE);
		$this->_assert_equals($recipe->title, "Classic Smashed Cheeseburger");
		$this->_assert_equals($recipe->author, "Rhoda Boone, Matt Duckor");
		$this->_assert_equals($recipe->servings, "Makes 4 burgers");
		$this->_assert_equals($recipe->prep_time, "20 minutes");
		$this->_assert_equals($recipe->total_time, "40 minutes");

		$this->_assert_equals(count($recipe->ingredients), 13);
		$this->_assert_equals($recipe->ingredients[0]->name, "freshly ground sirloin");
		
		$this->_assert_equals(count($recipe->directions), 5);
		$this->_assert_equals($recipe->directions[0], "In a large bowl, use your hands to gently combine ground sirloin and brisket. Divide into four equal-sized, loosely meat pucks about 2 1/2 inches thick. Place patties on a plate lined with plastic wrap or parchment; transfer to the freezer for 15 minutes (do not exceed this amount of time).");

		$this->deleteRecipe($recipe->id);


		// Recipe 2
 		$fields = array (
 			'url' => 'http://www.epicurious.com/recipes/food/views/roasted-chicken-thighs-with-white-beans-lemon-and-capers-56389497'
		);

 		$curlResult = $this->doCurlPost("/api/1/recipes/import", $fields);
		$this->message .= "<BR>" . $curlResult['body'];
		$api_result = json_decode($curlResult['body']);

		$this->_assert_equals($api_result->success, 1);
		$this->_assert_true($api_result->result > 0);
		$recipe = $this->getRecipe($api_result->result);
		//$this->message .= "<BR>" . print_r($recipe, true);
		
		$this->_assert_true(stripos($recipe->image_url, "recipecache") !== FALSE);
		$this->_assert_equals($recipe->title, "Roasted Chicken Thighs with White Beans, Lemon, and Capers");
		$this->_assert_equals($recipe->author, "The Epicurious Test Kitchen");
		$this->_assert_equals($recipe->servings, "Serves 4");
		$this->_assert_equals($recipe->prep_time, "20 minutes");
		$this->_assert_equals($recipe->total_time, "1 hour");
		
		$this->_assert_equals(count($recipe->ingredients), 9);
		$this->_assert_equals($recipe->ingredients[0]->name, "(15-ounce) cans white beans");
		
		$this->_assert_equals(count($recipe->directions), 3);
		$this->_assert_equals($recipe->directions[0], "Position rack in upper third of oven and preheat to 425°F. Toss beans and capers in a 13x9\" baking dish and spread out evenly on bottom of pan. Spread 1 tsp. mustard on skin of each chicken thigh and nestle, skin side up, into beans and capers. Arrange lemon slices under and around chicken and add enough water (about 1/3 cup) to come up sides of chicken by 1/2\". Drizzle chicken with oil and season whole dish with salt and pepper.");

		$this->deleteRecipe($recipe->id);
		

		// Recipe 3
 		$fields = array (
 			'url' => 'http://www.epicurious.com/recipes/food/views/broccoli-rabe-and-provolone-grinders-388710'
		);

 		$curlResult = $this->doCurlPost("/api/1/recipes/import", $fields);
		$this->message .= "<BR>" . $curlResult['body'];
		$api_result = json_decode($curlResult['body']);

		$this->_assert_equals($api_result->success, 1);
		$this->_assert_true($api_result->result > 0);
		$recipe = $this->getRecipe($api_result->result);
		//$this->message .= "<BR>" . print_r($recipe, true);
		
		$this->_assert_true(stripos($recipe->image_url, "recipecache") !== FALSE);
		$this->_assert_equals($recipe->title, "Broccoli Rabe and Provolone Grinders");
		$this->_assert_equals($recipe->author, "Bon Appétit Test Kitchen");
		$this->_assert_equals($recipe->servings, "Makes 8 servings");
		$this->_assert_equals($recipe->prep_time, "1 1/4 hours");
		$this->_assert_equals($recipe->total_time, "1 1/2 hours");

		$this->_assert_equals(count($recipe->ingredients), 17);
		$this->_assert_equals($recipe->ingredients[0]->name, "White bean purée:");
		
		$this->_assert_equals(count($recipe->directions), 6);
		$this->_assert_equals(urlencode($recipe->directions[0]), "For+white+bean+pur%C3%A9e%3A");

		$this->deleteRecipe($recipe->id);
	}

	function test_import_allrecipes_recipe()
	{
		$this->establishSession();

		// Recipe 1
 		$fields = array (
 			'url' => 'http://allrecipes.com/recipe/109782/jalapeno-steak/'
		);

 		$curlResult = $this->doCurlPost("/api/1/recipes/import", $fields);
		$this->message = $curlResult['body'];
		$api_result = json_decode($curlResult['body']);

		$this->_assert_equals($api_result->success, 1);
		$this->_assert_true($api_result->result > 0);
		$recipe = $this->getRecipe($api_result->result);
		//$this->message .= "<BR>" . print_r($recipe, TRUE);

		$this->_assert_true(stripos($recipe->image_url, "recipecache") !== FALSE);
		$this->_assert_equals($recipe->title, "Jalapeno Steak");
		$this->_assert_equals($recipe->author, "Sara B.");
		$this->_assert_equals($recipe->servings, "6");
		$this->_assert_equals($recipe->prep_time, "5 m");
		$this->_assert_equals($recipe->total_time, "8 h 15 m");

		$this->_assert_equals(count($recipe->ingredients), 7);
		$this->_assert_equals($recipe->ingredients[0]->name, "jalapeno peppers");

		$this->_assert_equals(count($recipe->directions), 4);
		$this->_assert_equals($recipe->directions[0], "Combine jalapenos, garlic, pepper, salt, lime juice and oregano in a blender. Blend until smooth.");

		$this->deleteRecipe($recipe->id);


		// Recipe 2
 		$fields = array (
 			'url' => urlencode('http://allrecipes.com/recipe/21680/savory-southwestern-crepes/')
		);

 		$curlResult = $this->doCurlPost("/api/1/recipes/import", $fields);
		$this->message .= "<BR>" . $curlResult['body'];
		$api_result = json_decode($curlResult['body']);

		$this->_assert_equals($api_result->success, 1);
		$this->_assert_true($api_result->result > 0);
		$recipe = $this->getRecipe($api_result->result);
		//$this->message .= "<BR>" . print_r($recipe, TRUE);

		$this->_assert_true(stripos($recipe->image_url, "recipecache") !== FALSE);
		$this->_assert_equals($recipe->title, "Savory Southwestern Crepes");
		$this->_assert_equals($recipe->author, "AGSTORMS");
		$this->_assert_equals($recipe->servings, "7");
		$this->_assert_equals($recipe->prep_time, "30 m");
		$this->_assert_equals($recipe->total_time, "50 m");

		$this->_assert_equals(count($recipe->ingredients), 18);
		$this->_assert_equals($recipe->ingredients[0]->name, "ground pork sausage");

		$this->_assert_equals(count($recipe->directions), 5);
		$this->_assert_equals($recipe->directions[0], "In a large skillet, cook sausage over medium heat. When sausage is half cooked through, drain off most of the fat. Add onion, red pepper, mushrooms and cilantro; cook until vegetables are tender and sausage is browned. Remove sausage and vegetables from pan, leaving a slight coating of oil.");

		$this->deleteRecipe($recipe->id);


		// Recipe 3
 		$fields = array (
 			'url' => urlencode('http://allrecipes.com/recipe/19127/restaurant-style-tequila-lime-chicken/')
		);

 		$curlResult = $this->doCurlPost("/api/1/recipes/import", $fields);
		$this->message .= "<BR>" . $curlResult['body'];
		$api_result = json_decode($curlResult['body']);

		$this->_assert_equals($api_result->success, 1);
		$this->_assert_true($api_result->result > 0);
		$recipe = $this->getRecipe($api_result->result);
		//$this->message .= "<BR>" . print_r($recipe, TRUE);

		$this->_assert_true(stripos($recipe->image_url, "recipecache") !== FALSE);
		$this->_assert_equals($recipe->title, "Restaurant-Style Tequila Lime Chicken");
		$this->_assert_equals($recipe->author, "Robbie Rice");
		$this->_assert_equals($recipe->servings, "4");
		$this->_assert_equals($recipe->prep_time, "30 m");
		$this->_assert_equals($recipe->total_time, "2 h 50 m");

		$this->_assert_equals(count($recipe->ingredients), 28);
		$this->_assert_equals($recipe->ingredients[0]->name, "Chicken Marinade:");

		$this->_assert_equals(count($recipe->directions), 6);
		$this->_assert_equals($recipe->directions[0], "Combine water, teriyaki sauce, lime juice, garlic, liquid smoke, salt, ginger, and tequila in a medium nonporous glass bowl and mix all together. Add chicken and turn to coat; cover bowl and refrigerate for 2 to 3 hours.");

		$this->deleteRecipe($recipe->id);
	}

	function test_import_bonappetit_recipe()
	{
		$this->establishSession();

		// Recipe 1
 		$fields = array (
 			'url' => 'http://www.bonappetit.com/recipe/maple-pecan-and-sour-cherry-granola-bars'
		);

 		$curlResult = $this->doCurlPost("/api/1/recipes/import", $fields);
		$this->message = $curlResult['body'];
		$api_result = json_decode($curlResult['body']);

		$this->_assert_equals($api_result->success, 1);
		$this->_assert_true($api_result->result > 0);
		$recipe = $this->getRecipe($api_result->result);
		//$this->message .= "<BR>" . print_r($recipe, TRUE);

		$this->_assert_true(stripos($recipe->image_url, "recipecache") !== FALSE);
		$this->_assert_equals($recipe->title, "Maple, Pecan, and Sour Cherry Granola Bars");
		$this->_assert_equals($recipe->author, "Rick Martinez");
		$this->_assert_equals($recipe->servings, "Makes 16 bars");
		$this->_assert_equals($recipe->prep_time, "");
		$this->_assert_equals($recipe->total_time, "");

		$this->_assert_equals(count($recipe->ingredients), 14);
		$this->_assert_equals($recipe->ingredients[0]->name, "old-fashioned oats");

		$this->_assert_equals(count($recipe->directions), 3);
		$this->_assert_equals($recipe->directions[0], "Preheat oven to 375°. Toss oats, pecans, pumpkin seeds, coconut, flaxseed, and almonds on a rimmed baking sheet. Toast, stirring occasionally, until coconut is lightly golden brown, 10–15 minutes.");

		$this->deleteRecipe($recipe->id);


		// Recipe 2
 		$fields = array (
 			'url' => 'http://www.bonappetit.com/recipe/grilled-eggplant-with-fresh-hot-sauce-and-crispy-eggs'
		);

 		$curlResult = $this->doCurlPost("/api/1/recipes/import", $fields);
		$this->message .= "<BR><BR>" . $curlResult['body'];
		$api_result = json_decode($curlResult['body']);

		$this->_assert_equals($api_result->success, 1);
		$this->_assert_true($api_result->result > 0);
		$recipe = $this->getRecipe($api_result->result);
		//$this->message .= "<BR>" . print_r($recipe, TRUE);

		$this->_assert_true(stripos($recipe->image_url, "recipecache") !== FALSE);
		$this->_assert_equals($recipe->title, "Grilled Eggplant with Fresh Hot Sauce and Crispy Eggs");

		$this->_assert_equals($recipe->author, "Chris Morocco");
		$this->_assert_equals($recipe->servings, "4  Servings");

		$this->_assert_equals($recipe->prep_time, "30 min");
		$this->_assert_equals($recipe->total_time, "30 min");

		$this->_assert_equals(count($recipe->ingredients), 12);
		$this->_assert_equals($recipe->ingredients[0]->name, "Fresno chiles");

		$this->_assert_equals(count($recipe->directions), 3);
		//$this->message .= "<BR>" . urlencode($recipe->directions[0]);
		$this->_assert_equals($recipe->directions[0], "Prepare grill for medium heat (or heat a grill pan over medium). Toss chiles, garlic, and a large pinch of salt in a small bowl. Let sit until just softened, 8–10 minutes. Stir vinegar and honey into hot sauce.");

		$this->deleteRecipe($recipe->id);
	}
	
	function test_import_blueapron_recipe()
	{
		$this->establishSession();

		// Recipe 1
 		$fields = array (
 			'url' => 'https://www.blueapron.com/recipes/chicken-udon-noodle-soup-with-napa-cabbage-dried-lime'
		);

 		$curlResult = $this->doCurlPost("/api/1/recipes/import", $fields);
		$this->message = $curlResult['body'];
		$api_result = json_decode($curlResult['body']);

		$this->_assert_equals($api_result->success, 1);
		$this->_assert_true($api_result->result > 0);
		$recipe = $this->getRecipe($api_result->result);
		//$this->message = print_r($recipe, TRUE);

		$this->_assert_true(stripos($recipe->image_url, "recipecache") !== FALSE);
		$this->_assert_equals($recipe->title, "Chicken & Udon Noodle Soup with Napa Cabbage & Dried Lime");
		
		$this->_assert_equals($recipe->author, "Blue Apron");
		$this->_assert_equals($recipe->servings, "2 Servings");
		$this->_assert_equals($recipe->total_time, "15-25mins");

		$this->_assert_equals(count($recipe->ingredients), 8);
		$this->_assert_equals($recipe->ingredients[1]->name, "fresh udon noodles");
		
		$this->_assert_equals(count($recipe->directions), 5);
		$this->_assert_equals($recipe->directions[0], "Wash and dry the fresh produce. Peel and mince the ginger. Cut out and discard the cabbage core; thinly slice the leaves. Pick the cilantro leaves off the stems; discard the stems. Cut the chicken into bite-sized pieces and transfer to a dish. Thoroughly wash your hands, knife and cutting board after handling the chicken.");

		$this->deleteRecipe($recipe->id);

		// Recipe 2
 		$fields = array (
 			'url' => 'https://www.blueapron.com/recipes/korean-style-rice-cakes-with-spicy-pork-ragu-gai-lan'
		);

 		$curlResult = $this->doCurlPost("/api/1/recipes/import", $fields);
		$this->message .= "<BR><BR>" . $curlResult['body'];
		$api_result = json_decode($curlResult['body']);

		$this->_assert_equals($api_result->success, 1);
		$this->_assert_true($api_result->result > 0);
		$recipe = $this->getRecipe($api_result->result);
		//$this->message = print_r($recipe, TRUE);

		$this->_assert_true(stripos($recipe->image_url, "recipecache") !== FALSE);
		$this->_assert_equals($recipe->title, "Korean Tteok & Spicy Pork \"Ragù\" with Baby Bok Choy");
		
		$this->_assert_equals($recipe->author, "Blue Apron");
		$this->_assert_equals($recipe->servings, "2 Servings");
		$this->_assert_equals($recipe->total_time, "20-30mins");

		$this->_assert_equals(count($recipe->ingredients), 10);
		$this->_assert_equals($recipe->ingredients[0]->name, "ground pork");
		
		$this->_assert_equals(count($recipe->directions), 6);
		$this->_assert_equals($recipe->directions[0], "Wash and dry the fresh produce. Heat a medium pot of salted water to boiling on high. Trim off and discard the root ends of the bok choy; roughly chop. Peel and mince the ginger. Thinly slice the garlic chives. Peel and small dice the onion.");

		$this->deleteRecipe($recipe->id);

		// Recipe 3
 		$fields = array (
 			'url' => 'https://www.blueapron.com/recipes/thai-chicken-meatball-soup-with-jasmine-rice-lacinato-kale-peanuts'
		);

 		$curlResult = $this->doCurlPost("/api/1/recipes/import", $fields);
		$this->message .= "<BR><BR>" . $curlResult['body'];
		$api_result = json_decode($curlResult['body']);

		$this->_assert_equals($api_result->success, 1);
		$this->_assert_true($api_result->result > 0);
		$recipe = $this->getRecipe($api_result->result);
		//$this->message = print_r($recipe, TRUE);

		$this->_assert_true(stripos($recipe->image_url, "recipecache") !== FALSE);
		$this->_assert_equals($recipe->title, "Thai Chicken Meatball Curry with Lemongrass & Jasmine Rice");
		
		$this->_assert_equals($recipe->author, "Blue Apron");
		$this->_assert_equals($recipe->servings, "4 Servings");
		$this->_assert_equals($recipe->total_time, "20-30mins");

		$this->_assert_equals(count($recipe->ingredients), 11);
		$this->_assert_equals($recipe->ingredients[0]->name, "ground chicken");
		
		$this->_assert_equals(count($recipe->directions), 6);
		$this->_assert_equals($recipe->directions[0], "Wash and dry the fresh produce. Cut off and discard the root ends of the scallions; thinly slice the scallions, separating the white bottoms and green tops. Peel and mince the ginger. Cut off and discard the root end of the lemongrass; peel off and discard the fibrous, outer layers until you reach the pliable, white core. Mince the lemongrass core. Remove and discard the kale stems; cut the leaves into ribbons. Roughly chop the peanuts. Quarter the lime.");

		$this->deleteRecipe($recipe->id);

		// Recipe 4
 		$fields = array (
 			'url' => 'https://www.blueapron.com/recipes/chicken-fried-chicken-with-baked-sweet-potato-collard-greens'
		);

 		$curlResult = $this->doCurlPost("/api/1/recipes/import", $fields);
		$this->message .= "<BR><BR>" . $curlResult['body'];
		$api_result = json_decode($curlResult['body']);

		$this->_assert_equals($api_result->success, 1);
		$this->_assert_true($api_result->result > 0);
		$recipe = $this->getRecipe($api_result->result);
		//$this->message = print_r($recipe, TRUE);

		$this->_assert_true(stripos($recipe->image_url, "recipecache") !== FALSE);
		$this->_assert_equals($recipe->title, "Chicken Fried Chicken with Baked Sweet Potato & Collard Greens");
		
		$this->_assert_equals($recipe->author, "Blue Apron");
		$this->_assert_equals($recipe->servings, "2 Servings");
		$this->_assert_equals($recipe->total_time, "30-40mins");

		$this->_assert_equals(count($recipe->ingredients), 8);
		$this->_assert_equals($recipe->ingredients[0]->name, "boneless, skinless chicken breasts");
		
		$this->_assert_equals(count($recipe->directions), 6);
		$this->_assert_equals($recipe->directions[0], "Preheat the oven to 475°F. Wash and dry the fresh produce. Halve the sweet potato lengthwise. Cut off and discard the root ends of the scallions; thinly slice the scallions, separating the white bottoms and green tops. Remove and discard the collard green stems; thinly slice the leaves. To make the batter, in a medium bowl, combine half the yogurt, half the vinegar and ¼ cup of water; stir until smooth and season with salt and pepper.");

		$this->deleteRecipe($recipe->id);
	}
	
	function getRecipe($recipeid)
	{
		$curlResult = $this->doCurlGet("/api/1/recipes/$recipeid");
		$api_result = json_decode($curlResult['body']);
		return $api_result->recipes[0];
	}
	
	function deleteRecipe($recipeid)
	{
		$curlResult = $this->doCurlDelete("/api/1/recipes/$recipeid", null);
		$api_result = json_decode($curlResult['body']);
	}
}
?>