# ProjectNom

## A website for loading, searching and organizing recipes.

### How to install a fresh ProjectNom site

**Prerequisites**

  * Apache 2.2

  * PHP 5.5+

  * MySQL 5.6+

**Setup**

  1. Download the ProjectNom repo.

  2. Run `npm install` to resolve dependencies.

  3. Run `gulp` to build JavaScript and CSS bundles.

  4. (Optional) Run `gulp predeploy` to build ./js/dist and ./css/dist versions
suitable for production deployment.

  5. Upload the files to your web server.

    * The test folders are optional as they're only used for unit testing and
aren't part of the core site.

    * The ./src folder is not required.

    * **For production:** only the JS and CSS located under js/dist and
css/dist are required.

  6. Create ./temp, ./recipecache & ./msgcache folders at the top level of the
ProjectNom directory structure.

  7. Create a database for ProjectNom to use, and run the
./setup/database/structure.sql file to build the necessary database structure.

  8. Open ./application/config/database.php.template and fill in the values
for hostname, username, password and database. Save the file as database.php.
You can delete the .template if you wish.

  9. Open ./application/config/config.php.template and fill in the value for
'encryption_key'. An easy way to get a value for this is to visit a site such
as http://randomkeygen.com/ -- grab one of the keys under CodeIgniter
Encryption Keys. Save the file as config.php. You can delete the .template if
you wish.

  10. Open ./application/config/email.php.template and fill in the values for
smtp_host, smtp_port, smtp_user & smtp_pass. Save the file as email.php. You
can delete the .template if you wish.

  11. You should now be able to load the site, create an account and log in.

  12. (Optional) CodeIgniter's primary entry point, ./index.php, contains a
few core settings. The very first identifies the execution environment.
'development' is the default, which gives you full error reporting, but
'production' and 'testing' are also valid values.

### Troubleshooting

**API calls return a 404**

Check the .htaccess file at the root of the site to make sure calls are being
routed properly. The default .htaccess file rewrites requests to add
'index.php' as part of the URL -- a requirement for CodeIgniter. However, this
default rewrite rule may not work if you put ProjectNom in a subfolder.
Contact me if you need help changing the rewrite rule.

