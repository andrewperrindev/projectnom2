<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="description" content="A free, intuitive way to organize your recipe collection.">
    <meta name="author" content="Andrew Perrin">

	<link rel="shortcut icon" href="/favicon/favicon.ico">
	<link rel="icon" sizes="16x16 32x32 64x64" href="/favicon/favicon.ico">
	<link rel="icon" type="image/png" sizes="196x196" href="/favicon/favicon-192.png">
	<link rel="icon" type="image/png" sizes="160x160" href="/favicon/favicon-160.png">
	<link rel="icon" type="image/png" sizes="96x96" href="/favicon/favicon-96.png">
	<link rel="icon" type="image/png" sizes="64x64" href="/favicon/favicon-64.png">
	<link rel="icon" type="image/png" sizes="32x32" href="/favicon/favicon-32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="/favicon/favicon-16.png">
	<link rel="apple-touch-icon" href="/favicon/favicon-57.png">
	<link rel="apple-touch-icon" sizes="114x114" href="/favicon/favicon-114.png">
	<link rel="apple-touch-icon" sizes="72x72" href="/favicon/favicon-72.png">
	<link rel="apple-touch-icon" sizes="144x144" href="/favicon/favicon-144.png">
	<link rel="apple-touch-icon" sizes="60x60" href="/favicon/favicon-60.png">
	<link rel="apple-touch-icon" sizes="120x120" href="/favicon/favicon-120.png">
	<link rel="apple-touch-icon" sizes="76x76" href="/favicon/favicon-76.png">
	<link rel="apple-touch-icon" sizes="152x152" href="/favicon/favicon-152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="/favicon/favicon-180.png">
	<meta name="msapplication-TileColor" content="#FFFFFF">
	<meta name="msapplication-TileImage" content="/favicon/favicon-144.png">
	<meta name="msapplication-config" content="/favicon/browserconfig.xml">

    <title><?php if (isset($title)) { print $title . " :: "; } ?>ProjectNom</title>

    <!-- Core CSS -->
	<?php 
		if (!isIosDevice())
		{
			add_css("grayscale_transitions.css");
			?><?php
		}
		print put_css(); 
	?>

    <!-- Custom Fonts -->
    <link href="/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="//fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic" rel="stylesheet" type="text/css">
    <link href="//fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">