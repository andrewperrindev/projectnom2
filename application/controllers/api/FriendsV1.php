<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once(APPPATH . '/controllers/api/ApiControllerBase.php');

class FriendsV1 extends ApiControllerBase
{
	public function __construct()
	{
	    parent::__construct();
	}

	public function index($id = 0)
	{
		$this->load->model('Friends_model', 'friendsService', TRUE);

		$data['username'] = $this->session->userdata('username');

		if ($this->input->server('REQUEST_METHOD') == "GET")
		{
			$data['result'] = $this->friendsService->getList($data['username']);
		}
		else if ($this->input->server('REQUEST_METHOD') == "POST")
		{
			$result = $this->friendsService->addFriend($data['username'], $this->input->post("email"));

			if ($result === TRUE)
			{
				$data['result'] = "Friend already added.";
			}
			else if ($result === FALSE)
			{
				$data['error'] = "Email does not exist.";
			}
			else
			{
				$data['result'] = (object) $result;
			}			
		}
		else if ($this->input->server('REQUEST_METHOD') == "DELETE")
		{
			$result = FALSE;
			
			if ($id > 0)
			{
				$result = $this->friendsService->removeFriend($data['username'], intval($id));
			}
			
			if (!$result)
			{
				$data['error'] = "Unable to remove friend";
			}			
		}

		$this->load->view('api/simple', $data);
	}
}
?>