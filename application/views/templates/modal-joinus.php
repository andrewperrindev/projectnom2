<!-- Sign Up Modal -->
<div class="modal fade bs-recipeimport-modal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header text-dark">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Recipe Import</h4>
			</div>
	    	<div class="modal-body text-dark">
				<div class="row">
					<div class="col-sm-12">
						<strong>Ready to start your recipe collection?</strong> Sign up now for a ProjectNom account and you can save this recipe -- as well as any other recipes you find online! They'll all be safe and sound in one place, ready for you to cook something awesome.
					</div>
				</div>
	    	</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default btn-cancel" data-dismiss="modal">No Thanks</button>
				<span id="login"><button type="submit" class="btn btn-default extra-padding">Log In</button></span>
				<span id="signup"><button type="submit" class="btn btn-primary btn-confirm extra-padding">Sign Up</button></span>
	    	</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->