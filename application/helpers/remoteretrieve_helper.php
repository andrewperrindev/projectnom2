<?php
	function file_get_contents_curl($url, $timeout = 5) 
	{
	    $ch = curl_init();
	
	    curl_setopt($ch, CURLOPT_AUTOREFERER, TRUE);
	    curl_setopt($ch, CURLOPT_HEADER, 0);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	    curl_setopt($ch, CURLOPT_URL, $url);
	    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
	    curl_setopt($ch, CURLOPT_ENCODING, 'identity');
	    curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.9) Gecko/20071025 Firefox/2.0.0.9");

		// The number of seconds to wait while trying to connect
		// Use 0 to wait indefinitely. 
    	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
    
	    $data = curl_exec($ch);
		$error = curl_error($ch);
		
		if (!empty($error))
		{
			log_message("error", "Problem retrieving $url => $error");
		}

	    curl_close($ch);
	
	    return $data;
	}	
?>