<?php
//Dynamically add Javascript files to header page
if(!function_exists('add_js')){
    function add_js($file='')
    {
        $str = '';
        $ci = &get_instance();
        $footer_js  = $ci->config->item('footer_js');

        if(empty($file)){
            return;
        }

        if(is_array($file)){
            if(!is_array($file) && count($file) <= 0){
                return;
            }
            foreach($file AS $item) {
                $footer_js[] = $item;
            }
        }else{
            $footer_js[] = $file;
        }

        $ci->config->set_item('footer_js',$footer_js);
    }
}

//Dynamically add CSS files to header page
if(!function_exists('add_css')){
    function add_css($file='')
    {
        $str = '';
        $ci = &get_instance();
        $header_css = $ci->config->item('header_css');

        if(empty($file)){
            return;
        }

        if(is_array($file)){
            if(!is_array($file) && count($file) <= 0){
                return;
            }
            foreach($file AS $item){   
                $header_css[] = $item;
            }
        }else{
            $header_css[] = $file;
        }

        $ci->config->set_item('header_css',$header_css);
    }
}

if(!function_exists('put_css')){
    function put_css()
    {
        $str = '';
        $ci = &get_instance();
        $header_css = $ci->config->item('header_css');

        foreach($header_css AS $item){
            $str .= '<link rel="stylesheet" href="'.$ci->config->item('css_path').add_jscss_suffix($item).'" type="text/css" />'."\n";
        }
        
        return $str;
    }
}

if(!function_exists('put_js')){
    function put_js()
    {
        $str = '';
        $ci = &get_instance();
        $footer_js  = $ci->config->item('footer_js');

        foreach($footer_js AS $item){
            $str .= '<script type="text/javascript" src="'.$ci->config->item('js_path').add_jscss_suffix($item).'"></script>'."\n";
        }

        return $str;
    }
}

if(!function_exists('add_jscss_suffix')) {
    function add_jscss_suffix($file)
    {
        $ci = &get_instance();
	    $suffix = $ci->config->item("jscss_suffix");
	    
	    if (!empty($suffix) && strpos($file, $suffix) === FALSE)
	    {
			$file = str_replace(array(".js",".css"), array("$suffix.js", "$suffix.css"), $file);
	    }
	    
	    return $file;
	}
}
?>