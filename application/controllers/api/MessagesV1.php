<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once(APPPATH . '/controllers/api/ApiControllerBase.php');

class MessagesV1 extends ApiControllerBase
{
	public function __construct()
	{
	    parent::__construct();
	}

	public function index($id = 0)
	{
		if ($this->input->server('REQUEST_METHOD') == "GET")
		{
			$this->page(0);
		}
		else
		{
			$this->load->model('Friends_model', 'friendsService', TRUE);
			$data['username'] = $this->session->userdata('username');
			
			if ($this->input->server('REQUEST_METHOD') == "POST")
			{
				$type = trim($this->input->post("type"));
				$message = trim($this->input->post("message"));
				$recipeid = intval($this->input->post("recipeid"));
				$toUsername = trim($this->input->post("to_user"));
				$img = $this->input->post("image");
				$tweet = $this->input->post("tweet") === "true";
				$imageUrlResult = $actual_img = null;
				
				if (empty($type) || empty($message))
				{
					$data['error'] = "Required information missing.";
				}
				else if (strlen($message) > 255)
				{
					$data['error'] = "Message too long.";
				}
				else
				{
					if ($type != "post" && $type != "direct")
					{
						$type = "post";
					}
					if (!empty($img))
					{
						$this->load->library('pnupload', $data);
						$temp_img = $this->pnupload->config['upload_path'] . $img;
						$actual_img = realpath(dirname(__FILE__)) . '/../../../msgcache/' . $img;

						if ( copy($temp_img, $actual_img) )
						{
							$imageUrlResult = "msgcache/" . basename($actual_img);
						}
					}
					
					$result = $this->friendsService->addMessage(
						$data['username'], 
						$type,
						$message,
						$recipeid,
						$toUsername,
						$imageUrlResult);
		
					if ($result === FALSE)
					{
						$data['error'] = "There was a problem posting this message.";
					}
					else
					{
						$data['result'] = $result;

						if ($tweet)
						{
							$this->load->model('User_model', 'userService', TRUE);							
							$userDetail = $this->userService->getAccountDetails($data['username']);
							
							$this->load->library('pntwitter', array(
								"oauth_token" => $userDetail["twitter_oauth_token"], 
								"oauth_token_secret" => $userDetail["twitter_oauth_token_secret"])
							);
							
							$twitter_message = $this->prepMessageForTwitter(
								$message, 
								$result["id"], 
								$this->pntwitter->getShortUrlLength());
							
							$postResult = $this->pntwitter->postStatus($twitter_message, $actual_img);
							
							if (!empty($postResult->id))
							{
								$this->friendsService->updateMessage($data['username'], $result['id'], $postResult->id);
								$data['result']['tweet_id'] = $postResult->id;
							}
							else
							{
								$data['result']['twitter_error'] = "Tweet not published";
							}
						}
					}
				}
			}
			else if ($this->input->server('REQUEST_METHOD') == "DELETE")
			{
				$result = FALSE;
				
				if ($id > 0)
				{
					$result = $this->friendsService->removeMessage($data['username'], intval($id));
				}
				
				if (!$result)
				{
					$data['error'] = "Unable to remove message";
				}
			}
			
			$this->load->view('api/simple', $data);
		}
	}
	
	public function page($page = 0)
	{
		$data['result'] = array();
		
		if (!is_numeric($page) || $page < 0)
		{
			$page = 0;
		}
		
		$this->load->model('Friends_model', 'friendsService', TRUE);

		$data['username'] = $this->session->userdata('username');

		if ($this->input->server('REQUEST_METHOD') == "GET")
		{
			$data['result'] = $this->friendsService->getMessages($data['username'], $page);
		}

		$this->load->view('api/simple', $data);
	}

	public function image()
	{
		if ($this->input->server('REQUEST_METHOD') == "GET")
		{
			header('HTTP/1.1 405 Method not allowed', true, 400);
			header('Content-Type: text/plain');
			print "This endpoint does not support GET.";
		}
		else
		{
			$data['username'] = $this->session->userdata('username');
			$recipe = null;

			if ($this->input->server('REQUEST_METHOD') == "POST")
			{
				$this->load->library('pnupload', $data);

				if ( !$this->pnupload->do_upload() )
				{
					//$error = array('error' => $this->upload->display_errors());

					header('HTTP/1.1 400 Bad Request', true, 400);
					header('Content-Type: text/html');
					print $this->pnupload->display_errors();
				}
				else
				{
					$upload_data = $this->pnupload->data();
					$data = array('result' => $upload_data['file_name']);

					$this->load->view('api/simple', $data);
				}
			}
			else
			{
				header('HTTP/1.1 400 Bad Request', true, 400);
				header('Content-Type: text/plain');
			}

		}
	}
	
	/* Private */
	private function prepMessageForTwitter($message, $messageId, $url_length = 0, $img_length = 0)
	{
		$url = (!empty($_SERVER['HTTPS']) ? "https" : "http") . "://" . $_SERVER['SERVER_NAME'] . "/landing/message/$messageId";
		$hashtag = " #projectnom";
		$ellipsis = "...";
		
		if (strlen($message) + $url_length + strlen($hashtag) + $img_length > 140)
		{
			// Extra 1 is for space after ellipsis
			$message_length = 140 - $url_length - strlen($hashtag) - strlen($ellipsis) - $img_length - 1;
			$message = substr($message, 0, $message_length) . $ellipsis;
		}

		return $message . " " . $url . $hashtag;
	}
}
?>